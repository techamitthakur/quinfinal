-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 14, 2019 at 01:46 AM
-- Server version: 5.6.44-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quintesting`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_adminuser`
--

CREATE TABLE `tbl_adminuser` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `verify` varchar(100) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `user_type` enum('0','1') NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_adminuser`
--

INSERT INTO `tbl_adminuser` (`id`, `name`, `email`, `password`, `status`, `verify`, `phone`, `user_type`, `date_created`, `date_modified`) VALUES
(26, 'manish', 'test23@yopmail.com', '23d42f5f3f66498b2c8ff4c20b8c5ac826e47146', 'active', '54255', '8957934889', '0', '0000-00-00 00:00:00', '2019-08-02 05:37:19'),
(27, 'neha', 'malikneha747@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'active', '', '9876543210', '0', '0000-00-00 00:00:00', '2019-08-02 05:37:19'),
(28, 'neha', 'malikneha@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'inactive', '55804', '9876543210', '0', '0000-00-00 00:00:00', '2019-08-02 05:37:19'),
(29, 'ruhi', 'abc@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'inactive', '77750', '9876543211', '0', '0000-00-00 00:00:00', '2019-08-02 05:37:19'),
(30, 'ruhi', 'ruhi@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'inactive', '11553', '9876543211', '0', '0000-00-00 00:00:00', '2019-08-02 05:37:19'),
(31, 'ruhi', 'ruhi11@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'inactive', '77787', '9876541211', '0', '0000-00-00 00:00:00', '2019-08-02 05:37:19'),
(32, 'ruhi', 'ruhi1111@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'inactive', '59158', '9876541211', '0', '0000-00-00 00:00:00', '2019-08-02 05:37:19'),
(33, 'ruhi', 'ruhimalik@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'active', '50126', '9876541211', '0', '0000-00-00 00:00:00', '2019-08-02 05:37:19'),
(34, 'ruhi', 'ruhimalik1@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'inactive', '62643', '9876541211', '0', '0000-00-00 00:00:00', '2019-08-02 05:37:19');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin_user`
--

CREATE TABLE `tbl_admin_user` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `admin_name` varchar(100) DEFAULT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `terms` varchar(10) NOT NULL,
  `created_on` varchar(50) NOT NULL,
  `last_login` varchar(50) DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `admin_phone` varchar(20) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `customer_id` int(16) NOT NULL,
  `dob` date NOT NULL,
  `language` char(2) NOT NULL DEFAULT 'EN',
  `emailnotifications` tinyint(1) NOT NULL DEFAULT '1',
  `smsnotifications` tinyint(1) NOT NULL DEFAULT '0',
  `whatsappnotifications` tinyint(1) NOT NULL DEFAULT '0',
  `numlogins` int(16) NOT NULL DEFAULT '0',
  `numreminders` int(16) NOT NULL DEFAULT '0',
  `access_level` int(11) NOT NULL,
  `status` enum('active','inactive','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin_user`
--

INSERT INTO `tbl_admin_user` (`id`, `ip_address`, `admin_name`, `admin_password`, `admin_email`, `terms`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `admin_phone`, `deleted`, `customer_id`, `dob`, `language`, `emailnotifications`, `smsnotifications`, `whatsappnotifications`, `numlogins`, `numreminders`, `access_level`, `status`) VALUES
(1, '', NULL, '7676aaafb027c825bd9abab78b234070e702752f625b752e55e55b48e607e358', 'admin@gmail.com', '', '2019/03/09 10:50:36am', '2019/08/05 06:27:19pm', 1, 'manish', 'kumar', NULL, '7458868452', 0, 0, '0000-00-00', 'EN', 1, 0, 0, 33, 0, 3, 'active'),
(2, '', NULL, '12345678', 'admin123@gmail.com', 'on', '', NULL, NULL, 'manish', 'kumar', NULL, NULL, 0, 0, '0000-00-00', 'EN', 1, 0, 0, 0, 0, 0, 'active'),
(3, '', NULL, '12345678', 'hellosdfsd@gmail.com', 'on', '', NULL, NULL, 'manish', 'kumardsfsdfsdfsdf', NULL, NULL, 0, 0, '0000-00-00', 'EN', 1, 0, 0, 0, 0, 0, 'active'),
(4, '', NULL, '12345678', 'admin23@gmail.com', 'on', '', NULL, NULL, 'manish', 'kumar', NULL, NULL, 0, 0, '0000-00-00', 'EN', 1, 0, 0, 0, 0, 0, 'active'),
(5, '', NULL, '12345678', 'admin2453@gmail.com', 'on', '', NULL, NULL, 'manish', 'kumar', NULL, NULL, 0, 0, '0000-00-00', 'EN', 1, 0, 0, 0, 0, 0, 'active'),
(6, '', NULL, '12345678', 'hellosdfsd23@gmail.com', 'on', '', NULL, NULL, 'manish', 'kumar', NULL, NULL, 0, 0, '0000-00-00', 'EN', 1, 0, 0, 0, 0, 0, 'active'),
(7, '', NULL, 'kumardsfsdfsdfsdf', 'hellosdfsdf@gmail.com', 'on', '', NULL, NULL, 'manish', 'kumardsfsdfsdfsdf', NULL, NULL, 0, 0, '0000-00-00', 'EN', 1, 0, 0, 0, 0, 0, 'active'),
(8, '', NULL, 'action_message', 'hellosdfsdsszxc@gmail.com', 'on', '', NULL, NULL, 'manish', 'kumar', NULL, NULL, 0, 0, '0000-00-00', 'EN', 1, 0, 0, 0, 0, 0, 'active'),
(9, '', NULL, 'kumardsfsdfsdfsdf', 'admin23asdasd@gmail.com', 'on', '', NULL, NULL, 'manish', 'kumardsfsdfsdfsdf', NULL, NULL, 0, 0, '0000-00-00', 'EN', 1, 0, 0, 0, 0, 0, 'active'),
(10, '', NULL, 'admin@123', 'admin@gmail.com', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '0000-00-00', 'EN', 1, 0, 0, 0, 0, 0, 'active'),
(11, '', NULL, 'a7c7f9a898df3aa6eb0660a652c4e7088a0985d4cb5c099268ae38254d37a005', 'manish123@gmail.com', 'on', '', NULL, NULL, 'manish kumar', 'kumar', NULL, NULL, 0, 0, '0000-00-00', 'EN', 1, 0, 0, 0, 0, 0, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(150) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_city`
--

CREATE TABLE `tbl_city` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `pin` int(7) NOT NULL,
  `date_created` datetime NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_city`
--

INSERT INTO `tbl_city` (`id`, `name`, `pin`, `date_created`, `status`, `date_modified`) VALUES
(1, 'asd', 0, '0000-00-00 00:00:00', '', '2019-08-02 06:25:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_config`
--

CREATE TABLE `tbl_config` (
  `id` int(11) NOT NULL,
  `category_name` varchar(150) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE `tbl_contact` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`id`, `name`, `subject`, `email`, `message`, `date_created`, `date_modified`, `status`) VALUES
(1, 'lkdlkf', '', 'malikneha747@gmail.com', 'kjsdkjd', '0000-00-00 00:00:00', '2019-08-02 04:58:53', 'active'),
(2, 'lkdlkf', 'sdfsdf', 'malikneha747@gmail.com', 'kjsdkjd', '0000-00-00 00:00:00', '2019-08-02 04:58:53', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_enquiry`
--

CREATE TABLE `tbl_enquiry` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` int(15) NOT NULL,
  `address` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_enquiry`
--

INSERT INTO `tbl_enquiry` (`id`, `name`, `email`, `phone`, `address`, `subject`, `message`, `date_created`, `date_modified`) VALUES
(1, 'lkdlkf', 'malikneha747@gmail.com', 0, 'sdfsdf', 'asdsad', 'kjsdkjd', '0000-00-00 00:00:00', '2019-08-02 05:00:09'),
(2, 'lkdlkf', 'malikneha747@gmail.com', 0, 'sdfsdf', 'asdsad', 'kjsdkjd', '0000-00-00 00:00:00', '2019-08-02 05:00:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_items`
--

CREATE TABLE `tbl_items` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_items`
--

INSERT INTO `tbl_items` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'titledescr', 'titledescrseds', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_keys`
--

CREATE TABLE `tbl_keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_keys`
--

INSERT INTO `tbl_keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, 0, '123456', 0, 0, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_portfolio`
--

CREATE TABLE `tbl_portfolio` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` int(15) NOT NULL,
  `experience` varchar(10) NOT NULL,
  `skills` varchar(255) NOT NULL,
  `pass_year` varchar(10) NOT NULL,
  `dob` varchar(100) NOT NULL,
  `language` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `place` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_portfolio`
--

INSERT INTO `tbl_portfolio` (`id`, `name`, `email`, `phone`, `experience`, `skills`, `pass_year`, `dob`, `language`, `date`, `place`, `date_created`, `date_modified`, `status`) VALUES
(1, 'lkdlkf', 'malikneha747@gmail.com', 0, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '2019-08-02 05:01:22', 'active'),
(2, 'lkdlkf', 'malikneha747@gmail.com', 0, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '2019-08-02 05:01:22', 'active'),
(3, 'lkdlkf', 'malikneha747@gmail.com', 0, 'sdf', 'sdf', 'dsf', 'sdf', 'sdafsd', 'jksdkf', 'sdfsdf', '0000-00-00 00:00:00', '2019-08-02 05:01:22', 'active'),
(4, 'lkdlkf', 'malikneha747@gmail.com', 0, 'sdf', 'sdf', 'dsf', 'sdf', 'sdafsd', 'jksdkf', 'sdfsdf', '0000-00-00 00:00:00', '2019-08-02 05:01:22', 'active'),
(5, 'lkdlkf', 'malikneha747@gmail.com', 0, 'sdf', 'sdf', 'dsf', 'sdf', 'sdafsd', 'jksdkf', 'sdfsdf', '0000-00-00 00:00:00', '2019-08-02 05:01:22', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products`
--

CREATE TABLE `tbl_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_provider`
--

CREATE TABLE `tbl_provider` (
  `id` int(11) NOT NULL,
  `pro_fname` varchar(100) NOT NULL,
  `pro_lname` varchar(100) NOT NULL,
  `pro_email` varchar(100) NOT NULL,
  `pro_password` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `verify` varchar(100) NOT NULL,
  `pro_phone` varchar(15) NOT NULL,
  `user_type` enum('0','1') NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_provider`
--

INSERT INTO `tbl_provider` (`id`, `pro_fname`, `pro_lname`, `pro_email`, `pro_password`, `status`, `verify`, `pro_phone`, `user_type`, `date_created`, `date_modified`, `user_id`) VALUES
(26, 'manish', '', 'test23@yopmail.com', '23d42f5f3f66498b2c8ff4c20b8c5ac826e47146', 'active', '54255', '8957934889', '0', '0000-00-00 00:00:00', '2019-08-02 05:37:19', 0),
(27, 'neha', '', 'malikneha747@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'active', '', '9876543210', '0', '0000-00-00 00:00:00', '2019-08-02 05:37:19', 0),
(28, 'neha', '', 'malikneha@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'inactive', '55804', '9876543210', '0', '0000-00-00 00:00:00', '2019-08-02 05:37:19', 0),
(29, 'ruhi', '', 'abc@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'inactive', '77750', '9876543211', '0', '0000-00-00 00:00:00', '2019-08-02 05:37:19', 0),
(30, 'ruhi', '', 'ruhi@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'inactive', '11553', '9876543211', '0', '0000-00-00 00:00:00', '2019-08-02 05:37:19', 0),
(31, 'ruhi', '', 'ruhi11@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'inactive', '77787', '9876541211', '0', '0000-00-00 00:00:00', '2019-08-02 05:37:19', 0),
(32, 'ruhi', '', 'ruhi1111@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'inactive', '59158', '9876541211', '0', '0000-00-00 00:00:00', '2019-08-02 05:37:19', 0),
(33, 'ruhi', '', 'ruhimalik@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'active', '50126', '9876541211', '0', '0000-00-00 00:00:00', '2019-08-02 05:37:19', 0),
(34, 'ruhi', '', 'ruhimalik1@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'inactive', '62643', '9876541211', '0', '0000-00-00 00:00:00', '2019-08-02 05:37:19', 0),
(35, 'sadasdsadsa', 'Last Name', 'admin@gmail.com', '12345678', 'inactive', '', '', '0', '0000-00-00 00:00:00', '2019-08-11 20:40:38', 0),
(36, 'sadasdsadsa', 'Last Namesadsad', 'admin@gmail.com', '12345678', 'inactive', '', '', '0', '0000-00-00 00:00:00', '2019-08-11 20:49:05', 0),
(37, 'manu', 'Kumar', 'mani@gmail.com', '12341234', 'inactive', '', '', '0', '0000-00-00 00:00:00', '2019-08-12 08:28:31', 0),
(38, 'manu', 'kumar', 'Mani@Gmail.Com', '1718c24b10aeb8099e3fc44960ab6949ab76a267352459f203ea1036bec382c2', 'inactive', '', '', '0', '0000-00-00 00:00:00', '2019-08-12 09:09:14', 0),
(40, 'provider123', 'test', 'Provider123@yopmail.com', '4e9363d6c007d56c3b44b90774d92a6c37b04a69c2124459b8acfd1b7d05a246', 'active', '73341', '', '0', '0000-00-00 00:00:00', '2019-08-13 13:32:48', 0),
(41, 'amit', 'singh', 'techamitthakur@gmail.com', '887f368b4b0c8d61e3648838317244763c71d41a0f5514fbd51c84bbfbaa8887', 'inactive', '46732', '', '0', '0000-00-00 00:00:00', '2019-08-12 13:04:05', 0),
(42, 'amit', 'singh', 'techamitthakur@gmail.com', 'ef79fdf759ac77f09731953f39cac4aa411082eb6b342fc7730bc83dd3c533fa', 'inactive', '87740', '', '0', '0000-00-00 00:00:00', '2019-08-13 06:31:04', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subcategory`
--

CREATE TABLE `tbl_subcategory` (
  `id` int(11) NOT NULL,
  `category_name` varchar(150) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_survey`
--

CREATE TABLE `tbl_survey` (
  `id` int(11) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `work` enum('yes','no') NOT NULL,
  `medical` varchar(500) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL,
  `fitness` varchar(150) DEFAULT NULL,
  `food` varchar(150) DEFAULT NULL,
  `beauty` varchar(150) DEFAULT NULL,
  `office` varchar(150) DEFAULT NULL,
  `teaching` varchar(150) DEFAULT NULL,
  `others` varchar(150) DEFAULT NULL,
  `tailoring` varchar(150) DEFAULT NULL,
  `services` varchar(100) NOT NULL,
  `area` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_survey`
--

INSERT INTO `tbl_survey` (`id`, `gender`, `work`, `medical`, `name`, `phone`, `email`, `fitness`, `food`, `beauty`, `office`, `teaching`, `others`, `tailoring`, `services`, `area`, `date_created`, `date_modified`) VALUES
(229, '', 'yes', NULL, 'Kuhu', '2342342323', 'admin321@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teaching', 'sadasdasdasdasdasd', '0000-00-00 00:00:00', '2019-08-10 10:15:03'),
(230, '', 'yes', NULL, 'Kuhu', '2342342323', 'admin321@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teaching', 'sadasdasdasdasdasd', '0000-00-00 00:00:00', '2019-08-10 10:15:24'),
(231, '', 'yes', NULL, 'Kuhu', '2342342323', 'admin321@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teaching', 'sadasdasdasdasdasd', '0000-00-00 00:00:00', '2019-08-10 10:15:27'),
(232, '', 'yes', NULL, 'Kuhu', '9786768768', 'admin321@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teaching', 'sdfsdfsdfsdf', '0000-00-00 00:00:00', '2019-08-10 10:15:47'),
(233, 'Sister', 'yes', 'Doctors', 'dd', '9786768768', 'admin@gmail.com', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'Teaching', '', '0000-00-00 00:00:00', '2019-08-10 10:25:12'),
(234, 'Sister', 'yes', 'Doctors', 'dd', '9786768768', 'admin@gmail.com', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'Teaching', '', '0000-00-00 00:00:00', '2019-08-10 10:25:14'),
(235, 'Wife', 'yes', 'null', 'Divya', '9988881531', 'divyapandey25@gmail.com', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'Beauty', '', '0000-00-00 00:00:00', '2019-08-12 15:47:28'),
(236, '', 'yes', NULL, 'D', '9988881531', 'divyapandy25@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teaching', 'V', '0000-00-00 00:00:00', '2019-08-13 06:11:44'),
(237, '', 'yes', NULL, 'D', '9988881531', 'divyapandy25@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teaching', 'V', '0000-00-00 00:00:00', '2019-08-13 06:11:44'),
(238, '', 'yes', NULL, 'D', '9988881531', 'divyapandy25@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teaching', 'V', '0000-00-00 00:00:00', '2019-08-13 06:11:44'),
(239, '', 'yes', NULL, 'D', '9988881531', 'divyapandy25@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teaching', 'V', '0000-00-00 00:00:00', '2019-08-13 06:11:44'),
(240, '', 'yes', NULL, 'D', '9988881531', 'divyapandy25@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teaching', 'V', '0000-00-00 00:00:00', '2019-08-13 06:11:44'),
(241, '', 'yes', NULL, 'D', '9988881531', 'divyapandy25@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teaching', 'V', '0000-00-00 00:00:00', '2019-08-13 06:11:45'),
(242, '', 'yes', NULL, 'D', '9988881531', 'divyapandy25@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teaching', 'V\r\n', '0000-00-00 00:00:00', '2019-08-13 06:11:46'),
(243, '', 'yes', NULL, 'D', '9988881531', 'divyapandy25@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teaching', 'V\r\n', '0000-00-00 00:00:00', '2019-08-13 06:11:46'),
(244, '', 'yes', NULL, 'Amit', '2222222222', 'techamitthakur@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tailoring', 'ddd', '0000-00-00 00:00:00', '2019-08-13 06:50:57'),
(245, '', 'yes', NULL, 'Divya', '9988882531', 'divyapandey25@gmail.fom', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Fitness', 'Acb', '0000-00-00 00:00:00', '2019-08-13 06:57:30'),
(246, 'Sister', 'yes', 'Doctors', 'Divya', '9988881531', 'divyapandey25@gmail.com', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'Office', '', '0000-00-00 00:00:00', '2019-08-13 07:00:26'),
(247, 'Sister', 'yes', 'null', 'divya', '9988881531', 'divypandey25@gmai.com', 'null', 'null', 'null', 'null', 'null', 'Decorators', 'null', 'Beauty', '', '0000-00-00 00:00:00', '2019-08-13 10:42:41'),
(248, 'Sister', 'yes', 'null', 'amit', '3232323333', 'techamitthakur@gmail.com', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'Beauty', 'sdd', '0000-00-00 00:00:00', '2019-08-13 11:57:16'),
(249, '', 'yes', NULL, 'shashnk', '7171711818', 'shashankbaghel18@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Office work', 'fvhhhh', '0000-00-00 00:00:00', '2019-08-13 12:07:37'),
(250, '', 'yes', NULL, 'shashnk', '7171711818', 'shashankbaghel18@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Office work', 'fvhhhh', '0000-00-00 00:00:00', '2019-08-13 12:07:39'),
(251, '', 'yes', NULL, 'amit', '2222222222', 'techamitthakur@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tailoring', 'ddd', '0000-00-00 00:00:00', '2019-08-13 12:13:06'),
(252, '', 'yes', NULL, 'amit', '2323233333', 'techamitthakur@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tailoring', 'dfdf', '0000-00-00 00:00:00', '2019-08-13 12:17:51'),
(253, 'Self', 'yes', 'null', 'amit', '3232323333', 'techamitthakur@gmail.com', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'Teaching', 'x', '0000-00-00 00:00:00', '2019-08-13 13:44:41'),
(254, 'Self', 'yes', 'null', 'komal', '9810260125', 'kittykomal0081@gmail.com', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'Beauty', '', '0000-00-00 00:00:00', '2019-08-13 14:21:18'),
(255, 'Others', 'yes', 'null', 'Divya Pandey', '9988881531', 'divyapandey25@gmail.com', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'Teaching', '', '0000-00-00 00:00:00', '2019-08-14 06:19:53'),
(256, 'Self', 'yes', 'null', 'sadasd', '9786768768', 'test123@gmail.com', 'null', 'food', 'null', 'null', 'null', 'null', 'null', 'Beauty', 'asdasdasd', '0000-00-00 00:00:00', '2019-08-14 08:10:45'),
(257, '', 'yes', NULL, 'gf', '8776767656', 'techamitthakur@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Fitness', 'sadasdasd', '0000-00-00 00:00:00', '2019-08-14 08:12:38'),
(258, 'Self', 'yes', 'null', 'divya', '9988881531', 'divypandey25@gmai.com', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'Teaching', '', '0000-00-00 00:00:00', '2019-08-14 08:45:47'),
(259, 'Self', 'yes', 'null', 'divya', '9988881531', 'divypandey25@gmai.com', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'Teaching', '', '0000-00-00 00:00:00', '2019-08-14 08:45:48');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_fname` varchar(100) NOT NULL,
  `user_lname` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `verify` varchar(100) NOT NULL,
  `user_phone` varchar(15) NOT NULL,
  `user_type` enum('0','1') NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `user_fname`, `user_lname`, `user_email`, `user_password`, `status`, `verify`, `user_phone`, `user_type`, `date_created`, `date_modified`) VALUES
(35, 'sd', 'Last Nameas', 'mani@gmail.com', '1718c24b10aeb8099e3fc44960ab6949ab76a267352459f203ea1036bec382c2', 'inactive', '', '', '0', '0000-00-00 00:00:00', '2019-08-12 09:10:50'),
(36, 'User123', 'Test', 'user123@gamil.com', 'b55b8d5879ec7e7a1ed3722e9fcaa3b34ace0d849becda59e76b8280fc9a50cb', 'inactive', '89348', '', '0', '0000-00-00 00:00:00', '2019-08-13 07:36:30'),
(37, 'test123', 'Test123', 'Test123@yopmail.com', 'fae4ee5710378f2aa14e4cf2f6597794cc59263b8546e550e35ec0a845239108', 'active', '70982', '', '0', '0000-00-00 00:00:00', '2019-08-13 11:15:57');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `user_password`, `user_email`, `status`, `ip_address`) VALUES
(17, 'sample user', '399c3f421abac40de10cac77cb4faa9e', 'sample@yahoo.com', 0, '::1');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `oauth_provider` enum('','facebook','google','twitter') COLLATE utf8_unicode_ci NOT NULL,
  `oauth_uid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_adminuser`
--
ALTER TABLE `tbl_adminuser`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_admin_user`
--
ALTER TABLE `tbl_admin_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_city`
--
ALTER TABLE `tbl_city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_config`
--
ALTER TABLE `tbl_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_enquiry`
--
ALTER TABLE `tbl_enquiry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_items`
--
ALTER TABLE `tbl_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_keys`
--
ALTER TABLE `tbl_keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_portfolio`
--
ALTER TABLE `tbl_portfolio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_products`
--
ALTER TABLE `tbl_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_provider`
--
ALTER TABLE `tbl_provider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_subcategory`
--
ALTER TABLE `tbl_subcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_survey`
--
ALTER TABLE `tbl_survey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_adminuser`
--
ALTER TABLE `tbl_adminuser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `tbl_admin_user`
--
ALTER TABLE `tbl_admin_user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_city`
--
ALTER TABLE `tbl_city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_config`
--
ALTER TABLE `tbl_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_enquiry`
--
ALTER TABLE `tbl_enquiry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_items`
--
ALTER TABLE `tbl_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_keys`
--
ALTER TABLE `tbl_keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_portfolio`
--
ALTER TABLE `tbl_portfolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_products`
--
ALTER TABLE `tbl_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_provider`
--
ALTER TABLE `tbl_provider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `tbl_subcategory`
--
ALTER TABLE `tbl_subcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_survey`
--
ALTER TABLE `tbl_survey`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=260;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
