<?php include 'header.php';?>

<!-- page-banner start-->
<section class="page-banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>login / register</h3>
                <ul class="banner-link text-center">
                    <li>
                        <a href="index-2.html">Home</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">pages</a>
                    </li>
                    <li>
                        <span class="active">login / register</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- page-banner ends-->

<!-- login start-->
<div class="login-section bg-w sp-100">
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-lg-6">

                <div id="removing">
                    <?php 
                    echo validation_errors('<div  class=" error alert alert-danger text-center text-sm" style="">', '</div>'); 
                    echo $this->session->flashdata('action_message'); 
                    ?>
                </div>
                <ul class="nav d-flex log-tab mb-5" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#login" role="tab" data-toggle="tab">login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#register" role="tab" data-toggle="tab">register</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active show" id="login">
                        <?php 
                            $attr = array('class' => 'custom-form', 'id'=>'');
                            echo form_open('user/loginAccount',$attr);?>
                        <div class="row">
                            <div class="col-12">
                                <select class="form-control" name="type" required>
                                    <option value="" hidden>Select Type</option>
                                    <option value="provider">Service Provider</option>
                                    <option value="client">Client</option>
                                </select>
                            </div>
                            <div class="col-12">
                                <span class="fa fa-user"></span>
                                <input type="text" name="email" class="form-control" placeholder="email" required="">
                            </div>
                            <div class="col-12">
                                <span class="fa fa-lock"></span>
                                <input type="password" name="password" class="form-control" placeholder="Password" required>
                            </div>
                            <!--                            <div class="col-6 text-left">
                                <a href="#" class="paswd"> forgot password ?</a>
                            </div>-->
                            <div class="col-12 mt-4">
                                <button type="submit" class="btn btn-one btn-anim w-100" id="submit-login" name="submit-login">
                                    login</button>
                            </div>
                        </div>
                        <div class="col-12 seprator mt-4 mb-3">
                            <span>or</span>
                        </div>
                        <div class="col-12 signin-socials">
                            <p class="text-capitalize mb-0">Sign in with...</p>
                            <div class="row mb-3">
                                <div class="col-sm-6 col-12 mt-4">
                                    <a href="<?php echo $authURL; ?>" class="btn btn-anim fb w-100">
                                        <i class="fab fa-facebook-f"></i>
                                        facebook</a>
                                </div>
                                <div class="col-sm-6 col-12 mt-4">
                                    <a href="#" class="btn btn-anim google w-100">
                                        <i class="fab fa-google-plus-g"></i>
                                        google</a>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close();?>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="register">
                        <!--  <form class="custom-form" id="registerform" method="post" action="#"> -->
                        <?php 
                            $attr = array('class' => 'custom-form', 'id'=>'registerform');
                            echo form_open('user/register',$attr);?>
                        <div class="row">
                            <div class="col-12">
                                <span class="fa fa-user"></span>
                                <select class="form-control" name="type" required>
                                    <option hidden>Select Type</option>
                                    <option value="provider">Service Provider</option>
                                    <option value="client">Client</option>
                                </select>
                            </div>
                            <div class="col-12">
                                <span class="fa fa-user"></span>
                                <input type="text" id="name2" name="firstname" class="form-control" placeholder="First Name" required="">
                            </div>
                            <div class="col-12">
                                <span class="fa fa-female"></span>
                                <input type="text" id="name2" name="lastname" class="form-control" placeholder="Last Name" required>
                            </div>
                            <div class="col-12">
                                <span class="fa fa-envelope"></span>
                                <input type="number" name="number" class="form-control" placeholder="Enter Phone" id="email2" autocomplete="off" maxlength="10" pattern="^[6-9]\d{9}$" required>
                            </div>
                            <div class="col-12">
                                <span class="fa fa-envelope"></span>
                                <input type="email" name="email" class="form-control" placeholder="Enter Email" id="email2" required>
                            </div>
                            <div class="col-12">
                                <span class="fa fa-lock"></span>
                                <input type="password" name="password" class="form-control" placeholder="Password" id="password" required>
                            </div>
                            <div class="col-12">
                                <span class="fa fa-lock"></span>
                                <input type="password" name="cpassword" class="form-control" placeholder="Confirm Password" id="c-password" required>
                            </div>
                            <!--
                                <div class="col-6">
                                    <input type="checkbox" name="checkbox" id="checkbox_id" value="value">
                                    <label for="checkbox_id">remember me</label>
                                </div>
-->
                            <div class="col-12">
                                <button type="submit" class="btn btn-one btn-anim w-100" id="submit" name="submit">
                                    register</button>
                            </div>
                            <div class="col-12 seprator mt-4">
                                <span>or</span>
                            </div>
                            <div class="col-12 signin-socials">
                                <p class="text-capitalize mb-0">Sign up with...</p>
                                <div class="row mb-3">
                                    <div class="col-sm-6 col-12 mt-4">
                                        <a href="#" class="btn btn-anim fb w-100">
                                            <i class="fab fa-facebook-f"></i>
                                            facebook</a>
                                    </div>
                                    <div class="col-sm-6 col-12 mt-4">
                                        <a href="#" class="btn btn-anim google w-100">
                                            <i class="fab fa-google-plus-g"></i>
                                            google</a>
                                    </div>

                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- login end -->
<!-- cta-one start-->
<section class="cta-one tri-bg-w text-lg-left text-center">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 my-lg-0 my-5 py-lg-0 py-5">
                <div class="cta-content">
                    <h3>Sign Up To Get Special Offers Every Day</h3>
                    <p>Lorem ipsum dolor sit amet, consectadetudzdae rcquisc adipiscing elit. Aenean socada commodo ligaui
                        egets dolor. </p>
                    <a href="login.html" class="btn btn-two btn-anim mt-2">
                        sign up
                    </a>
                </div>
            </div>
            <div class="col-lg-6 d-lg-block d-none">
                <div class="cta-img mt-4">
                    <img src="<?php echo base_url('public/')?>fassets/img/bottom.png" alt="image">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- cta-one end -->

<?php include 'footer.php';?>
