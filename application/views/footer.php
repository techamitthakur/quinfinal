<!-- footer starts -->
<footer class="footer footer-two">
    <div class="foot-top">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-12 col-12 mb-60">
                    <div class="company-details row">
                        <div class="col-md-4">
                            <img src="<?php echo base_url('public/')?>fassets/img/logoblack.png" class="foot-logo mb-4" alt="lister">
                        </div>
                        <div class="col-md-8">
                            <p class="pb-2">Search for Anything, Anytime, Anywhere using Quinn App by Quinn Pvt. Ltd., India’s No. 1 Local Search Engine. You can chat with businesses, get phone numbers, addresses, learn of best deals, latest reviews and ratings for all businesses instantly.</p>
                        </div>

                        <div class="col-md-12 socials mt-4">
                            <a href="#">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-instagram"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                    <div class="recent-post">
                        <div class="foot-title">
                            <h4>useful links</h4>
                        </div>
                        <ul class="quick-link">
                            <li>
                                <a href="#">Home</a>
                            </li>
                            <li>
                                <a href="#">About</a>
                            </li>
                            <li>
                                <a href="login.php">Login</a>
                            </li>
                            <li>
                                <a href="#">Faq</a>
                            </li>
                            <li>
                                <a href="#">Disclaimer</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                    <div class="recent-post">
                        <div class="foot-title">
                            <h4>my account</h4>
                        </div>
                        <ul class="quick-link">
                            <li>
                                <a href="#">create account</a>
                            </li>
                            <li>
                                <a href="#">dashboard</a>
                            </li>
                            <li>
                                <a href="#">profile</a>
                            </li>
                            <li>
                                <a href="#">my Services</a>
                            </li>
                            <li>
                                <a href="#">favorites</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                    <div class="recent-post">
                        <div class="foot-title">
                            <h4>categories</h4>
                        </div>
                        <ul class="quick-link">
                            <li>
                                <a href="#">Restaurants</a>
                            </li>
                            <li>
                                <a href="#">Shopping</a>
                            </li>
                            <li>
                                <a href="#">Hotels</a>
                            </li>
                            <li>
                                <a href="#">Destinations</a>
                            </li>
                            <li>
                                <a href="#">Nightlife</a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="foot-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <p class="text-capitalize">Copyright © 2019, All rights Reserved. Designed &amp; Developed by
                        <a href="https://www.kcmkkinesi.co.in">KCMK KINESI PVT LTD</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer end -->
<!-- JavaScript Libraries -->
<script src="<?php echo base_url('public/')?>fassets/js/jquery-3.3.1.min.js"></script>
<script src="<?php echo base_url('public/')?>fassets/js/jquery.menu-aim.js"></script> <!-- menu aim -->
<script src="<?php echo base_url('public/')?>fassets/js/main.js"></script> <!-- Resource jQuery -->
<script src="<?php echo base_url('public/')?>fassets/js/popper.min.js"></script>
<script src="<?php echo base_url('public/')?>fassets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('public/')?>fassets/js/cities.js"></script>
<script src="<?php echo base_url('public/')?>fassets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url('public/')?>fassets/js/slick.min.js"></script>
<script src="<?php echo base_url('public/')?>fassets/js/tilt.jquery.js"></script>
<script src="<?php echo base_url('public/')?>fassets/js/custom.js"></script>
<script src="<?php echo base_url('public/')?>fassets/js/common.js"></script>

<script type="text/javascript">
$("#removing").delay(5000).fadeOut("slow");

    
    $(document).ready(function () {
        $("#removing").fadeout(5000);
    });


</script>
</body>

</html>
