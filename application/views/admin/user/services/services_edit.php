

<?php   
    $this->load->view('admin/commons/header');
        $this->load->view('admin/commons/sidebar');
?>


        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Update Menu</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Update Menu</li>
                            </ol>
               
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- End Info box -->
                <!-- ============================================================== -->
               
          

<section>
  <div>
    <p><?php echo validation_errors(); 
     if (isset($error)){
                    echo $error;
                }

//print_r($services);
  ?></p></div>
              
<div class="col-sm-6">
   <?php 
                                   
                                foreach($services as $abt)
                                {

                               
                                  ?>
                    <form class="form-horizontal" action="<?php echo base_url('user/services_edit/'.$abt->id);?>" method="post" enctype="multipart/form-data">
                             

                        <div class="form-group">
                
                              <label for="sel1">Select Category:</label>
                              <select class="form-control" name="heading" value="<?php echo $abt->cat_id; ?>">

                                <?php 
                                  
                                   
                                foreach($sub_category as $subcat)
                                {

                                    echo "<option value=".$subcat->sub_name.">".$subcat->sub_name."</option>";
                                   }                                

                                ?>
                                
                                
                              </select>
                         
                            </div>
                           <!--    <div class="form-group">

                        <label class="control-label " for="">Service Sub heading</label>
                       
                         <select class="form-control" name="sub_heading" required>
                            <option value="HairLoss">HairLoss</option>
                            <option value="ABC_Hair">AbC Hair</option>
                          
                              </select>
                        </div> -->

                        <!--  <div class="form-group">

                        <label class="control-label " for="">Service Sub heading</label>
                       
                          <input type="text" class="form-control"  name="sub_heading" value="<?php echo $abt->sub_heading; ?>" placeholder="Enter sub_heading name" required>
                        </div>
                        <div class="form-group">

                        <label class="control-label " for="">Service Content heading</label>
                       
                          <input type="text" class="form-control"  name="heading" value="<?php echo $abt->heading; ?>" placeholder="Enter Heading name" required>
                        </div> -->
                        <div class="form-group">

                        <label class="control-label " for="">Service Content</label>
                       
                          <textarea class="form-control"  name="content" value="<?php echo $abt->content; ?>" placeholder="Enter Content here" required rows="10" cols="10"><?php echo $abt->content; ?></textarea>
                        </div>
                         <div class="form-group" style="">

                        <label class="control-label " for="">Service Image</label>
                        <img src="<?php echo site_url('public/upload/services/')?><?php echo $abt->image; ?>" style="height:80px; width:80px;">
                          <input type="file" class="form-control" value="<?php echo $abt->heading; ?>"  name="file_name" >
                        </div>

                         <div class="form-group">

                        <label class="control-label " for="">Active</label>
                       
                          <select class="form-control" name="active" required>
                            <option value="<?php echo $abt->active; ?>" hidden><?php echo $abt->active; ?></option>
                            <option value="active">Active</option>
                            <option value="inactive">inActive</option>
                          
                              </select>
                        </div> 

                      <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                      </div>

                       
                    </form>
                     <?php }                                

                                ?>
    </div>
</section>
           
           
           
  </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->


<?php

        $this->load->view('admin/commons/footer');

?>