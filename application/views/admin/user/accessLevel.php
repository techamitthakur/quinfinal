  <!-- ============================================================== -->
  <!-- Container fluid  -->
  <!-- ============================================================== -->
  <div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Datatable</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0)">Home</a>
                    </li>
                    <li class="breadcrumb-item active">Access Level</li>
                </ol>

            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-8">
            <div class="card">
                <div class="card-body" >
                    <h4 class="card-title">User Details</h4>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Access Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="access-alldata" url='<?php echo site_url('user/userAccessData');?>'>
                                <?php
                                $a=1;
                                foreach ($accessLevels as $accessLevel) {
                                    ?>
                                    <tr>
                                        <td><?php echo $a;?></td>
                                        <td><?php echo $accessLevel['access_name'];?></td>
                                        <td>
                                            <a href="<?php echo site_url('user/editAccessLevel/'.$accessLevel['access_id']);?>" class="edit-access" ><i class="fa fa-edit"></i></a>&emsp;
                                            <a href="<?php echo site_url('user/deleteAccessLevel/'.$accessLevel['access_id']);?>" class="delete-access"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                    $a++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> 
        <div class="col-4">
            <div class="card">
                <div class="card-body edit-access-data" url="<?php echo site_url('user/userAccess');?>">
                    <!-- <h4 class="card-title">Add Access Level</h4> -->
                    <?php $content = array('id' => 'add-access'); echo form_open('user/doAccessLevel', $content);?>
                    <p id="error_msg"></p>
                    <div class="row">
                        <div class="form-group">
                            <label for="access_name">Add Access Name</label>
                            <input type="text" value="<?php if(!empty($acessData)){ echo $acessData['access_name'];}?>" name="access_name" id="access_name" class="form-control" placeholder="Enter Access Name...">
                        </div>
                        <button class="btn btn-info">Submit</button>
                    </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->