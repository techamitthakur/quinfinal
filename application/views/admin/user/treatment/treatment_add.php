

<?php   
    $this->load->view('admin/commons/header');
        $this->load->view('admin/commons/sidebar');
?>


        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Add Treatment Manage</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Add Treatment Manage</li>
                            </ol>
                          
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- End Info box -->
                <!-- ============================================================== -->
               
          

<section>
  <div><p><?php echo validation_errors(); 

// print_r($sub_category);
//  print_r($category);
  ?></p></div>
              
<div class="col-sm-6">
                    <form class="form-horizontal" action="<?php echo base_url('user/treatment_add');?>" method="post">


                               <div class="form-group">

                        <label class="control-label " for="">Service Sub heading</label>
                       
                         <select class="form-control" name="sub_heading"  required>
                            <option value="Hair-Replacement-Treatments">Hair Replacement Treatments</option>
                            <option value="Hair-Loss">Hair Loss</option>
                            <option value="Hair-Wigs_Extensions">Hair Wigs Extensions</option>
                          
                        </select>
                        </div>
                         
                             <div class="form-group">
                
                              <label for="sel1">Select Sub Category:</label>
                              <select class="form-control" name="sub_id" required>

                                <?php 
                                   
                                foreach($sub_category as $subcat){

                                    echo "<option value=".$subcat->sub_name." selected>".$subcat->sub_name."</option>";
                                   }                                

                                ?>
                                
                                
                              </select>
                         
                            </div>

                          
                       
                        <div class="form-group">

                        <label class="control-label " for="">Treatment heading</label>
                       
                          <input type="text" class="form-control"  name="heading" placeholder="Enter Heading name" required>
                        </div>
                        <div class="form-group">

                        <label class="control-label " for="">Treatment Content</label>
                       
                          <textarea class="form-control"  name="content" placeholder="Enter Content here" required rows="10" cols="10"></textarea>
                        </div>

                         <div class="form-group">

                        <label class="control-label " for="">Active</label>
                       
                          <select class="form-control" name="active" required>
                            <option value="active">Active</option>
                            <option value="inactive">inActive</option>
                          
                              </select>
                        </div> 


                      <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                      </div>
                    </form>
    </div>
</section>

           
           
           
  </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->


<?php

        $this->load->view('admin/commons/footer');

?>