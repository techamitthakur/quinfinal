


<?php   
    $this->load->view('admin/commons/header');
        $this->load->view('admin/commons/sidebar');
?>


        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Update Treatment Manage</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Update Treatment Manage</li>
                            </ol>
               
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- End Info box -->
                <!-- ============================================================== -->
               
          

<section>
  <div>
    <p><?php echo validation_errors(); 

//print_r($services);
  ?></p></div>
              
<div class="col-sm-6">
   <?php 
                                   
                                foreach($treatment as $abt)
                                {

                               
                                  ?>
                    <form class="form-horizontal" action="<?php echo base_url('user/treatment_edit/'.$abt->id);?>" method="post">
                                 <div class="form-group">

                        <label class="control-label " for="">Service Sub heading</label>
                       
                         <select class="form-control" name="sub_heading" value="<?php echo $abt->sub_heading; ?>" required>
                            <option value="<?php echo $abt->sub_heading; ?>" hidden><?php echo $abt->sub_heading; ?></option>
                            <option value="hair_replacement_treatments">Hair Replacement Treatments</option>
                            <option value="hair_loss">Hair Loss</option>
                            <option value="hair_wigs_extensions">Hair Wigs Extensions</option>
                          
                        </select>
                        </div>

                          <div class="form-group">
                           
                              <label for="sel1">Select Category:</label>
                              <select class="form-control" name="sub_id" value='<?php echo $abt->sub_id; ?>'>

                                <?php 
                                  echo "<option value=".$abt->sub_id." selected>".$abt->sub_id."</option>";
                                   
                                foreach($sub_category as $cat)
                                {

                                    echo "<option value=".$cat->sub_name.">".$cat->sub_name."</option>";
                                   }                                

                                ?>
                                
                                
                              </select>
                         
                            </div> 
                         

                       <div class="form-group">

                        <label class="control-label " for="">About heading</label>
                       
                          <input type="text" class="form-control"  name="heading" placeholder="Enter Heading name" value="<?php echo $abt->heading; ?>" required>
                        </div>
                        <div class="form-group">

                        <label class="control-label " for="">About Content</label>
                       
                          <textarea class="form-control"  name="content" placeholder="Enter Content here" required rows="10" cols="10" value="<?php echo $abt->heading; ?>"><?php echo $abt->heading; ?></textarea>
                        </div>
                          <div class="form-group">

                        <label class="control-label " for="">Active</label>
                       
                          <select class="form-control" name="active" value="<?php echo $abt->active; ?>" required>
                            <option value="active">Active</option>
                            <option value="inactive">inActive</option>
                          
                              </select>
                        </div> 

                          <div class="form-group" style="display:none;">

                        <label class="control-label " for="">About Image</label>
                       
                          <input type="file" class="form-control"  name="image" value="<?php echo $abt->image; ?>" >
                        </div>

                      <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                      </div>

                       
                    </form>
                     <?php }                                

                                ?>
    </div>
</section>
           
           
           
  </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->


<?php

        $this->load->view('admin/commons/footer');

?>