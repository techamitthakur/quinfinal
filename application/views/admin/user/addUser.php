<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor"><?php if (isset($singleUser)) { echo "Edit"; } else { ?>Add <?php } ?>User</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0)">Home</a>
                    </li>
                    <li class="breadcrumb-item active"><a href="<?php echo site_url('user/viewUser'); ?>" class="back">Back</a></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body" >
                    <p id="error_msg"></p>
                    <?php
                    $content = array('id' => 'add-useres');
                   if (isset($singleUser)) { echo form_open('user/doEditUser/'.$singleUser['id'], $content); } else { echo form_open('user/doAdduser', $content); }
                    ?>
                    <div class="row">

                        <div class="col-sm-6 form-group">
                            <label for="first_name">Enter first_name</label>
                            <input type="text" class="form-control" name="first_name" value="<?php if (isset($singleUser)) {
                        echo $singleUser['first_name'];
                    } ?>" id="first_name" />
                        </div>

                        <div class="col-sm-6 form-group">
                            <label for="last_name">Enter last_name</label>
                            <input type="text" class="form-control" value="<?php if (isset($singleUser)) {
                        echo $singleUser['last_name'];
                    } ?>" name="last_name" id="last_name" /> </div>


                        <div class="col-sm-6 form-group">
                            <label for="email">Enter email</label>
                            <input type="email" class="form-control" name="email" value="<?php if (isset($singleUser)) {
                        echo $singleUser['email'];
                    } ?>" id="email" />
                        </div>

                        <div class="col-sm-6 form-group">
                            <label for="phone">Enter Mobile phone</label>
                            <input type="number" class="form-control" value="<?php if (isset($singleUser)) {
                        echo $singleUser['phone'];
                    } ?>" name="phone" id="phone" />
                        </div>
<?php if (!isset($singleUser)) { ?>
                            <div class="col-sm-6 form-group">
                                <label for="password">Enter password</label>
                                <input type="password" class="form-control" name="password"  id="password" />
                            </div>

                            <div class="col-sm-6 form-group">
                                <label for="confirm_password">confirm_password</label>
                                <input type="password" class="form-control" name="confirm_password" id="confirm_password" />
                            </div>
                                <?php
                            }
                            ?>

                        <div class="col-sm-12 form-group">
                            <label for="group">Access level:</label>
<?php
foreach ($accessLevels as $accessLevel) {
    ?>     
                                <input type="radio" <?php if (isset($singleUser)) {
        if ($accessLevel['access_id'] == $singleUser['access_level']) {
            echo "checked";
        }
    } ?> id="group" name="group" value="<?php echo $accessLevel['access_id']; ?>">
    <?php echo $accessLevel['access_name']; ?>          
    <?php
}
?>
                        </div>


                    </div> 

                    <button type="submit" class="btn btn-info waves-effect pull-right" ><?php if (isset($singleUser)) { echo "Update"; } else { ?>Submit<?php } ?></button>

<?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>