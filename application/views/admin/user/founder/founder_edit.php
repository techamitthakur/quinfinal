

<?php   
    $this->load->view('admin/commons/header');
        $this->load->view('admin/commons/sidebar');
?>


        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Update Founder</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Update Founder</li>
                            </ol>
               
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- End Info box -->
                <!-- ============================================================== -->
               
          

<section>
  <div>
    <p><?php echo validation_errors(); 

//print_r($services);
  ?></p></div>
              
<div class="col-sm-6">
   <?php 
                                   
                                foreach($founder as $abt)
                                {

                               
                                  ?>
                       <form class="form-horizontal" action="<?php echo base_url('user/founder_edit/'.$abt->id);?>" method="post" enctype="multipart/form-data">

                          <div class="form-group">

                        <label class="control-label " for="">Name</label>
                       
                          <input type="text" class="form-control"  name="name" value="<?php echo $abt->name; ?>"  required>
                        </div>
                         <div class="form-group">

                        <label class="control-label " for="">heading</label>
                       
                          <input type="text" class="form-control"  name="heading" value="<?php echo $abt->heading; ?>" required>
                        </div>
                         <div class="form-group">

                        <label class="control-label " for="">Content</label>
                       
                          <input type="text" class="form-control"  name="content" value="<?php echo $abt->content; ?>" required>
                        </div>
                          
                        <div class="form-group">
                        <img src="<?php echo site_url('public/upload/')?><?php echo $abt->image; ?>" style="height:80px; width:80px;">
                        <label class="control-label " for=""></label>
                       
                          <input type="file" class="form-control"  name="file_name" value="<?php echo $abt->image; ?>" >
                        </div>
                      
                         <div class="form-group">

                        <label class="control-label " for="">Active</label>
                       
                          <select class="form-control" name="active" value="<?php echo $abt->active; ?>" required>
                            <option value="active">Active</option>
                            <option value="inactive">inActive</option>
                          
                              </select>
                        </div>

                      <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                      </div>
                    </form>

                       
                    </form>
                     <?php }                                

                                ?>
    </div>
</section>
           
           
           
  </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->


<?php

        $this->load->view('admin/commons/footer');

?>