<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Datatable</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0)">Home</a>
                    </li>
                    <li class="breadcrumb-item active">User Detail</li>
                </ol>
                <a href="<?php echo site_url('user/addUser');?>" alt="default" class="btn btn-info d-none d-lg-block m-l-15 side-menu">
                    <i class="fa fa-plus-circle"></i>Add User</a>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
      <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body" >
                    <h4 class="card-title">User Details</h4>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Last Login</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if(!empty($Users))
                                {
                                    $a=1;
                                foreach ($Users as $user)
                                {
                                ?>
                                <tr>
                                    <td><?php echo $a;?></td>
                                    <td><?php echo $user['first_name'];?></td>
                                    <td><?php echo $user['last_name'];?></td>
                                    <td><?php echo $user['email'];?></td>
                                    <td><?php if($user['active'] == '1') { echo "active";}?></td>
                                    <td><?php echo $user['last_login'];?></td>
                                    <td>
                                        <a href="<?php echo site_url('user/addUser/'.$user['id']);?>" class="edit-user" ><i class="fa fa-edit"></i></a>&emsp;
                                            <a href="<?php echo site_url('user/deleteUser/'.$user['id']);?>" class="delete-user"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                      <?php
                      $a++;
                                }
                      }
                      ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
