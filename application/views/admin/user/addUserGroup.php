<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor"><?php if (isset($singleUserGroup)) { echo "Edit"; } else { ?>Add <?php } ?>User</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0)">Home</a>
                    </li>
                    <li class="breadcrumb-item active"><a href="<?php echo site_url('user/viewUserGroups'); ?>" class="back">Back</a></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <div class="card">
                <div class="card-body" >
                    <p id="error_msg"></p>
                    <?php
                    $content = array('id' => 'add-user-groups');
                   if (isset($singleUserGroup)) { echo form_open('user/doEditUserGroup/'.$singleUserGroup['id'], $content); } else { echo form_open('user/doAdduserGroups', $content); }
                    ?>
                    <div class="row">

                        <div class="col-sm-12 form-group">
                            <label for="name">Group Name</label>
                            <input type="text" class="form-control" name="name" value="<?php if (isset($singleUserGroup)) {
                        echo $singleUserGroup['name'];
                    } ?>" id="name" />
                        </div>

                        <div class="col-sm-12 form-group">
                            <label for="description">Description</label>
                            <input type="text" class="form-control" value="<?php if (isset($singleUserGroup)) {
                        echo $singleUserGroup['description'];
                    } ?>" name="description" id="description" /> </div>


                   
                    </div> 

                    <button type="submit" class="btn btn-info waves-effect pull-right" ><?php if (isset($singleUserGroup)) { echo "Update"; } else { ?>Submit<?php } ?></button>

<?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>