

<?php   
    $this->load->view('admin/commons/header');
        $this->load->view('admin/commons/sidebar');
?>


        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Add Sub Category</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Add Sub Category</li>
                            </ol>
                          
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- End Info box -->
                <!-- ============================================================== -->
               
          

<section>
  <div><p><?php echo validation_errors(); 

if(isset($error)){

  echo $error;
}

  ?></p></div>
              
<div class="col-sm-6">
                    <form class="form-horizontal" action="<?php echo base_url('admin/sub_category_add');?>" method="post" enctype="multipart/form-data">


                            <div class="form-group">
                
                              <label for="sel1">Select Category:</label>
                              <select class="form-control" id="ser" name="category">

                                <?php 
                                   
                                foreach($category as $cat){

                                    echo "<option  value=".$cat->category_name.">".$cat->category_name."</option>";
                                   }                                

                                ?>
                                
                                
                              </select>
                         
                            </div>
                      <div class="form-group">

                        <label class="control-label " for="">Sub Category</label>
                       
                          <input type="text" class="form-control" id="subCategory" name="subcategory" placeholder="Enter Sub Category name" required>
                        </div> 

                        <!--  <div class="form-group" id="hide">

                        <label class="control-label " for="">Add image</label>
                       
                          <input type="file" class="form-control" id="sub_image" name="sub_image" >
                          <input type="file" name="file_name"  />
                        </div> -->
                         <div class="form-group">

                        <label class="control-label " for="">Active</label>
                       
                          <select class="form-control" name="active" required>
                            <option value="active">Active</option>
                            <option value="inactive">inActive</option>
                          
                              </select>
                        </div>                   
                      <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                      </div>
                    </form>
    </div>
</section>

           
           
           
  </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->


<?php

        $this->load->view('admin/commons/footer');

?>