

<?php   
    $this->load->view('admin/commons/header');
        $this->load->view('admin/commons/sidebar');
?>


        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">View Menu</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">View Menu</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- End Info box -->
                <!-- ============================================================== -->
               
          

<section>
                

              <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body" >
                    <h4 class="card-title">User Details</h4>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Member</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if(!empty($userGroups))
                                {
                                    $a=1;
                                foreach ($userGroups as $userGroup)
                                {
                                ?>
                                <tr>
                                    <td><?php echo $a;?></td>
                                    <td><?php echo $userGroup['name'];?></td>
                                    <td><?php echo $userGroup['description'];?></td>
                                   
                                    <td> <?php
                                                $count=0;
                                                foreach ($totalUserGroups as $totalUserGroup) {
                                                    if($totalUserGroup['membergroup_id'] == $userGroup['id'])
                                                    {
                                                        $count++;
                                                    }
                                                }
                                                ?><?php echo $count;?></td>
                                    <td>
                                        <a href="<?php echo site_url('user/addUserGroups/'.$userGroup['id']);?>" class="edit-user" ><i class="fa fa-edit"></i></a>&emsp;
                                            <a href="<?php echo site_url('user/deleteUserGroup/'.$userGroup['id']);?>" class="delete-user"><i class="fa fa-trash"></i></a>&emsp;
                                            <a href="<?php echo site_url('user/addMember/'.$userGroup['id']);?>" class="edit-user"><i class="fa fa fa-users"></i></a>
                                    </td>
                                </tr>
                      <?php
                      $a++;
                                }
                      }
                      ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

           
           
           
  </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->


<?php

        $this->load->view('admin/commons/footer');

?>