

<?php   
    $this->load->view('admin/commons/header');
        $this->load->view('admin/commons/sidebar');
?>


        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Update About Manage</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Update About Manage</li>
                            </ol>
               
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- End Info box -->
                <!-- ============================================================== -->
               
          

<section>
  <div>
    <p><?php echo validation_errors(); 

//print_r($about);
  ?></p></div>
              
<div class="col-sm-6">
   <?php 
                                   
                                foreach($about as $abt)
                                {

                               
                                  ?>
                    <form class="form-horizontal" action="<?php echo base_url('user/about_edit/'.$abt->id);?>" method="post">
                             

                        <div class="form-group">
                
                              <label for="sel1">Select Sub Category:</label>
                              <select class="form-control" name="sub_id" value="<?php echo $abt->cat_id; ?>">

                                <?php 
                                   echo "<option value=".$abt->sub_id." selected>".$abt->sub_id."</option>";
                                   
                                foreach($category as $cat)
                                {

                                    echo "<option value=".$abt->sub_id.">".$abt->sub_id."</option>";
                                   }                                

                                ?>
                                
                                
                              </select>
                         
                            </div> 

                       <div class="form-group">

                        <label class="control-label " for="">About heading</label>
                       
                          <input type="text" class="form-control"  name="heading" placeholder="Enter Heading name" value="<?php echo $abt->heading; ?>" required>
                        </div>
                        <div class="form-group">

                        <label class="control-label " for="">About Content</label>
                       
                          <textarea class="form-control"  name="content" placeholder="Enter Content here" required rows="10" cols="10" value="<?php echo $abt->heading; ?>"><?php echo $abt->heading; ?></textarea>
                        </div>

                          <div class="form-group">

                        <label class="control-label " for="">About Image</label>
                       
                          <input type="file" class="form-control"  name="image" value="<?php echo $abt->heading; ?>" >
                        </div>

                      <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                      </div>

                       
                    </form>
                     <?php }                                

                                ?>
    </div>
</section>
           
           
           
  </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->


<?php

        $this->load->view('admin/commons/footer');

?>