

<?php   
    $this->load->view('admin/commons/header');
        $this->load->view('admin/commons/sidebar');


?>


        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">View Menu</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">View Menu</li>
                            </ol>
                           
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- End Info box -->
                <!-- ============================================================== -->
               
          

<section>
             <div class="row">

        <?php 
           
      // Get Flash data on view 
           echo $this->session->flashdata('sub_message');
         echo $this->session->flashdata('action_message');
            ?>

      
     </div>    

              <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body" >
                    <h4 class="card-title">User Details</h4>
                     <a href="<?php echo base_url('admin/category_add'); ?>" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Add New</a>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                   
                                    <th>Active</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                


                                if(!empty($category))
                                {
                                    $a=1;
                                foreach ($category as $cate)
                                {
                                ?>
                                <tr>
                                    <td><?php echo $a;?></td>
                                    <td><?php echo $cate->category_name;?></td>
                                    <td><?php echo $cate->status;?></td>
                                   
                                    <td>
                                        <a href="<?php echo site_url('admin/category_fetch_edit/'.$cate->id);?>"  ><i class="fa fa-edit"></i></a>&emsp;
                                        <a href="<?php echo site_url('admin/category_delete/'.$cate->id);?>" ><i class="fa fa fa-trash"></i></a>
                                    </td>
                                </tr>
                      <?php
                      $a++;
                                }
                      }
                      ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

           
           
           
  </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->


<?php

        $this->load->view('admin/commons/footer');

?>