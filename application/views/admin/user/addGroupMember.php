<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor"><?php
                if (isset($groupMember)) {
                    echo "Edit";
                } else {
                    ?>Add <?php } ?>User</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0)">Home</a>
                    </li>
                    <li class="breadcrumb-item active"><a href="<?php echo site_url('user/viewUserGroups'); ?>" class="back">Back</a></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-2"></div>
        <div class="col-8">
            <div class="card">
                <div class="card-body" >
                    <p id="error_msg"></p>
                    <h6> Name : <?php
                        if (isset($groupMember)) {
                            echo $groupMember['name'];
                        }
                        ?><br/>
                        Description : <?php
                        if (isset($groupMember)) {
                            echo $groupMember['description'];
                        }
                        ?></h6>
                    <?php
                    $content = array('id' => 'add-member');
                    if (isset($groupMember)) {
                        echo form_open('user/doAddMember/' . $groupMember['id'], $content);
                    }
                    ?>
                    <div class="row">

                        <div class="col-sm-5 form-group">
                            <label for="name">Members: </label>
                            <div class="group-member add-member">
                                <?php foreach ($groupUsersMembers as $groupUsersMember) { ?>
                                    <a href="javascript:;" class="member" id="<?php echo $groupUsersMember['id']; ?>">
                                        <input type="hidden" name="member[]" value="<?php echo $groupUsersMember['id']; ?>">  
                                        <?php echo $groupUsersMember['first_name'] . " " . $groupUsersMember['last_name'] . "<br>"; ?>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-5 form-group">
                            <label for="description">Available users:</label>

                            <div class="group-member remove-member">

                                <?php
                                foreach ($groupUsersUnmembers as $members) {
                                    ?>
                                    <a href="javascript:;" class="unmember" id="<?php echo $members['id']; ?>">
                                        <input type="hidden" name="unmember[]" value="<?php echo $members['id']; ?>">
                                        <?php echo $members['first_name'] . " " . $members['last_name'] . "<br>"; ?>
                                    </a>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>



                    </div> 

                    <button type="submit" class="btn btn-info waves-effect pull-right" >Submit</button>

                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>