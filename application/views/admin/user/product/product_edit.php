

<?php   
    $this->load->view('admin/commons/header');
        $this->load->view('admin/commons/sidebar');
?>


        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Update Product manage</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Update Product manage</li>
                            </ol>
               
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- End Info box -->
                <!-- ============================================================== -->
               
          

<section>
  <div>
    <p><?php echo validation_errors(); 

//print_r($product);
 //print_r($category);

       if (isset($error)){
                    echo $error;
                }
  ?></p></div>
              
<div class="col-sm-6">
   <?php 
                                   
                                foreach($product as $subcat)
                                {

                               
                                  ?>
                    <form class="form-horizontal" action="<?php echo base_url('user/product_edit/'.$subcat->id);?>" method="post" enctype="multipart/form-data">
                             
                       <div class="form-group">
                
                              <label for="sel1">Select Category:</label>
                              <select class="form-control" name="subcat_id" value="<?php echo $subcat->product_name; ?>">

                                <?php 
                                   
                                foreach($subcategory as $subcat){

                                    echo "<option value=".$subcat->url_id.">".$subcat->sub_name."</option>";
                                   }                                

                                ?>
                                
                                
                              </select>
                         
                            </div> 

                      <div class="form-group">

                        <label class="control-label " for="">Product Name</label>
                       
                          <input type="text" class="form-control" id="" name="product_name" value="<?php echo $subcat->product_name; ?>" placeholder="Enter Product Name name" required>
                        </div> 

                          <div class="form-group">

                        <label class="control-label " for="">Product Type</label>
                            
                            <select class="form-control" name="product_type" value="<?php echo $subcat->product_type; ?>" required>
                            <option value="<?php echo $subcat->product_type; ?>"  selected><?php echo $subcat->product_type; ?></option>
                            <option value="New">New</option>
                            <option value="Offer">Offer</option>
                            <option value="Normal">Normal</option>
                            </select>
                       
                         
                        </div> 

                          <div class="form-group">

                        <label class="control-label " for="">Product Price</label>
                       
                          <input type="text" class="form-control" id="" name="product_price" value="<?php echo $subcat->product_price; ?>" placeholder="Enter Product Price name" required>
                        </div> 

                          <div class="form-group">

                        <label class="control-label " for="">Product Offer</label>
                         <select class="form-control" name="product_offer" value="<?php echo $subcat->product_offer; ?>" required>
                            <option value="<?php echo $subcat->product_offer; ?>" selected><?php echo $subcat->product_offer; ?></option>
                            <option value="Advertised">Advertised</option>
                            <option value="Today">Today</option>
                            </select>
                       
                        </div> 

                        <div class="form-group">

                        <label class="control-label " for="">Product Description</label>
                       
                          <input type="text" class="form-control" id="" name="product_description" value="<?php echo $subcat->product_description; ?>" placeholder="Enter Product Offer name" required>
                          <textarea rows="10"class="form-control" id="" name="product_description" value="<?php echo $subcat->product_description; ?>"  required><?php echo $subcat->product_description; ?></textarea>
                        </div> 

                         <div class="form-group">

                        <div class="form-group">
                            <img src="<?php echo site_url('public/upload/product/')?><?php echo $subcat->product_image; ?>" style="height:80px; width:80px;">
                        <label class="control-label " for="">Product Image</label>
                       
                          <input type="file" class="form-control" id="" name="file_name" value="<?php echo $subcat->product_image; ?>">
                        </div> 

                         <div class="form-group">

                        <label class="control-label " for="">Active</label>
                       
                          <select class="form-control" name="active" value="<?php echo $subcat->active; ?>" required>
                            <option value="<?php echo $subcat->active; ?>" ><?php echo $subcat->active; ?></option>
                            <option value="active">Active</option>
                            <option value="inactive">InActive</option>
                          
                          </select>
                        </div>                   
                      <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                      </div>
                       
                    </form>
                     <?php }                                

                                ?>
    </div>
</section>
           
           
           
  </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->


<?php

        $this->load->view('admin/commons/footer');

?>