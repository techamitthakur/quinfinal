

<?php   
    $this->load->view('admin/commons/header');
        $this->load->view('admin/commons/sidebar');
?>


        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Add Product manage</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Add Product manage</li>
                            </ol>
                          
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- End Info box -->
                <!-- ============================================================== -->
               
          

<section>
  <div><p><?php echo validation_errors();
   ?></p></div>

    <?php
                if (isset($error)){
                    echo $error;
                }
            ?>
              
<div class="col-sm-6">
                    <form class="form-horizontal" action="<?php echo base_url('user/product_add');?>" method="POST" enctype="multipart/form-data" >
                     

                     

                             <div class="form-group">
                
                              <label for="sel1">Select Category:</label>
                              <select class="form-control" name="subcat_id">

                                <?php 
                                   print_r($subcategory);
                                foreach($subcategory as $subcat){

                                    echo "<option value=".$subcat->url_id.">".$subcat->sub_name."</option>";
                                   }                                

                                ?>
                                
                                
                              </select>
                         
                            </div> 
                      <div class="form-group">

                        <label class="control-label " for="">Product Name</label>
                       
                          <input type="text" class="form-control" id="" name="product_name" placeholder="Enter Product Name " required>
                        </div> 

                       
                          <div class="form-group">

                        <label class="control-label " for="">Product Type</label>
                            
                            <select class="form-control" name="product_type"  required>
                           
                            <option value="New">New</option>
                            <option value="Offer">Offer</option>
                            <option value="Normal">Normal</option>
                            </select>
                       
                         
                        </div> 

                          <div class="form-group">

                        <label class="control-label " for="">Product Price</label>
                       
                          <input type="text" class="form-control" id="" name="product_price" placeholder="Enter Product Price name" required>
                        </div> 

                         <div class="form-group">

                        <label class="control-label " for="">Product Offer</label>
                         <select class="form-control" name="product_offer" value="" required>
                           
                            <option value="Advertised">Advertised</option>
                            <option value="Today">Today</option>
                            </select>
                       
                        </div>  

                        <div class="form-group">

                        <label class="control-label " for="">Product Description</label>
                       
                          <textarea rows="10" class="form-control" id="" name="product_description" placeholder="Enter Product Description" required></textarea>
                        </div> 

                         <div class="form-group">

                        <label class="control-label " for="">Product Image</label>
                       
                          <input type="file" class="form-control" id="" name="file_name"  required>
                        </div> 

                         <div class="form-group">

                        <label class="control-label " for="">Active</label>
                       
                          <select class="form-control" name="active" required>
                            <option value="active">Active</option>
                            <option value="inactive">inActive</option>
                          
                              </select>
                        </div>                   
                      <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                      </div>
                    </form>
    </div>
</section>

           
           
           
  </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->


<?php

        $this->load->view('admin/commons/footer');

?>