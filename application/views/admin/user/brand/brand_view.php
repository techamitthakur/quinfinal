

<?php   
    $this->load->view('admin/commons/header');
        $this->load->view('admin/commons/sidebar');
?>


        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">View Brand Manage</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">View Brand Manage</li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- End Info box -->
                <!-- ============================================================== -->
               
          

<section>
      <div class="row">

        <?php 
           
      // Get Flash data on view 
           echo $this->session->flashdata('sub_message');
         echo $this->session->flashdata('action_message');
            ?>

      
     </div>
                

              <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body" >
                    <h4 class="card-title">User Details</h4>
                    <a href="<?php echo base_url('user/brand_add'); ?>" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Add New</a>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <!-- <th>Category Name</th> -->
                                   
                                    
                                   
                                    <th>Image</th>
                                    <th>Active</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if(!empty($brand))
                                {
                                  
                                  $a=1;
                                foreach ($brand as $abt)
                                {
                                ?>
                                <tr>
                                
                                    <td><?php echo $a; ?></td>
                                    
                                   <td><img src="<?php echo site_url('public/upload/brand/')?><?php echo $abt->image; ?>" style="height:80px; width:80px;"></td>
                                   <td><?php echo $abt->active; ?></td>
                                  
                                    <td>
                                        <a href="<?php echo site_url('user/brand_fetch_edit/'.$abt->id);?>" class="" ><i class="fa fa-edit"></i></a>&emsp;
                                            <a href="<?php echo site_url('user/brand_delete/'. $abt->id);?>" class=""><i class="fa fa-trash"></i></a>&emsp;
                                            
                                    </td>
                                </tr>
                      <?php

                            $a++;
                                 }
                      }
                      ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

           
           
           
  </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->


<?php

        $this->load->view('admin/commons/footer');

?>