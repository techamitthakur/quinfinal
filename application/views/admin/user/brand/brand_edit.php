

<?php   
    $this->load->view('admin/commons/header');
        $this->load->view('admin/commons/sidebar');
?>


        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Update Brand manage</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Update Brand manage</li>
                            </ol>
               
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- End Info box -->
                <!-- ============================================================== -->
               
          

<section>
  <div>
    <p><?php echo validation_errors(); 

//print_r($services);
  ?></p></div>
              
<div class="col-sm-6">
   <?php 
                                   
                                foreach($brand as $abt)
                                {

                               
                                  ?>
                       <form class="form-horizontal" action="<?php echo base_url('user/brand_edit/'.$abt->id);?>" method="post" enctype="multipart/form-data">

                         <div class="form-group">

                        <label class="control-label " for="">Brand Name</label>
                       
                          <input type="text" class="form-control"  name="brand_name" value="<?php echo $abt->brand_name; ?>"  required>
                        </div>
                          
                        <div class="form-group">
                        <img src="<?php echo site_url('public/upload/brand/')?><?php echo $abt->image; ?>" style="height:80px; width:80px;">
                        <label class="control-label " for=""></label>
                       
                          <input type="file" class="form-control"  name="file_name" value="<?php echo $abt->image; ?>" >
                        </div>
                      
                         <div class="form-group">

                        <label class="control-label " for="">Active</label>
                       
                          <select class="form-control" name="active" value="<?php echo $abt->active; ?>" required>
                            <option value="<?php echo $abt->active; ?>" selected><?php echo $abt->active; ?></option>
                            <option value="active">Active</option>
                            <option value="inactive">inActive</option>
                          
                              </select>
                        </div>

                      <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                      </div>
                    </form>

                       
                    </form>
                     <?php }                                

                                ?>
    </div>
</section>
           
           
           
  </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->


<?php

        $this->load->view('admin/commons/footer');

?>
