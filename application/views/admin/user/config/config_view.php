

<?php   
    $this->load->view('admin/commons/header');
        $this->load->view('admin/commons/sidebar');
?>


        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Config Website</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Config Website</li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- End Info box -->
                <!-- ============================================================== -->
               
          

<section>
      <div class="row">

        <?php 
           
      // Get Flash data on view 
           echo $this->session->flashdata('sub_message');
         echo $this->session->flashdata('action_message');
            ?>

      
     </div>
                

              <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body" >
                    <h4 class="card-title">User Config</h4>
                    <!-- <a href="<?php echo base_url('user/product_add'); ?>" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Add New</a> -->
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    
                                    <th>Logo Name</th>
                                    <th> Contact</th>
                                    <th> Email</th>
                                    <th> Address</th>
                                    <th> facebook</th>
                                    <th> instagram</th>
                                    <th> twitter</th>
                                    <th> Logo</th>
                                    
                                   
                                   
                                    <th>Operation</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if(!empty($config))
                                {
                                  
                                  $a=1;
                                foreach ($config as $cat)
                                {
                                ?>
                                <tr>
                                
                                    <td><?php echo $a; ?></td>
                                    <td><?php echo $cat->logo_name; ?></td>
                                    <td><?php echo $cat->contact; ?></td>
                                    <td><?php echo $cat->email; ?></td>
                                    <td><?php echo $cat->address; ?></td>
                                    <td><?php echo $cat->facebook; ?></td>
                                    <td><?php echo $cat->instagram; ?></td>
                                    <td><?php echo $cat->twitter; ?></td>
                                     <td><img src="<?php echo site_url('public/upload/')?><?php echo $cat->logo; ?>" style="height:80px; width:80px;"></td>
                                   
                                  
                                  
                                    <td>
                                        <a href="<?php echo site_url('user/config_fetch_edit/1');?>" class="" ><i class="fa fa-edit"></i></a>&emsp;
                                           
                                            
                                    </td>
                                </tr>
                      <?php

                            $a++;
                                 }
                      }
                      ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

           
           
           
  </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->


<?php

        $this->load->view('admin/commons/footer');

?>