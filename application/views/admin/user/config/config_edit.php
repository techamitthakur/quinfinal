

<?php   
    $this->load->view('admin/commons/header');
        $this->load->view('admin/commons/sidebar');
?>


        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Config Website</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Config Website</li>
                            </ol>
               
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- End Info box -->
                <!-- ============================================================== -->
               
          

<section>
  <div>
    <p><?php echo validation_errors(); 

//print_r($product);
 //print_r($category);

       if (isset($error)){
                    echo $error;
                }
  ?></p></div>
              
<div class="col-sm-6">
   <?php 
                                   
                                foreach($config as $subcat)
                                {

                               
                                  ?>
                    <form class="form-horizontal" action="<?php echo base_url('user/config_edit/1');?>" method="post" enctype="multipart/form-data">
                             

                    
                          <div class="form-group">

                        <label class="control-label " for="">Website contact</label>
                       
                          <input type="text" class="form-control" id="" name="contact" value="<?php echo $subcat->contact; ?>" placeholder="Enter Website contact" required>
                        </div> 

                          <div class="form-group">

                        <label class="control-label " for="">Website address</label>
                       
                          <input type="text" class="form-control" id="" name="address" value="<?php echo $subcat->address; ?>" placeholder="Enter Website address" required>
                        </div> 

                          <div class="form-group">

                        <label class="control-label " for="">Website email</label>
                       
                          <input type="email" class="form-control" id="" name="email" value="<?php echo $subcat->email; ?>" placeholder="Enter Website email" required>
                        </div> 

                        <div class="form-group">

                        <label class="control-label " for="">Website facebook Link </label>
                       
                          <input type="text" class="form-control" id="" name="facebook" value="<?php echo $subcat->facebook; ?>" placeholder="Enter facebook link" required>
                        </div> 
                         <div class="form-group">

                        <label class="control-label " for="">Website instagram Link</label>
                       
                          <input type="text" class="form-control" id="" name="instagram" value="<?php echo $subcat->instagram; ?>" placeholder="Enter instagram link" required>
                        </div>
                         <div class="form-group">

                        <label class="control-label " for="">Website twitter Link</label>
                       
                          <input type="text" class="form-control" id="" name="twitter" value="<?php echo $subcat->twitter; ?>" placeholder="Enter twitter link" required>
                        </div>
                        <div class="form-group">

                        <label class="control-label " for="">Website logo name</label>
                       
                          <input type="text" class="form-control" id="" name="logo_name" value="<?php echo $subcat->logo_name; ?>" placeholder="Enter logo name" required>
                        </div>
                         <div class="form-group">

                        <div class="form-group">
                            <img src="<?php echo site_url('public/upload/')?><?php echo $subcat->logo; ?>" style="height:80px; width:80px;">
                        <label class="control-label " for="">Website logo</label>
                       
                          <input type="file" class="form-control" id="" name="file_name" value="<?php echo $subcat->logo; ?>">
                        </div> 

                                           
                      <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                      </div>
                       
                    </form>
                     <?php }                                

                                ?>
    </div>
</section>
           
           
           
  </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->


<?php

        $this->load->view('admin/commons/footer');

?>