
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User Profile-->
                <div class="user-profile">
                    <div class="user-pro-body">
                        <div>
                            <img src="<?php echo base_url();?>public/dist/assets/images/users/2.jpg" alt="user-img" class="img-circle">
                        </div>
                        <div class="dropdown">
                            <a href="javascript:void(0)" class="dropdown-toggle u-dropdown link hide-menu" data-toggle="dropdown" role="button" aria-haspopup="true"
                                aria-expanded="false">Admin Panel
                                <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu animated flipInY">
                                <!-- text--
                                <a href="javascript:void(0)" class="dropdown-item">
                                    <i class="ti-user"></i> My Profile</a>
                                <!-- text-->
                               
                                <!-- text--
                                <a href="javascript:void(0)" class="dropdown-item">
                                    <i class="ti-settings"></i> Account Setting</a>
                                <!-- text-->
                                <div class="dropdown-divider"></div>
                                <!-- text-->
                                <a href="<?php echo site_url('admin/logout');?>" class="dropdown-item">
                                    <i class="fa fa-power-off"></i> Logout</a>
                                <!-- text-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                       
                       <!-- <li>
                            <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                                <i class="icon-speedometer"></i>
                                <span class="hide-menu">Admin
                                    <span class="badge badge-pill badge-cyan ml-auto">4</span>
                                </span>
                            </a>
                            <ul aria-expanded="false" class="collapse">
                                <li>
                                    <a href="<?php echo site_url('admin/viewUser');?>" class="side-menu">Users </a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('admin/accessLevel');?>" class="side-menu">Access Level</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('admin/viewUserGroups');?>" class="side-menu">User Groupsdd</a>
                                </li>
                                <li>
                                    <a href="index4.html">Modern</a>
                                </li>
                            </ul>
                        </li>-->

                        <!--***********new addded here*****************-->
                          <li>
                            <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                                <i class="icon-speedometer"></i>
                                <span class="hide-menu">Menu Manage
                                   
                                </span>
                            </a>
                            <ul aria-expanded="false" class="collapse">
                                
                                <li>
                                    <a href="<?php echo site_url('admin/category_view');?>" class="">Category</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('admin/sub_category_view');?>" class="">Sub category</a>
                                </li>
                              
                            </ul>
                        </li>
                          <li>
                            <a class="waves-effect waves-dark" href="<?php echo site_url('admin/queries');?>" aria-expanded="false">
                                <i class="icon-speedometer"></i>
                                <span class="hide-menu">Queries Details
                                   
                                </span>
                            </a>
                         </li> 
                          <!-- <li>
                            <a class="waves-effect waves-dark" href="<?php echo site_url('admin/services_view');?>" aria-expanded="false">
                                <i class="icon-speedometer"></i>
                                <span class="hide-menu">Service Manage
                                   
                                </span>
                            </a>
                         </li> -->
                        <!--  <li>
                            <a class="waves-effect waves-dark" href="<?php echo site_url('admin/treatment_view');?>" aria-expanded="false">
                                <i class="icon-speedometer"></i>
                                <span class="hide-menu">Treatment Manage
                                   
                                </span>
                            </a>
                         </li> -->
                        <!--  <li style="display: none">
                            <a class="waves-effect waves-dark" href="<?php echo site_url('admin/contact_view');?>" aria-expanded="false">
                                <i class="icon-speedometer"></i>
                                <span class="hide-menu">Contact Manage
                                   
                                </span>
                            </a>
                         </li>
                          <li>
                            <a class="waves-effect waves-dark" href="<?php echo site_url('admin/product_view');?>" aria-expanded="false">
                                <i class="icon-speedometer"></i>
                                <span class="hide-menu">Product Manage
                                   
                                </span>
                            </a>
                         </li>
                           <li>
                            <a class="waves-effect waves-dark" href="<?php echo site_url('admin/slider_view');?>" aria-expanded="false">
                                <i class="icon-speedometer"></i>
                                <span class="hide-menu">Slider Manage
                                   
                                </span>
                            </a>
                         </li>
                           <li>
                            <a class="waves-effect waves-dark" href="<?php echo site_url('admin/brand_view');?>" aria-expanded="false">
                                <i class="icon-speedometer"></i>
                                <span class="hide-menu">Brand Manage
                                   
                                </span>
                            </a>
                         </li> -->
                         <li>
                            <a class="waves-effect waves-dark" href="#" aria-expanded="false">
                                <i class="icon-speedometer"></i>
                                <span class="hide-menu">Website Manage
                                   
                                </span>
                            </a>
                         </li>
                          <!--  <li>
                            <a class="waves-effect waves-dark" href="<?php echo site_url('admin/team_view');?>" aria-expanded="false">
                                <i class="icon-speedometer"></i>
                                <span class="hide-menu">Team Manage
                                   
                                </span>
                            </a>
                         </li> -->
                           <li>
                            <a class="waves-effect waves-dark" href="<?php echo site_url('admin/testimonial_view');?>" aria-expanded="false">
                                <i class="icon-speedometer"></i>
                                <span class="hide-menu">Testimonial Manage
                                   
                                </span>
                            </a>
                         </li>
                         <!-- <li>
                            <a class="waves-effect waves-dark" href="<?php echo site_url('admin/founder_view');?>" aria-expanded="false">
                                <i class="icon-speedometer"></i>
                                <span class="hide-menu">Founder Profile
                                   
                                </span>
                            </a>
                         </li> -->

                        
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
