<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from wpshopmart.com/html/demo/dlister/blog-detail.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Jul 2019 04:34:03 GMT -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- favicon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon.png">

    <!--  CSS Libraries -->
    <link rel="stylesheet" href="assets/css/plugins.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">

    <!-- PAGE TITLE -->
    <title>D-Lister | Directory & Listing Responsive HTML5 Template </title>

</head>

<body>

    <!-- ====== Go to top ====== -->
    <a id="toTop" title="Go to top" href="javascript:void(0)">
        <span id="toTopHover"></span>TOP
    </a>

    <!-- Preloader start-->
    <div class="preloader">
        <div class="loader-inner">
            <div class="dash one">
                <i class="fa fa-map-marker-alt"></i>
            </div>
            <div class="dash two">
                <i class="fa fa-map-marker-alt"></i>
            </div>
            <div class="dash three">
                <i class="fa fa-map-marker-alt"></i>
            </div>
        </div>
    </div>
    <!-- Preloader end -->

    <!-- Header start-->
    <header class="header">
        <div class="head-top head-top-one d-none d-lg-block">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <div class="socials socials-header text-lg-left text-center">
                            <a href="#">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-instagram"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <ul class="head-contact text-lg-right text-center">
                            <li>
                                <i class="fa fa-phone"></i>
                                +123-456-7890
                            </li>
                            <li>
                                <i class="fa fa-envelope"></i>
                                info@company.com
                            </li>
                            <li class="language-drop">
                                <div class="dropdown">
                                    <a href="#" class="dropdown-toggle text-capitalize" data-toggle="dropdown">
                                        english
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#">hindi</a>
                                        <a class="dropdown-item" href="#">urdu</a>
                                        <a class="dropdown-item" href="#">english</a>
                                    </div>
                                </div>
                            </li>
                            <li class="ad-list">
                                <a href="add-list.html" class="btn btn-two btn-anim">
                                    <i class="fa fa-plus-circle"></i> add listing
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="theme-header-one affix">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-12">
                        <div class="logo-one logo-wrap">
                            <div class="logo my-1">
                                <a href="index-2.html">
                                    <img src="assets/img/logo.png" alt="logo">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-9 col-md-12">
                        <div class="menu menu-one">
                            <nav class="navbar navbar-expand-lg">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-content" aria-controls="nav-content"
                                    aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon bar1"></span>
                                    <span class="navbar-toggler-icon bar2"></span>
                                    <span class="navbar-toggler-icon bar3"></span>
                                </button>
                                <!-- Links -->
                                <div class="main-menu collapse navbar-collapse" id="nav-content">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item dropdown">
                                            <a href="#" class="nav-link dropdown-toggle current" data-toggle="dropdown">home</a>
                                            <ul class="dropdown-menu">
                                                <li class="dropdown-item dropdown">
                                                    <a href="index-2.html">home 1</a>
                                                </li>
                                                <li class="dropdown-item dropdown">
                                                    <a href="index-3.html">home 2</a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="about.html">about us</a>
                                        </li>

                                        <li class="nav-item dropdown">
                                            <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown">listing</a>
                                            <ul class="dropdown-menu">
                                                <li class="dropdown-item">
                                                    <a href="listing.html">all listings</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="listing-sidebar.html">listing with sidebar</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="listing-with-map.html">listing with map</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="listing-detail.html">listing detail</a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-item dropdown">
                                            <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown">blogs</a>
                                            <ul class="dropdown-menu">
                                                <li class="dropdown-item">
                                                    <a href="blog-page.html">blog page</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="blog-right-sidebar.html">blog rightbar</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="blog-left-sidebar.html">blog leftbar</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="blog-detail.html">blog detail</a>
                                                </li>
                                                <li class="dropdown-item dropdown">
                                                    <a class="dropdown-toggle" href="javascript:void(0)">level 1</a>
                                                    <ul class="dropdown-menu">
                                                        <li class="dropdown-item">
                                                            <a href="javascript:void(0)">level 2</a>
                                                        </li>
                                                        <li class="dropdown-item dropdown">
                                                            <a href="javascript:void(0)" class="dropdown-toggle">level 2</a>
                                                            <ul class="dropdown-menu">
                                                                <li class="dropdown-item">
                                                                    <a href="javascript:void(0)">level 3</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-item dropdown has-mega">
                                            <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                pages
                                            </a>
                                            <ul class="dropdown-menu mega-dropdown">
                                                <li>
                                                    <ul>
                                                        <li class="dropdown-item">
                                                            <a href="about.html">about us</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="faq.html">faq</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="service.html">service</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="pricing.html">pricing</a>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li>
                                                    <ul>
                                                        <li class="dropdown-item">
                                                            <a href="error-page.html">error page</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="add-list.html">add list</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="contact.html">contact us</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="login.html">login / signup</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <ul>
                                                        <li class="dropdown-item">
                                                            <a href="listing.html">all listings</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="listing-sidebar.html">listing with sidebar</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="listing-with-map.html">listing with map</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="listing-detail.html">listing detail</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <ul>
                                                        <li class="dropdown-item">
                                                            <a href="blog-page.html">blog page</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="blog-right-sidebar.html">blog rightbar</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="blog-left-sidebar.html">blog leftbar</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="blog-detail.html">blog detail</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="contact.html">contact us</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#search">
                                                <i class="fa fa-search"></i>
                                            </a>
                                        </li>
                                        <li class="nav-item join-nav d-lg-block d-none">
                                            <a class="nav-link" href="login.html">
                                                <i class="fa fa-user c-theme mr-2"></i> join us</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Header end -->

    <!-- Search Form -->
    <div id="search" class="top-search d-flex">
        <span class="close">
            <i class="fa fa-times"></i>
        </span>
        <div class="w-100 text-center mt-4">
            <h3 class="c-white fw-5">search here</h3>
            <form role="search" id="searchform" action="#" method="get" class="search-bar">
                <input value="" name="q" type="search" placeholder="type to search..." class="form-control">
                <button type="submit" class="submit-btn">
                    <i class="fa fa-search"></i>
                </button>
            </form>
        </div>
    </div>

    <!-- page-banner start-->
    <section class="page-banner">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3>blog detail</h3>
                    <ul class="banner-link text-center">
                        <li>
                            <a href="index-2.html">Home</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">blogs</a>
                        </li>
                        <li>
                            <span class="active">blog detail</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- page-banner ends-->

    <!-- blog detail start-->
    <section class="bg-w sp-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="blog-detail">
                        <div id="blog-slider2" class="carousel blog-slider slide mb-4" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="assets/img/blog/blog-lg1.png" alt="listing">
                                </div>
                                <div class="carousel-item">
                                    <img src="assets/img/blog/blog-lg2.png" alt="listing">
                                </div>
                                <div class="carousel-item">
                                    <img src="assets/img/blog/blog-lg3.png" alt="listing">
                                </div>
                            </div>
                            <div class="carousel-nav">
                                <a class="carousel-control-prev" href="#blog-slider2" data-slide="prev">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                                <a class="carousel-control-next" href="#blog-slider2" data-slide="next">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                            <ul class="carousel-indicators">
                                <li data-target="#blog-slider2" data-slide-to="0" class="active"></li>
                                <li data-target="#blog-slider2" data-slide-to="1"></li>
                                <li data-target="#blog-slider2" data-slide-to="2"></li>
                            </ul>
                        </div>
                        <div class="blog-d-content">
                            <h4>Exploring some of the best rental cities </h4>
                            <p>Lorem ipsum dolor sit amet, consectadetudzdae rcquisc adipiscing elit. Aenean socada com modoq
                                ligaui egets dolor. Aenean magsfssa. Cum socadaiis nato qfuae pentua ibaus et magnsfis dis
                                partu rient mon nascqetu rs idicfulus mus donefc quamaem felis, ultriciddedes nec, pefflslen
                                tesqdfe eu, pr etium fa quis sem magns.</p>
                            <p>Socadaiis nato qfuae pent parturient mon tes, nascqetur rsidicful enean mhgs uem socadaiis nato
                                qfuae pent. Ibaus et magnsfis dis parturient mon tes, nascqetur rsidicfulus rtis mus. Donefc
                                qua maem felis, ultriciddedes nec, pefflsl tesquwdfe eu, pr etium quis, sem.</p>
                            <blockquote>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget mag dolor. Cum sociis natoque penatibus
                                    et magnis dis parturient montes nascetur ridiculus ru mus. Do quam felis ultricies nec
                                    pellentesque eu pretium quis sem.
                                </p>
                            </blockquote>
                            <div class="row">
                                <div class="col-lg-4 mb-4">
                                    <img src="assets/img/blog/blog1.png" alt="listing" class="br-10 w-100">
                                </div>
                                <div class="col-lg-4 mb-4">
                                    <img src="assets/img/blog/blog2.png" alt="listing" class="br-10 w-100">
                                </div>
                                <div class="col-lg-4 mb-4">
                                    <img src="assets/img/blog/blog3.png" alt="listing" class="br-10 w-100">
                                </div>
                            </div>
                            <p>Ascqetur rsidicfulus mus donefc quamaem felis, ultriciddedes nec, pefflsl tesquwdfe eu, pr etiu
                                qui fa cadaiis sem. Cum socadaiis nato qfuae pent ibaus et magnsfis dis parturient mon tes,
                                nascqetur rsidi icfulus mus. Ibaus et magnsfis dis parturient te.</p>
                            <ul class="blog-meta centering justify-content-start">
                                <li>
                                    <i class="far fa-user mr-1"></i>
                                    <a href="#">Johnathan Doe</a>
                                </li>
                                <li>

                                    <i class="far fa-calendar-alt mr-1"></i>
                                    21 Jan 2019
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-eye mr-1"></i>
                                        20
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="far fa-comments mr-1"></i>
                                        06 Comments
                                    </a>
                                </li>
                            </ul>
                            <ul class="share-tag d-xl-flex justify-content-between">
                                <li class="tagcloud mb-0 centering justify-content-xl-between justify-content-start">
                                    <h4 class="mb-0 mr-3"> tags : </h4>
                                    <ul>
                                        <li class="my-1">
                                            <a href="#">Restaurants</a>
                                        </li>
                                        <li class="my-1">
                                            <a href="#">Events</a>
                                        </li>
                                        <li class="my-1">
                                            <a href="#">Tourism</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="blog-socials centering justify-content-xl-between justify-content-start mt-xl-0 mt-3">
                                    <h4 class="mb-0 mr-3">share : </h4>
                                    <div class="share socials"></div>
                                </li>
                            </ul>
                        </div>
                        <div class="blog-navigation mt-30 centering justify-content-between mb-60">
                            <div class="navi">
                                <a href="#">
                                    <i class="fa fa-angle-left"></i>
                                    <span class="ml-2"> prev</span>
                                </a>
                            </div>
                            <div class="navi">
                                <a href="#">
                                    <span class="mr-2"> next</span>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="comments-area">
                            <h4 class="title-sep2 mb-30">
                                comments
                            </h4>
                            <div class="clearfix">
                                <ol class="comment-list mb-60">
                                    <li class="comment">
                                        <div class="comment-body">
                                            <div class="comment-author">
                                                <img class="avatar photo" src="assets/img/blog/com-1.jpg" alt="author">
                                                <b class="fn">
                                                    <a href="#"> Gregory mccarron</a>
                                                </b>
                                                <span class="says"> says:</span>
                                            </div>
                                            <div class="comment-meta">
                                                <a href="#">April 20, 2019 at 7:15 am</a>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vitae neqnsectetur
                                                adipiscing elit. Nam Aliquip esse ad eiusmod fugiat aute adipisicing duis
                                                consequat commodo ation. elit.
                                            </p>
                                            <div class="reply">
                                                <a href="#" class="comment-reply-link">Reply</a>
                                            </div>
                                        </div>
                                        <ol class="children">
                                            <li class="comment odd parent">
                                                <div class="comment-body">
                                                    <div class="comment-author vcard">
                                                        <img class="avatar photo" src="assets/img/blog/com-2.jpg" alt="author">
                                                        <b class="fn">ban Francese</b>
                                                        <span class="says">says:</span>
                                                    </div>
                                                    <div class="comment-meta">
                                                        <a href="#">April 22, 2019 at 7:15 am</a>
                                                    </div>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vitae neque
                                                        vitae sapien malesuada aliquet. Fusce et massa eu ante ornare molestie.
                                                        Sed vestibulum sem felis.</p>
                                                    <div class="reply">
                                                        <a href="#" class="comment-reply-link">Reply</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ol>
                                    </li>
                                    <li class="comment">
                                        <div class="comment-body">
                                            <div class="comment-author">
                                                <img class="avatar photo" src="assets/img/blog/com-3.jpg" alt="author">
                                                <b class="fn">
                                                    <a href="#"> Gregory mccarron</a>
                                                </b>
                                                <span class="says"> says:</span>
                                            </div>
                                            <div class="comment-meta">
                                                <a href="#">April 24, 2019 at 7:15 am</a>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vitae neqnsectetur
                                                adipiscing elit. Nam Aliquip esse ad eiusmod fugiat aute adipisicing duis
                                                consequat commodo ation elit.
                                            </p>
                                            <div class="reply">
                                                <a href="#" class="comment-reply-link">Reply</a>
                                            </div>
                                        </div>
                                    </li>
                                </ol>

                                <div class="comment-respond" id="respond">
                                    <h4 class="title-sep2 mb-30">
                                        leave comment
                                    </h4>
                                    <form class="comment-form" id="commentform" method="post" action="#">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                <span class="fa fa-user"></span>
                                                <input type="text" id="name" class="form-control" placeholder="Enter Name">
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <span class="fa fa-envelope"></span>
                                                <input type="email" class="form-control" placeholder="Enter Email" name="Ented email" id="email">
                                            </div>
                                            <div class="col-12">
                                                <textarea rows="5" name="comment" class="form-control" placeholder="Your Message" id="comment"></textarea>
                                            </div>
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-one btn-anim" id="submit" name="submit">
                                                    <i class="fa fa-paper-plane"></i> submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- Form END -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12">
                    <aside class="sidebar">
                        <div class="widget widget_search">
                            <h4 class="title-sep2 mb-30">search</h4>
                            <div class="search-box">
                                <form role="search" method="post">
                                    <div class="input-group">
                                        <input name="text" class="form-control" placeholder="Search..." type="text">
                                        <button type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="widget">
                            <h4 class="title-sep2 mb-30">about author</h4>
                            <div class="widget-author">
                                <div class="author-top text-right mb-4">
                                    <div class="img-holder">
                                        <img src="assets/img/blog/aut-big.png" alt="#">
                                    </div>
                                    <div class="author-info">
                                        <h5 class="c-white">Julia Holmes</h5>
                                        <span>co manager</span>
                                    </div>
                                </div>
                                <p class="bor-b pb-3">Excepteur enim aute dolor culpa aliqua nulla. Non labore est irure ipsum cupidatat amet fugiat
                                    quis voluptate cillum velit qui.</p>
                                <div class="socials">
                                    <a href="#">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-google-plus-g"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="widget">
                            <h4 class="title-sep2 mb-30">categories</h4>
                            <div class="widget-categories">
                                <ul class="widget-list">
                                    <li>
                                        <a href="#">Restaurants
                                        </a>
                                        23
                                    </li>
                                    <li>
                                        <a href="#">Destinations
                                        </a>
                                        51
                                    </li>
                                    <li>
                                        <a href="#">Beauty & Spa
                                        </a>
                                        06
                                    </li>
                                    <li>
                                        <a href="#">Shopping
                                        </a>
                                        65
                                    </li>
                                    <li>
                                        <a href="#">Entertainment
                                        </a>
                                        05
                                    </li>
                                    <li>
                                        <a href="#">Health & Care
                                        </a>
                                        11
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="widget">
                            <h4 class="title-sep2 mb-30">popular posts</h4>
                            <div class="widget-post">
                                <ul>
                                    <li class="news-post">
                                        <figure class="thumb">
                                            <a href="blog-detail.html">
                                                <img src="assets/img/blog/post-thumb1.png" alt="news">
                                            </a>
                                        </figure>
                                        <div class="news-content">
                                            <a href="blog-detail.html">Dis parturient montes pede justo na etus rid iculs musg.</a>
                                            <p>
                                                15 April, 2019</p>
                                        </div>
                                    </li>
                                    <li class="news-post">
                                        <figure class="thumb">
                                            <a href="blog-detail.html">
                                                <img src="assets/img/blog/post-thumb2.png" alt="news">
                                            </a>
                                        </figure>
                                        <div class="news-content">
                                            <a href="blog-detail.html">Dis parturient montes pede justo na etus rid iculs musg.</a>
                                            <p>
                                                28 April, 2019</p>
                                        </div>
                                    </li>
                                    <li class="news-post">
                                        <figure class="thumb">
                                            <a href="blog-detail.html">
                                                <img src="assets/img/blog/post-thumb3.png" alt="news">
                                            </a>
                                        </figure>
                                        <div class="news-content">
                                            <a href="blog-detail.html">Dis parturient montes pede justo na etus rid iculs musg.</a>
                                            <p>
                                                20 April, 2019</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="widget">
                            <h4 class="title-sep2 mb-30">tags</h4>
                            <div class="widget-post">
                                <div class="tagcloud">
                                    <ul>
                                        <li>
                                            <a href="#">Restaurants</a>
                                        </li>
                                        <li>
                                            <a href="#">Events</a>
                                        </li>
                                        <li>
                                            <a href="#">Tourism</a>
                                        </li>
                                        <li>
                                            <a href="#">Trending</a>
                                        </li>
                                        <li>
                                            <a href="#">Travel</a>
                                        </li>
                                        <li>
                                            <a href="#">Nightlife</a>
                                        </li>
                                        <li>
                                            <a href="#">Photography</a>
                                        </li>
                                        <li>
                                            <a href="#">Lifestyle</a>
                                        </li>
                                        <li>
                                            <a href="#">food</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>
    <!-- blog detail end -->

    <!-- cta-one start-->
    <section class="cta-one tri-bg-w text-lg-left text-center">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 my-lg-0 my-5 py-lg-0 py-5">
                    <div class="cta-content">
                        <h3>Sign Up To Get Special Offers Every Day</h3>
                        <p>Lorem ipsum dolor sit amet, consectadetudzdae rcquisc adipiscing elit. Aenean socada commodo ligaui
                            egets dolor. </p>
                        <a href="login.html" class="btn btn-two btn-anim mt-2">
                            sign up
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 d-lg-block d-none">
                    <div class="cta-img mt-4">
                        <img src="assets/img/home/cta-bg.png" alt="image">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- cta-one end -->

    <!-- footer starts -->
    <footer class="footer footer-one">
        <div class="foot-top">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-lg-12 col-12 mb-60">
                        <div class="company-details">
                            <img src="assets/img/logo.png" class="foot-logo mb-4" alt="lister">
                            <p class="pb-2">Lorem ipsum dolor sit amet, consect tuer adipi cin elit. Aen sociis nato pe na ibus magnis dis
                                partur monte na et ridiculu onec quam felis. castei efte tuerg scn kt isceding elit ende
                                cd magum socadaiis nato ipsf.</p>
                            <div class="socials mt-4">
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-google-plus-g"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                        <div class="recent-post">
                            <div class="foot-title">
                                <h4>useful links</h4>
                            </div>
                            <ul class="quick-link">
                                <li>
                                    <a href="add-list.html">add listing</a>
                                </li>
                                <li>
                                    <a href="register.html">register</a>
                                </li>
                                <li>
                                    <a href="sign-in.html">sign in</a>
                                </li>
                                <li>
                                    <a href="about.html">how it work</a>
                                </li>
                                <li>
                                    <a href="contact.html">contact us</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                        <div class="recent-post">
                            <div class="foot-title">
                                <h4>my account</h4>
                            </div>
                            <ul class="quick-link">
                                <li>
                                    <a href="login.html">create account</a>
                                </li>
                                <li>
                                    <a href="#">dashboard</a>
                                </li>
                                <li>
                                    <a href="#">profile</a>
                                </li>
                                <li>
                                    <a href="#">my listing</a>
                                </li>
                                <li>
                                    <a href="#">favorites</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                        <div class="recent-post">
                            <div class="foot-title">
                                <h4>categories</h4>
                            </div>
                            <ul class="quick-link">
                                <li>
                                    <a href="#">Restaurants</a>
                                </li>
                                <li>
                                    <a href="#">Shopping</a>
                                </li>
                                <li>
                                    <a href="#">Hotels</a>
                                </li>
                                <li>
                                    <a href="#">Destinations</a>
                                </li>
                                <li>
                                    <a href="#">Nightlife</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                        <div class="recent-post">
                            <div class="foot-title">
                                <h4>quick links</h4>
                            </div>
                            <ul class="quick-link">
                                <li>
                                    <a href="about.html">About Us</a>
                                </li>
                                <li>
                                    <a href="price.html">Pricing</a>
                                </li>
                                <li>
                                    <a href="blog-grid.html">Blogs</a>
                                </li>
                                <li>
                                    <a href="#">Testimonials</a>
                                </li>
                                <li>
                                    <a href="#">Our Partners</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="foot-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <p class="text-capitalize">Copyright © 2019, All rights Reserved. Created by
                            <a href="#">Company Name</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer end -->

    <!-- JavaScript Libraries -->
    <script src="assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/slick.min.js"></script>
    <script src="assets/js/jssocials.js"></script>
    <script src="assets/js/custom.js"></script>


</body>


<!-- Mirrored from wpshopmart.com/html/demo/dlister/blog-detail.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Jul 2019 04:34:05 GMT -->
</html>