<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from wpshopmart.com/html/demo/dlister/listing-detail.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Jul 2019 04:33:59 GMT -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- favicon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon.png">

    <!--  CSS Libraries -->
    <link rel="stylesheet" href="assets/css/plugins.css">    
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">

    <!-- PAGE TITLE -->
    <title>D-Lister | Directory & Listing Responsive HTML5 Template </title>

</head>

<body>

    <!-- ====== Go to top ====== -->
    <a id="toTop" title="Go to top" href="javascript:void(0)">
        <span id="toTopHover"></span>TOP
    </a>

    <!-- Preloader start-->
    <div class="preloader">
        <div class="loader-inner">
            <div class="dash one">
                <i class="fa fa-map-marker-alt"></i>
            </div>
            <div class="dash two">
                <i class="fa fa-map-marker-alt"></i>
            </div>
            <div class="dash three">
                <i class="fa fa-map-marker-alt"></i>
            </div>
        </div>
    </div>
    <!-- Preloader end -->

    <!-- Header start-->
    <header class="header">
        <div class="head-top head-top-one d-none d-lg-block">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <div class="socials socials-header text-lg-left text-center">
                            <a href="#">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-instagram"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <ul class="head-contact text-lg-right text-center">
                            <li>
                                <i class="fa fa-phone"></i>
                                +123-456-7890
                            </li>
                            <li>
                                <i class="fa fa-envelope"></i>
                                info@company.com
                            </li>
                            <li class="language-drop">
                                <div class="dropdown">
                                    <a href="#" class="dropdown-toggle text-capitalize" data-toggle="dropdown">
                                        english
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#">hindi</a>
                                        <a class="dropdown-item" href="#">urdu</a>
                                        <a class="dropdown-item" href="#">english</a>
                                    </div>
                                </div>
                            </li>
                            <li class="ad-list">
                                <a href="add-list.html" class="btn btn-two btn-anim">
                                    <i class="fa fa-plus-circle"></i> add listing
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="theme-header-one affix">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-12">
                        <div class="logo-one logo-wrap">
                            <div class="logo my-1">
                                <a href="index-2.html">
                                    <img src="assets/img/logo.png" alt="logo">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-9 col-md-12">
                        <div class="menu menu-one">
                            <nav class="navbar navbar-expand-lg">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-content" aria-controls="nav-content"
                                    aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon bar1"></span>
                                    <span class="navbar-toggler-icon bar2"></span>
                                    <span class="navbar-toggler-icon bar3"></span>
                                </button>
                                <!-- Links -->
                                <div class="main-menu collapse navbar-collapse" id="nav-content">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item dropdown">
                                            <a href="#" class="nav-link dropdown-toggle current" data-toggle="dropdown">home</a>
                                            <ul class="dropdown-menu">
                                                <li class="dropdown-item dropdown">
                                                    <a href="index-2.html">home 1</a>
                                                </li>
                                                <li class="dropdown-item dropdown">
                                                    <a href="index-3.html">home 2</a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="about.html">about us</a>
                                        </li>

                                        <li class="nav-item dropdown">
                                            <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown">listing</a>
                                            <ul class="dropdown-menu">
                                                <li class="dropdown-item">
                                                    <a href="listing.html">all listings</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="listing-sidebar.html">listing with sidebar</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="listing-with-map.html">listing with map</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="listing-detail.html">listing detail</a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-item dropdown">
                                            <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown">blogs</a>
                                            <ul class="dropdown-menu">
                                                <li class="dropdown-item">
                                                    <a href="blog-page.html">blog page</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="blog-right-sidebar.html">blog rightbar</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="blog-left-sidebar.html">blog leftbar</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="blog-detail.html">blog detail</a>
                                                </li>
                                                <li class="dropdown-item dropdown">
                                                    <a class="dropdown-toggle" href="javascript:void(0)">level 1</a>
                                                    <ul class="dropdown-menu">
                                                        <li class="dropdown-item">
                                                            <a href="javascript:void(0)">level 2</a>
                                                        </li>
                                                        <li class="dropdown-item dropdown">
                                                            <a href="javascript:void(0)" class="dropdown-toggle">level 2</a>
                                                            <ul class="dropdown-menu">
                                                                <li class="dropdown-item">
                                                                    <a href="javascript:void(0)">level 3</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-item dropdown has-mega">
                                            <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                pages
                                            </a>
                                            <ul class="dropdown-menu mega-dropdown">
                                                <li>
                                                    <ul>
                                                        <li class="dropdown-item">
                                                            <a href="about.html">about us</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="faq.html">faq</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="service.html">service</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="pricing.html">pricing</a>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li>
                                                    <ul>
                                                        <li class="dropdown-item">
                                                            <a href="error-page.html">error page</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="add-list.html">add list</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="contact.html">contact us</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="login.html">login / signup</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <ul>
                                                        <li class="dropdown-item">
                                                            <a href="listing.html">all listings</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="listing-sidebar.html">listing with sidebar</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="listing-with-map.html">listing with map</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="listing-detail.html">listing detail</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <ul>
                                                        <li class="dropdown-item">
                                                            <a href="blog-page.html">blog page</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="blog-right-sidebar.html">blog rightbar</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="blog-left-sidebar.html">blog leftbar</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="blog-detail.html">blog detail</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="contact.html">contact us</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#search">
                                                <i class="fa fa-search"></i>
                                            </a>
                                        </li>
                                        <li class="nav-item join-nav d-lg-block d-none">
                                            <a class="nav-link" href="login.html">
                                                <i class="fa fa-user c-theme mr-2"></i> join us</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Header end -->

    <!-- Search Form -->
    <div id="search" class="top-search d-flex">
        <span class="close">
            <i class="fa fa-times"></i>
        </span>
        <div class="w-100 text-center mt-4">
            <h3 class="c-white fw-5">search here</h3>
            <form role="search" id="searchform" action="#" method="get" class="search-bar">
                <input value="" name="q" type="search" placeholder="type to search..." class="form-control">
                <button type="submit" class="submit-btn">
                    <i class="fa fa-search"></i>
                </button>
            </form>
        </div>
    </div>

    <!-- page-banner start-->
    <section class="page-banner">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3>listing detail</h3>
                    <ul class="banner-link text-center">
                        <li>
                            <a href="index-2.html">Home</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">listing</a>
                        </li>
                        <li>
                            <span class="active">listing detail</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- page-banner ends-->

    <!-- listing slider start-->
    <div class="bg-w sp-100">
        <div class="container-fluid">
            <div class="listing-detail-slider owl-carousel owl-theme px-4">
                <div class="list-slide">
                    <img src="assets/img/list/list-slide1.jpg" alt="listing">
                </div>
                <div class="list-slide">
                    <img src="assets/img/list/list-slide2.jpg" alt="listing">
                </div>
                <div class="list-slide">
                    <img src="assets/img/list/list-slide3.jpg" alt="listing">
                </div>
                <div class="list-slide">
                    <img src="assets/img/list/list-slide4.jpg" alt="listing">
                </div>
                <div class="list-slide">
                    <img src="assets/img/list/list-slide1.jpg" alt="listing">
                </div>
                <div class="list-slide">
                    <img src="assets/img/list/list-slide2.jpg" alt="listing">
                </div>
                <div class="list-slide">
                    <img src="assets/img/list/list-slide3.jpg" alt="listing">
                </div>
                <div class="list-slide">
                    <img src="assets/img/list/list-slide4.jpg" alt="listing">
                </div>
            </div>
        </div>
    </div>
    <!-- listing slider end -->

    <!-- listing detail start-->
    <div class="bg-w sp-100 pt-0">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-12">
                    <div class="listing-detail mb-60">
                        <h3 class="mb-30">grand heritage hotel</h3>
                        <div class="abt-listing">
                            <ul class="ctg-info centering justify-content-start">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>345 Canal St, New York, USA</a>
                                </li>
                                <li class="my-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i> 0123-456-789</a>
                                </li>
                                
                                <li class="my-1">
                                    <span> open now</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="mb-60">
                        <h4 class="title-sep3 mb-30">
                            description
                        </h4>
                        <p>Officia irure in pariatur deserunt proident Lorem. Non ullamco ipsum adipisicing nisi do. Excepteur
                            ea amet ex dolor nisi laborum non anim non velit aute culpa nisi. Officia nostrud enim aliquip
                            ea ad occaecat. Irure magna consectetur eiusmod eu officia voluptate duis pariatur quis et. Incididunt
                            in deserunt duis adipisicing commodo sit in voluptate.</p>
                        <p>Velit cupidatat voluptate eu do ullamco est incididunt deserunt. Duis ut aliquip quis nostrud laboris
                            labore irure fugiat non ea pariatur. Nostrud culpa Lorem nulla excepteur et ut eu amet qui consequat
                            adipisicing nulla tesquwdfe eu, pr etium quis, sem.</p>
                    </div>
                    <div class="mb-60">
                        <h4 class="title-sep3 mb-30">
                            facilities
                        </h4>
                        <div class="row minus-pad">
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id6" value="value">
                                    <label for="checkbox_id6">Card Payment</label>
                                </div>
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id7" value="value">
                                    <label for="checkbox_id7">Free Parking</label>
                                </div>
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id8" value="value">
                                    <label for="checkbox_id8">Free Wi-Fi</label>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id9" value="value">
                                    <label for="checkbox_id9">Family Friendly</label>
                                </div>
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id10" value="value">
                                    <label for="checkbox_id10">Wheelchair</label>
                                </div>
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id11" value="value">
                                    <label for="checkbox_id11">Air Conditioning</label>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id12" value="value">
                                    <label for="checkbox_id12">Fitness Center</label>
                                </div>
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id13" value="value">
                                    <label for="checkbox_id13">Reservations</label>
                                </div>
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id14" value="value">
                                    <label for="checkbox_id14">Smoking Allowed</label>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id15" value="value">
                                    <label for="checkbox_id15">Swimming Pool</label>
                                </div>
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id16" value="value">
                                    <label for="checkbox_id16">Coupons</label>
                                </div>
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id17" value="value">
                                    <label for="checkbox_id17">Pet Friendly</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-30">
                        <h4 class="title-sep3 mb-30">
                            available rooms
                        </h4>
                        <div class="row">
                            <div class="col-12">
                                <div class="general-listing">
                                    <div class="img-list">
                                        <div class="image-holder">
                                            <img src="assets/img/home/pl-slide1.jpg" alt="listing">
                                        </div>
                                    </div>
                                    <div class="listing-content">
                                        <h5>standard single room</h5>
                                        <div class="centering justify-content-start">
                                            <p class="mr-4">Price Range :
                                                <span class="c-theme">60$ - 80$</span>
                                            </p>
                                            <p>Max Person :
                                                <span class="c-theme">3</span>
                                            </p>
                                        </div>
                                        <p> Mollit ullamco laborum laborum Lorem magna ipsum duis exercitation proident officia.</p>
                                        <div class="row minus-pad">
                                            <div class="col-xl-4 col-sm-6 col-12">
                                                <div class="aminities">
                                                    <i class="flaticon-bed-1"></i>
                                                    <span>single bad</span>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-sm-6 col-12">
                                                <div class="aminities">
                                                    <i class="flaticon-computer-screen"></i>
                                                    <span>Tv Inside</span>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-sm-6 col-12">
                                                <div class="aminities">
                                                    <i class="flaticon-shower"></i>
                                                    <span>Shower</span>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-sm-6 col-12">
                                                <div class="aminities">
                                                    <i class="flaticon-air-conditioner"></i>
                                                    <span>Air Condition</span>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-sm-6 col-12">
                                                <div class="aminities">
                                                    <i class="flaticon-wifi"></i>
                                                    <span>Free Wifi</span>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-sm-6 col-12">
                                                <div class="aminities">
                                                    <i class="flaticon-meal"></i>
                                                    <span>Breakfast</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="general-listing">
                                    <div class="img-list">
                                        <div class="image-holder">
                                            <img src="assets/img/home/pl-slide2.jpg" alt="listing">
                                        </div>
                                    </div>
                                    <div class="listing-content">
                                        <h5>Deluxe Double Room</h5>
                                        <div class="centering justify-content-start">
                                            <p class="mr-4">Price Range :
                                                <span class="c-theme">70$ - 110$</span>
                                            </p>
                                            <p>Max Person :
                                                <span class="c-theme">6</span>
                                            </p>
                                        </div>
                                        <p> Mollit ullamco laborum laborum Lorem magna ipsum duis exercitation proident officia.</p>
                                        <div class="row minus-pad">
                                            <div class="col-xl-4 col-sm-6 col-12">
                                                <div class="aminities">
                                                    <i class="flaticon-bed-1"></i>
                                                    <span>double bad</span>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-sm-6 col-12">
                                                <div class="aminities">
                                                    <i class="flaticon-computer-screen"></i>
                                                    <span>Tv Inside</span>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-sm-6 col-12">
                                                <div class="aminities">
                                                    <i class="flaticon-shower"></i>
                                                    <span>Shower</span>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-sm-6 col-12">
                                                <div class="aminities">
                                                    <i class="flaticon-air-conditioner"></i>
                                                    <span>Air Condition</span>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-sm-6 col-12">
                                                <div class="aminities">
                                                    <i class="flaticon-wifi"></i>
                                                    <span>Free Wifi</span>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-sm-6 col-12">
                                                <div class="aminities">
                                                    <i class="flaticon-meal"></i>
                                                    <span>Breakfast</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="general-listing">
                                    <div class="img-list">
                                        <div class="image-holder">
                                            <img src="assets/img/home/pl-slide3.jpg" alt="listing">
                                        </div>
                                    </div>
                                    <div class="listing-content">
                                        <h5>Superior Family Room</h5>
                                        <div class="centering justify-content-start">
                                            <p class="mr-4">Price Range :
                                                <span class="c-theme">100$ - 150$</span>
                                            </p>
                                            <p>Max Person :
                                                <span class="c-theme">9</span>
                                            </p>
                                        </div>
                                        <p> Mollit ullamco laborum laborum Lorem magna ipsum duis exercitation proident officia.</p>
                                        <div class="row minus-pad">
                                            <div class="col-xl-4 col-sm-6 col-12">
                                                <div class="aminities">
                                                    <i class="flaticon-bed"></i>
                                                    <span>double Bed</span>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-sm-6 col-12">
                                                <div class="aminities">
                                                    <i class="flaticon-computer-screen"></i>
                                                    <span>Tv Inside</span>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-sm-6 col-12">
                                                <div class="aminities">
                                                    <i class="flaticon-bathtub"></i>
                                                    <span>bathtub</span>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-sm-6 col-12">
                                                <div class="aminities">
                                                    <i class="flaticon-air-conditioner"></i>
                                                    <span>Air Condition</span>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-sm-6 col-12">
                                                <div class="aminities">
                                                    <i class="flaticon-wifi"></i>
                                                    <span>Free Wifi</span>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-sm-6 col-12">
                                                <div class="aminities">
                                                    <i class="flaticon-meal"></i>
                                                    <span>Breakfast</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-30">
                        <h4 class="title-sep3 mb-30">
                            Statistic
                        </h4>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-12">
                                <div class="stat-box">
                                    <i class="fa fa-eye"></i>
                                    <h5>540 Views</h5>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-12">
                                <div class="stat-box">
                                    <i class="fa fa-star"></i>
                                    <h5>5 ratings</h5>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-12">
                                <div class="stat-box">
                                    <i class="fa fa-share-alt"></i>
                                    <h5>10 shares</h5>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-12">
                                <div class="stat-box">
                                    <i class="fa fa-heart"></i>
                                    <h5>7 favourites</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-30">
                        <h4 class="title-sep3 mb-30">
                            tags
                        </h4>
                        <ul class="tagcloud minus-pad">
                            <li>
                                <a href="#">Restaurants</a>
                            </li>
                            <li>
                                <a href="#">Events</a>
                            </li>
                            <li>
                                <a href="#">Tourism</a>
                            </li>
                            <li>
                                <a href="#">Trending</a>
                            </li>
                            <li>
                                <a href="#">Travel</a>
                            </li>
                        </ul>

                        <div class="row mt-30">
                            <div class="col-lg-3 col-md-4 mb-30">
                                <div class="rating-big-box mb-3">
                                    <h2>4.5</h2>
                                    <h6>superb</h6>
                                </div>
                                <div class="rating-btn-down">
                                    <a href="#" class="btn w-100 review-btn btn-anim">3 Reviews</a>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-8 mb-30">
                                <div class="rating-bar-box">
                                    <h6>
                                        <span>rating</span>
                                        <span>3.0</span>
                                    </h6>
                                    <div class="rating-bars">
                                        <span class="bar-fill" style="width:60%"></span>
                                    </div>
                                </div>
                                <div class="rating-bar-box">
                                    <h6>
                                        <span>facilities</span>
                                        <span>4</span>
                                    </h6>
                                    <div class="rating-bars">
                                        <span class="bar-fill" style="width:80%"></span>
                                    </div>
                                </div>
                                <div class="rating-bar-box">
                                    <h6>
                                        <span>staff</span>
                                        <span>4.5</span>
                                    </h6>
                                    <div class="rating-bars">
                                        <span class="bar-fill" style="width:90%"></span>
                                    </div>
                                </div>
                                <div class="rating-bar-box">
                                    <h6>
                                        <span>price</span>
                                        <span>3.5</span>
                                    </h6>
                                    <div class="rating-bars">
                                        <span class="bar-fill" style="width:70%"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-30">
                        <h4 class="title-sep3 mb-30">
                            3 Reviews
                        </h4>
                        <div class="row">
                            <div class="col-12">
                                <div class="review-box mb-30">
                                    <div class="image-holder">
                                        <img src="assets/img/blog/com-1.jpg" alt="author">
                                    </div>
                                    <div class="review-content">
                                        <div class="centering justify-content-between mb-3">
                                            <div>
                                                <h5 class="mb-0">Tom Perrins</h5>
                                                <p class="c-theme mb-0">26 april 2019</p>
                                            </div>
                                            <div class="d-flex flex-wrap">
                                                <div class="rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-alt"></i>
                                                </div>
                                                <div class="rate-bg ml-4">
                                                    4.5
                                                </div>
                                            </div>
                                        </div>
                                        <p class="mb-0">Mollit aute dolore nisi sint tempor veniam ut magna aute. Et officia elit eu eu adipisicing
                                            consectetur cillum elit eiusmod dolore est culpa..</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="review-box mb-30">
                                    <div class="image-holder">
                                        <img src="assets/img/blog/com-2.jpg" alt="author">
                                    </div>
                                    <div class="review-content">
                                        <div class="centering justify-content-between mb-3">
                                            <div>
                                                <h5 class="mb-0">Kathy Brown</h5>
                                                <p class="c-theme mb-0">30 april 2019</p>
                                            </div>
                                            <div class="d-flex flex-wrap">
                                                <div class="rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-alt"></i>
                                                    <i class="far fa-star"></i>
                                                </div>
                                                <div class="rate-bg ml-4">
                                                    3.5
                                                </div>
                                            </div>
                                        </div>
                                        <p class="mb-0">Mollit aute dolore nisi sint tempor veniam ut magna aute. Et officia elit eu eu adipisicing
                                            consectetur cillum elit eiusmod dolore est culpa..</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="review-box mb-30">
                                    <div class="image-holder">
                                        <img src="assets/img/blog/com-3.jpg" alt="author">
                                    </div>
                                    <div class="review-content">
                                        <div class="centering justify-content-between mb-3">
                                            <div>
                                                <h5 class="mb-0">William Dixion</h5>
                                                <p class="c-theme mb-0">28 april 2019</p>
                                            </div>
                                            <div class="d-flex flex-wrap">
                                                <div class="rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="far fa-star"></i>
                                                    <i class="far fa-star"></i>
                                                </div>
                                                <div class="rate-bg ml-4">
                                                    3.0
                                                </div>
                                            </div>
                                        </div>
                                        <p class="mb-0">Mollit aute dolore nisi sint tempor veniam ut magna aute. Et officia elit eu eu adipisicing
                                            consectetur cillum elit eiusmod dolore est culpa..</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-0">
                        <h4 class="title-sep3 mb-30">
                            add review
                        </h4>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-12">
                                <div class="add-review">
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </div>
                                    <h5>Rating*</h5>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-12">
                                <div class="add-review">
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </div>
                                    <h5>Facilities*</h5>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-12">
                                <div class="add-review">
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </div>
                                    <h5>Staff*</h5>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-12">
                                <div class="add-review">
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </div>
                                    <h5>Price*</h5>
                                </div>
                            </div>
                        </div>
                        <form class="comment-form" id="commentform" method="post" action="#">
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <span class="fa fa-user"></span>
                                    <input type="text" id="name" class="form-control" placeholder="Enter Name">
                                </div>
                                <div class="col-md-6 col-12">
                                    <span class="fa fa-envelope"></span>
                                    <input type="email" class="form-control" placeholder="Enter Email" name="Ented email" id="email">
                                </div>
                                <div class="col-12">
                                    <textarea rows="5" name="comment" class="form-control" placeholder="Your Message" id="comment"></textarea>
                                </div>
                                <div class="col-12">
                                    <button type="submit" class="btn btn-one btn-anim" id="submit" name="submit">
                                        <i class="fa fa-paper-plane"></i> submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-4 col-12">
                    <aside class="sidebar">
                        <div class="widget">
                            <h4 class="title-sep2 mb-30">booking</h4>
                            <div class="booking-form">
                                <div class="row">
                                    <div class="col-12">
                                        <label>room type</label>
                                        <select class="form-control custom-select" id="room">
                                            <option>select room</option>
                                            <option>standard room</option>
                                            <option>Deluxe room</option>
                                            <option>suite room</option>
                                            <option>Superior Family Room</option>
                                        </select>
                                    </div>
                                    <div class="col-12">
                                        <label>when to book</label>
                                        <input class="form-control custom-select" type="date" name="dateinout">
                                    </div>
                                    <div class="col-6">
                                        <label>adults</label>
                                        <div class="add-person form-control px-1">
                                            <div class="inc-dec-contain centering">
                                                <div class="inc-dec-control">
                                                    <button class="cart-qty-plus" type="button" value="+">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                                <input type="text" name="qty" maxlength="12" value="0" class="input-text qty" />
                                                <div class="inc-dec-control">
                                                    <button class="cart-qty-minus" type="button" value="-">
                                                        <i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label>children</label>
                                        <div class="add-person form-control px-1">
                                            <div class="inc-dec-contain centering">
                                                <div class="inc-dec-control">
                                                    <button class="cart-qty-plus" type="button" value="+">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                                <input type="text" name="qty" maxlength="12" value="0" class="input-text qty" />
                                                <div class="inc-dec-control">
                                                    <button class="cart-qty-minus" type="button" value="-">
                                                        <i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 mb-30">
                                        <div class="centering total-cost justify-content-between">
                                            <p>total cost</p>
                                            <span class="c-theme">0.00$</span>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-one btn-anim w-100" id="submit2" name="submit2">
                                            <i class="fa fa-paper-plane"></i> submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="widget">
                            <h4 class="title-sep2 mb-30">business info</h4>
                            <ul class="contact-info mt-4">
                                <li>
                                    <i class="fa fa-phone"></i>
                                    123-456-7890, 0123-456-789
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    Support@company.com
                                </li>
                                <li>
                                    <i class="fa fa-map-marker-alt"></i>
                                    Consectetur 241, sed Ac-1252, UK
                                </li>
                                <li>
                                    <i class="fa fa-globe-asia"></i>
                                    www.websiteaname.com
                                </li>
                            </ul>
                            <div class="socials sidebar-socials mt-3 mb-30">
                                <div class="side-icon">
                                    <i class="fa fa-share-alt"></i>
                                </div>
                                <div>
                                    <a href="#">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-google-plus-g"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-vine"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="map map-sidebar">
                                <div id="theme-map"></div>
                            </div>
                        </div>
                        <div class="widget">
                            <h4 class="title-sep2 mb-30">Opening Hours</h4>
                            <div class="centering opening-hours justify-content-between">
                                <p>Opening Hours :</p>
                                <span class="c-theme">Open 24/7</span>
                            </div>
                        </div>
                        <div class="widget">
                            <h4 class="title-sep2 mb-30">Recently Added</h4>
                            <div class="sidebar-listing-slider owl-carousel owl-theme">
                                <div class="listing-item p-2">
                                    <div class="img-holder">
                                        <span class="offer">save 49%
                                        </span>
                                        <img src="assets/img/home/pl-slide1.jpg" alt="list">
                                        <div class="rate-like centering justify-content-between">
                                            <div class="rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="far fa-star"></i>
                                                <i class="far fa-star"></i>
                                            </div>
                                            <a class="likes" href="#">
                                                <i class="far fa-heart"></i> 41
                                            </a>
                                        </div>
                                    </div>
                                    <div class="list-content p-2">
                                        <h5 class="mt-3 mb-2">
                                            <a href="#">the lounge &amp; bar</a>
                                        </h5>
                                        <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                            <li class="mt-1">
                                                <a href="#">
                                                    <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                            </li>
                                            <li class="mt-1">
                                                <a href="#">
                                                    <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="listing-item p-2">
                                    <div class="img-holder">
                                        <span class="offer">save 30%
                                        </span>
                                        <img src="assets/img/home/pl-slide2.jpg" alt="list">
                                        <div class="rate-like centering justify-content-between">
                                            <div class="rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="far fa-star"></i>
                                                <i class="far fa-star"></i>
                                                <i class="far fa-star"></i>
                                            </div>
                                            <a class="likes" href="#">
                                                <i class="far fa-heart"></i> 22
                                            </a>
                                        </div>
                                    </div>
                                    <div class="list-content p-2">
                                        <h5 class="mt-3 mb-2">
                                            <a href="#">the best shop in city</a>
                                        </h5>
                                        <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                            <li class="mt-1">
                                                <a href="#">
                                                    <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                            </li>
                                            <li class="mt-1">
                                                <a href="#">
                                                    <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="listing-item p-2">
                                    <div class="img-holder">
                                        <span class="offer">save 19%
                                        </span>
                                        <img src="assets/img/home/pl-slide3.jpg" alt="list">
                                        <div class="rate-like centering justify-content-between">
                                            <div class="rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="far fa-star"></i>
                                            </div>
                                            <a class="likes" href="#">
                                                <i class="far fa-heart"></i> 06
                                            </a>
                                        </div>
                                    </div>
                                    <div class="list-content p-2">
                                        <h5 class="mt-3 mb-2">
                                            <a href="#">enjoy best nightlife</a>
                                        </h5>
                                        <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                            <li class="mt-1">
                                                <a href="#">
                                                    <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                            </li>
                                            <li class="mt-1">
                                                <a href="#">
                                                    <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="widget">
                            <h4 class="title-sep2 mb-30">about author</h4>
                            <div class="widget-author">
                                <div class="author-top text-right mb-4">
                                    <div class="img-holder">
                                        <img src="assets/img/blog/aut-big.png" alt="#">
                                    </div>
                                    <div class="author-info">
                                        <h5 class="c-white">Julia Holmes</h5>
                                        <span>co manager</span>
                                    </div>
                                </div>
                                <p class="bor-b pb-3">Excepteur enim aute dolor culpa aliqua nulla. Non labore est irure ipsum cupidatat amet fugiat
                                    quis voluptate cillum velit qui.</p>
                                <div class="socials">
                                    <a href="#">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-google-plus-g"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
    <!-- listing detail end -->

    <!-- cta-one start-->
    <section class="cta-one tri-bg-w text-lg-left text-center">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 my-lg-0 my-5 py-lg-0 py-5">
                    <div class="cta-content">
                        <h3>Sign Up To Get Special Offers Every Day</h3>
                        <p>Lorem ipsum dolor sit amet, consectadetudzdae rcquisc adipiscing elit. Aenean socada commodo ligaui
                            egets dolor. </p>
                        <a href="login.html" class="btn btn-two btn-anim mt-2">
                            sign up
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 d-lg-block d-none">
                    <div class="cta-img mt-4">
                        <img src="assets/img/home/cta-bg.png" alt="image">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- cta-one end -->

    <!-- footer starts -->
    <footer class="footer footer-one">
        <div class="foot-top">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-lg-12 col-12 mb-60">
                        <div class="company-details">
                            <img src="assets/img/logo.png" class="foot-logo mb-4" alt="lister">
                            <p class="pb-2">Lorem ipsum dolor sit amet, consect tuer adipi cin elit. Aen sociis nato pe na ibus magnis dis
                                partur monte na et ridiculu onec quam felis. castei efte tuerg scn kt isceding elit ende
                                cd magum socadaiis nato ipsf.</p>
                            <div class="socials mt-4">
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-google-plus-g"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                        <div class="recent-post">
                            <div class="foot-title">
                                <h4>useful links</h4>
                            </div>
                            <ul class="quick-link">
                                <li>
                                    <a href="add-list.html">add listing</a>
                                </li>
                                <li>
                                    <a href="register.html">register</a>
                                </li>
                                <li>
                                    <a href="sign-in.html">sign in</a>
                                </li>
                                <li>
                                    <a href="about.html">how it work</a>
                                </li>
                                <li>
                                    <a href="contact.html">contact us</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                        <div class="recent-post">
                            <div class="foot-title">
                                <h4>my account</h4>
                            </div>
                            <ul class="quick-link">
                                <li>
                                    <a href="login.html">create account</a>
                                </li>
                                <li>
                                    <a href="#">dashboard</a>
                                </li>
                                <li>
                                    <a href="#">profile</a>
                                </li>
                                <li>
                                    <a href="#">my listing</a>
                                </li>
                                <li>
                                    <a href="#">favorites</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                        <div class="recent-post">
                            <div class="foot-title">
                                <h4>categories</h4>
                            </div>
                            <ul class="quick-link">
                                <li>
                                    <a href="#">Restaurants</a>
                                </li>
                                <li>
                                    <a href="#">Shopping</a>
                                </li>
                                <li>
                                    <a href="#">Hotels</a>
                                </li>
                                <li>
                                    <a href="#">Destinations</a>
                                </li>
                                <li>
                                    <a href="#">Nightlife</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                        <div class="recent-post">
                            <div class="foot-title">
                                <h4>quick links</h4>
                            </div>
                            <ul class="quick-link">
                                <li>
                                    <a href="about.html">About Us</a>
                                </li>
                                <li>
                                    <a href="price.html">Pricing</a>
                                </li>
                                <li>
                                    <a href="blog-grid.html">Blogs</a>
                                </li>
                                <li>
                                    <a href="#">Testimonials</a>
                                </li>
                                <li>
                                    <a href="#">Our Partners</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="foot-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <p class="text-capitalize">Copyright © 2019, All rights Reserved. Created by
                            <a href="#">Company Name</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer end -->

    <!-- JavaScript Libraries -->
    <script src="assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/isotope.min.js"></script>
    <script src="assets/js/slick.min.js"></script>
    <!-- google map api  -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAAz77U5XQuEME6TpftaMdX0bBelQxXRlM&amp;callback=initMap"></script>

    <script src="assets/js/custom.js"></script>


</body>


<!-- Mirrored from wpshopmart.com/html/demo/dlister/listing-detail.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Jul 2019 04:34:01 GMT -->
</html>