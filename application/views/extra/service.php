<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from wpshopmart.com/html/demo/dlister/service.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Jul 2019 04:34:05 GMT -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- favicon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/img/favicon.png">

    <!--  CSS Libraries -->
    <link rel="stylesheet" href="../assets/css/plugins.css"> 
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="../assets/css/responsive.css">

    <!-- PAGE TITLE -->
    <title>D-Lister | Directory & Listing Responsive HTML5 Template </title>

</head>

<body>

    <!-- ====== Go to top ====== -->
    <a id="toTop" title="Go to top" href="javascript:void(0)">
        <span id="toTopHover"></span>TOP
    </a>

    <!-- Preloader start-->
    <div class="preloader">
        <div class="loader-inner">
            <div class="dash one">
                <i class="fa fa-map-marker-alt"></i>
            </div>
            <div class="dash two">
                <i class="fa fa-map-marker-alt"></i>
            </div>
            <div class="dash three">
                <i class="fa fa-map-marker-alt"></i>
            </div>
        </div>
    </div>
    <!-- Preloader end -->

    <!-- Header start-->
    <header class="header">
        <div class="head-top head-top-one d-none d-lg-block">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <div class="socials socials-header text-lg-left text-center">
                            <a href="#">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-instagram"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <ul class="head-contact text-lg-right text-center">
                            <li>
                                <i class="fa fa-phone"></i>
                                +123-456-7890
                            </li>
                            <li>
                                <i class="fa fa-envelope"></i>
                                info@company.com
                            </li>
                            <li class="language-drop">
                                <div class="dropdown">
                                    <a href="#" class="dropdown-toggle text-capitalize" data-toggle="dropdown">
                                        english
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#">hindi</a>
                                        <a class="dropdown-item" href="#">urdu</a>
                                        <a class="dropdown-item" href="#">english</a>
                                    </div>
                                </div>
                            </li>
                            <li class="ad-list">
                                <a href="add-list.html" class="btn btn-two btn-anim">
                                    <i class="fa fa-plus-circle"></i> add listing
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="theme-header-one affix">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-12">
                        <div class="logo-one logo-wrap">
                            <div class="logo my-1">
                                <a href="index-2.html">
                                    <img src="../assets/img/logo.png" alt="logo">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-9 col-md-12">
                        <div class="menu menu-one">
                            <nav class="navbar navbar-expand-lg">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-content" aria-controls="nav-content"
                                    aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon bar1"></span>
                                    <span class="navbar-toggler-icon bar2"></span>
                                    <span class="navbar-toggler-icon bar3"></span>
                                </button>
                                <!-- Links -->
                                <div class="main-menu collapse navbar-collapse" id="nav-content">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item dropdown">
                                            <a href="#" class="nav-link dropdown-toggle current" data-toggle="dropdown">home</a>
                                            <ul class="dropdown-menu">
                                                <li class="dropdown-item dropdown">
                                                    <a href="index-2.html">home 1</a>
                                                </li>
                                                <li class="dropdown-item dropdown">
                                                    <a href="index-3.html">home 2</a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="about.html">about us</a>
                                        </li>

                                        <li class="nav-item dropdown">
                                            <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown">listing</a>
                                            <ul class="dropdown-menu">
                                                <li class="dropdown-item">
                                                    <a href="listing.html">all listings</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="listing-sidebar.html">listing with sidebar</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="listing-with-map.html">listing with map</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="listing-detail.html">listing detail</a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-item dropdown">
                                            <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown">blogs</a>
                                            <ul class="dropdown-menu">
                                                <li class="dropdown-item">
                                                    <a href="blog-page.html">blog page</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="blog-right-sidebar.html">blog rightbar</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="blog-left-sidebar.html">blog leftbar</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="blog-detail.html">blog detail</a>
                                                </li>
                                                <li class="dropdown-item dropdown">
                                                    <a class="dropdown-toggle" href="javascript:void(0)">level 1</a>
                                                    <ul class="dropdown-menu">
                                                        <li class="dropdown-item">
                                                            <a href="javascript:void(0)">level 2</a>
                                                        </li>
                                                        <li class="dropdown-item dropdown">
                                                            <a href="javascript:void(0)" class="dropdown-toggle">level 2</a>
                                                            <ul class="dropdown-menu">
                                                                <li class="dropdown-item">
                                                                    <a href="javascript:void(0)">level 3</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-item dropdown has-mega">
                                            <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                pages
                                            </a>
                                            <ul class="dropdown-menu mega-dropdown">
                                                <li>
                                                    <ul>
                                                        <li class="dropdown-item">
                                                            <a href="about.html">about us</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="faq.html">faq</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="service.html">service</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="pricing.html">pricing</a>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li>
                                                    <ul>
                                                        <li class="dropdown-item">
                                                            <a href="error-page.html">error page</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="add-list.html">add list</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="contact.html">contact us</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="login.html">login / signup</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <ul>
                                                        <li class="dropdown-item">
                                                            <a href="listing.html">all listings</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="listing-sidebar.html">listing with sidebar</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="listing-with-map.html">listing with map</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="listing-detail.html">listing detail</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <ul>
                                                        <li class="dropdown-item">
                                                            <a href="blog-page.html">blog page</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="blog-right-sidebar.html">blog rightbar</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="blog-left-sidebar.html">blog leftbar</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="blog-detail.html">blog detail</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="contact.html">contact us</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#search">
                                                <i class="fa fa-search"></i>
                                            </a>
                                        </li>
                                        <li class="nav-item join-nav d-lg-block d-none">
                                            <a class="nav-link" href="login.html">
                                                <i class="fa fa-user c-theme mr-2"></i> join us</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Header end -->

    <!-- Search Form -->
    <div id="search" class="top-search d-flex">
        <span class="close">
            <i class="fa fa-times"></i>
        </span>
        <div class="w-100 text-center mt-4">
            <h3 class="c-white fw-5">search here</h3>
            <form role="search" id="searchform" action="#" method="get" class="search-bar">
                <input value="" name="q" type="search" placeholder="type to search..." class="form-control">
                <button type="submit" class="submit-btn">
                    <i class="fa fa-search"></i>
                </button>
            </form>
        </div>
    </div>

    <!-- page-banner start-->
    <section class="page-banner">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3>services</h3>
                    <ul class="banner-link text-center">
                        <li>
                            <a href="index-2.html">Home</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">pages</a>
                        </li>
                        <li>
                            <span class="active">services</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- page-banner ends-->

    <!-- services start-->
    <section class="bg-dull sp-100-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="feature-item">
                        <div class="icon-box">
                            <i class="flaticon-rising"></i>
                        </div>
                        <h5>our vision</h5>
                        <p>Lorem ipsum dolor sit ame conse castei efte tuerg scn kt iscedring elit ende cd magum socadaiis nato
                            ipsf.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="feature-item">
                        <div class="icon-box">
                            <i class="flaticon-list"></i>
                        </div>
                        <h5>missions</h5>
                        <p>Lorem ipsum dolor sit ame conse castei efte tuerg scn kt iscedring elit ende cd magum socadaiis nato
                            ipsf.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="feature-item">
                        <div class="icon-box">
                            <i class="flaticon-medal"></i>
                        </div>
                        <h5>our value</h5>
                        <p>Lorem ipsum dolor sit ame conse castei efte tuerg scn kt iscedring elit ende cd magum socadaiis nato
                            ipsf.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="feature-item">
                        <div class="icon-box">
                            <i class="flaticon-find-1"></i>
                        </div>
                        <h5>find your listing</h5>
                        <p>Lorem ipsum dolor sit ame conse castei efte tuerg scn kt iscedring elit ende cd magum socadaiis nato
                            ipsf.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="feature-item">
                        <div class="icon-box">
                            <i class="flaticon-map"></i>
                        </div>
                        <h5>find the way</h5>
                        <p>Lorem ipsum dolor sit ame conse castei efte tuerg scn kt iscedring elit ende cd magum socadaiis nato
                            ipsf.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="feature-item">
                        <div class="icon-box">
                            <i class="flaticon-support"></i>
                        </div>
                        <h5>24/7 support</h5>
                        <p>Lorem ipsum dolor sit ame conse castei efte tuerg scn kt iscedring elit ende cd magum socadaiis nato
                            ipsf.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- services end -->

    <!-- features start-->
    <section class="features sp-100-70 bg-w">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="all-title">
                        <h3 class="sec-title">
                            Why We Are Best
                        </h3>
                        <svg class="title-sep">
                            <path fill-rule="evenodd" d="M32.000,13.000 L32.000,7.000 L35.000,7.000 L35.000,13.000 L32.000,13.000 ZM24.000,4.000 L27.000,4.000 L27.000,16.000 L24.000,16.000 L24.000,4.000 ZM16.000,-0.000 L19.000,-0.000 L19.000,20.000 L16.000,20.000 L16.000,-0.000 ZM8.000,4.000 L11.000,4.000 L11.000,16.000 L8.000,16.000 L8.000,4.000 ZM-0.000,7.000 L3.000,7.000 L3.000,13.000 L-0.000,13.000 L-0.000,7.000 Z"
                            />
                        </svg>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor penatibus
                            ean massa. Cum sociis natoqu penatibus et magnis dis parturient montes.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="feature-item type-2">
                        <div class="icon-box">
                            <i class="flaticon-rising"></i>
                        </div>
                        <h5>our vision</h5>
                        <p>Lorem ipsum dolor sit ame conse castei efte tuerg scn kt iscedring elit ende cd magum socadaiis nato
                            ipsf.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="feature-item type-2">
                        <div class="icon-box">
                            <i class="flaticon-list"></i>
                        </div>
                        <h5>missions</h5>
                        <p>Lorem ipsum dolor sit ame conse castei efte tuerg scn kt iscedring elit ende cd magum socadaiis nato
                            ipsf.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="feature-item type-2">
                        <div class="icon-box">
                            <i class="flaticon-medal"></i>
                        </div>
                        <h5>our value</h5>
                        <p>Lorem ipsum dolor sit ame conse castei efte tuerg scn kt iscedring elit ende cd magum socadaiis nato
                            ipsf.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="feature-item type-2">
                        <div class="icon-box">
                            <i class="flaticon-find-1"></i>
                        </div>
                        <h5>find your listing</h5>
                        <p>Lorem ipsum dolor sit ame conse castei efte tuerg scn kt iscedring elit ende cd magum socadaiis nato
                            ipsf.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="feature-item type-2">
                        <div class="icon-box">
                            <i class="flaticon-map"></i>
                        </div>
                        <h5>find the way</h5>
                        <p>Lorem ipsum dolor sit ame conse castei efte tuerg scn kt iscedring elit ende cd magum socadaiis nato
                            ipsf.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="feature-item type-2">
                        <div class="icon-box">
                            <i class="flaticon-support"></i>
                        </div>
                        <h5>24/7 support</h5>
                        <p>Lorem ipsum dolor sit ame conse castei efte tuerg scn kt iscedring elit ende cd magum socadaiis nato
                            ipsf.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- features end -->

    <!-- testimonial start-->
    <section class="testimonial sp-100 bg-dull">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="all-title">
                        <h3 class="sec-title">
                            Testimonials
                        </h3>
                        <svg class="title-sep">
                            <path fill-rule="evenodd" d="M32.000,13.000 L32.000,7.000 L35.000,7.000 L35.000,13.000 L32.000,13.000 ZM24.000,4.000 L27.000,4.000 L27.000,16.000 L24.000,16.000 L24.000,4.000 ZM16.000,-0.000 L19.000,-0.000 L19.000,20.000 L16.000,20.000 L16.000,-0.000 ZM8.000,4.000 L11.000,4.000 L11.000,16.000 L8.000,16.000 L8.000,4.000 ZM-0.000,7.000 L3.000,7.000 L3.000,13.000 L-0.000,13.000 L-0.000,7.000 Z"></path>
                        </svg>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor penatibus
                            ean massa. Cum sociis natoqu penatibus et magnis dis parturient montes.</p>
                    </div>
                </div>
            </div>
            <div class="testi-slider1 owl-carousel owl-theme">
                <div class="slide-item">
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="../assets/img/home/testi1.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">jessica thomas</h5>
                            <span class="c-theme mb-3 d-block">Manager at Travel Agency</span>
                            <p class="mb-0">Lorem ipsum donec pede justo, fringilla vel, aliqu et nectei vulp utate eget fg arcu. In enim
                                justo, rhcusut imp erdiet venenatis vitae.</p>
                        </div>
                    </div>
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="../assets/img/home/testi2.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">Paul Anderson</h5>
                            <span class="c-theme mb-3 d-block">Web Designer</span>
                            <p class="mb-0">Lorem ipsum donec pede justo, fringilla vel, aliqu et nectei vulp utate eget fg arcu. In enim
                                justo, rhcusut imp erdiet venenatis vitae.</p>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="../assets/img/home/testi3.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">Harison Doe</h5>
                            <span class="c-theme mb-3 d-block">Happy Customer</span>
                            <p class="mb-0">Lorem ipsum donec pede justo, fringilla vel, aliqu et nectei vulp utate eget fg arcu. In enim
                                justo, rhcusut imp erdiet venenatis vitae.</p>
                        </div>
                    </div>
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="../assets/img/home/testi4.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">Ashley Marcov</h5>
                            <span class="c-theme mb-3 d-block">Co-Owner</span>
                            <p class="mb-0">Lorem ipsum donec pede justo, fringilla vel, aliqu et nectei vulp utate eget fg arcu. In enim
                                justo, rhcusut imp erdiet venenatis vitae.</p>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="../assets/img/home/testi1.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">jessica thomas</h5>
                            <span class="c-theme mb-3 d-block">Manager at Travel Agency</span>
                            <p class="mb-0">Lorem ipsum donec pede justo, fringilla vel, aliqu et nectei vulp utate eget fg arcu. In enim
                                justo, rhcusut imp erdiet venenatis vitae.</p>
                        </div>
                    </div>
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="../assets/img/home/testi2.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">Paul Anderson</h5>
                            <span class="c-theme mb-3 d-block">Web Designer</span>
                            <p class="mb-0">Lorem ipsum donec pede justo, fringilla vel, aliqu et nectei vulp utate eget fg arcu. In enim
                                justo, rhcusut imp erdiet venenatis vitae.</p>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="../assets/img/home/testi3.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">Harison Doe</h5>
                            <span class="c-theme mb-3 d-block">Happy Customer</span>
                            <p class="mb-0">Lorem ipsum donec pede justo, fringilla vel, aliqu et nectei vulp utate eget fg arcu. In enim
                                justo, rhcusut imp erdiet venenatis vitae.</p>
                        </div>
                    </div>
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="../assets/img/home/testi4.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">Ashley Marcov</h5>
                            <span class="c-theme mb-3 d-block">Co-Owner</span>
                            <p class="mb-0">Lorem ipsum donec pede justo, fringilla vel, aliqu et nectei vulp utate eget fg arcu. In enim
                                justo, rhcusut imp erdiet venenatis vitae.</p>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="../assets/img/home/testi1.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">jessica thomas</h5>
                            <span class="c-theme mb-3 d-block">Manager at Travel Agency</span>
                            <p class="mb-0">Lorem ipsum donec pede justo, fringilla vel, aliqu et nectei vulp utate eget fg arcu. In enim
                                justo, rhcusut imp erdiet venenatis vitae.</p>
                        </div>
                    </div>
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="../assets/img/home/testi2.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">Paul Anderson</h5>
                            <span class="c-theme mb-3 d-block">Web Designer</span>
                            <p class="mb-0">Lorem ipsum donec pede justo, fringilla vel, aliqu et nectei vulp utate eget fg arcu. In enim
                                justo, rhcusut imp erdiet venenatis vitae.</p>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="../assets/img/home/testi3.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">Harison Doe</h5>
                            <span class="c-theme mb-3 d-block">Happy Customer</span>
                            <p class="mb-0">Lorem ipsum donec pede justo, fringilla vel, aliqu et nectei vulp utate eget fg arcu. In enim
                                justo, rhcusut imp erdiet venenatis vitae.</p>
                        </div>
                    </div>
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="../assets/img/home/testi4.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">Ashley Marcov</h5>
                            <span class="c-theme mb-3 d-block">Co-Owner</span>
                            <p class="mb-0">Lorem ipsum donec pede justo, fringilla vel, aliqu et nectei vulp utate eget fg arcu. In enim
                                justo, rhcusut imp erdiet venenatis vitae.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- testimonial end -->

    <!-- cta-one start-->
    <section class="cta-one tri-bg-w text-lg-left text-center">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 my-lg-0 my-5 py-lg-0 py-5">
                    <div class="cta-content">
                        <h3>Sign Up To Get Special Offers Every Day</h3>
                        <p>Lorem ipsum dolor sit amet, consectadetudzdae rcquisc adipiscing elit. Aenean socada commodo ligaui
                            egets dolor. </p>
                        <a href="login.html" class="btn btn-two btn-anim mt-2">
                            sign up
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 d-lg-block d-none">
                    <div class="cta-img mt-4">
                        <img src="../assets/img/home/cta-bg.png" alt="image">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- cta-one end -->

    <!-- footer starts -->
    <footer class="footer footer-one">
        <div class="foot-top">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-lg-12 col-12 mb-60">
                        <div class="company-details">
                            <img src="../assets/img/logo.png" class="foot-logo mb-4" alt="lister">
                            <p class="pb-2">Lorem ipsum dolor sit amet, consect tuer adipi cin elit. Aen sociis nato pe na ibus magnis dis
                                partur monte na et ridiculu onec quam felis. castei efte tuerg scn kt isceding elit ende
                                cd magum socadaiis nato ipsf.</p>
                            <div class="socials mt-4">
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-google-plus-g"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                        <div class="recent-post">
                            <div class="foot-title">
                                <h4>useful links</h4>
                            </div>
                            <ul class="quick-link">
                                <li>
                                    <a href="add-list.html">add listing</a>
                                </li>
                                <li>
                                    <a href="register.html">register</a>
                                </li>
                                <li>
                                    <a href="sign-in.html">sign in</a>
                                </li>
                                <li>
                                    <a href="about.html">how it work</a>
                                </li>
                                <li>
                                    <a href="contact.html">contact us</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                        <div class="recent-post">
                            <div class="foot-title">
                                <h4>my account</h4>
                            </div>
                            <ul class="quick-link">
                                <li>
                                    <a href="login.html">create account</a>
                                </li>
                                <li>
                                    <a href="#">dashboard</a>
                                </li>
                                <li>
                                    <a href="#">profile</a>
                                </li>
                                <li>
                                    <a href="#">my listing</a>
                                </li>
                                <li>
                                    <a href="#">favorites</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                        <div class="recent-post">
                            <div class="foot-title">
                                <h4>categories</h4>
                            </div>
                            <ul class="quick-link">
                                <li>
                                    <a href="#">Restaurants</a>
                                </li>
                                <li>
                                    <a href="#">Shopping</a>
                                </li>
                                <li>
                                    <a href="#">Hotels</a>
                                </li>
                                <li>
                                    <a href="#">Destinations</a>
                                </li>
                                <li>
                                    <a href="#">Nightlife</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                        <div class="recent-post">
                            <div class="foot-title">
                                <h4>quick links</h4>
                            </div>
                            <ul class="quick-link">
                                <li>
                                    <a href="about.html">About Us</a>
                                </li>
                                <li>
                                    <a href="price.html">Pricing</a>
                                </li>
                                <li>
                                    <a href="blog-grid.html">Blogs</a>
                                </li>
                                <li>
                                    <a href="#">Testimonials</a>
                                </li>
                                <li>
                                    <a href="#">Our Partners</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="foot-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <p class="text-capitalize">Copyright © 2019, All rights Reserved. Created by
                            <a href="#">Company Name</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer end -->

    <!-- JavaScript Libraries -->
    <script src="../assets/js/jquery-3.3.1.min.js"></script>
    <script src="../assets/js/popper.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/owl.carousel.min.js"></script>
    <script src="../assets/js/isotope.min.js"></script>
    <script src="../assets/js/slick.min.js"></script>
    <script src="../assets/js/custom.js"></script>


</body>


<!-- Mirrored from wpshopmart.com/html/demo/dlister/service.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Jul 2019 04:34:05 GMT -->
</html>