<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from wpshopmart.com/html/demo/dlister/add-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Jul 2019 04:33:53 GMT -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- favicon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon.png">

    <!--  CSS Libraries -->
    <link rel="stylesheet" href="assets/css/plugins.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">

    <!-- PAGE TITLE -->
    <title>D-Lister | Directory & Listing Responsive HTML5 Template </title>

</head>

<body>

    <!-- ====== Go to top ====== -->
    <a id="toTop" title="Go to top" href="javascript:void(0)">
        <span id="toTopHover"></span>TOP
    </a>

    <!-- Preloader start-->
    <div class="preloader">
        <div class="loader-inner">
            <div class="dash one">
                <i class="fa fa-map-marker-alt"></i>
            </div>
            <div class="dash two">
                <i class="fa fa-map-marker-alt"></i>
            </div>
            <div class="dash three">
                <i class="fa fa-map-marker-alt"></i>
            </div>
        </div>
    </div>
    <!-- Preloader end -->

    <!-- Header start-->
    <header class="header">
        <div class="head-top head-top-one d-none d-lg-block">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <div class="socials socials-header text-lg-left text-center">
                            <a href="#">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-instagram"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <ul class="head-contact text-lg-right text-center">
                            <li>
                                <i class="fa fa-phone"></i>
                                +123-456-7890
                            </li>
                            <li>
                                <i class="fa fa-envelope"></i>
                                info@company.com
                            </li>
                            <li class="language-drop">
                                <div class="dropdown">
                                    <a href="#" class="dropdown-toggle text-capitalize" data-toggle="dropdown">
                                        english
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#">hindi</a>
                                        <a class="dropdown-item" href="#">urdu</a>
                                        <a class="dropdown-item" href="#">english</a>
                                    </div>
                                </div>
                            </li>
                            <li class="ad-list">
                                <a href="add-list.html" class="btn btn-two btn-anim">
                                    <i class="fa fa-plus-circle"></i> add listing
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="theme-header-one affix">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-12">
                        <div class="logo-one logo-wrap">
                            <div class="logo my-1">
                                <a href="index-2.html">
                                    <img src="assets/img/logo.png" alt="logo">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-9 col-md-12">
                        <div class="menu menu-one">
                            <nav class="navbar navbar-expand-lg">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-content" aria-controls="nav-content"
                                    aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon bar1"></span>
                                    <span class="navbar-toggler-icon bar2"></span>
                                    <span class="navbar-toggler-icon bar3"></span>
                                </button>
                                <!-- Links -->
                                <div class="main-menu collapse navbar-collapse" id="nav-content">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item dropdown">
                                            <a href="#" class="nav-link dropdown-toggle current" data-toggle="dropdown">home</a>
                                            <ul class="dropdown-menu">
                                                <li class="dropdown-item dropdown">
                                                    <a href="index-2.html">home 1</a>
                                                </li>
                                                <li class="dropdown-item dropdown">
                                                    <a href="index-3.html">home 2</a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="about.html">about us</a>
                                        </li>

                                        <li class="nav-item dropdown">
                                            <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown">listing</a>
                                            <ul class="dropdown-menu">
                                                <li class="dropdown-item">
                                                    <a href="listing.html">all listings</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="listing-sidebar.html">listing with sidebar</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="listing-with-map.html">listing with map</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="listing-detail.html">listing detail</a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-item dropdown">
                                            <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown">blogs</a>
                                            <ul class="dropdown-menu">
                                                <li class="dropdown-item">
                                                    <a href="blog-page.html">blog page</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="blog-right-sidebar.html">blog rightbar</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="blog-left-sidebar.html">blog leftbar</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="blog-detail.html">blog detail</a>
                                                </li>
                                                <li class="dropdown-item dropdown">
                                                    <a class="dropdown-toggle" href="javascript:void(0)">level 1</a>
                                                    <ul class="dropdown-menu">
                                                        <li class="dropdown-item">
                                                            <a href="javascript:void(0)">level 2</a>
                                                        </li>
                                                        <li class="dropdown-item dropdown">
                                                            <a href="javascript:void(0)" class="dropdown-toggle">level 2</a>
                                                            <ul class="dropdown-menu">
                                                                <li class="dropdown-item">
                                                                    <a href="javascript:void(0)">level 3</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-item dropdown has-mega">
                                            <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                pages
                                            </a>
                                            <ul class="dropdown-menu mega-dropdown">
                                                <li>
                                                    <ul>
                                                        <li class="dropdown-item">
                                                            <a href="about.html">about us</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="faq.html">faq</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="service.html">service</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="pricing.html">pricing</a>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li>
                                                    <ul>
                                                        <li class="dropdown-item">
                                                            <a href="error-page.html">error page</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="add-list.html">add list</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="contact.html">contact us</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="login.html">login / signup</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <ul>
                                                        <li class="dropdown-item">
                                                            <a href="listing.html">all listings</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="listing-sidebar.html">listing with sidebar</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="listing-with-map.html">listing with map</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="listing-detail.html">listing detail</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <ul>
                                                        <li class="dropdown-item">
                                                            <a href="blog-page.html">blog page</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="blog-right-sidebar.html">blog rightbar</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="blog-left-sidebar.html">blog leftbar</a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="blog-detail.html">blog detail</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="contact.html">contact us</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#search">
                                                <i class="fa fa-search"></i>
                                            </a>
                                        </li>
                                        <li class="nav-item join-nav d-lg-block d-none">
                                            <a class="nav-link" href="login.html">
                                                <i class="fa fa-user c-theme mr-2"></i> join us</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Header end -->

    <!-- Search Form -->
    <div id="search" class="top-search d-flex">
        <span class="close">
            <i class="fa fa-times"></i>
        </span>
        <div class="w-100 text-center mt-4">
            <h3 class="c-white fw-5">search here</h3>
            <form role="search" id="searchform" action="#" method="get" class="search-bar">
                <input value="" name="q" type="search" placeholder="type to search..." class="form-control">
                <button type="submit" class="submit-btn">
                    <i class="fa fa-search"></i>
                </button>
            </form>
        </div>
    </div>

    <!-- page-banner start-->
    <section class="page-banner">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3>add listing</h3>
                    <ul class="banner-link text-center">
                        <li>
                            <a href="index-2.html">Home</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">listing</a>
                        </li>
                        <li>
                            <span class="active">add listing</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- page-banner ends-->

    <!-- add-list start-->
    <section class="bg-w sp-100">
        <div class="container">
            <div class="row mb-30">
                <div class="col-12">
                    <h4 class="title-sep3 mb-30">
                        basic info
                    </h4>
                </div>
                <div class="col-12">
                    <form class="listing-form" action="#">
                        <div class="row">
                            <div class="col-12">
                                <label>listing title</label>
                                <input type="text" id="name" class="form-control" placeholder="type here">
                            </div>
                            <div class="col-md-6 col-12">
                                <label>category</label>
                                <select class="form-control custom-select" id="categories">
                                    <option>select categories</option>
                                    <option>hotel</option>
                                    <option>tour</option>
                                    <option>pharmacy</option>
                                    <option>shops</option>
                                </select>
                            </div>
                            <div class="col-md-6 col-12">
                                <label>keywords</label>
                                <input type="text" id="keywords" class="form-control" placeholder="Keywords Here">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row mb-30">
                <div class="col-12">
                    <h4 class="title-sep3 mb-30">
                        location
                    </h4>
                </div>
                <div class="col-12">
                    <form class="listing-form" action="#">
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <label>city</label>
                                <select class="form-control custom-select" id="categories2">
                                    <option>select city</option>
                                    <option>hotel</option>
                                    <option>tour</option>
                                    <option>pharmacy</option>
                                    <option>shops</option>
                                </select>
                            </div>
                            <div class="col-md-6 col-12">
                                <label>address</label>
                                <input type="text" id="adress" class="form-control" placeholder="address here">
                            </div>
                            <div class="col-md-6 col-12">
                                <label>state</label>
                                <input type="text" id="state" class="form-control" placeholder="enter Here">
                            </div>
                            <div class="col-md-6 col-12">
                                <label>zip code</label>
                                <input type="text" id="code" class="form-control" placeholder="enter Here">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-12">
                    <div class="map mb-30">
                        <div id="theme-map"></div>
                    </div>
                </div>
            </div>
            <div class="row mb-30">
                <div class="col-12">
                    <h4 class="title-sep3 mb-30">
                        details
                    </h4>
                </div>
                <div class="col-12">
                    <form class="listing-form" action="#">
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <label>city</label>
                                <textarea rows="10" name="comment" class="form-control" placeholder="write here" id="comment"></textarea>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="row">
                                    <div class="col-lg-6 col-12">
                                        <label>phone</label>
                                        <input type="text" id="phn" class="form-control" placeholder="phone no">
                                    </div>
                                    <div class="col-lg-6 col-12">
                                        <label>facebook</label>
                                        <input type="text" id="fb" class="form-control" placeholder="facebook">
                                    </div>
                                    <div class="col-lg-6 col-12">
                                        <label>e mail</label>
                                        <input type="text" id="mail" class="form-control" placeholder="e mail">
                                    </div>
                                    <div class="col-lg-6 col-12">
                                        <label>google +</label>
                                        <input type="text" id="google" class="form-control" placeholder="google +">
                                    </div>
                                    <div class="col-lg-6 col-12">
                                        <label>website</label>
                                        <input type="text" id="website" class="form-control" placeholder="website">
                                    </div>
                                    <div class="col-lg-6 col-12">
                                        <label>twitter</label>
                                        <input type="text" id="twitter" class="form-control" placeholder="twitter">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row mb-30">
                <div class="col-12">
                    <h4 class="title-sep3 mb-30">
                        facilities
                    </h4>
                </div>
                <div class="col-12">
                    <form class="listing-form minus-pad" action="#">
                        <div class="row mb-30">
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                                <input type="checkbox" name="checkbox" id="checkbox_id1" value="value">
                                <label for="checkbox_id1">Card Payment</label>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                                <input type="checkbox" name="checkbox" id="checkbox_id2" value="value">
                                <label for="checkbox_id2">Family Friendly</label>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                                <input type="checkbox" name="checkbox" id="checkbox_id3" value="value">
                                <label for="checkbox_id3">Fitness Center</label>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                                <input type="checkbox" name="checkbox" id="checkbox_id4" value="value">
                                <label for="checkbox_id4">Swimming Pool</label>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                                <input type="checkbox" name="checkbox" id="checkbox_id5" value="value">
                                <label for="checkbox_id5">Free Wi-Fi</label>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                                <input type="checkbox" name="checkbox" id="checkbox_id6" value="value">
                                <label for="checkbox_id6">Free Parking</label>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                                <input type="checkbox" name="checkbox" id="checkbox_id7" value="value">
                                <label for="checkbox_id7">Air Conditioning</label>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                                <input type="checkbox" name="checkbox" id="checkbox_id8" value="value">
                                <label for="checkbox_id8">Wheelchair</label>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                                <input type="checkbox" name="checkbox" id="checkbox_id9" value="value">
                                <label for="checkbox_id9">Reservations</label>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                                <input type="checkbox" name="checkbox" id="checkbox_id10" value="value">
                                <label for="checkbox_id10">Coupons</label>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                                <input type="checkbox" name="checkbox" id="checkbox_id11" value="value">
                                <label for="checkbox_id11">Smoking Allowed</label>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                                <input type="checkbox" name="checkbox" id="checkbox_id12" value="value">
                                <label for="checkbox_id12">Pet Friendly</label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-12 mb-60">
                    <h4 class="title-sep3 mb-30">
                        gallery
                    </h4>
                    <div class="drop-file">
                        <a href="#" class="w-100">
                            <i class="far fa-plus-square"></i>
                            <p class="mb-0">Drop files here to upload</p>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-12 mb-60">
                    <h4 class="title-sep3 mb-30">
                        cover image
                    </h4>
                    <div class="drop-file">
                        <a href="#" class="w-100">
                            <i class="far fa-plus-square"></i>
                            <p class="mb-0">Drop files here to upload</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row mb-30">
                <div class="col-12">
                    <h4 class="title-sep3 mb-30">
                        opening hours
                    </h4>
                </div>
                <div class="col-12 mb-30">
                    <div class="clone-wrap">
                        <div class="row clone-section">
                            <div class="col-sm-11 col-12">
                                <form class="listing-form" action="#">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-6 col-12">
                                            <select class="form-control custom-select" id="day">
                                                <option>day</option>
                                                <option>monday</option>
                                                <option>tuesday</option>
                                                <option>wednesday</option>
                                                <option>thrusday</option>
                                                <option>thrusday</option>
                                                <option>friday</option>
                                                <option>saturday</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-12">
                                            <select class="form-control custom-select" id="opening-time">
                                                <option>Opening Time</option>
                                                <option>11am</option>
                                                <option>12am</option>
                                                <option>1pm</option>
                                                <option>2pm</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-12">
                                            <select class="form-control custom-select" id="closing-time">
                                                <option>closing Time</option>
                                                <option>11pm</option>
                                                <option>12pm</option>
                                                <option>1pm</option>
                                                <option>2pm</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-1 col-12 text-sm-right text-center">
                                <span class="remove-section">
                                    <i class="fa fa-times"></i>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 text-center">
                                <span class="add-section">
                                    <i class="fa fa-plus"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-30">
                <div class="col-12">
                    <h4 class="title-sep3 mb-30">
                        pricing
                    </h4>
                </div>
                <div class="col-12 mb-30">
                    <div class="clone-wrap2">
                        <div class="row clone-section">
                            <div class="col-sm-11 col-12">
                                <form class="listing-form" action="#">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-6 col-12">
                                            <input type="text" id="title" class="form-control" placeholder="title">
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-12">
                                            <input type="text" id="description" class="form-control" placeholder="description">
                                        </div>
                                        <div class="col-lg-2 col-md-6 col-12">
                                            <input type="text" id="price" class="form-control" placeholder="price">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-1 col-12 text-sm-right text-center">
                                <span class="remove-section">
                                    <i class="fa fa-times"></i>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 text-center">
                                <span class="add-section">
                                    <i class="fa fa-plus"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="listing-submit">
                        <button type="submit" class="btn btn-one btn-anim">save listing</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- add-list end -->

    <!-- footer starts -->
    <footer class="footer footer-one">
        <div class="foot-top">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-lg-12 col-12 mb-60">
                        <div class="company-details">
                            <img src="assets/img/logo.png" class="foot-logo mb-4" alt="lister">
                            <p class="pb-2">Lorem ipsum dolor sit amet, consect tuer adipi cin elit. Aen sociis nato pe na ibus magnis dis
                                partur monte na et ridiculu onec quam felis. castei efte tuerg scn kt isceding elit ende
                                cd magum socadaiis nato ipsf.</p>
                            <div class="socials mt-4">
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-google-plus-g"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                        <div class="recent-post">
                            <div class="foot-title">
                                <h4>useful links</h4>
                            </div>
                            <ul class="quick-link">
                                <li>
                                    <a href="add-list.html">add listing</a>
                                </li>
                                <li>
                                    <a href="register.html">register</a>
                                </li>
                                <li>
                                    <a href="sign-in.html">sign in</a>
                                </li>
                                <li>
                                    <a href="about.html">how it work</a>
                                </li>
                                <li>
                                    <a href="contact.html">contact us</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                        <div class="recent-post">
                            <div class="foot-title">
                                <h4>my account</h4>
                            </div>
                            <ul class="quick-link">
                                <li>
                                    <a href="login.html">create account</a>
                                </li>
                                <li>
                                    <a href="#">dashboard</a>
                                </li>
                                <li>
                                    <a href="#">profile</a>
                                </li>
                                <li>
                                    <a href="#">my listing</a>
                                </li>
                                <li>
                                    <a href="#">favorites</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                        <div class="recent-post">
                            <div class="foot-title">
                                <h4>categories</h4>
                            </div>
                            <ul class="quick-link">
                                <li>
                                    <a href="#">Restaurants</a>
                                </li>
                                <li>
                                    <a href="#">Shopping</a>
                                </li>
                                <li>
                                    <a href="#">Hotels</a>
                                </li>
                                <li>
                                    <a href="#">Destinations</a>
                                </li>
                                <li>
                                    <a href="#">Nightlife</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                        <div class="recent-post">
                            <div class="foot-title">
                                <h4>quick links</h4>
                            </div>
                            <ul class="quick-link">
                                <li>
                                    <a href="about.html">About Us</a>
                                </li>
                                <li>
                                    <a href="price.html">Pricing</a>
                                </li>
                                <li>
                                    <a href="blog-grid.html">Blogs</a>
                                </li>
                                <li>
                                    <a href="#">Testimonials</a>
                                </li>
                                <li>
                                    <a href="#">Our Partners</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="foot-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <p class="text-capitalize">Copyright © 2019, All rights Reserved. Created by
                            <a href="#">Company Name</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer end -->

    <!-- JavaScript Libraries -->
    <script src="assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/isotope.min.js"></script>
    <script src="assets/js/slick.min.js"></script>
    <!-- google map api  -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAAz77U5XQuEME6TpftaMdX0bBelQxXRlM&amp;callback=initMap"></script>

    <script src="assets/js/custom.js"></script>


</body>


<!-- Mirrored from wpshopmart.com/html/demo/dlister/add-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Jul 2019 04:33:53 GMT -->
</html>