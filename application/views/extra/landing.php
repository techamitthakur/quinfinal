<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon.png">
    <link rel="stylesheet" href="assets/css/plugins.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <title>Quinn </title>
</head>

<body>

    <!-- ====== Go to top ====== -->
    <a id="toTop" title="Go to top" href="javascript:void(0)">
        <span id="toTopHover"></span>TOP
    </a>

    <!-- Preloader start-->
    <div class="preloader">
        <div class="loader-inner">
            <div class="dash one">
                <i class="fa fa-map-marker-alt"></i>
            </div>
            <div class="dash two">
                <i class="fa fa-map-marker-alt"></i>
            </div>
            <div class="dash three">
                <i class="fa fa-map-marker-alt"></i>
            </div>
        </div>
    </div>
    <!-- Preloader end -->

    <!-- Header start-->
    <header class="header">
        <div class="head-top head-top-two d-none d-lg-block py-1">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <div class="socials socials-header text-lg-left text-center">
                            <a href="#">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-instagram"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <ul class="head-contact text-lg-right text-center">
                            <li>
                                <i class="fa fa-phone"></i>
                                +123-456-7890
                            </li>
                            <li>
                                <i class="fa fa-envelope"></i>
                                info@quinn.com
                            </li>
                            <li>
                                <i class="fa fa-map-marker-alt"></i>
                                H - 61, 2nd Floor, Sector 63, Noida, Uttar Pradesh
                            </li>

                            <li class="login-sign">
                                <a href="#">
                                    <i class="fa fa-user"></i> login / signup
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="theme-header-two affix">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-12">
                        <div class="logo-two logo-wrap">
                            <div class="logo my-1">
                                <a href="index.html">
                                    <!--
                                    <img src="assets/img/logo5.png" alt="logo">
-->
                                    <span style="font-size: 42px;color:#fff;font-family:'Josefin Sans',sans-serif;">Quinn</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-9 col-md-12">
                        <div class="menu menu-two">
                            <nav class="navbar navbar-expand-lg">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-content" aria-controls="nav-content" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon bar1"></span>
                                    <span class="navbar-toggler-icon bar2"></span>
                                    <span class="navbar-toggler-icon bar3"></span>
                                </button>
                                <!-- Links -->
                                <div class="main-menu collapse navbar-collapse" id="nav-content">
                                    <ul class="navbar-nav ml-auto align-items-lg-center">
                                        <li class="nav-item">
                                            <a class="nav-link" href="index.html">Home</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">about us</a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown">Services</a>
                                            <ul class="dropdown-menu">
                                                <li class="dropdown-item">
                                                    <a href="#">all Services</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="#">Tution teacher </a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="#">Dance teacher </a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="#">Beautician</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="#">PPT preparation</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="#">Yoga/Fitness/ Zumba teacher</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="#">Child Day Care / activity classes</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="#">House Maids</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="#">Miscellaneous service</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Blog</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">contact us</a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="#search">
                                                <i class="fa fa-search"></i>
                                            </a>
                                        </li>
                                        <li class="nav-item d-lg-block d-none">
                                            <a href="#" class="btn btn-one btn-anim br-5 px-3 nav-btn">
                                                <i class="fa fa-plus-circle"></i> add Services
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Header end -->

    <!-- Search Form -->
    <div id="search" class="top-search d-flex">
        <span class="close">
            <i class="fa fa-times"></i>
        </span>
        <div class="w-100 text-center mt-4">
            <h3 class="c-white fw-5">search here</h3>
            <form role="search" id="searchform" action="#" method="get" class="search-bar">
                <input value="" name="q" type="search" placeholder="type to search..." class="form-control">
                <button type="submit" class="submit-btn">
                    <i class="fa fa-search"></i>
                </button>
            </form>
        </div>
    </div>

    <!-- banner start -->
    <section class="main-banner-2">
        <div class="banner-image2">
            <div class="tilt-anim" data-tilt>
                <div class="banner-content">
                  <form>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Email</label>
      <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">Password</label>
      <input type="password" class="form-control" id="inputPassword4" placeholder="Password">
    </div>
  </div>
  <div class="form-group">
    <label for="inputAddress">Address</label>
    <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
  </div>
  <div class="form-group">
    <label for="inputAddress2">Address 2</label>
    <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputCity">City</label>
      <input type="text" class="form-control" id="inputCity">
    </div>
    <div class="form-group col-md-4">
      <label for="inputState">State</label>
      <select id="inputState" class="form-control">
        <option selected>Choose...</option>
        <option>...</option>
      </select>
    </div>
    <div class="form-group col-md-2">
      <label for="inputZip">Zip</label>
      <input type="text" class="form-control" id="inputZip">
    </div>
  </div>
  <div class="form-group">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" id="gridCheck">
      <label class="form-check-label" for="gridCheck">
        Check me out
      </label>
    </div>
  </div>
  <button type="submit" class="btn btn-primary">Sign in</button>
</form>
        </div>
    </section>
    <!-- banner end -->

    <!-- Services start -->
    <section class="bg-w tri-bg sp-100-70 o-hide">
        <div class="container">
            <div class="list-category-slider owl-carousel owl-theme mb-60">
                <div class="list-items2 btn-anim">
                    <div class="icon-box">
                        <i class="flaticon-find"></i>
                    </div>
                    <h5 class="mb-0 mt-3">
                        <a href="#">Tution Teacher </a>
                    </h5>
                </div>
                <div class="list-items2 btn-anim">
                    <div class="icon-box">
                        <i class="flaticon-spa"></i>
                    </div>
                    <h5 class="mb-0 mt-3">
                        <a href="#">Dance Teacher</a>
                    </h5>
                </div>
                <div class="list-items2 btn-anim">
                    <div class="icon-box">
                        <i class="flaticon-cheers"></i>
                    </div>
                    <h5 class="mb-0 mt-3">
                        <a href="#">Beautician</a>
                    </h5>
                </div>
                <div class="list-items2 btn-anim">
                    <div class="icon-box">
                        <i class="flaticon-hotel"></i>
                    </div>
                    <h5 class="mb-0 mt-3">
                        <a href="#">PPT Preparation</a>
                    </h5>
                </div>
                <div class="list-items2 btn-anim">
                    <div class="icon-box">
                        <i class="flaticon-cutlery"></i>
                    </div>
                    <h5 class="mb-0 mt-3">
                        <a href="#">Yoga</a>
                    </h5>
                </div>
                <div class="list-items2 btn-anim">
                    <div class="icon-box">
                        <i class="flaticon-shop"></i>
                    </div>
                    <h5 class="mb-0 mt-3">
                        <a href="#">Child Day Care</a>
                    </h5>
                </div>
                <div class="list-items2 btn-anim">
                    <div class="icon-box">
                        <i class="flaticon-drama"></i>
                    </div>
                    <h5 class="mb-0 mt-3">
                        <a href="#">House maids</a>
                    </h5>
                </div>
            </div>
            <div class="row justify-content-md-center">
                <div class="col-lg-3 col-md-6">
                    <input type="text" class="form-control" placeholder="What are you looking for?">
                </div>
                <div class="col-lg-3 col-md-6">
                    <select class="form-control custom-select" id="service2">
                        <option>all categories</option>
                        <option>Tution Teacher</option>
                        <option>Dance teacher </option>
                        <option>Beautician</option>
                        <option>House Maids</option>
                    </select>

                </div>
                <div class="col-lg-2 col-md-4">
                    <button type="submit" class="btn btn-one btn-anim br-5 w-100 mb-30">
                        <i class="fa fa-search"></i> search</button>
                </div>
            </div>
        </div>
    </section>
    <!-- Services start -->


    <!-- popular Services start -->
    <section class="bg-dull sp-100-70">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="all-title">
                        <h3 class="sec-title">
                            popular Services
                        </h3>
                        <svg class="title-sep">
                            <path fill-rule="evenodd" d="M32.000,13.000 L32.000,7.000 L35.000,7.000 L35.000,13.000 L32.000,13.000 ZM24.000,4.000 L27.000,4.000 L27.000,16.000 L24.000,16.000 L24.000,4.000 ZM16.000,-0.000 L19.000,-0.000 L19.000,20.000 L16.000,20.000 L16.000,-0.000 ZM8.000,4.000 L11.000,4.000 L11.000,16.000 L8.000,16.000 L8.000,4.000 ZM-0.000,7.000 L3.000,7.000 L3.000,13.000 L-0.000,13.000 L-0.000,7.000 Z" />
                        </svg>
                        <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                    <div class="Services-item2 mb-30">
                        <div class="img-holder">
                            <span class="offer">save 30%
                            </span>
                            <img src="assets/img/home/pl-slide1.jpg" alt="list">
                            <a class="likes" href="#">
                                <i class="far fa-heart"></i> 14
                            </a>
                        </div>
                        <div class="list-content">
                            <h5>
                                <a href="#">Tution teacher</a>
                            </h5>
                            <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return</p>
                            <div class="centering justify-content-between">
                                <ul class="ctg-info">
                                    <li class="my-1">
                                        <a href="#">
                                            <i class="flaticon-cutlery mr-2"></i> Service</a>
                                    </li>
                                    <li class="my-1">
                                        <span class="c-theme"> open now</span>
                                    </li>
                                </ul>
                                <ul>
                                    <li class="list-rating mb-1">
                                        <span>
                                            <i class="fa fa-star mr-1"></i>3.0</span>
                                    </li>
                                </ul>
                            </div>
                            <ul class="ctg-info2 pt-2 mt-3 centering justify-content-between">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>Noida , IND</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                    <div class="Services-item2 mb-30">
                        <div class="img-holder">
                            <span class="offer">save 33%
                            </span>
                            <img src="assets/img/home/pl-slide2.jpg" alt="list">
                            <a class="likes" href="#">
                                <i class="far fa-heart"></i>55
                            </a>
                        </div>
                        <div class="list-content">
                            <h5>
                                <a href="#">Dance teacher</a>
                            </h5>
                            <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return</p>
                            <div class="centering justify-content-between">
                                <ul class="ctg-info">
                                    <li class="my-1">
                                        <a href="#">
                                            <i class="flaticon-shop mr-2"></i>Service</a>
                                    </li>
                                    <li class="my-1">
                                        <span class="c-theme"> open now</span>
                                    </li>
                                </ul>
                                <ul>
                                    <li class="list-rating mb-1">
                                        <span>
                                            <i class="fa fa-star mr-1"></i>2.5</span>
                                    </li>
                                </ul>
                            </div>
                            <ul class="ctg-info2 pt-2 mt-3 centering justify-content-between">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>Noida , IND</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                    <div class="Services-item2 mb-30">
                        <div class="img-holder">
                            <span class="offer">save 26%
                            </span>
                            <img src="assets/img/home/pl-slide3.jpg" alt="list">
                            <a class="likes" href="#">
                                <i class="far fa-heart"></i>31
                            </a>
                        </div>
                        <div class="list-content">
                            <h5>
                                <a href="#">Activity Classes</a>
                            </h5>
                            <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return</p>
                            <div class="centering justify-content-between">
                                <ul class="ctg-info">
                                    <li class="my-1">
                                        <a href="#">
                                            <i class="flaticon-cheers mr-2"></i>Service</a>
                                    </li>
                                    <li class="my-1">
                                        <span class="c-theme"> open now</span>
                                    </li>
                                </ul>
                                <ul>
                                    <li class="list-rating mb-1">
                                        <span>
                                            <i class="fa fa-star mr-1"></i>3.2</span>
                                    </li>
                                </ul>
                            </div>
                            <ul class="ctg-info2 pt-2 mt-3 centering justify-content-between">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>Noida , IND</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                    <div class="Services-item2 mb-30">
                        <div class="img-holder">
                            <span class="offer">save 22%
                            </span>
                            <img src="assets/img/home/pl-slide4.jpg" alt="list">
                            <a class="likes" href="#">
                                <i class="far fa-heart"></i>05
                            </a>
                        </div>
                        <div class="list-content">
                            <h5>
                                <a href="#">Service services</a>
                            </h5>
                            <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return</p>
                            <div class="centering justify-content-between">
                                <ul class="ctg-info">
                                    <li class="my-1">
                                        <a href="#">
                                            <i class="flaticon-spa mr-2"></i>Service</a>
                                    </li>
                                    <li class="my-1">
                                        <span class="c-theme"> open now</span>
                                    </li>
                                </ul>
                                <ul>
                                    <li class="list-rating mb-1">
                                        <span>
                                            <i class="fa fa-star mr-1"></i>4.6</span>
                                    </li>
                                </ul>
                            </div>
                            <ul class="ctg-info2 pt-2 mt-3 centering justify-content-between">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>Noida , IND</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                    <div class="Services-item2 mb-30">
                        <div class="img-holder">
                            <span class="offer">save 29%
                            </span>
                            <img src="assets/img/home/pl-slide5.jpg" alt="list">
                            <a class="likes" href="#">
                                <i class="far fa-heart"></i>36
                            </a>
                        </div>
                        <div class="list-content">
                            <h5>
                                <a href="#">Child day care</a>
                            </h5>
                            <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return</p>
                            <div class="centering justify-content-between">
                                <ul class="ctg-info">
                                    <li class="my-1">
                                        <a href="#">
                                            <i class="flaticon-find mr-2"></i>Service</a>
                                    </li>
                                    <li class="my-1">
                                        <span class="c-theme"> open now</span>
                                    </li>
                                </ul>
                                <ul>
                                    <li class="list-rating mb-1">
                                        <span>
                                            <i class="fa fa-star mr-1"></i>3.5</span>
                                    </li>
                                </ul>
                            </div>
                            <ul class="ctg-info2 pt-2 mt-3 centering justify-content-between">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>Noida , IND</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                    <div class="Services-item2 mb-30">
                        <div class="img-holder">
                            <span class="offer">save 49%
                            </span>
                            <img src="assets/img/home/pl-slide6.jpg" alt="list">
                            <a class="likes" href="#">
                                <i class="far fa-heart"></i>61
                            </a>
                        </div>
                        <div class="list-content">
                            <h5>
                                <a href="#">House Maids</a>
                            </h5>
                            <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return</p>
                            <div class="centering justify-content-between">
                                <ul class="ctg-info">
                                    <li class="my-1">
                                        <a href="#">
                                            <i class="flaticon-hotel mr-2"></i>Service</a>
                                    </li>
                                    <li class="my-1">
                                        <span class="c-theme"> open now</span>
                                    </li>
                                </ul>
                                <ul>
                                    <li class="list-rating mb-1">
                                        <span>
                                            <i class="fa fa-star mr-1"></i>4.0</span>
                                    </li>
                                </ul>
                            </div>
                            <ul class="ctg-info2 pt-2 mt-3 centering justify-content-between">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>Noida , IND</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- popular Services start -->

    <!-- counter starts -->
    <section class="counters sp-100-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 mb-30">
                    <div class="counter-box">
                        <div class="icon-box">
                            <i class="flaticon-list-1"></i>
                        </div>
                        <h2 class="count c-theme" data-count="5256">0</h2>
                        <p>total Services</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 mb-30">
                    <div class="counter-box">
                        <div class="icon-box">
                            <i class="flaticon-team"></i>
                        </div>
                        <h2 class="count c-theme" data-count="1250">0</h2>
                        <p>total Services</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 mb-30">
                    <div class="counter-box">
                        <div class="icon-box">
                            <i class="flaticon-trophy"></i>
                        </div>
                        <h2 class="count c-theme" data-count="160">0</h2>
                        <p>total Services</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 mb-30">
                    <div class="counter-box">
                        <div class="icon-box">
                            <i class="flaticon-appointment"></i>
                        </div>
                        <h2 class="count c-theme" data-count="12">0</h2>
                        <p>total Services</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- counter ends -->

    <!-- features start-->
    <section class="features sp-100-70 bg-w">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="all-title">
                        <h3 class="sec-title">
                            Why We Are Best
                        </h3>
                        <svg class="title-sep">
                            <path fill-rule="evenodd" d="M32.000,13.000 L32.000,7.000 L35.000,7.000 L35.000,13.000 L32.000,13.000 ZM24.000,4.000 L27.000,4.000 L27.000,16.000 L24.000,16.000 L24.000,4.000 ZM16.000,-0.000 L19.000,-0.000 L19.000,20.000 L16.000,20.000 L16.000,-0.000 ZM8.000,4.000 L11.000,4.000 L11.000,16.000 L8.000,16.000 L8.000,4.000 ZM-0.000,7.000 L3.000,7.000 L3.000,13.000 L-0.000,13.000 L-0.000,7.000 Z" />
                        </svg>
                        <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="feature-item type-2">
                        <div class="icon-box">
                            <i class="flaticon-rising"></i>
                        </div>
                        <h5>our vision</h5>
                        <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="feature-item type-2">
                        <div class="icon-box">
                            <i class="flaticon-list"></i>
                        </div>
                        <h5>missions</h5>
                        <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="feature-item type-2">
                        <div class="icon-box">
                            <i class="flaticon-medal"></i>
                        </div>
                        <h5>our value</h5>
                        <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="feature-item type-2">
                        <div class="icon-box">
                            <i class="flaticon-find-1"></i>
                        </div>
                        <h5>find your Services</h5>
                        <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="feature-item type-2">
                        <div class="icon-box">
                            <i class="flaticon-map"></i>
                        </div>
                        <h5>find the way</h5>
                        <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="feature-item type-2">
                        <div class="icon-box">
                            <i class="flaticon-support"></i>
                        </div>
                        <h5>24/7 support</h5>
                        <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- features end -->

    <!-- categories start-->
    <section class="categories sp-100-70 bg-dull">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="all-title">
                        <h3 class="sec-title">
                            top categories
                        </h3>
                        <svg class="title-sep">
                            <path fill-rule="evenodd" d="M32.000,13.000 L32.000,7.000 L35.000,7.000 L35.000,13.000 L32.000,13.000 ZM24.000,4.000 L27.000,4.000 L27.000,16.000 L24.000,16.000 L24.000,4.000 ZM16.000,-0.000 L19.000,-0.000 L19.000,20.000 L16.000,20.000 L16.000,-0.000 ZM8.000,4.000 L11.000,4.000 L11.000,16.000 L8.000,16.000 L8.000,4.000 ZM-0.000,7.000 L3.000,7.000 L3.000,13.000 L-0.000,13.000 L-0.000,7.000 Z" />
                        </svg>
                        <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return</p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-30">
                    <div class="ctg-item">
                        <div class="icon-box" style="background-image:url('assets/img/home/cat1.jpg')">
                            <i class="flaticon-map"></i>
                        </div>
                        <div class="content-box p-4">
                            <h5 class="mb-1">
                                <a href="#">Tution teacher </a>
                            </h5>
                            <p class="mb-0">35 Services</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-30">
                    <div class="ctg-item">
                        <div class="icon-box" style="background-image:url('assets/img/home/cat2.jpg')">
                            <i class="flaticon-cutlery"></i>
                        </div>
                        <div class="content-box p-4">
                            <h5 class="mb-1">
                                <a href="#">Dance teacher</a>
                            </h5>
                            <p class="mb-0">20 Services</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-30">
                    <div class="ctg-item">
                        <div class="icon-box" style="background-image:url('assets/img/home/cat3.jpg')">
                            <i class="flaticon-shop"></i>
                        </div>
                        <div class="content-box p-4">
                            <h5 class="mb-1">
                                <a href="#"> Beautician</a>
                            </h5>
                            <p class="mb-0">15 Services</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-30">
                    <div class="ctg-item">
                        <div class="icon-box" style="background-image:url('assets/img/home/cat4.jpg')">
                            <i class="flaticon-hotel"></i>
                        </div>
                        <div class="content-box p-4">
                            <h5 class="mb-1">
                                <a href="#"> PPT Preparation</a>
                            </h5>
                            <p class="mb-0">12 Services</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-30">
                    <div class="ctg-item">
                        <div class="icon-box" style="background-image:url('assets/img/home/cat5.jpg')">
                            <i class="flaticon-spa"></i>
                        </div>
                        <div class="content-box p-4">
                            <h5 class="mb-1">
                                <a href="#"> Yoga Teacher</a>
                            </h5>
                            <p class="mb-0">18 Services</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-30">
                    <div class="ctg-item">
                        <div class="icon-box" style="background-image:url('assets/img/home/cat6.jpg')">
                            <i class="flaticon-drama"></i>
                        </div>
                        <div class="content-box p-4">
                            <h5 class="mb-1">
                                <a href="#"> Child Day Care </a>
                            </h5>
                            <p class="mb-0">14 Services</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-30">
                    <div class="ctg-item">
                        <div class="icon-box" style="background-image:url('assets/img/home/cat7.jpg')">
                            <i class="flaticon-cheers"></i>
                        </div>
                        <div class="content-box p-4">
                            <h5 class="mb-1">
                                <a href="#"> House maids</a>
                            </h5>
                            <p class="mb-0">25 Services</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-30">
                    <div class="ctg-item">
                        <div class="icon-box" style="background-image:url('assets/img/home/cat8.jpg')">
                            <i class="flaticon-fast-food"></i>
                        </div>
                        <div class="content-box p-4">
                            <h5 class="mb-1">
                                <a href="#"> Zumba Teacher</a>
                            </h5>
                            <p class="mb-0">10 Services</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- categories end -->


    <!-- cta-one start-->
    <section class="cta-one tri-bg-w text-lg-left text-center">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 my-lg-0 my-5 py-lg-0 py-5">
                    <div class="cta-content">
                        <h3>Sign Up To Get Special Services Every Day</h3>
                        <p>Singup to get updated with latest services near you</p>
                        <a href="#" class="btn btn-two btn-anim br-5 mt-2">
                            sign up
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 d-lg-block d-none">
                    <div class="cta-img mt-4">
                        <img src="assets/img/home/cta-bg.png" alt="image">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- cta-one end -->


    <!-- testimonial start-->
    <section class="sp-100 bg-dull">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="all-title">
                        <h3 class="sec-title">
                            Testimonials
                        </h3>
                        <svg class="title-sep">
                            <path fill-rule="evenodd" d="M32.000,13.000 L32.000,7.000 L35.000,7.000 L35.000,13.000 L32.000,13.000 ZM24.000,4.000 L27.000,4.000 L27.000,16.000 L24.000,16.000 L24.000,4.000 ZM16.000,-0.000 L19.000,-0.000 L19.000,20.000 L16.000,20.000 L16.000,-0.000 ZM8.000,4.000 L11.000,4.000 L11.000,16.000 L8.000,16.000 L8.000,4.000 ZM-0.000,7.000 L3.000,7.000 L3.000,13.000 L-0.000,13.000 L-0.000,7.000 Z"></path>
                        </svg>
                        <p>love from our clients</p>
                    </div>
                </div>
            </div>
            <div class="testi-slider1 owl-carousel owl-theme">
                <div class="slide-item">
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="assets/img/home/testi1.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">jessica thomas</h5>
                            <span class="c-theme mb-3 d-block">Manager at Travel Agency</span>
                            <p class="mb-0">I started using this application when I newly moved to Mumbai and was looking for a place to stay. Right from finding an Estate Agent to other services JD was my one -stop solution. It helped in getting service providers close to where I was staying.</p>
                        </div>
                    </div>
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="assets/img/home/testi2.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">Paul Anderson</h5>
                            <span class="c-theme mb-3 d-block">Web Designer</span>
                            <p class="mb-0">I started using this application when I newly moved to Mumbai and was looking for a place to stay. Right from finding an Estate Agent to other services JD was my one -stop solution. It helped in getting service providers close to where I was staying.</p>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="assets/img/home/testi3.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">Harison Doe</h5>
                            <span class="c-theme mb-3 d-block">Happy Customer</span>
                            <p class="mb-0">I started using this application when I newly moved to Mumbai and was looking for a place to stay. Right from finding an Estate Agent to other services JD was my one -stop solution. It helped in getting service providers close to where I was staying.</p>
                        </div>
                    </div>
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="assets/img/home/testi4.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">Ashley Marcov</h5>
                            <span class="c-theme mb-3 d-block">Co-Owner</span>
                            <p class="mb-0">I started using this application when I newly moved to Mumbai and was looking for a place to stay. Right from finding an Estate Agent to other services JD was my one -stop solution. It helped in getting service providers close to where I was staying.</p>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="assets/img/home/testi1.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">jessica thomas</h5>
                            <span class="c-theme mb-3 d-block">Manager at Travel Agency</span>
                            <p class="mb-0">I started using this application when I newly moved to Mumbai and was looking for a place to stay. Right from finding an Estate Agent to other services JD was my one -stop solution. It helped in getting service providers close to where I was staying.</p>
                        </div>
                    </div>
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="assets/img/home/testi2.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">Paul Anderson</h5>
                            <span class="c-theme mb-3 d-block">Web Designer</span>
                            <p class="mb-0">I started using this application when I newly moved to Mumbai and was looking for a place to stay. Right from finding an Estate Agent to other services JD was my one -stop solution. It helped in getting service providers close to where I was staying.</p>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="assets/img/home/testi3.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">Harison Doe</h5>
                            <span class="c-theme mb-3 d-block">Happy Customer</span>
                            <p class="mb-0">I started using this application when I newly moved to Mumbai and was looking for a place to stay. Right from finding an Estate Agent to other services JD was my one -stop solution. It helped in getting service providers close to where I was staying.</p>
                        </div>
                    </div>
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="assets/img/home/testi4.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">Ashley Marcov</h5>
                            <span class="c-theme mb-3 d-block">Co-Owner</span>
                            <p class="mb-0">I started using this application when I newly moved to Mumbai and was looking for a place to stay. Right from finding an Estate Agent to other services JD was my one -stop solution. It helped in getting service providers close to where I was staying.</p>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="assets/img/home/testi1.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">jessica thomas</h5>
                            <span class="c-theme mb-3 d-block">Manager at Travel Agency</span>
                            <p class="mb-0">I started using this application when I newly moved to Mumbai and was looking for a place to stay. Right from finding an Estate Agent to other services JD was my one -stop solution. It helped in getting service providers close to where I was staying.</p>
                        </div>
                    </div>
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="assets/img/home/testi2.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">Paul Anderson</h5>
                            <span class="c-theme mb-3 d-block">Web Designer</span>
                            <p class="mb-0">I started using this application when I newly moved to Mumbai and was looking for a place to stay. Right from finding an Estate Agent to other services JD was my one -stop solution. It helped in getting service providers close to where I was staying.</p>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="assets/img/home/testi3.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">Harison Doe</h5>
                            <span class="c-theme mb-3 d-block">Happy Customer</span>
                            <p class="mb-0">I started using this application when I newly moved to Mumbai and was looking for a place to stay. Right from finding an Estate Agent to other services JD was my one -stop solution. It helped in getting service providers close to where I was staying.</p>
                        </div>
                    </div>
                    <div class="testi-item mb-30">
                        <div class="img-holder">
                            <img src="assets/img/home/testi4.png" alt="lister">
                        </div>
                        <div class="testi-content">
                            <h5 class="mb-1">Ashley Marcov</h5>
                            <span class="c-theme mb-3 d-block">Co-Owner</span>
                            <p class="mb-0">I started using this application when I newly moved to Mumbai and was looking for a place to stay. Right from finding an Estate Agent to other services JD was my one -stop solution. It helped in getting service providers close to where I was staying.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- testimonial end -->

    <!-- app start-->
    <div class="app-section bg-red tri-bg-w sp-100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 d-none d-lg-block">
                    <div class="app-img mr-5">
                        <img src="assets/img/home/app-phn.png" alt="lister-app">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="app-content text-lg-left text-center">
                        <h3>Android App and iOS App Available </h3>
                        <p class="mb-4">Quinn Android / ios App is also available at store and play store</p>
                        <div class="app-icon">
                            <a href="#" class="mr-4">
                                <img src="assets/img/home/google-play.png" alt="google-play">
                            </a>
                            <a href="#">
                                <img src="assets/img/home/play-store.png" alt="play-store">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- app end -->

    <!-- footer starts -->
    <footer class="footer footer-two">
        <div class="foot-top">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-lg-12 col-12 mb-60">
                        <div class="company-details">
                            <img src="assets/img/logo2.png" class="foot-logo mb-4" alt="lister">
                            <p class="pb-2">Search for Anything, Anytime, Anywhere using Quinn App by Quinn Pvt. Ltd., India’s No. 1 Local Search Engine. You can chat with businesses, get phone numbers, addresses, learn of best deals, latest reviews and ratings for all businesses instantly.</p>
                            <div class="socials mt-4">
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-google-plus-g"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                        <div class="recent-post">
                            <div class="foot-title">
                                <h4>useful links</h4>
                            </div>
                            <ul class="quick-link">
                                <li>
                                    <a href="#">add Services</a>
                                </li>
                                <li>
                                    <a href="#">register</a>
                                </li>
                                <li>
                                    <a href="#">sign in</a>
                                </li>
                                <li>
                                    <a href="#">how it work</a>
                                </li>
                                <li>
                                    <a href="#">contact us</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                        <div class="recent-post">
                            <div class="foot-title">
                                <h4>my account</h4>
                            </div>
                            <ul class="quick-link">
                                <li>
                                    <a href="#">create account</a>
                                </li>
                                <li>
                                    <a href="#">dashboard</a>
                                </li>
                                <li>
                                    <a href="#">profile</a>
                                </li>
                                <li>
                                    <a href="#">my Services</a>
                                </li>
                                <li>
                                    <a href="#">favorites</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                        <div class="recent-post">
                            <div class="foot-title">
                                <h4>categories</h4>
                            </div>
                            <ul class="quick-link">
                                <li>
                                    <a href="#">Restaurants</a>
                                </li>
                                <li>
                                    <a href="#">Shopping</a>
                                </li>
                                <li>
                                    <a href="#">Hotels</a>
                                </li>
                                <li>
                                    <a href="#">Destinations</a>
                                </li>
                                <li>
                                    <a href="#">Nightlife</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6 col-12 mb-60">
                        <div class="recent-post">
                            <div class="foot-title">
                                <h4>quick links</h4>
                            </div>
                            <ul class="quick-link">
                                <li>
                                    <a href="#">About Us</a>
                                </li>
                                <li>
                                    <a href="price.html">Pricing</a>
                                </li>
                                <li>
                                    <a href="blog-grid.html">Blogs</a>
                                </li>
                                <li>
                                    <a href="#">Testimonials</a>
                                </li>
                                <li>
                                    <a href="#">Our Partners</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="foot-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <p class="text-capitalize">Copyright © 2019, All rights Reserved. Designed &amp;Developed by
                            <a href="#">KCMK KINESI PVT LTD</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer end -->

    <!-- JavaScript Libraries -->
    <script src="assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/slick.min.js"></script>
    <script src="assets/js/tilt.jquery.js"></script>
    <script src="assets/js/custom.js"></script>


</body>


<!-- Mirrored from wpshopmart.com/html/demo/dlister/index-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Jul 2019 04:33:56 GMT -->

</html>
