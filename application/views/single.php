<?php include 'header.php';?>

<!-- page-banner start-->
<section class="page-banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>Service detail</h3>
                <ul class="banner-link text-center">
                    <li>
                        <a href="index-2.html">Home</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">listing</a>
                    </li>
                    <li>
                        <span class="active">Service detail</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- page-banner ends-->


<!-- listing detail start-->
<div class="bg-w sp-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-12">
                <div class="listing-detail mb-60">
                    <h3 class="mb-30">Teaching</h3>
                    <div class="abt-listing">
                        <ul class="ctg-info centering justify-content-start">
                            <li>
                                <a href="#">
                                    <i class="fa fa-map-marker-alt mr-2"></i>345 Canal St, New York, USA</a>
                            </li>
                            <li class="my-1">
                                <a href="#">
                                    <i class="fa fa-phone mr-2"></i> 0123-456-789</a>
                            </li>

                            <li class="my-1">
                                <span> open now</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="mb-60">
                    <h4 class="title-sep3 mb-30">
                        description
                    </h4>
                    <p>Teaching is a highly complex activity.This is partially because teaching is a social practice, that takes place in a specific context (time, place, culture, socio-political-economic situation etc.) and therefore is shaped by the values of that specific context. Factors that influence what is expected (or required) of teachers include history and tradition, social views about the purpose of education, accepted theories about learning, etc.</p>

                </div>
                <div class="mb-60">
                    <h4 class="title-sep3 mb-30">
                        Payment Method
                    </h4>
                    <div class="row mb-30">
                        <div class="col-xl-6 col-lg-3 col-md-3 col-sm-6 col-12 mb-2">
                            <i class="fas fa-wallet"></i> <label for="checkbox_id1">Credit Card</label>
                        </div>
                        <div class="col-xl-6 col-lg-3 col-md-3 col-sm-6 col-12 mb-2">
                            <i class="fas fa-wallet"></i> <label for="checkbox_id2">Debit Card</label>
                        </div>
                        <div class="col-xl-6 col-lg-3 col-md-3 col-sm-6 col-12 mb-2">
                            <i class="fas fa-wallet"></i> <label for="checkbox_id3">Cash Payment</label>
                        </div>
                        <div class="col-xl-6 col-lg-3 col-md-3 col-sm-6 col-12 mb-2">
                            <i class="fas fa-wallet"></i> <label for="checkbox_id4">Paytm</label>
                        </div>

                    </div>
                </div>

                <div class="mb-30">
                    <h4 class="title-sep3 mb-30">
                        tags
                    </h4>
                    <ul class="tagcloud minus-pad">
                        <li>
                            <a href="#">Teacher</a>
                        </li>
                        <li>
                            <a href="#">Service</a>
                        </li>

                    </ul>

                    <div class="row mt-30">
                        <div class="col-lg-3 col-md-4 mb-30">
                            <div class="rating-big-box mb-3">
                                <h2>4.5</h2>
                                <h6>superb</h6>
                            </div>
                            <div class="rating-btn-down">
                                <a href="#" class="btn w-100 review-btn btn-anim">3 Reviews</a>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-8 mb-30">
                            <div class="rating-bar-box">
                                <h6>
                                    <span>rating</span>
                                    <span>3.0</span>
                                </h6>
                                <div class="rating-bars">
                                    <span class="bar-fill" style="width:60%"></span>
                                </div>
                            </div>
                            <div class="rating-bar-box">
                                <h6>
                                    <span>facilities</span>
                                    <span>4</span>
                                </h6>
                                <div class="rating-bars">
                                    <span class="bar-fill" style="width:80%"></span>
                                </div>
                            </div>
                            <div class="rating-bar-box">
                                <h6>
                                    <span>staff</span>
                                    <span>4.5</span>
                                </h6>
                                <div class="rating-bars">
                                    <span class="bar-fill" style="width:90%"></span>
                                </div>
                            </div>
                            <div class="rating-bar-box">
                                <h6>
                                    <span>price</span>
                                    <span>3.5</span>
                                </h6>
                                <div class="rating-bars">
                                    <span class="bar-fill" style="width:70%"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mb-30">
                    <h4 class="title-sep3 mb-30">
                        3 Reviews
                    </h4>
                    <div class="row">
                        <div class="col-12">
                            <div class="review-box mb-30">
                                <div class="image-holder">
                                    <img src="assets/img/blog/com-1.jpg" alt="author">
                                </div>
                                <div class="review-content">
                                    <div class="centering justify-content-between mb-3">
                                        <div>
                                            <h5 class="mb-0">Tom Perrins</h5>
                                            <p class="c-theme mb-0">26 april 2019</p>
                                        </div>
                                        <div class="d-flex flex-wrap">
                                            <div class="rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-half-alt"></i>
                                            </div>
                                            <div class="rate-bg ml-4">
                                                4.5
                                            </div>
                                        </div>
                                    </div>
                                    <p class="mb-0">Mollit aute dolore nisi sint tempor veniam ut magna aute. Et officia elit eu eu adipisicing
                                        consectetur cillum elit eiusmod dolore est culpa..</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="review-box mb-30">
                                <div class="image-holder">
                                    <img src="assets/img/blog/com-2.jpg" alt="author">
                                </div>
                                <div class="review-content">
                                    <div class="centering justify-content-between mb-3">
                                        <div>
                                            <h5 class="mb-0">Kathy Brown</h5>
                                            <p class="c-theme mb-0">30 april 2019</p>
                                        </div>
                                        <div class="d-flex flex-wrap">
                                            <div class="rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-half-alt"></i>
                                                <i class="far fa-star"></i>
                                            </div>
                                            <div class="rate-bg ml-4">
                                                3.5
                                            </div>
                                        </div>
                                    </div>
                                    <p class="mb-0">Mollit aute dolore nisi sint tempor veniam ut magna aute. Et officia elit eu eu adipisicing
                                        consectetur cillum elit eiusmod dolore est culpa..</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="review-box mb-30">
                                <div class="image-holder">
                                    <img src="assets/img/blog/com-3.jpg" alt="author">
                                </div>
                                <div class="review-content">
                                    <div class="centering justify-content-between mb-3">
                                        <div>
                                            <h5 class="mb-0">William Dixion</h5>
                                            <p class="c-theme mb-0">28 april 2019</p>
                                        </div>
                                        <div class="d-flex flex-wrap">
                                            <div class="rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="far fa-star"></i>
                                                <i class="far fa-star"></i>
                                            </div>
                                            <div class="rate-bg ml-4">
                                                3.0
                                            </div>
                                        </div>
                                    </div>
                                    <p class="mb-0">Mollit aute dolore nisi sint tempor veniam ut magna aute. Et officia elit eu eu adipisicing
                                        consectetur cillum elit eiusmod dolore est culpa..</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mb-0">
                    <h4 class="title-sep3 mb-30">
                        add review
                    </h4>
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="add-review">
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                </div>
                                <h5>Rating*</h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="add-review">
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                </div>
                                <h5>Facilities*</h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="add-review">
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                </div>
                                <h5>Staff*</h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="add-review">
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                </div>
                                <h5>Price*</h5>
                            </div>
                        </div>
                    </div>
                    <form class="comment-form" id="commentform" method="post" action="#">
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <span class="fa fa-user"></span>
                                <input type="text" id="name" class="form-control" placeholder="Enter Name">
                            </div>
                            <div class="col-md-6 col-12">
                                <span class="fa fa-envelope"></span>
                                <input type="email" class="form-control" placeholder="Enter Email" name="Ented email" id="email">
                            </div>
                            <div class="col-12">
                                <textarea rows="5" name="comment" class="form-control" placeholder="Your Message" id="comment"></textarea>
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-one btn-anim" id="submit" name="submit">
                                    <i class="fa fa-paper-plane"></i> submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-4 col-12">
                <aside class="sidebar">
                    <div class="widget">
                        <h4 class="title-sep2 mb-30">book service</h4>
                        <div class="booking-form">
                            <div class="row">

                                <div class="col-12">
                                    <label>when to book</label>
                                    <input class="form-control custom-select" type="date" name="dateinout">
                                </div>

                                <div class="col-12">
                                    <button type="submit" class="btn btn-one btn-anim w-100" id="submit2" name="submit2">
                                        <i class="fa fa-paper-plane"></i> Book</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="widget">
                        <h4 class="title-sep2 mb-30">business info</h4>
                        <ul class="contact-info mt-4">
                            <li>
                                <i class="fa fa-phone"></i>
                                123-456-7890
                            </li>
                            <li>
                                <i class="fa fa-envelope"></i>
                                info@quinn.in
                            </li>
                            <li>
                                <i class="fa fa-map-marker-alt"></i>
                                Sector 63, Noida
                            </li>
                            <li>
                                <i class="fa fa-globe-asia"></i>
                                www.quinn.in
                            </li>
                        </ul>
                        <div class="socials sidebar-socials mt-3 mb-30">
                            <div class="side-icon">
                                <i class="fa fa-share-alt"></i>
                            </div>
                            <div>
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>

                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>

                            </div>
                        </div>

                    </div>
                    <div class="widget">
                        <h4 class="title-sep2 mb-30">Opening Hours</h4>
                        <div class="centering opening-hours justify-content-between">
                            <p>Opening Hours :</p>
                            <span class="c-theme">Open 24/7</span>
                        </div>
                    </div>
                    <div class="widget">
                        <h4 class="title-sep2 mb-30">Recently Added</h4>
                        <div class="sidebar-listing-slider owl-carousel owl-theme">
                            <div class="listing-item p-2">
                                <div class="img-holder">
                                    <span class="offer">save 49%
                                    </span>
                                    <img src="assets/img/home/pl-slide1.jpg" alt="list">
                                    <div class="rate-like centering justify-content-between">
                                        <div class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                        <a class="likes" href="#">
                                            <i class="far fa-heart"></i> 41
                                        </a>
                                    </div>
                                </div>
                                <div class="list-content p-2">
                                    <h5 class="mt-3 mb-2">
                                        <a href="#">the lounge &amp; bar</a>
                                    </h5>
                                    <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                        <li class="mt-1">
                                            <a href="#">
                                                <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                        </li>
                                        <li class="mt-1">
                                            <a href="#">
                                                <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="listing-item p-2">
                                <div class="img-holder">
                                    <span class="offer">save 30%
                                    </span>
                                    <img src="assets/img/home/pl-slide2.jpg" alt="list">
                                    <div class="rate-like centering justify-content-between">
                                        <div class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                        <a class="likes" href="#">
                                            <i class="far fa-heart"></i> 22
                                        </a>
                                    </div>
                                </div>
                                <div class="list-content p-2">
                                    <h5 class="mt-3 mb-2">
                                        <a href="#">the best shop in city</a>
                                    </h5>
                                    <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                        <li class="mt-1">
                                            <a href="#">
                                                <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                        </li>
                                        <li class="mt-1">
                                            <a href="#">
                                                <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="listing-item p-2">
                                <div class="img-holder">
                                    <span class="offer">save 19%
                                    </span>
                                    <img src="assets/img/home/pl-slide3.jpg" alt="list">
                                    <div class="rate-like centering justify-content-between">
                                        <div class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                        <a class="likes" href="#">
                                            <i class="far fa-heart"></i> 06
                                        </a>
                                    </div>
                                </div>
                                <div class="list-content p-2">
                                    <h5 class="mt-3 mb-2">
                                        <a href="#">enjoy best nightlife</a>
                                    </h5>
                                    <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                        <li class="mt-1">
                                            <a href="#">
                                                <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                        </li>
                                        <li class="mt-1">
                                            <a href="#">
                                                <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="widget">
                        <h4 class="title-sep2 mb-30">about service provider</h4>
                        <div class="widget-author">
                            <div class="author-top text-right mb-4">
                                <div class="img-holder">
                                    <img src="assets/img/blog/aut-big.png" alt="#">
                                </div>
                                <div class="author-info">
                                    <h5 class="c-white">Nikita Singh</h5>
                                    <span>Teacher</span>
                                </div>
                            </div>
                            <p class="bor-b pb-3">A teacher (also called a school teacher or, in some contexts, an educator) is a person who helps students to acquire knowledge, competence or virtue. </p>
                            <div class="socials">
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-google-plus-g"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</div>
<!-- listing detail end -->

<?php include 'footer.php';?>
