<?php include 'header.php';?>

<!-- error start-->
<section class="error-page bg-w">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="error-content">
                    <h2>404</h2>
                    <span>page not found !</span>
                    <p>Oops! The page you are looking for does not exist. It might have been moved or deleted.</p>
                    <div class="search-box mt-5">
                        <form role="search" method="post">
                            <div class="input-group">
                                <input name="text" class="form-control" placeholder="Search..." type="text">
                                <button type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="back-to-home mt-5">
                        <a href="index-2.html">
                            <i class="fa fa-home"></i>
                            <br>
                            back to home
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <img src="assets/img/pages/error-img.png" alt="error-img"> -->
</section>
<!-- error end -->
<?php include 'footer.php';?>
