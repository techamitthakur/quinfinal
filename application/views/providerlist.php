<?php include 'header.php';?>

<!-- page-banner start-->
<style>
    .profile-pic {

        display: block;
    }

    .file-upload {
        display: none;
    }

    .circle {
        border-radius: 1000px !important;
        overflow: hidden;
        width: 75%;
        height: auto;
        border: 8px solid rgba(255, 255, 255, 0.7);
        position: absolute;
    }

    .p-image {
        top: 218px !important;
        position: absolute;
        right: 115px;
        color: #ff3a54;
        transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
    }

    .p-image:hover {
        transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
    }

    .upload-button {
        font-size: 3.2em;
    }

    .upload-button:hover {
        transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
        color: #999;
    }

    .fab {
        padding-right: 5px;
    }

</style>
<section>
    <div class="container-fluid" style="padding:0px;">
        <div class="col-12" style="padding: 0px;">
            <div class="circle" style="width: 15%;top: 55%;left: 10%;border: 3px solid rgba(255, 255, 255, 0.7);">
                <!-- User Profile Image -->
                <img class="profile-pic" src="assets/img/img_avatar.png" style="position: relative;">
                <!-- Default Image -->
            </div><img src="assets/img/banner.png" class="img-responsive" style="width: 100%;height: 350px;">
            <div class="p-image" style="top: 102%!important;left: 280px;">
                <i class="fa fa-camera upload-button" style="font-size:2em"></i>
                <input class="file-upload" type="file" accept="image/*">
            </div>
        </div>
    </div>
</section>
<!-- add-list start-->
<section class="bg-w sp-100" style="padding: 50px 0px 0px;">
    <div class="container">
        <div class="row mb-30">
            <div class="col-12">
                <form class="listing-form" action="#">
                    <div class="row">
                        <div class="col-md-3 col-12 mt-1">
                            <h5><i class="fa fa-cog" aria-hidden="true"></i>&nbsp;Teaching</h5>
                            <h5><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;Noida,India</h5>
                            <h5><i class="far fa-thumbs-up"></i>&nbsp;12,344</h5>
                            <div>
                                <a href="#"><i class="fab fa-facebook" style="color:#3b5999"></i></a>
                                <a href="#"><i class="fab fa-instagram" style="color:#e4405f"></i></a>
                                <a href="#"><i class="fab fa-twitter" style="color:#55acee"></i></a>
                                <a href="#"><i class="fab fa-linkedin" style="color:#0077B5"></i></a>
                            </div>
                        </div>
                        <div class="col-md-7 col-12">
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <h4>Neha Singh</h4>
                                    <p>Neha is extremely talented and help your child to crack any competitive exam (Engineering or Medical). They have best in class infrastructure along with resources which help your child to grasp the concept quite easily. </p>
                                </div>
                                <div class="col-md-2 col-12">
                                    <button class="btn btn-one btn-anim contact">Like</button>
                                </div>
                                <div class="col-md-2 col-12">
                                    <button class="btn btn-one btn-anim contact">Follow</button>
                                </div>
<!--
                                <div class="col-md-2 col-12">
                                    <button class="btn btn-one btn-anim contact">Message</button>
                                </div>
-->
                            </div>
                        </div>
                        <div class="col-md-2 col-12">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12 col-12 mt-2">
                                        <a class="btn btn-one btn-anim callbtn">Call Now</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-12 mt-2">
                                        <a class="btn btn-one btn-anim callbtn">Chat Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row mb-30">
            <div class="col-12">
                <h4 class="title-sep3 mb-30">
                    Payment options
                </h4>
            </div>
            <div class="col-12">
                <form class="listing-form minus-pad" action="#">
                    <div class="row mb-30">
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <i class="fas fa-wallet"></i> <label for="checkbox_id1">Credit Card</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <i class="fas fa-wallet"></i> <label for="checkbox_id2">Debit Card</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <i class="fas fa-wallet"></i> <label for="checkbox_id3">Cash Payment</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <i class="fas fa-wallet"></i> <label for="checkbox_id4">Paytm</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <i class="fas fa-wallet"></i> <label for="checkbox_id5">Google Pay</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <i class="fas fa-wallet"></i> <label for="checkbox_id6">Phone Pe</label>
                        </div>

                    </div>
                </form>
            </div>
        </div>

    </div>
</section>
<!-- add-list end -->

<?php include 'footer.php';?>
