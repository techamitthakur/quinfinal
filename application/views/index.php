<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta NAME="robots" CONTENT="index, follow">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="google-site-verification" content="bq4ZUfIIeNmqo1zB3j3H3O0IzGT153_lI2vGd1IDd9c" />
    <title>Quinn</title>
    <link rel="icon" href="<?php echo base_url('public/assets/');?>images/favicon.ico">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('public/assets/');?>images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('public/assets/');?>images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('public/assets/');?>images/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url('public/assets/');?>images/site.webmanifest">
    <link rel="mask-icon" href="<?php echo base_url('public/assets/');?>images/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="<?php echo base_url('public/assets/');?>images/favicon.ico">
    <meta name="msapplication-TileColor" content="#00aba9">
    <meta name="msapplication-config" content="<?php echo base_url('public/assets/');?>images/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/');?>css/style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/');?>fonts/flaticon.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/');?>css/responsive.css" />
    <script src="<?php echo base_url('public/assets/');?>js/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url('public/assets/');?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url('public/assets/');?>css/style2.css" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Allura&display=swap" rel="stylesheet">
     <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/');?>src/example-styles.css">
<link href="https://fonts.googleapis.com/css?family=Signika+Negative&display=swap" rel="stylesheet">

    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">-->

<!-- 
    <script>
        $(function() {
            $("#accordion").accordion({
                heightStyle: "content"
            });
        });
    </script> -->
    <style>
    
/*================== 
    back-to-top
==================*/
   .spancol{position: absolute;top: 6%;z-index: 99999;left: 45px;font-weight: bolder;color: #fff;font-size: 20px;padding: 5px;font-family: 'Signika Negative', sans-serif;
    }
    #fp-nav{
        display:none;
    }
    
#toTop {
    border: none;
    color: #000;
    font-size: 11px;
    font-weight: bold;
    height: 60px;
    line-height: 60px;
    opacity: 0.8;
    position: fixed;
    top: 12%;
    right: 30px;
    text-align: center;
    text-decoration: none;
    width: 60px;
    z-index: 999;
    text-shadow: 0 0 5px #ffffff;
}

#toTop:hover,
#toTop span {
    opacity: 1;
}

#toTop:active,
#toTop:focus {
    outline: none;
}

#toTop:before,
#toTop:after,
#toTop span {
    content: "";
    display: table;
}

#toTop:before,
#toTop:after,
#toTop span {
    height: 0px;
    margin: auto;
    position: absolute;
    bottom: 20px;
    left: 0;
    right: 0;
    width: 0px;
    -webkit-animation: enlarge_1 2s infinite;
    animation: enlarge_1 2s infinite;
}

#toTop:after {
    -webkit-animation: enlarge_2 2s infinite 0.5s;
    animation: enlarge_2 2s infinite 0.5s;
}

#toTop span {
    -webkit-animation: enlarge_3 2s infinite 0.75s;
    animation: enlarge_3 2s infinite 0.75s;
}


#toBtm {
       border: none;
    color: #000;
    font-size: 11px;
    font-weight: bold;
    height: 60px;
    line-height: 60px;
    opacity: 0.8;
    position: fixed;
    bottom: 5%;
    right: 30px;
    text-align: center;
    transform: rotate(180deg);
    text-decoration: none;
    width: 60px;
    z-index: 999;
    text-shadow: 0 0 5px #ffffff;
}

#toBtm:hover,
#toBtm span {
    opacity: 1;
}

#toBtm:active,
#toBtm:focus {
    outline: none;
}

#toBtm:before,
#toBtm:after,
#toBtm span {
    content: "";
    display: table;
}

#toBtm:before,
#toBtm:after,
#toBtm span {
    height: 0px;
    margin: auto;
    position: absolute;
    bottom: 20px;
    left: 0;
    right: 0;
    width: 0px;
    -webkit-animation: enlarge_1 2s infinite;
    animation: enlarge_1 2s infinite;
}

#toBtm:after {
    -webkit-animation: enlarge_2 2s infinite 0.5s;
    animation: enlarge_2 2s infinite 0.5s;
}

#toBtm span {
    -webkit-animation: enlarge_3 2s infinite 0.75s;
    animation: enlarge_3 2s infinite 0.75s;
}

@-webkit-keyframes enlarge_1 {
    from {
        opcatiy: 1;
        border-left: 2px solid transparent;
        border-right: 2px solid transparent;
        border-bottom: 4px solid #ff3a54;
        margin-top: 0px;
    }
    to {
        opacity: 0;
        border-left: 30px solid transparent;
        border-right: 30px solid transparent;
        border-bottom: 60px solid #ff3a54;
        margin-top: -60px;
    }
}

@keyframes enlarge_1 {
    from {
        opcatiy: 1;
        border-left: 2px solid transparent;
        border-right: 2px solid transparent;
        border-bottom: 4px solid #ff3a54;
        margin-top: 0px;
    }
    to {
        opacity: 0;
        border-left: 30px solid transparent;
        border-right: 30px solid transparent;
        border-bottom: 60px solid #ff3a54;
        margin-top: -60px;
    }
}

@-webkit-keyframes enlarge_2 {
    from {
        opcatiy: 1;
        border-left: 2px solid transparent;
        border-right: 2px solid transparent;
        border-bottom: 4px solid #ff3a54;
        margin-top: -4px;
    }
    to {
        opacity: 0;
        border-left: 30px solid transparent;
        border-right: 30px solid transparent;
        border-bottom: 60px solid #ff3a54;
        margin-top: -60px;
    }
}

@keyframes enlarge_2 {
    from {
        opcatiy: 1;
        border-left: 2px solid transparent;
        border-right: 2px solid transparent;
        border-bottom: 4px solid #ff3a54;
        margin-top: -4px;
    }
    to {
        opacity: 0;
        border-left: 30px solid transparent;
        border-right: 30px solid transparent;
        border-bottom: 60px solid #ff3a54;
        margin-top: -60px;
    }
}

@-webkit-keyframes enlarge_3 {
    from {
        opcatiy: 1;
        border-left: 2px solid transparent;
        border-right: 2px solid transparent;
        border-bottom: 4px solid #ff3a54;
        margin-top: -4px;
    }
    to {
        opacity: 0;
        border-left: 30px solid transparent;
        border-right: 30px solid transparent;
        border-bottom: 60px solid #ff3a54;
        margin-top: -60px;
    }
}

@keyframes enlarge_3 {
    from {
        opcatiy: 1;
        border-left: 2px solid transparent;
        border-right: 2px solid transparent;
        border-bottom: 4px solid #ff3a54;
        margin-top: -4px;
    }
    to {
        opacity: 0;
        border-left: 30px solid transparent;
        border-right: 30px solid transparent;
        border-bottom: 60px solid #ff3a54;
        margin-top: -60px;
    }
}

    .fa-female,
    .fa-globe,
    .fa-bolt {
                color: #e02217!important;
             }
    
    .pdda{
        padding: 0px 30px;
    }
    
    .shift{
            margin-left: 40px;
    }
    
    @media (max-width: 870px){
    .sidr-open #main-nav {
        z-index: 200;
        }
        
        }
        
        h1{
            font-size: 3em;
        }
        a {
            color: #fff;
        }

        a:hover {
            text-decoration: none;
        }

        table {
            font-size: 1em;
        }

        .ui-draggable,
        .ui-droppable {
            background-position: top;
        }


        /*form styles*/
        #msform {
            /*   width: 400px;
            margin: 50px auto;*/
            text-align: center;
            position: relative;
        }

        #msform fieldset {
            background: white;
            border: 0 none;
            border-radius: 10px;
            /*
            box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
*/
            box-shadow: 0 0 19px 2px rgb(229, 53, 81);
            /*
            padding: 30px 50px;
*/
            padding: 30px 10px;

            box-sizing: border-box;
            position: relative;
        }

        /*Hide all except first fieldset*/
        #msform fieldset:not(:first-of-type) {
            display: none;
        }

        /*inputs*/
        #msform input,
        #msform textarea {
            padding: 15px;
            border: 1px solid #ccc;
            border-radius: 3px;
            margin-bottom: 10px;
            box-sizing: border-box;
            font-family: montserrat;
            color: #2C3E50;
            font-size: 13px;
        }

        /*buttons*/
        #msform .action-button {
            width: 100px;
            background: #27AE60;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 1px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 5px;
        }

        #msform .action-button:hover,
        #msform .action-button:focus {
            box-shadow: 0 0 0 2px white, 0 0 0 3px #27AE60;
        }

        /*headings*/
        .fs-title {
            font-size: 30px;
            color: #2C3E50;
            font-weight: bolder;
            margin-bottom: 20px;
            margin-top: 0px;
            font-family: none;
        }

        .fs-subtitle {
            font-weight: normal;
            font-size: 18px;
            color: #666;
            margin-bottom: 20px;
        }

        /*progressbar*/
        #progressbar {
            margin-bottom: 30px;
            overflow: hidden;
            /*CSS counters to number the steps*/
            counter-reset: step;
        }

        #progressbar li {
            list-style-type: none;
            color: white;
            text-transform: uppercase;
            font-size: 9px;
            width: 33.33%;
            float: left;
            position: relative;
        }

        #progressbar li:before {
            content: counter(step);
            counter-increment: step;
            width: 20px;
            line-height: 20px;
            display: block;
            font-size: 10px;
            color: #333;
            background: white;
            border-radius: 3px;
            margin: 0 auto 5px auto;
        }

        /*progressbar connectors*/
        #progressbar li:after {
            content: '';
            width: 100%;
            height: 2px;
            background: white;
            position: absolute;
            left: -50%;
            top: 9px;
            z-index: -1;
            /*put it behind the numbers*/
        }

        #progressbar li:first-child:after {
            /*connector not needed before the first step*/
            content: none;
        }

        /*marking active/completed steps green*/
        /*The number of the step and the connector before it = green*/
        #progressbar li.active:before,
        #progressbar li.active:after {
            background: #27AE60;
            color: white;
        }

        label {
            color: #333;

        }

        input[type="radio"] {
            width: 20px;
            height: 20px;
            margin-bottom: 0px !important;
        }

        h1 {
            font-size: 3em;
        }

        a:hover {
            color: #fff;

        }
        
        body{
background: url(https://quinn.in/public/assets/backka.png);}


        #social-icon{
                bottom: 0px;
                position: fixed;
                right: 47%;
                /* top: 65%; */
                -webkit-writing-mode: vertical-lr;
        }
       @media screen and (max-width: 768px) {
           .shift{
    margin-left: auto!important;

    }
              body{
background: url(https://quinn.in/public/assets/backka.png);}

           .pdda{
               padding: 0px 15px;
           }
          .box p{
            font-size: 14px;
          }
          h2{
                font-size: 35px;
                    margin-top: 50px;


          }
          .tooltip{
            font-size: 15px;
          }
          .formc{
                margin-bottom: 10px;

          }

           #msform fieldset {
            background: white;
            border: 0 none;
            border-radius: 10px;
            /*
            box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
*/
            box-shadow: 0 0 19px 2px rgb(229, 53, 81);
            /*
            padding: 30px 50px;
*/
            padding: 20px 10px;

            box-sizing: border-box;
            position: relative;
        }

.gh{
        height: auto;
    float: left;
    color: #fff;
    width: 47%;
    box-sizing: border-box;
    border-radius: 4px;
    font-family: Open Sans;
    border: none;
    font-weight: 300;
    font-size: 10px;
    margin-right: 3%;
    padding: 4px;
    background: rgba(0, 0, 0, .5);
    margin-bottom: 10px;
    -webkit-transition: .3s;
    -o-transition: .3s;
    transition: .3s;
    }

    #preloader img {
    position: absolute;
    top: 25%;
    left: 40%;
    /*margin-left: -140px;*/
}
 
        }
        @media (max-width: 640px) and (min-width: 320px){
            .ttl{
                    line-height: 1.2;
            }
            
            .spancol{
position: absolute;
    top: 3%;
    z-index: 99999;
    left: 18px;
    font-weight: bolder;
    color: #fff;
    font-size: 12px;
    padding: 5px;
    font-family: 'Signika Negative', sans-serif;
    }
 
            
            .active .screenshots-wrapper {
    max-width: 190px;
    margin-top: -130px;
    margin-right: -100px;
}
#social-icons {
      top: 3%;
    left: 33%!important;
}

        }
 @media (max-width: 374px){
            h1{
                    font-size: 2em;

            }
            p{
                    font-size: 12px;

            }
            #preloader img {
    position: absolute;
    top: 15%;
    left: 20%;
}
#social-icons {
       top: 3%;
    left: 38%;
}
 
}

        .browser-mobile .overlay {
    opacity: 1;
    background: url(../images/bg.jpg) no-repeat 0 0;
    background-size: cover;
        background: #000;
    opacity: 0.7;
}

.col-md-6{
    position:static !important;
}

@media (max-width: 320px){
#preloader img {
    position: absolute;
    top: 30%;
    left: 40%;
}
}
   </style>

</head>

<body >

    <!-- PRELOADER -->
    <div id="preloader"><img src="<?php echo base_url('public/assets/');?>images/logo.png" alt="" style="z-index: 99999"></div>
    <div class="overlay"></div>

    <!-- MAIN NAV -->
<!--    <a id="main-nav" href="#sidr"><span class="flaticon-menu9"></span></a>
-->    <div id="sidr" class="sidr">

        <!-- MAIN NAV LOGO -->
        <a href="<?php echo base_url(); ?>" id="menu-logo" ><img src="<?php echo base_url('public/assets/');?>images/logo.png" alt="" style="width: 100%;"></a>

        <!-- MAIN NAV LINKS -->
        <ul>
            <li><a href="#Home"><span class="icons flaticon-house3"></span>Home</a>
            </li>
            <li><a href="#Features"><span class="icons flaticon-drawer1"></span>Features</a>
            </li>
            <!--<li><a href="#About"><span class="icons flaticon-cursor7"></span>About</a>
            </li>
            <li><a href="#Video"><span class="icons flaticon-key9"></span>Video</a>
            </li>
            <li><a href="#Clients"><span class="icons flaticon-comment2"></span>Clients</a>
            </li>-->
            <li><a href="#Video"><span class="icons flaticon-camera19"></span>Details</a>
            </li>
            <li><a href="#Clients"><span class="icons flaticon-download11"></span>Download</a>
            </li>
            <li><a href="#Screenshots"><span class="icons flaticon-small72"></span>Contact</a>
            </li>
        </ul>
        <!-- END MAIN NAV LINKS -->
    </div>
    <!-- END MAIN NAV -->

    <!-- PAGE LOGO -->
    <div class="wrap">
        <div id="logo">
            <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url('public/assets/');?>images/newlogo.png" alt="" style="width: 55px; top:0px;"> </a>
            <h4 style="color: #fff;margin: 0px -45px;padding: 0px;font-family: 'Allura', cursive;font-size: 25px;">Promote Yourself</h4>
        </div>
    </div>
    <!-- END PAGE LOGO -->

    <!-- LANDING PAGE CONTENT -->
    <div id="fullpage">

        <!-- RIGHT HAND & PHONE MOCK-UP IMAGES -->
        <div class="wrap">
            <div class="section-image">
                <!-- Home IMAGE --><img src="<?php echo base_url('public/assets/');?>images/1.png" alt="">
                <!-- Features IMAGE --><img src="<?php echo base_url('public/assets/');?>images/2.png" alt="">
                <!-- About IMAGE --><img src="<?php echo base_url('public/assets/');?>images/3.png" alt="">
                <!-- Video IMAGE --><img src="<?php echo base_url('public/assets/');?>images/4.png" alt="">
                <!-- Clients IMAGE --><img src="<?php echo base_url('public/assets/');?>images/6.png" alt="">
                <!-- Screenshots IMAGE --><img src="<?php echo base_url('public/assets/');?>images/5.png" alt="">
            </div>
            <div id="hand"></div>
        </div>
        <!-- END RIGHT HAND & PHONE MOCK-UP -->
<!--<span class="spancol">HELLO!</span>
-->        <!-- SECTION HOME -->
        <div class="section " id="section0">
            <div class="wrap">
                <div class="box">
                    <!-- SECTION HOME CONTENT -->
                    <h1 class="ttl">QUINN App is India’s First <strong >'WOMEN ONLY'</strong>  Service Provider App</h1>
                    <p>Quinn is a platform for Women which includes not only the Working Professionals, but Housewifes, College Students or any girl/ woman seeking Freelancing work and wants to be independent. We are here to provide a Social Networking Platform for people to connect with such strong, independent women, who are adept at providing products & services in various categories.</p> <a href="#Features" class="simple-button">Know More</a>
                </div>
                <!-- END SECTION HOME CONTENT -->
            </div>
        </div>
        <!-- END SECTION HOME -->

         <!-- SECTION HOME -->
        <div class="section" id="section0">
            <div class="wrap">
                <div class="box" >
                   
                        <!-- progressbar -->
                        <!--  <ul id="progressbar">
                            <li class="active">Account Setup</li>
                            <li>Social Profiles</li>
                            <li>Personal Details</li>
                        </ul>-->
                        <!-- fieldsets -->
                        
                   <form id="msform" method="post" class="mystyle" action="<?php echo site_url('user/doServey');?>">
                        <div id="frmmain" >
                            <fieldset style="z-index:999">
                                <h2 class="fs-title">On Behalf of Whom</h2>
                                <div class="col-md-6" style="float: left;">
                                  <input type="radio" name="gender" value="Self"  checked>
                                    <label for="male">Self&nbsp;</label><br>
                                </div>
                                <div class="col-md-6">
                                    <input type="radio" name="gender"  value="Others">
                                    <label for="male">Others</label><br>
                                </div>
                                <input type="button" name="next" class="next action-button" value="Next" required />
                            </fieldset>
                            <fieldset style="z-index:999">
                                <h2 class="fs-title">Looking a Job without a BOSS ?</h2>
                                <input type="button" name="work" class="aa next action-button" value="Yes" required />
                                <!-- Trigger the modal with a button -->
                               <!--  <button type="button" class="aa btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="margin-top: 0px;">Yes</button> -->
                               <input type="button" id="button" name="work" class="aa action-button" value="No" required />
                            </fieldset>
                            <fieldset style="z-index:999">
                                <h2 class="fs-title">Which services you can provide ?</h2>
                                <div style="color:black">
                                    <!-- content here -->

                                    <style type="text/css">
                                        div>p>select {

                                            /float: left;
                                        }

                                    </style>
                                    <div class="col-xs-12">
                                        <div class="padda" style="float: left;">
                                            <input type="checkbox" name="beauty" id="beauty" value="beauty">
                                            <label for="male">Beauty&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><br>
                                            <input type="checkbox" name="teaching" id="teaching" value="teaching">
                                            <label for="male">Teaching&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><br>
                                            <input type="checkbox" name="fitness" id="fitness" value="fitness">
                                            <label for="male">Fitness&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><br>
                                            <input type="checkbox" name="office" id="office" value="office">
                                            <label for="male">Office work&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><br>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="medical" id="medical" value="medical">
                                            <label for="male">Medical&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><br>
                                            <input type="checkbox" name="food" id="food" value="food">
                                            <label for="male">Food&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><br> <input type="checkbox" name="tailoring" id="tailoring" value="tailoring">
                                            <label for="male">Tailoring&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><br><input type="checkbox" name="others" id="others" value="Professional">
                                            <label for="male">Professional</label><br>
                                        </div>
                                    </div>

                                                       
                     </div>
                     <style type="text/css">
                         .as{
                 
    color: #b9b9b9!important;
    min-width: 130px;

    text-decoration: none;
border: 1px solid #b9b9b9 !important;
    /* float: left; */
    -webkit-transition: .3s;
    -o-transition: .3s;
    transition: .3s;
    background: #ffffff!important;
                         }

                     </style>
                    <input type="button" name="previous" class="aa previous action-button" value="Previous" required />
                              
                    <input type="button" name="next" class="aa next action-button" id="check24" value="Next" required />
                    <input type="button"  class="as action-button" id="check4" value="Next" required />
                </fieldset>
                   
                              <fieldset style="z-index:999">
                                <h2 class="fs-title">Please fill the form below</h2>
<!--                                <h3 class="fs-subtitle"</h3>
-->                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" placeholder="Full Name" required="" />
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="phone" placeholder="Phone No."  maxlength="10" pattern="^[6-9]\d{9}$" title="Please enter Valid 10 Number" required="" />
                                </div>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" placeholder="Email" required="" title="Enter Valid email" /> </div>
                                <div class="col-md-6">
                                    <select class="form-control formc" id="services" name="services" STYLE="DISPLAY:NONE;">
                                        <option value="null"  disabled>Select Service</option>
                                        <option value="NOTHING" selected>NOTHING</option>
                                        <option value="Teaching">Teaching</option>
                                        <option value="Fitness">Fitness</option>
                                        <option value="Office">Office work</option>
                                        <option value="Medical" >Medical</option>
                                        <option value="Food">Food</option>
                                        <option value="Tailoring">Tailoring</option>
                                        <option value="Professional">Professional</option>
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <textarea class="form-control" name="area" placeholder="Post - if you are looking for any other services !"></textarea>
                                </div>
            <!--                        <input type="button" name="previous" class="aa previous action-button" value="Previous" required style="float: left;" />-->
                    <input type="button" name="previous" class="aa previous action-button" value="Previous" required  style="float: left;" />
                    <input type="submit" name="submit" class="aa action-button" value="Submit" required style="float: right;" />          
                   <!--  <input type="button" name="next" class="aa next action-button"  value="Submit" style="float: right;" />
                    --> <!--<input type="button"  class="aa action-button" id="check4" value="next" required />-->
                            </fieldset>
                                <!--  <fieldset style="z-index:999">
                                <h4 class="fs-title">Would you like to  use the APP if u found your working criteria in application?</h4>
                                    <div style="text-align: center">
                                    <img src="<?php echo base_url('public/assets/');?>images/checked.png"><h4 style="color: #333">Submited</h4>
                                    </div>
                                <div style="text-align: center;display:inline-block;">
                            <a href="#" class="simple-button appstore-button btnsss"><span class="icon appstore"></span>App Store</a> 
                            <a href="#" class="simple-button playstore-button btnsss"><span class="icon playstore"></span>Play Store</a>
                                </div>
                        </fieldset> -->
                        </div>
                           
                        <fieldset id="frmsub" class="disp" style="">
                            <div style="text-align: center">
                                    <img src="<?php echo base_url('public/assets/');?>images/checked.png"><h4 style="color: #333">                   
                                    Submited Successfully</h4>
                                    </div>
                                <div style="text-align: center;display:inline-block;">
                                
                            <a href="#" class="simple-button appstore-button btnsss"><span class="icon appstore"></span>App Store</a> 
                            <a href="#" class="simple-button playstore-button btnsss"><span class="icon playstore"></span>Play Store</a>
                                </div>
                        </fieldset>
                        </form>
                    </div>
                <!-- END SECTION HOME CONTENT -->
            </div>
        </div>
        <!-- END SECTION HOME -->

        <!-- SECTION FEATURES -->
        <div class="section " id="section1">
            <div class="wrap">
                <div class="box">
                    <!-- SECTION FEATURES CONTENT -->
                    <h2>Showcase your hidden <strong>skills &amp; talent</strong></h2>
                    <p>Quinn app focuses on your interest such as art , music, dance, beauty, teaching, fashion, cooking and many more! Showcase your work, share your thoughts and ideas freely with the community.</p>
                    <!-- END SECTION FEATURES CONTENT -->
                </div>
            </div>
        </div>
        <!-- END SECTION FEATURES -->

        <!-- SECTION SCREENSHOTS -->
        <div class="section" id="section5">
            <div class="wrap">
                <div class="box">
                    <div class="fp-tableCell screenshots-content">
                        <!-- SECTION SCREENSHOTS CONTENT -->
                        <h2>Our App Focuses on</h2>
                        <ul class="features">
                             <li><a class="tooltip"><span class="icon flaticon-laptop3"></span>Its an Social networking hub for women service provider.</a> </li>
                            <li><a class="tooltip"><span class="fa fa-female"></span> India's first only women service provider app.</a> </li>
                            <li><a class="tooltip"><span class="fa fa-globe"></span> Empower women, develop the nation.</a> </li>
                            <li><a class="tooltip"><span class="fa fa-bolt"></span> Let the equality bloom.</a> </li> 
                           
                        </ul>
                        <!-- END SECTION SCREENSHOTS CONTENT -->
                    </div>

                </div>
                <div class="screenshots-wrapper">

                    <!-- SECTION SCREENSHOTS IMAGES -->
                    <div class="slide" id="slide1" data-anchor="slide1"> <img src="<?php echo base_url('public/assets/');?>images/1.png" alt=""> </div>
                    <div class="slide" id="slide2" data-anchor="slide2"> <img src="<?php echo base_url('public/assets/');?>images/2.png" alt=""> </div>
                    <div class="slide" id="slide3" data-anchor="slide3"> <img src="<?php echo base_url('public/assets/');?>images/3.png" alt=""> </div>
                    <div class="slide" id="slide4" data-anchor="slide4"> <img src="<?php echo base_url('public/assets/');?>images/4.png" alt=""> </div> 
                    <div class="slide" id="slide5" data-anchor="slide5"> <img src="<?php echo base_url('public/assets/');?>images/5.png" alt=""> </div> 
                    <div class="slide" id="slide6" data-anchor="slide6"> <img src="<?php echo base_url('public/assets/');?>images/6.png" alt=""> </div>
                   
                    <!-- END SECTION SCREENSHOTS IMAGES -->
                </div>
            </div>
        </div>
        <!-- END SECTION SCREENSHOTS -->

        <!-- SECTION DOWNLOAD -->
        <div class="section" id="section7" data-anchor="Download">
            <div class="wrap">
                <div class="box">
                    <!-- SECTION DOWNLOAD CONTENT-->
                    <h2><strong>App</strong> Coming Soon!</h2>
                 <!--   <p>it is a Social Networking platform for women to express their views, skills &amp; potential.<br><br>  Coming soon on App Store and Play Store!</p>-->
                    <!-- DOWNLOAD APPSTORE-->
                    <a href="#" class="simple-button appstore-button shift" ><span class="icon appstore"></span>App Store</a>
                    <!-- DOWNLOAD PLAYSTORE-->
                    <a href="#" class="simple-button playstore-button"><span class="icon playstore"></span>Play Store</a>

                    <!-- END SECTION DOWNLOAD -->
                </div>
            </div>
        </div>
        <!-- END SECTION DOWNLOAD -->


        <!-- SECTION CONTACT -->
        <div class="section" id="section8">
            <div class="wrap">
                <div class="box">
                    <!-- SECTION CONTACT CONTENT-->
                    <h2><strong>Get</strong> in touch</h2>
                    <ul class="features">
                        <li><a class="tooltip" href="#"><span class="icon flaticon-telephone1"></span> Call Us now<span class="tooltip-content"><span class="tooltip-text"><span class="tooltip-inner"><span class="icon flaticon-telephone1"></span>Support : 0120 4981750 </span></span></span></a> </li>
                    <!--    <li><span class="tooltip"><span class="icon flaticon-map5"></span>See our location<span class="tooltip-content"><span class="tooltip-text"><span class="tooltip-inner"><span class="icon flaticon-map5"></span>2nd Floor, Office No, H - 61, Sector 63 Rd, Near Nirala World, Noida, Uttar Pradesh 201309
                                            <br><a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3501.958739745565!2d77.37951061508254!3d28.63099868241844!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cef0c707b75a3%3A0x96fa69a73269420c!2sAD+Corporate+Solutions+-+Bonfiglioli+Gearbox+Dealer+%26+Distributor+in+Delhi%2FGujrat%2FMaharshtra!5e0!3m2!1sen!2sin!4v1564491047698!5m2!1sen!2sin" target="_blank">View Map</a>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        </li>-->
                          <li><a class="tooltip" href="#"><span class="icon flaticon-mail9"></span> Send Email<span class="tooltip-content"><span class="tooltip-text"><span class="tooltip-inner"><span class="icon flaticon-mail9"></span><span>info@quinn.in</span></span></span></span></a> </li>
                          
                    </ul>
                    <ul class="features">
                        <li><span class="tooltip"><span class="icon flaticon-cursor7"></span>Official Website <span class="tooltip-content"><span class="tooltip-text"><span class="tooltip-inner"><span class="icon flaticon-cursor7"></span>
                                            <br> <a href="https://quinn.in/" target="_blank">www.quinn.in</a>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        </li>
                    
                    </ul>
                    <style type="text/css">
                        
                        .gh{
                            outline: none;
                        background: 0 0;
                        float: left;
                        color: #fff;
                        width: 47%;
                        height: 46px;
                        box-sizing: border-box;
                        border-radius: 4px;
                        font-family: Open Sans;
                        border: none;
                        font-weight: 300;
                        font-size: 13px;
                        margin-right: 3%;
                        padding: 10px;
                        background: rgba(0, 0, 0, .5);
                        margin-bottom: 0px;
                        -webkit-transition: .3s;
                        -o-transition: .3s;
                        transition: .3s;
                        }
                        @media (max-width: 640px) and (min-width: 320px){
                    #contact-form textarea, #contact-form input {
                        font-size: 11px;
                        margin-right: 3%;
                        padding: 5px;
                        height: 47px;
                    }
                    }
                    </style>
                    <!-- SECTION CONTACT FORM-->
                    <form role="form" method="post" class="getintouch" id="contact-form" action="<?php echo site_url('user/getintouch');?>">
                        <input type="text" placeholder="Name" name="name" id="Name" autocomplete="off" required>
                        <input type="email" placeholder="Email" name="email" id="Email" autocomplete="off" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" placeholder="Email" required="" title="Enter Valid email">
                        <input type="text" placeholder="Phone" name="phone" id="Phone" autocomplete="off"  maxlength="10" pattern="^[6-9]\d{9}$" title="Please enter Valid number" required="">
                         <select class="form-control gh" id="Subject" name="services" required style="-webkit-appearance: none;">
                                        <option selected disabled>Select Service</option>
                                        <option value="Beauty">Beauty Services</option>
                                        <option value="Teaching">Teaching</option>
                                        <option value="Fitness">Fitness</option>
                                        <option value="Office">Office work</option>
                                        <option value="Medical">Medical</option>
                                        <option vlaue="Food">Food</option>
                                        <option value="Tailoring">Tailoring</option>
                                        <option value="Professional">Professional</option>
                                    </select>
                        <textarea placeholder="Message" name="area" id="Message" ></textarea>
                        <input type="submit" name="submit" id="submitd" class="btn btn-danger" value="Send" style="background: red; float: left;">
                        <div id="success"></div>
                    </form>
                    <!-- END SECTION CONTACT -->
                </div>
            </div>
        </div>
    </div>
    <!-- SECTION CONTACT -->

    <!-- SOCIAL ICONS -->
    <div class="wrap" style="z-index: 9976;">
        <div id="social-icons">
            <ul>
                <li><a href="https://www.facebook.com/thequinnapp/"><i class="flaticon-facebook6"></i></a> </li>
                <li><a href="https://twitter.com/thequinnapp"><i class="flaticon-social19"></i></a> </li>
                <li><a href="https://www.instagram.com/thequinnapp/"><i class="fa fa-instagram"></i></a> </li>  
                <li><a href="https://www.youtube.com/channel/UCeynl1Sze4XVAgItI-xjtAw"><i class="fa fa-youtube-play"></i></a> </li>

            </ul>
        </div>
    </div>
    <!-- END SOCIAL ICONS -->
 
      <!-- UP ICONS -->
   <div class="wrap" style="z-index: 99999;">
    <div id="up-icons" >
            <ul>
                <li>
                      <a id="toTop" title="Go to top" href="#Home">
                    <span id="toTopHover"></span>TOP
                </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- END UP ICONS -->
        
      <!-- bottom ICONS -->
   <div class="wrap" style="z-index: 99998;">
    <div id="up-icons">
            <ul>
                <li>
                       <a id="toBtm" title="Go to top" href="#Screenshots">
                    <span id="toTopHover"></span><p style="transform: rotate(180deg);">Bottom</p>
                </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- END bottom ICONS -->
    
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!--Model end here-->

  <!-- SCRIPTS -->
    <script src="<?php echo base_url('public/assets/');?>js/jquery.min.js"></script>
    <script src="<?php echo base_url('public/assets/');?>js/bootstrap.min.js"></script>

    <script src="<?php echo base_url('public/assets/');?>js/script.js"></script>
    <script src="<?php echo base_url('public/assets/');?>js/jquery.easings.min.js"></script>
    <script src="<?php echo base_url('public/assets/');?>js/jquery.fullPage.js"></script>
    <script src="<?php echo base_url('public/assets/');?>js/cbpFWTabs.js"></script>
    <script src="<?php echo base_url('public/assets/');?>js/jquery.sidr.min.js"></script>
    <script src="<?php echo base_url('public/assets/');?>js/scripts.js"></script>
    <!-- <script type="text/javascript" src="<?php echo base_url('public/assets/');?>js/video.js"></script> -->
     <!-- <script type="text/javascript" src="<?php echo base_url('public/assets/');?>src/jquery-2.2.4.min.js"></script> -->
    <script type="text/javascript" src="<?php echo base_url('public/assets/');?>src/jquery.multi-select.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
     <script>
         $(document).ready(function(){
     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('#back-to-top').tooltip('show');
        });
     </script>
     <script type="text/javascript">
    // $(function(){
    //     $('#beauty').multiSelect();
    //     $('#teaching').multiSelect();
    //     $('#food').multiSelect();
    //     $('#tailoring').multiSelect();
    //     $('#fitness').multiSelect();
    //     $('#medical').multiSelect();
    //     $('#office').multiSelect();
    //     $('#others').multiSelect();
        
    // });

$( document ).ready(function() {

     $("#frmsub").hide();
     $("#frmmain").show();
     
});

    $(document).on('submit', '#msform', function (evt) {
//            alert("okk");

            evt.preventDefault();
            var url = $(this).attr("action");
            var postdata = $(this).serialize();
            $.post(url, postdata, function (out) {
                var don=JSON.parse(out);
                // alert(don.result);
                if (don.result === 1) {
               swal("Thank You ","your request has been submitted successfully","success");
               setTimeout("window.location='https://quinn.in/'",3000);     
                    //location.reload(true);
                   $("#frmsub").show();
                    $("#frmmain").hide();

                    // alert("Data Inserted")
                }else{
                    //  $("#frmsub").hide();
                    // $("#frmmain").show();
                      swal("Please try again after some time");
                }
            });
        });

   $(document).on('submit', '#contact-form', function (evt) {
           // alert("okk"); die;
               // $("#submitd").disabled();
            $("#submitd").attr("disabled", true);
            evt.preventDefault();
            var url = $(this).attr("action");
            var postdata = $(this).serialize();
            $.post(url, postdata, function (out) {
                var don=JSON.parse(out);
                // alert(don.result);
               
                if (don.result === 1) {
                  swal("Thank You ","your request has been submitted successfully","success");
                    setTimeout("window.location='https://quinn.in/'",3000);}else{

                    swal("Error","Please try again after some time");
                }
            });
        });
        $("#button").click(function() {
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#section8").offset().top
            }, 2000);
        });
        
        $(document).on('click','.multi-select-menuitem', function(){
            // alert("okkkk")
            // if($(".multi-select-menuitem").children("input").prop("checked") == true){
            //         alert("okkkk")
            // }else{
            //         alert("hiii")
            // }
        })

$(document).ready(function(){
        //alert("hiiii")


   // alert("ldkhfkjd");
// $("#check24").attr("disabled", true);
$('#check24').hide();
$('#check4').show();

// $('#check4').click(function(){

$('#medical,#food,#tailoring,#teaching,#fitness,#office,#others,#beauty').click(function(){
if(document.getElementById('medical').checked || document.getElementById('fitness').checked || document.getElementById('office').checked  || document.getElementById('teaching').checked  || document.getElementById('tailoring').checked  || document.getElementById('others').checked  || document.getElementById('beauty').checked  || document.getElementById('food').checked) {
    // $("#txtAge").show();
    $('#check24').show();
    $('#check4').hide();

   
} else {
   $('#check24').hide();
   $('#check4').show();     
}
});
});
//     $(document).ready(function(){
//     $(".inputTextBox").keypress(function(event){
//         var inputValue = event.charCode;
//         if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)){
//             alert("Only Text allowed");
//             event.preventDefault();
//         }
//     });
// });


// $(document).ready(function(){
//     $(".inputTextNotAllow").keypress(function(event){
//         var inputValue = event.charCode;
//         if((inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)){
//                 swal("Only Numbers allowed");
//             event.preventDefault();
//         }
//     });
// });
    </script>

</body></html>
