<?php include 'header.php';?>

<!-- Services start -->
<section class="bg-w tri-bg sp-100-70 o-hide mainpadd">
    <div class="container">
        <h1 class="text-center">Select Your Service</h1>
        <div class="row justify-content-md-center">
            <div class="col-lg-3 col-md-6">
                <input type="text" class="form-control" placeholder="What are you looking for?">
            </div>
            <div class="col-lg-3 col-md-6">
                <select class="form-control custom-select" id="service2">
                    <option>all categories</option>
                    <option>Tution Teacher</option>
                    <option>Dance teacher </option>
                    <option>Beautician</option>
                    <option>House Maids</option>
                </select>

            </div>
            <div class="col-lg-2 col-md-4">
                <button type="submit" class="btn btn-one btn-anim br-5 w-100 mb-30">
                    <i class="fa fa-search"></i> search</button>
            </div>
        </div>
        <div class="list-category-slider owl-carousel owl-theme mb-60">
            <div class="list-items2 btn-anim">
                <div class="icon-box">
                    <i class="flaticon-find"></i>
                </div>
                <h5 class="mb-0 mt-3">
                    <a href="#">Tution Teacher </a>
                </h5>
            </div>
            <div class="list-items2 btn-anim">
                <div class="icon-box">
                    <i class="flaticon-spa"></i>
                </div>
                <h5 class="mb-0 mt-3">
                    <a href="#">Dance Teacher</a>
                </h5>
            </div>
            <div class="list-items2 btn-anim">
                <div class="icon-box">
                    <i class="flaticon-cheers"></i>
                </div>
                <h5 class="mb-0 mt-3">
                    <a href="#">Beautician</a>
                </h5>
            </div>
            <div class="list-items2 btn-anim">
                <div class="icon-box">
                    <i class="flaticon-hotel"></i>
                </div>
                <h5 class="mb-0 mt-3">
                    <a href="#">PPT Preparation</a>
                </h5>
            </div>
            <div class="list-items2 btn-anim">
                <div class="icon-box">
                    <i class="flaticon-cutlery"></i>
                </div>
                <h5 class="mb-0 mt-3">
                    <a href="#">Yoga</a>
                </h5>
            </div>
            <div class="list-items2 btn-anim">
                <div class="icon-box">
                    <i class="flaticon-shop"></i>
                </div>
                <h5 class="mb-0 mt-3">
                    <a href="#">Child Day Care</a>
                </h5>
            </div>
            <div class="list-items2 btn-anim">
                <div class="icon-box">
                    <i class="flaticon-drama"></i>
                </div>
                <h5 class="mb-0 mt-3">
                    <a href="#">House maids</a>
                </h5>
            </div>
        </div>

    </div>
</section>
<!-- Services start -->


<!-- categories start-->
<section class="categories sp-100-70 bg-dull">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="all-title">
                    <h3 class="sec-title">
                        top categories
                    </h3>
                    <svg class="title-sep">
                        <path fill-rule="evenodd" d="M32.000,13.000 L32.000,7.000 L35.000,7.000 L35.000,13.000 L32.000,13.000 ZM24.000,4.000 L27.000,4.000 L27.000,16.000 L24.000,16.000 L24.000,4.000 ZM16.000,-0.000 L19.000,-0.000 L19.000,20.000 L16.000,20.000 L16.000,-0.000 ZM8.000,4.000 L11.000,4.000 L11.000,16.000 L8.000,16.000 L8.000,4.000 ZM-0.000,7.000 L3.000,7.000 L3.000,13.000 L-0.000,13.000 L-0.000,7.000 Z" />
                    </svg>
                    <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return</p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-30">
                <div class="ctg-item">
                    <div class="icon-box" style="background-image:url('assets/img/home/cat1.jpg')">
                        <i class="flaticon-map"></i>
                    </div>
                    <div class="content-box p-4">
                        <h5 class="mb-1">
                            <a href="#">Tution teacher </a>
                        </h5>
                        <p class="mb-0">35 Services</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-30">
                <div class="ctg-item">
                    <div class="icon-box" style="background-image:url('assets/img/home/cat2.jpg')">
                        <i class="flaticon-cutlery"></i>
                    </div>
                    <div class="content-box p-4">
                        <h5 class="mb-1">
                            <a href="#">Dance teacher</a>
                        </h5>
                        <p class="mb-0">20 Services</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-30">
                <div class="ctg-item">
                    <div class="icon-box" style="background-image:url('assets/img/home/cat3.jpg')">
                        <i class="flaticon-shop"></i>
                    </div>
                    <div class="content-box p-4">
                        <h5 class="mb-1">
                            <a href="#"> Beautician</a>
                        </h5>
                        <p class="mb-0">15 Services</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-30">
                <div class="ctg-item">
                    <div class="icon-box" style="background-image:url('assets/img/home/cat4.jpg')">
                        <i class="flaticon-hotel"></i>
                    </div>
                    <div class="content-box p-4">
                        <h5 class="mb-1">
                            <a href="#"> PPT Preparation</a>
                        </h5>
                        <p class="mb-0">12 Services</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-30">
                <div class="ctg-item">
                    <div class="icon-box" style="background-image:url('assets/img/home/cat5.jpg')">
                        <i class="flaticon-spa"></i>
                    </div>
                    <div class="content-box p-4">
                        <h5 class="mb-1">
                            <a href="#"> Yoga Teacher</a>
                        </h5>
                        <p class="mb-0">18 Services</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-30">
                <div class="ctg-item">
                    <div class="icon-box" style="background-image:url('assets/img/home/cat6.jpg')">
                        <i class="flaticon-drama"></i>
                    </div>
                    <div class="content-box p-4">
                        <h5 class="mb-1">
                            <a href="#"> Child Day Care </a>
                        </h5>
                        <p class="mb-0">14 Services</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-30">
                <div class="ctg-item">
                    <div class="icon-box" style="background-image:url('assets/img/home/cat7.jpg')">
                        <i class="flaticon-cheers"></i>
                    </div>
                    <div class="content-box p-4">
                        <h5 class="mb-1">
                            <a href="#"> House maids</a>
                        </h5>
                        <p class="mb-0">25 Services</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-30">
                <div class="ctg-item">
                    <div class="icon-box" style="background-image:url('assets/img/home/cat8.jpg')">
                        <i class="flaticon-fast-food"></i>
                    </div>
                    <div class="content-box p-4">
                        <h5 class="mb-1">
                            <a href="#"> Zumba Teacher</a>
                        </h5>
                        <p class="mb-0">10 Services</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- categories end -->

<!-- app start-->
<div class="app-section bg-red tri-bg-w sp-100">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 d-none d-lg-block">
                <div class="app-img mr-5">
                    <img src="assets/img/home/app-phn.png" alt="lister-app">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="app-content text-lg-left text-center">
                    <h3>Android App and iOS App Available </h3>
                    <p class="mb-4">Quinn Android / ios App is also available at store and play store</p>
                    <div class="app-icon">
                        <a href="#" class="mr-4">
                            <img src="assets/img/home/google-play.png" alt="google-play">
                        </a>
                        <a href="#">
                            <img src="assets/img/home/play-store.png" alt="play-store">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- app end -->
<?php include 'footer.php';?>
