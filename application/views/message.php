<?php include 'header.php';?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?php echo base_url('public/')?>fassets/css/style3.css">
<!--
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
-->


<style>
    section {
        /*   padding-top: 80px;
        padding-bottom: 60px;*/
    }

    ::-webkit-scrollbar {
        width: 2px;
    }


    ::-webkit-scrollbar-thumb {
        background: #000;
        border-radius: 5px;
    }

    .chat-list-box {
        display: inline-block;
        width: 100%;
        background: #fff;
        box-shadow: 0px 10px 30px 0px rgba(50, 50, 50, 0.16);
    }

    .flat-icon li {
        display: inline-block;
        padding: 0px 8px;
        vertical-align: middle;
        position: relative;
        top: 7px;
    }

    .flat-icon a img {
        width: 22px;
        border-radius: unset !important;
    }

    ul.list-inline.text-left.d-inline-block.float-left {
        margin-bottom: 0;
    }

    .chat-list-box ul li img {
        border-radius: 50px;
    }

    .message-box {
        display: inline-block;
        width: 100%;
        background: #fff;
        box-shadow: 0px 10px 30px 0px rgba(50, 50, 50, 0.16);
    }

    .msg-box li {
        display: inline-block;
        padding-left: 10px;
    }

    .msg-box img {
        border-radius: 50px;
    }

    .msg-box li span {
        padding-left: 8px;
        color: #545454;
        font-weight: 550;
    }

    .head-box {
        display: flow-root;
        padding: 10px;
        background: #ff3a54;
    }

    .head-box ul li a {
        color: #fff;
    }

    .chat-person-list {
        padding: 14px;
    }

    .chat-person-list ul li img {
        width: 30px;
        border-radius: 50px;
    }

    .chat-person-list ul li span {
        padding-left: 20px;
    }

    .chat-person-list ul li {
        line-height: 55px;
    }

    span.chat-time {
        float: right;
        font-size: 12px;
    }

    .head-box-1 {
        display: flow-root;
        padding: 10px;
        background: #ff3a54;
    }

    .head-box-1 ul li i {
        color: #fff;
        cursor: pointer;
    }

    .head-box-1 ul li span {
        color: #fff;
        position: relative;
        top: -10px;
    }

    .msg_history {
        padding: 10px;
        height: 280px;
        overflow: overlay;
    }

    .incoming_msg_img {
        display: inline-block;
        width: 6%;
    }

    .timee {
        position: absolute;
        left: 115px;
        top: 30px;
        color: #fff;
    }

    .received_msg {
        display: inline-block;
        padding: 0 0 0 10px;
        vertical-align: top;
        width: 92%;
    }

    .received_withd_msg {
        width: 57%;
    }

    .received_withd_msg p {
        background: #000;
        border-radius: 3px;
        color: #ffffff;
        font-size: 14px;
        margin: 0;
        padding: 5px 10px 5px 22px;
        width: 100%;
        border-bottom-right-radius: 50px;
        border-top-left-radius: 50px;
    }

    .time_date {
        color: #747474;
        display: block;
        font-size: 12px;
        margin: 8px 0 0;
    }

    .incoming_msg_img img {
        width: 100%;
        border-radius: 50px;
        float: left;
    }

    .outgoing_msg {
        overflow: hidden;
        margin: 10px 0 10px;
    }

    .sent_msg {
        float: right;
        width: 46%;
    }

    .sent_msg p {
        background: #ddd;
        border-radius: 3px;
        font-size: 14px;
        margin: 0;
        color: #333;
        padding: 5px 10px 5px 22px;
        width: 100%;
        border-bottom-right-radius: 50px;
        border-top-left-radius: 50px;
    }

    .chat-person-list ul li a {
        color: #545454;
        text-decoration: none;
    }

    .attachement {
        background: #777;
        position: absolute;
        width: 220px;
        right: 30%;
        top: 42px;
        display: none;
    }

    .attachement ul li {
        display: -webkit-inline-box;
        margin: 0px 19px 15px 20px;
    }

    .attachement ul li a {
        color: #fff;
    }

    .setting-drop {
        display: none;
        position: absolute;
        width: 130px;
        height: 148px;
        right: 0;
        top: 42px;
        background: #ffffff;
        color: #545454;
        box-shadow: 1px 1px 15px 1px #0000001f;
    }

    .send-message {
        padding: 15px;
        background: #ff3a54;
        height: auto;
    }

    .send-message textarea:focus {
        box-shadow: none;
        outline: none;
        border-color: #ddd;
    }

    .send-message ul li {
        display: -webkit-inline-box;
        padding-left: 15px;
    }

    .send-message ul li i {
        color: #ff3a54;
    }

    .send-message ul li a {
        color: #0056b3;
    }

    .send-message ul {
        position: absolute;
        right: 53px;
        padding: 10px;
        border-radius: 12px;
        top: 75%;
        background: #fff;
    }

    .send-message .form-control {
        border-radius: 50px;
    }

    @media only screen and (max-width: 800px) {

        .message-box {
            display: none;
            position: relative;
            top: -100%;
        }

    }

</style>
<?php 
    //print_r($senderdetail);
//echo $senderdetail[0]->pro_email;
//echo $senderdetail[0]->pro_fname;
//echo $senderdetail[0]->pro_lname;
//echo $senderdetail[0]->image;
//print_r($chatdetail);
     
?>
<section style="    margin-top: 25px;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="chat-list-box">
                    <div class="head-box">
                        <ul class="list-inline text-left d-inline-block float-left">
                            <li> <img src="https://i.ibb.co/fCzfFJw/person.jpg" alt="" width="40px"> </li>
                        </ul>
                        <ul class="flat-icon list-inline text-right d-inline-block float-right">
                            <li> <a href="#"> <i class="fas fa-search"></i> </a> </li>
                            <li> <a href="#"> <i class="fas fa-ellipsis-v"></i> </a> </li>
                        </ul>
                    </div>

                    <div class="chat-person-list">
                        <ul class="list-inline">
                            <li> <a href="#" class="flip"> <img src="https://i.ibb.co/6JpcfrK/p4.png" alt=""> <span> Naveen Mandwariya</span> <span class="chat-time">12:00 Am</span> </a> </li>
                            <li> <a href="#" class="flip"> <img src="https://i.ibb.co/vdyYVvp/p1.png" alt=""> <span> Sunena Daksh </span> <span class="chat-time">11:45 Pm</span> </a> </li>
                            <li> <a href="#" class="flip"> <img src="https://i.ibb.co/vY406Hp/p3.png" alt=""> <span> Arpit Singh </span> <span class="chat-time">12:15 Pm</span> </a> </li>
                            <li> <a href="#" class="flip"> <img src="https://i.ibb.co/KhYZwPg/p2.png" alt=""> <span> Arpita </span> <span class="chat-time">09:10 Am</span> </a> </li>
                            <li> <a href="#" class="flip"> <img src="https://i.ibb.co/ChGLXKZ/p5.png" alt=""> <span> Sorasth parmar </span> <span class="chat-time">02:00 Pm</span> </a> </li>
                            <li> <a href="#" class="flip"> <img src="https://i.ibb.co/KDZymW5/p6.png" alt=""> <span> Sushmita </span> <span class="chat-time">08:00 Am</span> </a> </li>
                            <li> <a href="#" class="flip"> <img src="https://i.ibb.co/KDZymW5/p6.png" alt=""> <span> Sushmita </span> <span class="chat-time">08:00 Am</span> </a> </li>
                        </ul>
                    </div>

                </div>
            </div> <!-- col-md-4 closed -->

            <div class="col-md-8">
                <div class="message-box">
                    <div class="head-box-1">

                        <ul class="msg-box list-inline text-left d-inline-block float-left">
                            <li> <i class="fas fa-arrow-left" id="back"></i> </li>
                            <li> <img src="https://i.ibb.co/fCzfFJw/person.jpg" alt="" width="40px"> <span> Naveen mandwariya </span> <br> <small class="timee"> 12:45 Pm </small> </li>
                        </ul>

                        <ul class="flat-icon list-inline text-right d-inline-block float-right">
                            <!--
                            <li> <a href="#"> <i class="fas fa-video"></i> </a> </li>
-->
                            <li> <a href="#"> <i class="fas fa-camera"></i> </a> </li>
                            <li>
                                <a href="#" id="dset"> <i class="fas fa-ellipsis-v"></i> </a>
                                <div class="setting-drop">
                                    <ul class="list-inline">
                                        <li> <a href="#"> My Profile</a> </li>
                                        <li> <a href="#"> Setting </a> </li>
                                        <li> <a href="#"> Privacy Policy </a> </li>
                                        <li> <a href="#"> Hidden chat </a> </li>
                                        <li> <a href="#"> Logout </a> </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>

                    </div>
                    
                    <div class="msg_history">
                        <?php  
//                        foreach($chatdetail as $chat){
                        
                        ?>
                        <div class="incoming_msg">
                            <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                            <div class="received_msg">
                                <div class="received_withd_msg">
                                    <p>I am also good thankyou!</p>
                                    <span class="time_date"> 11:01 AM | Yesterday</span>
                                </div>
                            </div>
                        </div>
                        <div class="msg1">
<!--
                        <div class="outgoing_msg">
                            <div class="sent_msg">
                                <p id="msg"> </p>
                                <p id=""><?php  //echo $chat->message; ?> </p>
                                <span class="time_date" > 11:01 AM | Today</span>
                            </div>
                            </div>
-->
                            </div>
                        <?php  //} ?>
                    </div>
                    
                    <div class="send-message">
                        <?php 
                        $attr=array('id'=>'chatform');
                        echo form_open_multipart("user/chat",$attr);?>
                        <!--
                            <textarea cols="10" rows="2" class="form-control" placeholder="Type your message here ..."> </textarea>
-->
                      
                            <div class="emotion">
                                <input type="text" class="input form-control" name="message" id="message" contenteditable="true" placeholder="Type your message here ...">
                                <span class="emotion-Icon">
                                    <i class="far fa-smile" aria-hidden="true"></i>
                                    <div class="emotion-area"></div>
                                </span>
                            </div>

                        <input type="hidden" class="input form-control" name="senderid" value="<?php echo $senderdetail[0]->pro_email; ?>"  >
<!--
                        <input type="hidden" class="input form-control" name="senderid" value="<?php echo $senderdetail[0]->pro_fname; ?>"  >
                        <input type="hidden" class="input form-control" name="senderid" value="<?php echo $senderdetail[0]->pro_lname; ?>"  >
                        <input type="hidden" class="input form-control" name="senderid" value="<?php echo $senderdetail[0]->image; ?>"  >
-->
                        
                        <ul class="list-inline">
                            <li>
                                <a href="#" id="attach"> <i class="fas fa-paperclip"></i> </a>
                                <div class="attachement">
                                    <ul class="list-inline">
                                        <li> <a href="#"> <i class="fas fa-file"></i> </a> </li>
                                        <li> <a href="#"> <i class="fas fa-camera"></i> </a> </li>
                                        <li> <a href="#"> <i class="fas fa-image"></i> </a> </li>
                                        <li> <a href="#"> <i class="far fa-play-circle"></i> </a> </li>
                                        <li> <a href="#"> <i class="fas fa-map-marker-alt"></i> </a> </li>
                                        <li> <a href="#"> <i class="fas fa-id-card"></i> </a> </li>
                                    </ul>
                                </div>
                            </li>
                            <li> <input type="submit" name="submit" value="Send"> </li>
                            <!--<li>
                                    <div class="emotion">
                                        <div class="input" contenteditable="true" placeholder="Enter text here..."></div>
                                        <span class="emotion-Icon">
                                            <i class="fa fa-smile-o" aria-hidden="true"></i>
                                            <div class="emotion-area"></div>
                                        </span>
                                    </div>
                                </li>-->
                        </ul>
                        <?php echo form_close();?>
                    </div>


                </div>
            </div>

        </div>
    </div>
</section>

<!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('public/')?>fassets/js/plugins.js"></script>

<script>
    $("#attach").click(function() {
        $(".attachement").toggle();
    });

</script>
<script>
    $("#dset").click(function() {
        $(".setting-drop").toggle('1000');
    });

</script>

<script>
    $(document).ready(function() {
        $(".flip").click(function() {
            $(".message-box").show("slide", {
                direction: "right"
            }, 10000);
        });
    });

</script>
<script>
    $(document).ready(function() {
        $("#back").click(function() {
            $(".message-box").hide("slide", {
                direction: "left"
            }, 10000);
        });
    });

</script>
<script>
    //***************************Ajax image upload*********************************************


    $(document).ready(function() {
         $(document).on('submit', '#chatform', function(e) {
            e.preventDefault();
             if($("#message").val() != ""){
                var formData = new FormData(this);
                var url = $('#chatform').attr('action');
                //       alert(url);
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        $('.msg1').html(data.content_wrapper)
                    },
                    error: function(data) {
                        alert("error")
                    }
                });
             }
        });


    });
    //end profile image upload



    //***************************jquery image show*********************************************

</script>


<?php include 'footer.php';?>
