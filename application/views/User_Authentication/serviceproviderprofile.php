<?php include 'header.php';?>

<script>
    var stateObject = {
        "India": {
            "Delhi": ["new Delhi", "North Delhi"],
            "Kerala": ["Thiruvananthapuram", "Palakkad"],
            "Goa": ["North Goa", "South Goa"],
        },
        "Australia": {
            "South Australia": ["Dunstan", "Mitchell"],
            "Victoria": ["Altona", "Euroa"]
        },
        "Canada": {
            "Alberta": ["Acadia", "Bighorn"],
            "Columbia": ["Washington", ""]
        },
    }
    window.onload = function() {
        var countySel = document.getElementById("countySel"),
            stateSel = document.getElementById("stateSel"),
            districtSel = document.getElementById("districtSel");
        for (var country in stateObject) {
            countySel.options[countySel.options.length] = new Option(country, country);
        }
        countySel.onchange = function() {
            stateSel.length = 1; // remove all options bar first
            districtSel.length = 1; // remove all options bar first
            if (this.selectedIndex < 1) return; // done 
            for (var state in stateObject[this.value]) {
                stateSel.options[stateSel.options.length] = new Option(state, state);
            }
        }
        countySel.onchange(); // reset in case page is reloaded
        stateSel.onchange = function() {
            districtSel.length = 1; // remove all options bar first
            if (this.selectedIndex < 1) return; // done 
            var district = stateObject[countySel.value][this.value];
            for (var i = 0; i < district.length; i++) {
                districtSel.options[districtSel.options.length] = new Option(district[i], district[i]);
            }
        }
    }

</script>
<!-- page-banner start-->
<style>
    .profile-pic {

        display: block;
    }

    .file-upload {
        display: none;
    }

    .circle {
        border-radius: 1000px !important;
        overflow: hidden;
        width: 75%;
        height: auto;
        border: 8px solid rgba(255, 255, 255, 0.7);
        position: absolute;
    }

    .p-image {
        top: 218px !important;
        position: absolute;
        right: 115px;
        color: #ff3a54;
        transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
    }

    .p-image:hover {
        transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
    }

    .upload-button {
        font-size: 3.2em;
    }

    .upload-button:hover {
        transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
        color: #999;
    }

    .fab {
        padding-right: 5px;
    }

    .checked {
        color: orange;
    }

</style>
<section>
    <div class="container-fluid" style="padding:0px;">
        <div class="col-12" style="padding: 0px;">
            <div class="circle" style="width: 15%;top: 55%;left: 10%;border: 3px solid rgba(255, 255, 255, 0.7);">
                <!-- User Profile Image -->
                <img class="profile-pic" src="assets/img/img_avatar.png" style="position: relative;">
                <!-- Default Image -->
            </div><img src="assets/img/banner.png" class="img-responsive" style="width: 100%;height: 350px;">
            <div class="p-image" style="top: 102%!important;left: 280px;">
                <i class="fa fa-camera upload-button" style="font-size:2em"></i>
                <input class="file-upload" type="file" accept="image/*">
            </div>
        </div>
    </div>
</section>
<!-- add-list start-->
<section class="bg-w sp-100" style="padding: 50px 0px 0px;">
    <div class="container">
        <div class="row mb-30">
            <div class="col-12">
                <form class="listing-form" action="#">
                    <div class="row">
                        <div class="col-md-3 col-12 mt-1">
                            <h5><i class="fa fa-cog" aria-hidden="true"></i>&nbsp;Teaching</h5>
                            <h5><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;Noida,India</h5>
                            <h5><i class="far fa-thumbs-up"></i>&nbsp;12,344</h5>
                            <div>
                                <a href="#"><i class="fab fa-facebook" style="color:#3b5999"></i></a>
                                <a href="#"><i class="fab fa-instagram" style="color:#e4405f"></i></a>
                                <a href="#"><i class="fab fa-twitter" style="color:#55acee"></i></a>
                                <a href="#"><i class="fab fa-linkedin" style="color:#0077B5"></i></a>
                            </div>
                        </div>
                        <div class="col-md-7 col-12">
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <h4>Nidhi Singh</h4>
                                    <p>Nidhi is extremely talented and help your child to crack any competitive exam (Engineering or Medical). They have best in class infrastructure along with resources which help your child to grasp the concept quite easily. </p>
                                </div>
                                <div class="col-md-6 col-12">
                                    <h5 style="padding: 11px 0px 8px;">Rating&nbsp;&nbsp;<span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span></h5>
                                </div>
                                <div class="col-md-2 col-12">
                                    <button class="btn btn-one btn-anim contact">Like</button>
                                </div>
                                <div class="col-md-2 col-12">
                                    <button class="btn btn-one btn-anim contact">Message</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-12">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12 col-12 mt-2">
                                        <a class="btn btn-one btn-anim callbtn" data-toggle="modal" data-target="#getservice">Call&nbsp; Now</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-12 mt-2">
                                        <a class="btn btn-one btn-anim callbtn">Chat Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>


        <div class="row mb-30">
            <div class="col-12">
                <h4 class="title-sep3 mb-30">
                    Service Provider info
                </h4>
            </div>
            <div class="col-12">
                <form class="listing-form" action="#">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <label>Name</label>
                            <input type="text" class="form-control" id="categories2" value="Nidhi Singh" disabled>
                        </div>

                        <div class="col-md-6 col-12">
                            <label>Phone</label>
                            <input type="tel" class="form-control" id="categories2" value="+91 98 7654 3210" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label>services</label>
                            <input type="text" id="name" class="form-control" placeholder="Teaching , Dancing , Fitness, Office work, Food , Tailoring" disabled>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-12">
                            <label>city</label>
                            <select class="form-control custom-select" id="categories2" disabled>
                                <option selected>Noida</option>
                            </select>

                        </div>

                        <div class="col-md-6 col-12">
                            <label>state</label>
                            <select class="form-control custom-select" id="categories2" disabled>
                                <option selected>Uttar Pradesh</option>
                            </select> </div>

                        <div class="col-md-6 col-12">
                            <label>Zip Code</label>
                            <input type="text" id="code" class="form-control" placeholder="201309" disabled>
                        </div>
                        <div class="col-md-6 col-12">
                            <label>address</label>
                            <input type="text" id="adress" class="form-control" placeholder="2nd Floor, Office No, H - 61, Sector 63 Rd, Near Nirala World" disabled>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="row mb-30">
            <div class="col-12">
                <h4 class="title-sep3 mb-30">
                    Payment options
                </h4>
            </div>
            <div class="col-12">
                <form class="listing-form minus-pad" action="#">
                    <div class="row mb-30">
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <i class="fas fa-wallet"></i> <label for="checkbox_id1">Credit Card</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <i class="fas fa-wallet"></i> <label for="checkbox_id2">Debit Card</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <i class="fas fa-wallet"></i> <label for="checkbox_id3">Cash Payment</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <i class="fas fa-wallet"></i> <label for="checkbox_id4">Paytm</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <i class="fas fa-wallet"></i> <label for="checkbox_id5">Google Pay</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <i class="fas fa-wallet"></i> <label for="checkbox_id6">Phone Pe</label>
                        </div>

                    </div>
                </form>
            </div>
            <div class="col-12 text-center">
                <div class="listing-submit">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-one btn-anim callbtn" data-toggle="modal" data-target="#getservice" style="font-size:20px;">
                        Get Service
                    </button>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- add-list end -->
<!-- Modal -->
<div class="modal fade" id="getservice" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="col-md-11 text-center">
                    <h4 style="margin:0px">Service Provider Info</h4>
                </div>
                <div class="col-md-1 text-center"> <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <!-- User Profile Image -->
                        <p><img class="profile-picc" src="assets/img/img_avatar.png" width="50%" style="width: 30%;border-radius: 50%;"></p>
                    </div>
                    <div class="col-md-12 text-center">
                        <h4 style="margin:0px">Nidhi Singh</h4>
                        <p style="margin:0px">Teaching</p>
                        <p><a href="tel:+91 9876 543 210"><i class="fas fa-phone"></i>&nbsp;+91 9876 543 210</a></p>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <div class="col-md-12 text-center">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-one btn-anim callbtn">Call Now</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {


        var readURL = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('.profile-pic').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }


        $(".file-upload").on('change', function() {
            readURL(this);
        });

        $(".upload-button").on('click', function() {
            $(".file-upload").click();
        });
    });

</script>

<?php include 'footer.php';?>
