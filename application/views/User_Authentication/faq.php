<?php include 'header.php';?>

<!-- page-banner start-->
<section class="page-banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>FAQ</h3>
                <ul class="banner-link text-center">
                    <li>
                        <a href="index-2.html">Home</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">pages</a>
                    </li>
                    <li>
                        <span class="active">FAQ</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- page-banner ends-->

<!-- faq start-->
<section class="bg-w sp-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <ul class="nav faq-tabs mb-lg-0 mb-5" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#faq1" role="tab" data-toggle="tab">Genral FAQ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#faq2" role="tab" data-toggle="tab">Listing Support</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#faq3" role="tab" data-toggle="tab">Category</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#faq4" role="tab" data-toggle="tab">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#faq5" role="tab" data-toggle="tab">Customize Options</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#faq6" role="tab" data-toggle="tab">Email Support</a>
                    </li>
                </ul>

            </div>
            <div class="col-lg-8">
                <h4 class="title-sep2 mb-30">Listing Support</h4>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active show" id="faq1">
                        <div id="faq-accordion1" class="theme-accordion">
                            <div class="acc-card">
                                <div class="panel-heading" id="headingOne">
                                    <button class="acc-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Etiam sit amet orci eget eros faucibus tincidunt sed fringilla mauris
                                    </button>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#faq-accordion1">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, df sed do eiu tempor
                                            incid uts hgia labore et dolor magna aliqua. Ut enim minim veniam. Aenean
                                            massa nat penatius magnis dis parturient montes, nasceturs et ridiculus musg.
                                            Donec felis, ultrhfj iciesne elente sque eu.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="acc-card">
                                <div class="panel-heading" id="headingTwo">
                                    <button class="acc-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Cras id dui aenean ut eros et nisl sagittis vestibulum. nullam nulla eros
                                    </button>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#faq-accordion1">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, df sed do eiu tempor
                                            incid uts hgia labore et dolor magna aliqua. Ut enim minim veniam. Aenean
                                            massa nat penatius magnis dis parturient montes, nasceturs et ridiculus musg.
                                            Donec felis, ultrhfj iciesne elente sque.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="acc-card">
                                <div class="panel-heading" id="headingThree">
                                    <button class="acc-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Phasellus leo dolor, tempus non, auctor et hendrerit quis, nisi curabitur
                                    </button>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#faq-accordion1">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, sed do eiu tempore
                                            incididunt ut labore et dolore magna aliqua. Ut eni minim veniam. Aenean
                                            massa cum sociis natoque natib magnis dis parturie ntut montes nascetu ridiculus
                                            muson quam felis.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="acc-card">
                                <div class="panel-heading" id="headingFour">
                                    <button class="acc-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        Faucibus orci luctus et ultrices posuere cubilia curae
                                    </button>
                                </div>
                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#faq-accordion1">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, df sed do eiu tempor
                                            incid uts hgia labore et dolor magna aliqua. Ut enim minim veniam. Aenean
                                            massa nat penatius magnis dis parturient montes, nasceturs et ridiculus musg.
                                            Donec felis, ultrhfj iciesne elente sque eu.Aenean massa cum sociis natoque
                                            penati magnis dis partu rient montes, nasceturs rid iculus mus.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="faq2">
                        <div id="faq-accordion2" class="theme-accordion">
                            <div class="acc-card">
                                <div class="panel-heading" id="heading2One">
                                    <button class="acc-link collapsed" data-toggle="collapse" data-target="#collapse2One" aria-expanded="true" aria-controls="collapseOne">
                                        Etiam sit amet orci eget eros faucibus tincidunt sed fringilla mauris
                                    </button>
                                </div>

                                <div id="collapse2One" class="collapse" aria-labelledby="heading2One" data-parent="#faq-accordion2">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, df sed do eiu tempor
                                            incid uts hgia labore et dolor magna aliqua. Ut enim minim veniam. Aenean
                                            massa nat penatius magnis dis parturient montes, nasceturs et ridiculus musg.
                                            Donec felis, ultrhfj iciesne elente sque eu.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="acc-card">
                                <div class="panel-heading" id="heading2Two">
                                    <button class="acc-link" data-toggle="collapse" data-target="#collapse2Two" aria-expanded="false" aria-controls="collapseTwo">
                                        Cras id dui aenean ut eros et nisl sagittis vestibulum. nullam nulla eros
                                    </button>
                                </div>
                                <div id="collapse2Two" class="collapse show" aria-labelledby="heading2Two" data-parent="#faq-accordion2">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, df sed do eiu tempor
                                            incid uts hgia labore et dolor magna aliqua. Ut enim minim veniam. Aenean
                                            massa nat penatius magnis dis parturient montes, nasceturs et ridiculus musg.
                                            Donec felis, ultrhfj iciesne elente sque.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="acc-card">
                                <div class="panel-heading" id="heading2Three">
                                    <button class="acc-link collapsed" data-toggle="collapse" data-target="#collapse2Three" aria-expanded="false" aria-controls="collapseThree">
                                        Phasellus leo dolor, tempus non, auctor et hendrerit quis, nisi curabitur
                                    </button>
                                </div>
                                <div id="collapse2Three" class="collapse" aria-labelledby="heading2Three" data-parent="#faq-accordion2">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, sed do eiu tempore
                                            incididunt ut labore et dolore magna aliqua. Ut eni minim veniam. Aenean
                                            massa cum sociis natoque natib magnis dis parturie ntut montes nascetu ridiculus
                                            muson quam felis.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="acc-card">
                                <div class="panel-heading" id="heading2Four">
                                    <button class="acc-link collapsed" data-toggle="collapse" data-target="#collapse2Four" aria-expanded="false" aria-controls="collapseFour">
                                        Faucibus orci luctus et ultrices posuere cubilia curae
                                    </button>
                                </div>
                                <div id="collapse2Four" class="collapse" aria-labelledby="heading2Four" data-parent="#faq-accordion2">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, df sed do eiu tempor
                                            incid uts hgia labore et dolor magna aliqua. Ut enim minim veniam. Aenean
                                            massa nat penatius magnis dis parturient montes, nasceturs et ridiculus musg.
                                            Donec felis, ultrhfj iciesne elente sque eu.Aenean massa cum sociis natoque
                                            penati magnis dis partu rient montes, nasceturs rid iculus mus.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="faq3">
                        <div id="faq-accordion3" class="theme-accordion">
                            <div class="acc-card">
                                <div class="panel-heading" id="heading3One">
                                    <button class="acc-link collapsed" data-toggle="collapse" data-target="#collapse3One" aria-expanded="true" aria-controls="collapseOne">
                                        Etiam sit amet orci eget eros faucibus tincidunt sed fringilla mauris
                                    </button>
                                </div>

                                <div id="collapse3One" class="collapse" aria-labelledby="heading3One" data-parent="#faq-accordion3">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, df sed do eiu tempor
                                            incid uts hgia labore et dolor magna aliqua. Ut enim minim veniam. Aenean
                                            massa nat penatius magnis dis parturient montes, nasceturs et ridiculus musg.
                                            Donec felis, ultrhfj iciesne elente sque eu.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="acc-card">
                                <div class="panel-heading" id="heading3Three">
                                    <button class="acc-link collapsed" data-toggle="collapse" data-target="#collapse3Three" aria-expanded="false" aria-controls="collapseThree">
                                        Phasellus leo dolor, tempus non, auctor et hendrerit quis, nisi curabitur
                                    </button>
                                </div>
                                <div id="collapse3Three" class="collapse" aria-labelledby="heading3Three" data-parent="#faq-accordion3">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, sed do eiu tempore
                                            incididunt ut labore et dolore magna aliqua. Ut eni minim veniam. Aenean
                                            massa cum sociis natoque natib magnis dis parturie ntut montes nascetu ridiculus
                                            muson quam felis.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="acc-card">
                                <div class="panel-heading" id="heading3Two">
                                    <button class="acc-link" data-toggle="collapse" data-target="#collapse3Two" aria-expanded="false" aria-controls="collapseTwo">
                                        Cras id dui aenean ut eros et nisl sagittis vestibulum. nullam nulla eros
                                    </button>
                                </div>
                                <div id="collapse3Two" class="collapse show" aria-labelledby="heading3Two" data-parent="#faq-accordion3">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, df sed do eiu tempor
                                            incid uts hgia labore et dolor magna aliqua. Ut enim minim veniam. Aenean
                                            massa nat penatius magnis dis parturient montes, nasceturs et ridiculus musg.
                                            Donec felis, ultrhfj iciesne elente sque.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="acc-card">
                                <div class="panel-heading" id="heading3Four">
                                    <button class="acc-link collapsed" data-toggle="collapse" data-target="#collapse3Four" aria-expanded="false" aria-controls="collapseFour">
                                        Lorem sit tempor cupidatat consequat cillum ipsum voluptate mollit.
                                    </button>
                                </div>
                                <div id="collapse3Four" class="collapse" aria-labelledby="heading3Four" data-parent="#faq-accordion3">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, df sed do eiu tempor
                                            incid uts hgia labore et dolor magna aliqua. Ut enim minim veniam. Aenean
                                            massa nat penatius magnis dis parturient montes, nasceturs et ridiculus musg.
                                            Donec felis, ultrhfj iciesne elente sque eu.Aenean massa cum sociis natoque
                                            penati magnis dis partu rient montes, nasceturs rid iculus mus.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="faq4">
                        <div id="faq-accordion4" class="theme-accordion">

                            <div class="acc-card">
                                <div class="panel-heading" id="heading4Three">
                                    <button class="acc-link collapsed" data-toggle="collapse" data-target="#collapse4Three" aria-expanded="false" aria-controls="collapseThree">
                                        Phasellus leo dolor, tempus non, auctor et hendrerit quis, nisi curabitur
                                    </button>
                                </div>
                                <div id="collapse4Three" class="collapse" aria-labelledby="heading4Three" data-parent="#faq-accordion4">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, sed do eiu tempore
                                            incididunt ut labore et dolore magna aliqua. Ut eni minim veniam. Aenean
                                            massa cum sociis natoque natib magnis dis parturie ntut montes nascetu ridiculus
                                            muson quam felis.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="acc-card">
                                <div class="panel-heading" id="heading4Two">
                                    <button class="acc-link" data-toggle="collapse" data-target="#collapse4Two" aria-expanded="false" aria-controls="collapseTwo">
                                        Cras id dui aenean ut eros et nisl sagittis vestibulum. nullam nulla eros
                                    </button>
                                </div>
                                <div id="collapse4Two" class="collapse show" aria-labelledby="heading4Two" data-parent="#faq-accordion4">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, df sed do eiu tempor
                                            incid uts hgia labore et dolor magna aliqua. Ut enim minim veniam. Aenean
                                            massa nat penatius magnis dis parturient montes, nasceturs et ridiculus musg.
                                            Donec felis, ultrhfj iciesne elente sque.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="acc-card">
                                <div class="panel-heading" id="heading4One">
                                    <button class="acc-link collapsed" data-toggle="collapse" data-target="#collapse4One" aria-expanded="true" aria-controls="collapseOne">
                                        Etiam sit amet orci eget eros faucibus tincidunt sed fringilla mauris
                                    </button>
                                </div>

                                <div id="collapse4One" class="collapse" aria-labelledby="heading4One" data-parent="#faq-accordion4">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, df sed do eiu tempor
                                            incid uts hgia labore et dolor magna aliqua. Ut enim minim veniam. Aenean
                                            massa nat penatius magnis dis parturient montes, nasceturs et ridiculus musg.
                                            Donec felis, ultrhfj iciesne elente sque eu.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="acc-card">
                                <div class="panel-heading" id="heading4Four">
                                    <button class="acc-link collapsed" data-toggle="collapse" data-target="#collapse4Four" aria-expanded="false" aria-controls="collapseFour">
                                        Lorem sit tempor cupidatat consequat cillum ipsum voluptate mollit.
                                    </button>
                                </div>
                                <div id="collapse4Four" class="collapse" aria-labelledby="heading4Four" data-parent="#faq-accordion4">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, df sed do eiu tempor
                                            incid uts hgia labore et dolor magna aliqua. Ut enim minim veniam. Aenean
                                            massa nat penatius magnis dis parturient montes, nasceturs et ridiculus musg.
                                            Donec felis, ultrhfj iciesne elente sque eu.Aenean massa cum sociis natoque
                                            penati magnis dis partu rient montes, nasceturs rid iculus mus.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="faq5">
                        <div id="faq-accordion5" class="theme-accordion">

                            <div class="acc-card">
                                <div class="panel-heading" id="heading5Three">
                                    <button class="acc-link collapsed" data-toggle="collapse" data-target="#collapse5Three" aria-expanded="false" aria-controls="collapseThree">
                                        Velit anim dolor elit Lorem cupidatat esse sunt elit laboris sunt.
                                    </button>
                                </div>
                                <div id="collapse5Three" class="collapse" aria-labelledby="heading5Three" data-parent="#faq-accordion5">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, sed do eiu tempore
                                            incididunt ut labore et dolore magna aliqua. Ut eni minim veniam. Aenean
                                            massa cum sociis natoque natib magnis dis parturie ntut montes nascetu ridiculus
                                            muson quam felis.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="acc-card">
                                <div class="panel-heading" id="heading5One">
                                    <button class="acc-link collapsed" data-toggle="collapse" data-target="#collapse5One" aria-expanded="true" aria-controls="collapseOne">
                                        Etiam sit amet orci eget eros faucibus tincidunt sed fringilla mauris
                                    </button>
                                </div>

                                <div id="collapse5One" class="collapse" aria-labelledby="heading5One" data-parent="#faq-accordion5">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, df sed do eiu tempor
                                            incid uts hgia labore et dolor magna aliqua. Ut enim minim veniam. Aenean
                                            massa nat penatius magnis dis parturient montes, nasceturs et ridiculus musg.
                                            Donec felis, ultrhfj iciesne elente sque eu.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="acc-card">
                                <div class="panel-heading" id="heading5Four">
                                    <button class="acc-link collapsed" data-toggle="collapse" data-target="#collapse5Four" aria-expanded="false" aria-controls="collapseFour">
                                        Lorem sit tempor cupidatat consequat cillum ipsum voluptate mollit.
                                    </button>
                                </div>
                                <div id="collapse5Four" class="collapse" aria-labelledby="heading5Four" data-parent="#faq-accordion5">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, df sed do eiu tempor
                                            incid uts hgia labore et dolor magna aliqua. Ut enim minim veniam. Aenean
                                            massa nat penatius magnis dis parturient montes, nasceturs et ridiculus musg.
                                            Donec felis, ultrhfj iciesne elente sque eu.Aenean massa cum sociis natoque
                                            penati magnis dis partu rient montes, nasceturs rid iculus mus.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="acc-card">
                                <div class="panel-heading" id="heading5Two">
                                    <button class="acc-link" data-toggle="collapse" data-target="#collapse5Two" aria-expanded="false" aria-controls="collapseTwo">
                                        Ea elit adipisicing et dolore excepteur deserunt fugiat ipsum culpa et.
                                    </button>
                                </div>
                                <div id="collapse5Two" class="collapse show" aria-labelledby="heading5Two" data-parent="#faq-accordion5">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, df sed do eiu tempor
                                            incid uts hgia labore et dolor magna aliqua. Ut enim minim veniam. Aenean
                                            massa nat penatius magnis dis parturient montes, nasceturs et ridiculus musg.
                                            Donec felis, ultrhfj iciesne elente sque.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="faq6">
                        <div id="faq-accordion6" class="theme-accordion">
                            <div class="acc-card">
                                <div class="panel-heading" id="heading6Three">
                                    <button class="acc-link collapsed" data-toggle="collapse" data-target="#collapse6Three" aria-expanded="false" aria-controls="collapseThree">
                                        Velit anim dolor elit Lorem cupidatat esse sunt elit laboris sunt.
                                    </button>
                                </div>
                                <div id="collapse6Three" class="collapse" aria-labelledby="heading6Three" data-parent="#faq-accordion6">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, sed do eiu tempore
                                            incididunt ut labore et dolore magna aliqua. Ut eni minim veniam. Aenean
                                            massa cum sociis natoque natib magnis dis parturie ntut montes nascetu ridiculus
                                            muson quam felis.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="acc-card">
                                <div class="panel-heading" id="heading6Two">
                                    <button class="acc-link" data-toggle="collapse" data-target="#collapse6Two" aria-expanded="false" aria-controls="collapseTwo">
                                        Laborum labore excepteur incididunt duis fugiat eu Lorem.
                                    </button>
                                </div>
                                <div id="collapse6Two" class="collapse show" aria-labelledby="heading6Two" data-parent="#faq-accordion6">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, df sed do eiu tempor
                                            incid uts hgia labore et dolor magna aliqua. Ut enim minim veniam. Aenean
                                            massa nat penatius magnis dis parturient montes, nasceturs et ridiculus musg.
                                            Donec felis, ultrhfj iciesne elente sque.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="acc-card">
                                <div class="panel-heading" id="heading6One">
                                    <button class="acc-link collapsed" data-toggle="collapse" data-target="#collapse6One" aria-expanded="true" aria-controls="collapseOne">
                                        Etiam sit amet orci eget eros faucibus tincidunt sed fringilla mauris
                                    </button>
                                </div>

                                <div id="collapse6One" class="collapse" aria-labelledby="heading6One" data-parent="#faq-accordion6">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, df sed do eiu tempor
                                            incid uts hgia labore et dolor magna aliqua. Ut enim minim veniam. Aenean
                                            massa nat penatius magnis dis parturient montes, nasceturs et ridiculus musg.
                                            Donec felis, ultrhfj iciesne elente sque eu.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="acc-card">
                                <div class="panel-heading" id="heading6Four">
                                    <button class="acc-link collapsed" data-toggle="collapse" data-target="#collapse6Four" aria-expanded="false" aria-controls="collapseFour">
                                        Et incididunt adipisicing ea aliqua aliqua id fugiat nostrud.
                                    </button>
                                </div>
                                <div id="collapse6Four" class="collapse" aria-labelledby="heading6Four" data-parent="#faq-accordion6">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, df sed do eiu tempor
                                            incid uts hgia labore et dolor magna aliqua. Ut enim minim veniam. Aenean
                                            massa nat penatius magnis dis parturient montes, nasceturs et ridiculus musg.
                                            Donec felis, ultrhfj iciesne elente sque eu.Aenean massa cum sociis natoque
                                            penati magnis dis partu rient montes, nasceturs rid iculus mus.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- faq end -->

<!-- cta-one start-->
<section class="cta-one tri-bg-w text-lg-left text-center">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 my-lg-0 my-5 py-lg-0 py-5">
                <div class="cta-content">
                    <h3>Sign Up To Get Special Offers Every Day</h3>
                    <p>Lorem ipsum dolor sit amet, consectadetudzdae rcquisc adipiscing elit. Aenean socada commodo ligaui
                        egets dolor. </p>
                    <a href="login.html" class="btn btn-two btn-anim mt-2">
                        sign up
                    </a>
                </div>
            </div>
            <div class="col-lg-6 d-lg-block d-none">
                <div class="cta-img mt-4">
                    <img src="assets/img/home/cta-bg.png" alt="image">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- cta-one end -->
<?php include 'footer.php';?>
