<?php include 'header.php';?>

<script>
    var stateObject = {
        "India": {
            "Delhi": ["new Delhi", "North Delhi"],
            "Kerala": ["Thiruvananthapuram", "Palakkad"],
            "Goa": ["North Goa", "South Goa"],
        },
        "Australia": {
            "South Australia": ["Dunstan", "Mitchell"],
            "Victoria": ["Altona", "Euroa"]
        },
        "Canada": {
            "Alberta": ["Acadia", "Bighorn"],
            "Columbia": ["Washington", ""]
        },
    }
    window.onload = function() {
        var countySel = document.getElementById("countySel"),
            stateSel = document.getElementById("stateSel"),
            districtSel = document.getElementById("districtSel");
        for (var country in stateObject) {
            countySel.options[countySel.options.length] = new Option(country, country);
        }
        countySel.onchange = function() {
            stateSel.length = 1; // remove all options bar first
            districtSel.length = 1; // remove all options bar first
            if (this.selectedIndex < 1) return; // done 
            for (var state in stateObject[this.value]) {
                stateSel.options[stateSel.options.length] = new Option(state, state);
            }
        }
        countySel.onchange(); // reset in case page is reloaded
        stateSel.onchange = function() {
            districtSel.length = 1; // remove all options bar first
            if (this.selectedIndex < 1) return; // done 
            var district = stateObject[countySel.value][this.value];
            for (var i = 0; i < district.length; i++) {
                districtSel.options[districtSel.options.length] = new Option(district[i], district[i]);
            }
        }
    }

</script>
<!-- page-banner start-->
<section class="page-banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>Profile</h3>
                <ul class="banner-link text-center">
                    <li>
                        <a href="index-2.html">Home</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">customer service</a>
                    </li>
                    <li>
                        <span class="active">customer Profile</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- page-banner ends-->

<!-- add-list start-->
<section class="bg-w sp-100">
    <div class="container">
        <div class="row mb-30">
            <div class="col-12">
                <h4 class="title-sep3 mb-30">
                    customer Profile
                </h4>
            </div>
            <div class="col-12">
                <form class="listing-form" action="#">
                    <div class="row">
                        <div class="col-md-4 col-12">
                            <!--                            <label>city</label>
                            <textarea rows="10" name="comment" class="form-control" placeholder="write here" id="comment"></textarea>-->
                            <style>
                                .profile-pic {

                                    display: block;
                                }

                                .file-upload {
                                    display: none;
                                }

                                .circle {
                                    border-radius: 1000px !important;
                                    overflow: hidden;
                                    width: 75%;
                                    height: auto;
                                    border: 8px solid rgba(255, 255, 255, 0.7);
                                    position: absolute;
                                }

                                .p-image {
                                    top: 218px !important;
                                    position: absolute;
                                    right: 115px;
                                    color: #ff3a54;
                                    transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
                                }

                                .p-image:hover {
                                    transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
                                }

                                .upload-button {
                                    font-size: 3.2em;
                                }

                                .upload-button:hover {
                                    transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
                                    color: #999;
                                }

                            </style>
                            <div class="small-12 medium-2 large-2 columns">
                                <div class="circle">
                                    <!-- User Profile Image -->
                                    <img class="profile-pic" src="assets/img/img_avatar.png">

                                    <!-- Default Image -->
                                    <!-- <i class="fa fa-user fa-5x"></i> -->
                                </div>
                                <div class="p-image">
                                    <i class="fa fa-camera upload-button"></i>
                                    <input class="file-upload" type="file" accept="image/*" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-12">
                            <div class="row">
                                <div class="col-lg-6 col-12">
                                    <label>Name</label>
                                    <input type="text" id="google" class="form-control" placeholder="Full Name">
                                </div>
                                <div class="col-lg-6 col-12">
                                    <label>phone</label>
                                    <input type="text" id="phn" class="form-control" placeholder="phone no">
                                </div>
                                <div class="col-lg-6 col-12">
                                    <label>e mail</label>
                                    <input type="text" id="mail" class="form-control" placeholder="e mail">
                                </div>
                                <div class="col-lg-6 col-12">
                                    <label>facebook</label>
                                    <input type="text" id="fb" class="form-control" placeholder="facebook">
                                </div>
                                <div class="col-lg-6 col-12">
                                    <label>twitter</label>
                                    <input type="text" id="twitter" class="form-control" placeholder="twitter">
                                </div>
                                <div class="col-lg-6 col-12">
                                    <label>website</label>
                                    <input type="text" id="website" class="form-control" placeholder="website">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row mb-30">
            <div class="col-12">
                <h4 class="title-sep3 mb-30">Location
                </h4>
            </div>
            <div class="col-12">
                <form class="listing-form" action="#">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <label>city</label>
                            <select class="form-control custom-select" name="state" id="countySel" size="1">
                                <option value="" selected="selected">Select Country</option>
                            </select>
                        </div>

                        <div class="col-md-6 col-12">
                            <label>state</label>
                            <select class="form-control" name="countrya" id="stateSel" size="1">
                                <option value="" selected="selected">Please select Country first</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-12">
                            <label>District</label>
                            <select class="form-control" name="district" id="districtSel" size="1">
                                <option value="" selected="selected">Please select State first</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-12">
                            <label>Zip Code</label>
                            <input type="text" id="code" class="form-control" placeholder="enter Here">
                        </div>
                        <div class="col-md-12 col-12">
                            <label>address</label>
                            <input type="text" id="adress" class="form-control" placeholder="address here">
                        </div>
                    </div>
                </form>
            </div>
            <!--<div class="col-12">
                <div class="map mb-30">
                    <div id="theme-map"></div>
                </div>
            </div>-->
        </div>
        <div class="row mb-30">
            <div class="col-12">
                <h4 class="title-sep3 mb-30">
                    Tell us about service you want
                </h4>
            </div>
            <div class="col-12">
                <form class="listing-form" action="#">
                    <div class="row">
                        <div class="col-12">
                            <label>service title</label>
                            <input type="text" id="name" class="form-control" placeholder="type here">
                        </div>
                        <div class="col-md-6 col-12">
                            <label>category</label>
                            <select class="form-control custom-select" id="categories">
                                <option disabled selected>select categories</option>
                                <option value="Beauty">Beauty Services</option>
                                <option value="Teaching">Teaching</option>
                                <option value="Fitness">Fitness</option>
                                <option value="Office">Office work</option>
                                <option value="Medical">Medical</option>
                                <option value="Food">Food</option>
                                <option value="Tailoring">Tailoring</option>
                                <option value="Others">Others</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-12">
                            <label>Sub category</label>
                            <select class="form-control custom-select" id="categories">
                                <option disabled selected>select categories</option>
                                <option value="Beauty">Beauty Services</option>
                                <option value="Teaching">Teaching</option>
                                <option value="Fitness">Fitness</option>
                                <option value="Office">Office work</option>
                                <option value="Medical">Medical</option>
                                <option value="Food">Food</option>
                                <option value="Tailoring">Tailoring</option>
                                <option value="Others">Others</option>
                            </select>
                        </div>
                        <div class="col-md-12 col-12">
                            <label>Other</label>
                            <input type="text" id="keywords" class="form-control" placeholder="tell us if your desired service not in list">
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row mb-30">
            <div class="col-12">
                <h4 class="title-sep3 mb-30">
                    Payment options
                </h4>
            </div>
            <div class="col-12">
                <form class="listing-form minus-pad" action="#">
                    <div class="row mb-30">
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <input type="checkbox" name="checkbox" id="checkbox_id1" value="value">
                            <label for="checkbox_id1">Credit Card</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <input type="checkbox" name="checkbox" id="checkbox_id2" value="value">
                            <label for="checkbox_id2">Debit Card</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <input type="checkbox" name="checkbox" id="checkbox_id3" value="value">
                            <label for="checkbox_id3">Cash Payment</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <input type="checkbox" name="checkbox" id="checkbox_id4" value="value">
                            <label for="checkbox_id4">Paytm</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <input type="checkbox" name="checkbox" id="checkbox_id5" value="value">
                            <label for="checkbox_id5">Google Pay</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <input type="checkbox" name="checkbox" id="checkbox_id6" value="value">
                            <label for="checkbox_id6">Phone Pe</label>
                        </div>

                    </div>
                </form>
            </div>
        </div>


        <div class="row">
            <div class="col-12 text-center">
                <div class="listing-submit">
                    <button type="submit" class="btn btn-one btn-anim">save profile</button>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- add-list end -->
<script>
    $(document).ready(function() {


        var readURL = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('.profile-pic').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }


        $(".file-upload").on('change', function() {
            readURL(this);
        });

        $(".upload-button").on('click', function() {
            $(".file-upload").click();
        });
    });

</script>
<?php include 'footer.php';?>
