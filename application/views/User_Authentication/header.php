<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon.png">
    <link rel="stylesheet" href="assets/css/plugins.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <title>Quinn - Promote Yourself </title>
</head>

<body>

    <!-- ====== Go to top ====== -->
    <a id="toTop" title="Go to top" href="javascript:void(0)">
        <span id="toTopHover"></span>TOP
    </a>

    <!-- Preloader start-->
    <div class="preloader">
        <div class="loader-inner">
            <div class="dash one">
                <i class="fa fa-map-marker-alt"></i>
            </div>
            <div class="dash two">
                <i class="fa fa-map-marker-alt"></i>
            </div>
            <div class="dash three">
                <i class="fa fa-map-marker-alt"></i>
            </div>
        </div>
    </div>
    <!-- Preloader end -->

    <!-- Header start-->
    <header class="header">
        <div class="head-top head-top-two d-none d-lg-block py-1">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <div class="socials socials-header text-lg-left text-center">
                            <a href="#">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-instagram"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <ul class="head-contact text-lg-right text-center">
                            <li>
                                <i class="fa fa-phone"></i>
                                +123-456-7890
                            </li>
                            <li>
                                <i class="fa fa-envelope"></i>
                                info@quinn.com
                            </li>
                            <li>
                                <i class="fa fa-map-marker-alt"></i>
                                H - 61, 2nd Floor, Sector 63, Noida, Uttar Pradesh
                            </li>

                            <li class="login-sign">
                                <a href="login.php">
                                    <i class="fa fa-user"></i> login / signup
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="theme-header-two affix">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-12">
                        <div class="logo-wrap">
                            <div class="logo my-1">
                                <a href="index.php">
                                    <img src="assets/img/logoblack.png" style="width: 35%;" alt="logo">
                                    <!--<span style="font-size: 42px;color:#fff;font-family:'Josefin Sans',sans-serif;">Quinn</span>-->
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-9 col-md-12">
                        <div class="menu menu-two">
                            <nav class="navbar navbar-expand-lg">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-content" aria-controls="nav-content" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon bar1"></span>
                                    <span class="navbar-toggler-icon bar2"></span>
                                    <span class="navbar-toggler-icon bar3"></span>
                                </button>
                                <!-- Links -->
                                <div class="main-menu collapse navbar-collapse" id="nav-content">
                                    <ul class="navbar-nav ml-auto align-items-lg-center">
                                        <li class="nav-item">
                                            <a class="nav-link" href="index.php">Home</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="about.php">about us</a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown">Services</a>
                                            <ul class="dropdown-menu">
                                                <li class="dropdown-item">
                                                    <a href="#">all Services</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="#">Tution teacher </a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="#">Dance teacher </a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="#">Beautician</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="#">PPT preparation</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="#">Yoga/Fitness/ Zumba teacher</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="#">Child Day Care / activity classes</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="#">House Maids</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="#">Miscellaneous service</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Blog</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="contact.php">contact us</a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="#search">
                                                <i class="fa fa-search"></i>
                                            </a>
                                        </li>
                                        <li class="nav-item d-lg-block d-none">
                                            <a href="addserviceproviderprofile.php" class="btn btn-one btn-anim br-5 px-3 nav-btn">
                                                <i class="fa fa-plus-circle"></i> add Services
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Header end -->

    <!-- Search Form -->
    <div id="search" class="top-search d-flex">
        <span class="close">
            <i class="fa fa-times"></i>
        </span>
        <div class="w-100 text-center mt-4">
            <h3 class="c-white fw-5">search here</h3>
            <form role="search" id="searchform" action="#" method="get" class="search-bar">
                <input value="" name="q" type="search" placeholder="type to search..." class="form-control">
                <button type="submit" class="submit-btn">
                    <i class="fa fa-search"></i>
                </button>
            </form>
        </div>
    </div>
