<?php include 'header.php';?>

<!-- page-banner start-->
<section class="page-banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>contact</h3>
                <ul class="banner-link text-center">
                    <li>
                        <a href="index-2.html">Home</a>
                    </li>
                    <li>
                        <span class="active">contact</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- page-banner ends-->

<!-- contact start-->
<section class="bg-w sp-100 pb-lg-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="contact-box">
                    <div class="d-block d-xl-flex">
                        <div class="contact-left">
                            <h4 class="mb-4">get in touch</h4>
                            <form class="custom-form" action="#">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <span class="fa fa-user"></span>
                                        <input type="text" id="name" class="form-control" placeholder="Enter Name">
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <span class="fa fa-envelope"></span>
                                        <input type="email" class="form-control" placeholder="Enter Email" name="Ented email" id="email">
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <span class="fa fa-phone"></span>
                                        <input type="text" id="phn" class="form-control" placeholder="Enter Name">
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <span class="fa fa-edit"></span>
                                        <input type="email" class="form-control" placeholder="Enter Email" name="Ented email" id="subject">
                                    </div>
                                    <div class="col-12">
                                        <span class="fa fa-edit"></span>
                                        <textarea rows="5" name="comment" class="form-control" placeholder="Your Message" id="comment"></textarea>
                                    </div>
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-one btn-anim" id="submit" name="submit">
                                            send message</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="contact-right">
                            <h4 class="mb-4">contact info</h4>
                            <p>Search for Anything, Anytime, Anywhere using Quinn App by Quinn Pvt. Ltd., India’s No. 1 Local Search Engine. You can chat with businesses, get phone numbers, addresses, learn of best deals, latest reviews and ratings for all businesses instantly.</p>
                            <ul class="contact-info mt-4">
                                <li><a href="tel:+91 120 4981750"> <i class="fa fa-phone"></i>
                                        +91 120 4981750 </a>
                                </li>
                                <li><a href="mailto:info@quinn.in"> <i class="fa fa-envelope"></i>
                                        info@quinn.in</a>
                                </li>

                            </ul>
                            <div class="socials contact-social">
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- contact end -->
<!-- cta-one start-->
<section class="cta-one tri-bg-w text-lg-left text-center">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 my-lg-0 my-5 py-lg-0 py-5">
                <div class="cta-content">
                    <h3>Sign Up To Get Special Offers Every Day</h3>
                    <p>Lorem ipsum dolor sit amet, consectadetudzdae rcquisc adipiscing elit. Aenean socada commodo ligaui
                        egets dolor. </p>
                    <a href="login.html" class="btn btn-two btn-anim mt-2">
                        sign up
                    </a>
                </div>
            </div>
            <div class="col-lg-6 d-lg-block d-none">
                <div class="cta-img mt-4">
                    <img src="<?php echo base_url('public/')?>fassets/img/home/cta-bg.png" alt="image">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- cta-one end -->
<?php include 'footer.php';?>
