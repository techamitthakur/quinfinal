<?php include 'header.php';?>

<script>
    var stateObject = {
        "India": {
            "Delhi": ["new Delhi", "North Delhi"],
            "Kerala": ["Thiruvananthapuram", "Palakkad"],
            "Goa": ["North Goa", "South Goa"],
        },
        "Australia": {
            "South Australia": ["Dunstan", "Mitchell"],
            "Victoria": ["Altona", "Euroa"]
        },
        "Canada": {
            "Alberta": ["Acadia", "Bighorn"],
            "Columbia": ["Washington", ""]
        },
    }
    window.onload = function() {
        var countySel = document.getElementById("countySel"),
            stateSel = document.getElementById("stateSel"),
            districtSel = document.getElementById("districtSel");
        for (var country in stateObject) {
            countySel.options[countySel.options.length] = new Option(country, country);
        }
        countySel.onchange = function() {
            stateSel.length = 1; // remove all options bar first
            districtSel.length = 1; // remove all options bar first
            if (this.selectedIndex < 1) return; // done 
            for (var state in stateObject[this.value]) {
                stateSel.options[stateSel.options.length] = new Option(state, state);
            }
        }
        countySel.onchange(); // reset in case page is reloaded
        stateSel.onchange = function() {
            districtSel.length = 1; // remove all options bar first
            if (this.selectedIndex < 1) return; // done 
            var district = stateObject[countySel.value][this.value];
            for (var i = 0; i < district.length; i++) {
                districtSel.options[districtSel.options.length] = new Option(district[i], district[i]);
            }
        }
    }

</script>
<!-- page-banner start-->
<section class="page-banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>Add Service</h3>
                <ul class="banner-link text-center">
                    <li>
                        <a href="index-2.html">Home</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">service</a>
                    </li>
                    <li>
                        <span class="active">Add Service</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- page-banner ends-->

<!-- add-list start-->
<section class="bg-w sp-100">
    <div class="container">
        <div class="row mb-30">
            <div class="col-12">
                <h4 class="title-sep3 mb-30">
                    Add Service
                </h4>
            </div>
            <div class="col-12">
                <form class="listing-form" action="#">
                    <div class="row">
                        <div class="col-md-4 col-12">
                            <!--                            <label>city</label>
                            <textarea rows="10" name="comment" class="form-control" placeholder="write here" id="comment"></textarea>-->
                            <style>
                                .profile-pic {

                                    display: block;
                                }

                                .file-upload {
                                    display: none;
                                }

                                .circle {
                                    border-radius: 1000px !important;
                                    overflow: hidden;
                                    width: 75%;
                                    height: auto;
                                    border: 8px solid rgba(255, 255, 255, 0.7);
                                    position: absolute;
                                }

                                .p-image {
                                    top: 218px !important;
                                    position: absolute;
                                    right: 115px;
                                    color: #ff3a54;
                                    transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
                                }

                                .p-image:hover {
                                    transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
                                }

                                .upload-button {
                                    font-size: 3.2em;
                                }

                                .upload-button:hover {
                                    transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
                                    color: #999;
                                }

                            </style>
                            <div class="small-12 medium-2 large-2 columns">
                                <div class="circle">
                                    <!-- User Profile Image -->
                                    <img class="profile-pic" src="assets/img/img_avatar.png">

                                    <!-- Default Image -->
                                    <!-- <i class="fa fa-user fa-5x"></i> -->
                                </div>
                                <div class="p-image">
                                    <i class="fa fa-camera upload-button"></i>
                                    <input class="file-upload" type="file" accept="image/*" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-12">
                            <div class="row">
                                <div class="col-lg-6 col-12">
                                    <label>Name</label>
                                    <input type="text" id="google" class="form-control" placeholder="Full Name">
                                </div>
                                <div class="col-lg-6 col-12">
                                    <label>phone</label>
                                    <input type="text" id="phn" class="form-control" placeholder="phone no">
                                </div>
                                <div class="col-lg-6 col-12">
                                    <label>e mail</label>
                                    <input type="text" id="mail" class="form-control" placeholder="e mail">
                                </div>
                                <div class="col-lg-6 col-12">
                                    <label>facebook</label>
                                    <input type="text" id="fb" class="form-control" placeholder="facebook">
                                </div>

                                <div class="col-lg-6 col-12">
                                    <label>website</label>
                                    <input type="text" id="website" class="form-control" placeholder="website">
                                </div>
                                <div class="col-lg-6 col-12">
                                    <label>twitter</label>
                                    <input type="text" id="twitter" class="form-control" placeholder="twitter">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row mb-30">
            <div class="col-12">
                <h4 class="title-sep3 mb-30">
                    Service info
                </h4>
            </div>
            <div class="col-12">
                <form class="listing-form" action="#">
                    <div class="row">
                        <div class="col-12">
                            <label>service title</label>
                            <input type="text" id="name" class="form-control" placeholder="type here">
                        </div>
                        <div class="col-md-6 col-12">
                            <label>category</label>
                            <select class="form-control custom-select" id="categories">
                                <option disabled selected>select categories</option>
                                <option value="Beauty">Beauty Services</option>
                                <option value="Teaching">Teaching</option>
                                <option value="Fitness">Fitness</option>
                                <option value="Office">Office work</option>
                                <option value="Medical">Medical</option>
                                <option value="Food">Food</option>
                                <option value="Tailoring">Tailoring</option>
                                <option value="Others">Others</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-12">
                            <label>Sub category</label>
                            <select class="form-control custom-select" id="categories">
                                <option disabled selected>select categories</option>
                                <option value="Beauty">Beauty Services</option>
                                <option value="Teaching">Teaching</option>
                                <option value="Fitness">Fitness</option>
                                <option value="Office">Office work</option>
                                <option value="Medical">Medical</option>
                                <option value="Food">Food</option>
                                <option value="Tailoring">Tailoring</option>
                                <option value="Others">Others</option>
                            </select>
                        </div>
                        <div class="col-md-12 col-12">
                            <label>keywords</label>
                            <input type="text" id="keywords" class="form-control" placeholder="Keywords Here">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row mb-30">
            <div class="col-12">
                <h4 class="title-sep3 mb-30">
                    Service Area
                </h4>
            </div>
            <div class="col-12">
                <form class="listing-form" action="#">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <label>city</label>
                            <!-- <select class="form-control custom-select" id="categories2">
                                <option>select city</option>
                                <option>hotel</option>
                                <option>tour</option>
                                <option>pharmacy</option>
                                <option>shops</option>
                            </select>-->
                            <select class="form-control custom-select" name="state" id="countySel" size="1">
                                <option value="" selected="selected">Select Country</option>
                            </select>
                        </div>

                        <div class="col-md-6 col-12">
                            <label>state</label>
                            <!--
                            <input type="text" id="state" class="form-control" placeholder="enter Here">
-->
                            <select class="form-control" name="countrya" id="stateSel" size="1">
                                <option value="" selected="selected">Please select Country first</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-12">
                            <label>District</label>
                            <!--
                            <input type="text" id="code" class="form-control" placeholder="enter Here">
-->
                            <select class="form-control" name="district" id="districtSel" size="1">
                                <option value="" selected="selected">Please select State first</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-12">
                            <label>Zip Code</label>
                            <input type="text" id="code" class="form-control" placeholder="enter Here">
                        </div>
                        <div class="col-md-12 col-12">
                            <label>address</label>
                            <input type="text" id="adress" class="form-control" placeholder="address here">
                        </div>
                    </div>
                </form>
            </div>
            <!--<div class="col-12">
                <div class="map mb-30">
                    <div id="theme-map"></div>
                </div>
            </div>-->
        </div>

        <div class="row mb-30">
            <div class="col-12">
                <h4 class="title-sep3 mb-30">
                    Payment facilities
                </h4>
            </div>
            <div class="col-12">
                <form class="listing-form minus-pad" action="#">
                    <div class="row mb-30">
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <input type="checkbox" name="checkbox" id="checkbox_id1" value="value">
                            <label for="checkbox_id1">Credit Card</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <input type="checkbox" name="checkbox" id="checkbox_id2" value="value">
                            <label for="checkbox_id2">Debit Card</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <input type="checkbox" name="checkbox" id="checkbox_id3" value="value">
                            <label for="checkbox_id3">Cash Payment</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <input type="checkbox" name="checkbox" id="checkbox_id4" value="value">
                            <label for="checkbox_id4">Paytm</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <input type="checkbox" name="checkbox" id="checkbox_id5" value="value">
                            <label for="checkbox_id5">Google Pay</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <input type="checkbox" name="checkbox" id="checkbox_id6" value="value">
                            <label for="checkbox_id6">Phone Pe</label>
                        </div>

                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-12 mb-60">
                <h4 class="title-sep3 mb-30">
                    gallery
                </h4>
                <div class="drop-file">
                    <a href="#" class="w-100">
                        <i class="far fa-plus-square"></i>
                        <p class="mb-0">Drop files here to upload</p>
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-12 mb-60">
                <h4 class="title-sep3 mb-30">
                    cover image
                </h4>
                <div class="drop-file">
                    <a href="#" class="w-100">
                        <i class="far fa-plus-square"></i>
                        <p class="mb-0">Drop files here to upload</p>
                    </a>
                </div>
            </div>
        </div>
        <div class="row mb-30">
            <div class="col-12">
                <h4 class="title-sep3 mb-30">
                    service hours
                </h4>
            </div>
            <div class="col-12 mb-30">
                <div class="clone-wrap">
                    <div class="row clone-section">
                        <div class="col-sm-11 col-12">
                            <form class="listing-form" action="#">
                                <div class="row">
                                    <div class="col-lg-4 col-md-6 col-12">
                                        <select class="form-control custom-select" id="day">
                                            <option>day</option>
                                            <option>monday</option>
                                            <option>tuesday</option>
                                            <option>wednesday</option>
                                            <option>thrusday</option>
                                            <option>thrusday</option>
                                            <option>friday</option>
                                            <option>saturday</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-12">
                                        <select class="form-control custom-select" id="opening-time">
                                            <option>Opening Time</option>
                                            <option>11am</option>
                                            <option>12am</option>
                                            <option>1pm</option>
                                            <option>2pm</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-12">
                                        <select class="form-control custom-select" id="closing-time">
                                            <option>closing Time</option>
                                            <option>11pm</option>
                                            <option>12pm</option>
                                            <option>1pm</option>
                                            <option>2pm</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-1 col-12 text-sm-right text-center">
                            <span class="remove-section">
                                <i class="fa fa-times"></i>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <span class="add-section">
                                <i class="fa fa-plus"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-30">
            <div class="col-12">
                <h4 class="title-sep3 mb-30">
                    pricing
                </h4>
            </div>
            <div class="col-12 mb-30">
                <div class="clone-wrap2">
                    <div class="row clone-section">
                        <div class="col-sm-11 col-12">
                            <form class="listing-form" action="#">
                                <div class="row">
                                    <div class="col-lg-4 col-md-6 col-12">
                                        <input type="text" id="title" class="form-control" placeholder="title">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-12">
                                        <input type="text" id="description" class="form-control" placeholder="description">
                                    </div>
                                    <div class="col-lg-2 col-md-6 col-12">
                                        <input type="text" id="price" class="form-control" placeholder="price">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-1 col-12 text-sm-right text-center">
                            <span class="remove-section">
                                <i class="fa fa-times"></i>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <span class="add-section">
                                <i class="fa fa-plus"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <div class="listing-submit">
                    <button type="submit" class="btn btn-one btn-anim">save profile</button>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- add-list end -->
<script>
    $(document).ready(function() {


        var readURL = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('.profile-pic').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }


        $(".file-upload").on('change', function() {
            readURL(this);
        });

        $(".upload-button").on('click', function() {
            $(".file-upload").click();
        });
    });

</script>
<?php include 'footer.php';?>
