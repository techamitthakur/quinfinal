<?php include 'header.php';?>

<?php include 'jsonscript.php';?>
<!-- page-banner start-->
<section class="page-banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>Profile</h3>
                <ul class="banner-link text-center">
                    <li>
                        <a href="index-2.html">Home</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">User Edit Profile</a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</section>
<!-- page-banner ends-->
<?php foreach($user_info as $user){ ?>
<!-- add-list start-->
<section class="bg-w sp-100">
    <div class="container">
        <form class="listing-form" id="userform" method="post" action="<?php echo site_url('user/updateUsers'); ?>">
            <div class="row mb-30">
                <div class="col-12">
                    <h4 class="title-sep3 mb-30">
                        User Profile

                    </h4>
                </div>

                <div class="col-12">

                    <div class="row">
                        <div class="col-md-4 col-12">
                            <!--                            <label>city</label>
                            <textarea rows="10" name="comment" class="form-control" placeholder="write here" id="comment"></textarea>-->
                            <style>
                                .profile-pic {

                                    display: block;
                                }

                                .file-upload {
                                    display: none;
                                }

                                .circle {
                                    border-radius: 1000px !important;
                                    overflow: hidden;
                                    width: 75%;
                                    height: auto;
                                    border: 8px solid rgba(255, 255, 255, 0.7);
                                    position: absolute;
                                }

                                .p-image {
                                    top: 218px !important;
                                    position: absolute;
                                    right: 115px;
                                    color: #ff3a54;
                                    transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
                                }

                                .p-image:hover {
                                    transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
                                }

                                .upload-button {
                                    font-size: 3.2em;
                                }

                                .upload-button:hover {
                                    transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
                                    color: #999;
                                }

                            </style>
                            <div class="small-12 medium-2 large-2 columns">
                                <div class="circle">
                                    <!-- User Profile Image -->
                                    <?php if($user->image!=''){?>
                                    <img class="profile-pic" src="<?php echo base_url('public/upload/providerpic/'.$user->image)?>">
                                    <?php }else{?>
                                    <img class="profile-pic" src="<?php echo base_url('public/')?>fassets/img/img_avatar.png">
                                    <?php } ?>

                                    <!-- Default Image -->
                                    <!-- <i class="fa fa-user fa-5x"></i> -->
                                </div>
                                <div class="p-image">
                                    <i class="fa fa-camera upload-button"></i>
                                    <input class="file-upload" name="image" type="file" accept="image/*" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-12">
                            <div class="row">
                                <div class="col-lg-6 col-12">
                                    <label>Name</label>
                                    <input type="text" class="form-control" name="user_fname" placeholder="Full Name" value="<?php  echo $user->user_fname; ?>">
                                </div>
                                <div class="col-lg-6 col-12">
                                    <label>Last Name</label>
                                    <input type="text" name="user_lname" class="form-control" placeholder="Last Name" value="<?php  echo $user->user_lname; ?>">
                                </div>
                                <div class="col-lg-6 col-12">
                                    <label>phone</label>
                                    <input type="text" name="user_phone" class="form-control" placeholder="phone no" value="<?php  echo $user->user_phone; ?>" disabled>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <label>e mail</label>
                                    <input type="text" name="user_email" class="form-control" placeholder="email" value="<?php  echo $user->user_email; ?>" disabled>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <label>facebook</label>
                                    <input type="text" name="facebook" class="form-control" placeholder="facebook" value="<?php  echo $user->facebook; ?>">
                                </div>
                                <div class="col-lg-6 col-12">
                                    <label>twitter</label>
                                    <input type="text" name="twitter" class="form-control" placeholder="twitter" value="<?php  echo $user->twitter; ?>">
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row mb-30">
                <div class="col-12">
                    <h4 class="title-sep3 mb-30">Location
                    </h4>
                </div>
                <div class="col-12">
                    <!--                <form class="listing-form" action="#">-->
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <label>Country</label>
                            <select class="form-control custom-select" name="country" id="countySel" size="1">
                                <option value="" selected="selected">Select Country</option>
                            </select>
                        </div>

                        <div class="col-md-6 col-12">
                            <label>state</label>
                            <select class="form-control" name="state" id="stateSel" size="1" value="<?php  echo $user->state; ?>">
                                <?php if($user->state!=''){?>
                                <option value="<?php echo $user->state; ?>" selected="selected"><?php  echo $user->state; ?></option>
                                <?php }else{?>
                                <option value="" selected="selected">Please select State first</option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="col-md-6 col-12">
                            <label>City</label>
                            <select class="form-control" name="city" id="districtSel" size="1" value="<?php  echo $user->city; ?>">
                                <?php if($user->city!=''){?>
                                <option value="<?php echo $user->city; ?>" selected="selected"><?php  echo $user->city; ?></option>
                                <?php }else{?>
                                <option value="" selected="selected">Please select State first</option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="col-md-6 col-12">
                            <label>Zip Code</label>
                            <input type="text" id="code" class="form-control" name="zipcode" placeholder="enter Here" value="<?php  echo $user->zipcode; ?>">
                        </div>
                        <div class="col-md-12 col-12">
                            <label>address</label>
                            <input type="text" id="address" class="form-control" name="address" placeholder="address here" value="<?php  echo $user->address; ?>">
                        </div>
                    </div>
                    <!--                </form>-->
                </div>
                <!--<div class="col-12">
                <div class="map mb-30">
                    <div id="theme-map"></div>
                </div>
            </div>-->
            </div>



            <div class="row">
                <div class="col-12 text-center">
                    <div class="listing-submit">
                        <input type="submit" class="btn btn-one btn-anim" id="subid" value="save profile">
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- add-list end -->
<?php } ?>
<?php include 'footer.php'; ?>

<script>
    $(document).ready(function(e) {
        $('#userform').on('submit', (function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            var url = $('#userform').attr('action');
            //        alert(formData);
            $.ajax({
                type: 'POST',
                url: url,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    if (data == 1) {
                        alert("User Profile updated");
                    } else {
                        alert("User Profile not updated");
                    }
                },
                error: function(data) {
                    console.log("error");
                    console.log(data);
                }
            });
        }));

        //    $("#ImageBrowse").on("change", function() {
        //        $("#providerform").submit();
        //    });
    });
    //    $(document).on('submit', '#userform', function(e) {
    //        e.preventDefault();
    //        //     
    //        //var data2=$("#userform").serialize(); // only use when passing string
    //
    //        var formdata = $("#userform").serialize();
    //        var url = $('#userform').attr('action');
    //
    //        //   alert(formdata); 
    //        $.ajax({
    //            type: "POST",
    //            url: url,
    //            data: formdata,
    //            dataType: "json",
    //            //     cache:false,
    //            //     contentType: false,
    //            //     processData: false,
    //            success: function(success) {
    //                //success message mybe...
    //                if (success == 1) {
    //                    alert("Profile Upadated");
    //                } else {
    //                    alert("Profile Not Upadated");
    //                }
    //            }
    //        });
    //    });

    $(document).ready(function() {



        var readURL = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('.profile-pic').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }


        $(".file-upload").on('change', function() {
            readURL(this);
        });

        $(".upload-button").on('click', function() {
            $(".file-upload").click();
        });
    });

</script>
