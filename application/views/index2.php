<?php include 'header.php';?>

<!-- banner start -->
<section class="main-banner-2">
    <div class="banner-image2">
        <div class="tilt-anim" data-tilt>
            <div class="banner-content">
                <p class="text-uppercase mb-4">we own a strong hold on local business information pan India</p>
                <h2>Find The Best Service In Your City</h2>
            </div>
        </div>
        <div class="mt-5 text-center">
            <a href="#" class="btn btn-two btn-anim mt-5 br-5 px-5">
                <i class="fa fa-search"></i>explore more </a>
        </div>
    </div>
</section>
<!-- banner end -->


<!-- Services start -->
<section class="bg-w tri-bg sp-100-70 o-hide">
    <div class="container">
        <div class="list-category-slider owl-carousel owl-theme mb-60">

            <?php foreach($category as $cat){ ?>
            <div class="list-items2 btn-anim">
                <div class="icon-box">
                    <i class="flaticon-find"></i>
                </div>
                <h5 class="mb-0 mt-3">
                    <a href="#"><?php echo $cat->category_name; ?></a>
                </h5>
            </div>
            <?php } ?>

        </div>
        <div class="row justify-content-md-center">
            <div class="col-lg-3 col-md-6">
                <input type="text" class="form-control" placeholder="What are you looking for?">
            </div>
            <div class="col-lg-3 col-md-6">
                <select class="form-control custom-select" id="service2">
                    <option>all categories</option>
                    <option>Tution Teacher</option>
                    <option>Dance teacher </option>
                    <option>Beautician</option>
                    <option>House Maids</option>
                </select>

            </div>
            <div class="col-lg-2 col-md-4">
                <button type="submit" class="btn btn-one btn-anim br-5 w-100 mb-30">
                    <i class="fa fa-search"></i> search</button>
            </div>
        </div>
    </div>
</section>
<!-- Services start -->


<!-- popular Services start -->
<section class="bg-dull sp-100-70">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="all-title">
                    <h3 class="sec-title">
                        popular Services
                    </h3>
                    <svg class="title-sep">
                        <path fill-rule="evenodd" d="M32.000,13.000 L32.000,7.000 L35.000,7.000 L35.000,13.000 L32.000,13.000 ZM24.000,4.000 L27.000,4.000 L27.000,16.000 L24.000,16.000 L24.000,4.000 ZM16.000,-0.000 L19.000,-0.000 L19.000,20.000 L16.000,20.000 L16.000,-0.000 ZM8.000,4.000 L11.000,4.000 L11.000,16.000 L8.000,16.000 L8.000,4.000 ZM-0.000,7.000 L3.000,7.000 L3.000,13.000 L-0.000,13.000 L-0.000,7.000 Z" />
                    </svg>
                    <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return</p>
                </div>
            </div>
        </div>
        <div class="row">

            <?php foreach($category as $cat){ ?>
            <div class="col-lg-4 col-md-6 col-12 mb-30">
                <div class="listing-item p-2">
                    <div class="img-holder">
                        <!--
                                <span class="offer">save 49%
                                </span>
-->
                        <img src="<?php echo base_url('public/')?>fassets/img/home/pl-slide1.jpg" alt="list">
                        <!--
                                <div class="rate-like centering justify-content-between">
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </div>
                                    <a class="likes" href="#">
                                        <i class="far fa-heart"></i>55
                                    </a>
                                </div>
-->
                    </div>
                    <div class="list-content p-2">
                        <!--
                                <ul class="ctg-info py-2 mb-3">
                                    <li>
                                        <a href="#">
                                            <i class="flaticon-cutlery mr-2"></i> restaurants</a>
                                    </li>
                                    <li>
                                        <span class="c-theme"> open now</span>
                                    </li>
                                </ul>
-->
                        <h5 class="mb-2">
                            <a href="listing-detail.html"><?php echo $cat->category_name; ?></a>
                        </h5>
                        <!--                                <p>Donec pede justo, fringilla vel, aliquet nectior</p>-->
                        <!--
                                <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                    <li class="mt-1">
                                        <a href="#">
                                            <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                    </li>
                                    <li class="mt-1">
                                        <a href="#">
                                            <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                    </li>
                                </ul>
-->
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
<!-- popular Services start -->

<!-- counter starts -->
<section class="counters sp-100-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-12 mb-30">
                <div class="counter-box">
                    <div class="icon-box">
                        <i class="flaticon-list-1"></i>
                    </div>
                    <h2 class="count c-theme" data-count="5256">0</h2>
                    <p>total Services</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12 mb-30">
                <div class="counter-box">
                    <div class="icon-box">
                        <i class="flaticon-team"></i>
                    </div>
                    <h2 class="count c-theme" data-count="1250">0</h2>
                    <p>total Services</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12 mb-30">
                <div class="counter-box">
                    <div class="icon-box">
                        <i class="flaticon-trophy"></i>
                    </div>
                    <h2 class="count c-theme" data-count="160">0</h2>
                    <p>total Services</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12 mb-30">
                <div class="counter-box">
                    <div class="icon-box">
                        <i class="flaticon-appointment"></i>
                    </div>
                    <h2 class="count c-theme" data-count="12">0</h2>
                    <p>total Services</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- counter ends -->

<!-- features start-->
<section class="features sp-100-70 bg-w">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="all-title">
                    <h3 class="sec-title">
                        Why We Are Best
                    </h3>
                    <svg class="title-sep">
                        <path fill-rule="evenodd" d="M32.000,13.000 L32.000,7.000 L35.000,7.000 L35.000,13.000 L32.000,13.000 ZM24.000,4.000 L27.000,4.000 L27.000,16.000 L24.000,16.000 L24.000,4.000 ZM16.000,-0.000 L19.000,-0.000 L19.000,20.000 L16.000,20.000 L16.000,-0.000 ZM8.000,4.000 L11.000,4.000 L11.000,16.000 L8.000,16.000 L8.000,4.000 ZM-0.000,7.000 L3.000,7.000 L3.000,13.000 L-0.000,13.000 L-0.000,7.000 Z" />
                    </svg>
                    <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-12 mb-30">
                <div class="feature-item type-2">
                    <div class="icon-box">
                        <i class="flaticon-rising"></i>
                    </div>
                    <h5>our vision</h5>
                    <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 mb-30">
                <div class="feature-item type-2">
                    <div class="icon-box">
                        <i class="flaticon-list"></i>
                    </div>
                    <h5>missions</h5>
                    <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 mb-30">
                <div class="feature-item type-2">
                    <div class="icon-box">
                        <i class="flaticon-medal"></i>
                    </div>
                    <h5>our value</h5>
                    <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return
                    </p>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- features end -->

<!-- categories start-->
<section class="categories sp-100-70 bg-dull">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="all-title">
                    <h3 class="sec-title">
                        categories
                    </h3>
                    <svg class="title-sep">
                        <path fill-rule="evenodd" d="M32.000,13.000 L32.000,7.000 L35.000,7.000 L35.000,13.000 L32.000,13.000 ZM24.000,4.000 L27.000,4.000 L27.000,16.000 L24.000,16.000 L24.000,4.000 ZM16.000,-0.000 L19.000,-0.000 L19.000,20.000 L16.000,20.000 L16.000,-0.000 ZM8.000,4.000 L11.000,4.000 L11.000,16.000 L8.000,16.000 L8.000,4.000 ZM-0.000,7.000 L3.000,7.000 L3.000,13.000 L-0.000,13.000 L-0.000,7.000 Z" />
                    </svg>
                    <p>We’re committed to providing the best customer service possible.When you place your trust in us, here’s what you can expect in return</p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <?php foreach($category as $cat1){ ?>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-30">
                <div class="ctg-item">
                    <div class="icon-box" style="background-image:url('assets/img/home/cat1.jpg')">
                        <i class="flaticon-map"></i>
                    </div>
                    <div class="content-box p-4">
                        <h5 class="mb-1">
                            <a href="#"><?php echo $cat1->category_name; ?> </a>
                        </h5>

                        <p class="mb-0"><?php echo $this->users_model->countSubCat($cat1->category_name); ?></p>

                    </div>
                </div>
            </div>


            <?php }?>


        </div>
    </div>
</section>
<!-- categories end -->

<!-- cta-one start-->
<section class="cta-one tri-bg-w text-lg-left text-center">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 my-lg-0 my-5 py-lg-0 py-5">
                <div class="cta-content">
                    <h3>Sign Up To Get Special Services Every Day</h3>
                    <p>Singup to get updated with latest services near you</p>
                    <a href="#" class="btn btn-two btn-anim br-5 mt-2">
                        sign up
                    </a>
                </div>
            </div>
            <div class="col-lg-6 d-lg-block d-none">
                <div class="cta-img mt-4">
                    <img src="<?php echo base_url('public/')?>fassets/img/home/cta-bg.png" alt="image">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- cta-one end -->


<!-- testimonial start-->
<section class="sp-100 bg-dull">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="all-title">
                    <h3 class="sec-title">
                        Testimonials
                    </h3>
                    <svg class="title-sep">
                        <path fill-rule="evenodd" d="M32.000,13.000 L32.000,7.000 L35.000,7.000 L35.000,13.000 L32.000,13.000 ZM24.000,4.000 L27.000,4.000 L27.000,16.000 L24.000,16.000 L24.000,4.000 ZM16.000,-0.000 L19.000,-0.000 L19.000,20.000 L16.000,20.000 L16.000,-0.000 ZM8.000,4.000 L11.000,4.000 L11.000,16.000 L8.000,16.000 L8.000,4.000 ZM-0.000,7.000 L3.000,7.000 L3.000,13.000 L-0.000,13.000 L-0.000,7.000 Z"></path>
                    </svg>
                    <p>love from our clients</p>
                </div>
            </div>
        </div>
        <div class="testi-slider1 owl-carousel owl-theme">

            <?php foreach($testimonial as $test){ ?>
            <div class="slide-item">


                <div class="testi-item mb-30">
                    <div class="img-holder">
                        <img src="<?php echo base_url('public/upload/testimonial/').$test->image ?>" alt="lister" style="width:400px; height:100px;">
                    </div>
                    <div class="testi-content">
                        <h5 class="mb-1"><?php echo $test->name; ?></h5>
                        <span class="c-theme mb-3 d-block"><?php echo $test->designation; ?></span>
                        <p class="mb-0"><?php echo $test->content; ?></p>
                    </div>
                </div>


            </div>
            <?php }?>


        </div>
    </div>
</section>
<!-- testimonial end -->

<!-- app start-->
<div class="app-section bg-red tri-bg-w sp-100">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 d-none d-lg-block">
                <div class="app-img mr-5">
                    <img src="<?php echo base_url('public/')?>fassets/img/home/app-phn.png" alt="lister-app">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="app-content text-lg-left text-center">
                    <h3>Android App and iOS App Available </h3>
                    <p class="mb-4">Quinn Android / ios App is also available at store and play store</p>
                    <div class="app-icon">
                        <a href="#" class="mr-4">
                            <img src="<?php echo base_url('public/')?>fassets/img/home/google-play.png" alt="google-play">
                        </a>
                        <a href="#">
                            <img src="<?php echo base_url('public/')?>fassets/img/home/play-store.png" alt="play-store">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- app end -->
<?php include 'footer.php';?>
