<?php include 'header.php';?>

<?php include 'jsonscript.php';?>
<!-- page-banner start-->
<section class="page-banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>Profile</h3>
                <ul class="banner-link text-center">
                    <li>
                        <a href="index-2.html">Home</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Provider Profile</a>
                    </li>
                   
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- page-banner ends-->
<?php foreach($provider_info as $pro_info){

                // print_r($pro_info);
                // echo $pro_info->id;

    ?>
<!-- add-list start-->
<section class="bg-w sp-100">
    <div class="container">
        <form class="listing-form" id="providerform" method="post" action="<?php echo site_url('user/updateProvider'); ?>" enctype="multipart/form-data">
            <div class="row mb-30">
                <div class="col-12">
                    <h3 class="title-sep3 mb-30">
                        <?php echo $pro_info->pro_fname.' '.$pro_info->pro_lname;?>
                    </h3>
                </div>
                <div class="col-12">
                    <!--                <form class="listing-form" action="#">-->
                    <div class="row">
                        <div class="col-md-4 col-12">
                            <!--                            <label>city</label>
                            <textarea rows="10" name="comment" class="form-control" placeholder="write here" id="comment"></textarea>-->
                            <style>
                                .profile-pic {

                                    display: block;
                                }

                                .file-upload {
                                    display: none;
                                }

                                .circle {
                                    border-radius: 1000px !important;
                                    overflow: hidden;
                                    width: 75%;
                                    height: auto;
                                    border: 8px solid rgba(255, 255, 255, 0.7);
                                    position: absolute;
                                }

                                .p-image {
                                    top: 218px !important;
                                    position: absolute;
                                    right: 115px;
                                    color: #ff3a54;
                                    transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
                                }

                                .p-image:hover {
                                    transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
                                }

                                .upload-button {
                                    font-size: 3.2em;
                                }

                                .upload-button:hover {
                                    transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
                                    color: #999;
                                }

                            </style>

                            <div class="small-12 medium-2 large-2 columns">
                                <div class="circle">
                                    <!-- User Profile Image -->
                                    <?php if($pro_info->image!=''){?>
                                    <img class="profile-pic" src="<?php echo base_url('public/upload/providerpic/'.$pro_info->image)?>">
                                    <?php }else{?>
                                    <img class="profile-pic" src="<?php echo base_url('public/')?>fassets/img/img_avatar.png">
                                    <?php } ?>

                                    <!-- Default Image -->
                                    <!-- <i class="fa fa-user fa-5x"></i> -->
                                </div>
                                <div class="p-image">
                                    <i class="fa fa-camera upload-button"></i>
                                    <input class="file-upload" name="image" type="file" accept="image/*" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-12">
                            <div class="row">
                                <div class="col-lg-6 col-12">
                                    <label>Name</label>
                                    <input type="text" id="firstname" class="form-control" name="pro_fname" value="<?php echo $pro_info->pro_fname;?>" placeholder="Full Name">
                                    <input type="file" class="file-upload" name="imagee" accept="image/*" />
                                </div>

                                <div class="col-lg-6 col-12">
                                    <label>website</label>
                                    <input type="text" id="lastname" class="form-control" name="pro_lname" value="<?php echo $pro_info->pro_lname;?>" placeholder="Last Name">
                                </div>
                                <div class="col-lg-6 col-12">
                                    <label>phone</label>
                                    <input type="text" id="phn" class="form-control" value="<?php echo $pro_info->pro_phone;?>" placeholder="phone no" disabled>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <label>e mail</label>
                                    <input type="email" id="mail" class="form-control" value="<?php echo $pro_info->pro_email;?>" placeholder="email" disabled>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <label>facebook</label>
                                    <input type="text" id="fb" class="form-control" name="facebook" value="<?php echo $pro_info->facebook;?>" placeholder="facebook" />
                                </div>


                                <div class="col-lg-6 col-12">
                                    <label>twitter</label>
                                    <input type="text" id="twitter" class="form-control" name="twitter" value="<?php echo $pro_info->twitter;?>" placeholder="twitter">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--                </form>-->
                </div>
            </div>
            <div class="row mb-30">
                <div class="col-12">
                    <h4 class="title-sep3 mb-30">
                        Service info
                    </h4>
                </div>
                <div class="col-12">


                    <div class="row">
                        <div class="col-12">
                            <label>service title</label>
                            <input type="text" id="name" name="sevice_title" class="form-control" placeholder="type here">
                        </div>
                        <div class="col-md-6 col-12">
                            <label>category</label>
                            <select class="form-control custom-select" name="category" id="categories" value="<?php echo $pro_info->pro_fname;?>">
                                <option value="<?php echo $pro_info->pro_fname;?>" selected>v<?php echo $pro_info->pro_fname;?></option>
                                <option value="Beauty">Beauty Services</option>
                                <option value="Teaching">Teaching</option>
                                <option value="Fitness">Fitness</option>
                                <option value="Office">Office work</option>
                                <option value="Medical">Medical</option>
                                <option value="Food">Food</option>
                                <option value="Tailoring">Tailoring</option>
                                <option value="Others">Others</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-12">
                            <label>Sub category</label>
                            <select class="form-control custom-select" id="categories">
                                <option disabled selected>select categories</option>
                                <option value="Beauty">Beauty Services</option>
                                <option value="Teaching">Teaching</option>
                                <option value="Fitness">Fitness</option>
                                <option value="Office">Office work</option>
                                <option value="Medical">Medical</option>
                                <option value="Food">Food</option>
                                <option value="Tailoring">Tailoring</option>
                                <option value="Others">Others</option>
                            </select>
                        </div>
                        <div class="col-md-12 col-12">
                            <label>keywords</label>
                            <input type="text" id="keywords" class="form-control" value="<?php echo $pro_info->pro_fname;?>" placeholder="Keywords Here">
                        </div>
                    </div>
                    <!--                </form>-->
                </div>
            </div>
            <div class="row mb-30">
                <div class="col-12">
                    <h4 class="title-sep3 mb-30">
                        Service Area
                    </h4>
                </div>
                <div class="col-12">
                    <!--                <form class="listing-form" action="#">-->
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <label>city</label>
                            <!-- <select class="form-control custom-select" id="categories2">
                                <option>select city</option>
                                <option>hotel</option>
                                <option>tour</option>
                                <option>pharmacy</option>
                                <option>shops</option>
                            </select>-->
                            <select class="form-control custom-select" name="state" id="countySel" size="1">
                                <option value="" selected="selected">Select Country</option>
                            </select>
                        </div>

                        <div class="col-md-6 col-12">
                            <label>state</label>
                            <!--
                            <input type="text" id="state" class="form-control" placeholder="enter Here">
-->
                            <select class="form-control" name="countrya" id="stateSel" size="1">
                                <option value="" selected="selected">Please select Country first</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-12">
                            <label>District</label>
                            <!--
                            <input type="text" id="code" class="form-control" placeholder="enter Here">
-->
                            <select class="form-control" name="district" id="districtSel" size="1">
                                <option value="" selected="selected">Please select State first</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-12">
                            <label>Zip Code</label>
                            <input type="text" id="code" class="form-control" placeholder="enter Here">
                        </div>
                        <div class="col-md-12 col-12">
                            <label>address</label>
                            <input type="text" id="adress" class="form-control" placeholder="address here">
                        </div>
                    </div>
                    <!--                </form>-->
                </div>
                <!--<div class="col-12">
                <div class="map mb-30">
                    <div id="theme-map"></div>
                </div>
            </div>-->
            </div>

            <!--
        <div class="row mb-30">
            <div class="col-12">
                <h4 class="title-sep3 mb-30">
                    Payment facilities
                </h4>
            </div>
            <div class="col-12">

                    <div class="row mb-30">
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <input type="checkbox" name="checkbox" id="checkbox_id1" value="value">
                            <label for="checkbox_id1">Credit Card</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <input type="checkbox" name="checkbox" id="checkbox_id2" value="value">
                            <label for="checkbox_id2">Debit Card</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <input type="checkbox" name="checkbox" id="checkbox_id3" value="value">
                            <label for="checkbox_id3">Cash Payment</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <input type="checkbox" name="checkbox" id="checkbox_id4" value="value">
                            <label for="checkbox_id4">Paytm</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <input type="checkbox" name="checkbox" id="checkbox_id5" value="value">
                            <label for="checkbox_id5">Google Pay</label>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <input type="checkbox" name="checkbox" id="checkbox_id6" value="value">
                            <label for="checkbox_id6">Phone Pe</label>
                        </div>

                    </div>

            </div>
        </div>
-->

            <div class="row mb-30">

                <div class="col-12">
                    <h4 class="title-sep3 mb-30">
                        service hours
                    </h4>
                </div>
                <div class="col-12 mb-30">
                    <div class="clone-wrap">
                        <div class="row clone-section">
                            <div class="col-sm-11 col-12">

                                <div class="row">
                                    <div class="col-lg-4 col-md-6 col-12">
                                        <select class="form-control custom-select" id="day">
                                            <option>day</option>
                                            <option>monday</option>
                                            <option>tuesday</option>
                                            <option>wednesday</option>
                                            <option>thrusday</option>
                                            <option>thrusday</option>
                                            <option>friday</option>
                                            <option>saturday</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-12">
                                        <select class="form-control custom-select" id="opening-time">
                                            <option>Opening Time</option>
                                            <option>11am</option>
                                            <option>12am</option>
                                            <option>1pm</option>
                                            <option>2pm</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-12">
                                        <select class="form-control custom-select" id="closing-time">
                                            <option>closing Time</option>
                                            <option>11pm</option>
                                            <option>12pm</option>
                                            <option>1pm</option>
                                            <option>2pm</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-1 col-12 text-sm-right text-center">
                                <span class="remove-section">
                                    <i class="fa fa-times"></i>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 text-center">
                                <span class="add-section">
                                    <i class="fa fa-plus"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--
        <div class="row mb-30">
            <div class="col-12">
                <h4 class="title-sep3 mb-30">
                    pricing
                </h4>
            </div>
            <div class="col-12 mb-30">
                <div class="clone-wrap2">
                    <div class="row clone-section">
                        <div class="col-sm-11 col-12">
                           <div class="row">
                                    <div class="col-lg-4 col-md-6 col-12">
                                        <input type="text" id="title" class="form-control" placeholder="title">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-12">
                                        <input type="text" id="description" class="form-control" placeholder="description">
                                    </div>
                                    <div class="col-lg-2 col-md-6 col-12">
                                        <input type="text" id="price" class="form-control" placeholder="price">
                                    </div>
                                </div>
                           </div>
                        <div class="col-sm-1 col-12 text-sm-right text-center">
                            <span class="remove-section">
                                <i class="fa fa-times"></i>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <span class="add-section">
                                <i class="fa fa-plus"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
-->
            <div class="row mb-60">
                <div class="col-12 text-center">
                    <div class="listing-submit">
                        <!--                    <button type="submit" class="btn btn-one btn-anim">save profile</button>-->
                        <input type="submit" class="btn btn-one btn-anim" id="hg" value="save profile">
                    </div>
                </div>
            </div>
        </form>
        <style>
            .backc {
                background: url(<?php echo base_url('public/upload/providerpic/cover/'.$pro_info->cover);
                ?>);
                background-repeat: no-repeat;
                background-size: cover;
            }

            .backg {
                background: url(<?php echo base_url('public/upload/providerpic/cover/'.$pro_info->gallery);
                ?>);
                background-repeat: no-repeat;
                background-size: auto;
            }

        </style>
        <!--******************Cover & gallery upload****************************-->
        <div class="row">
            <div class="col-lg-12 col-12 ">
                <h4 class="title-sep3 mb-30">
                    Cover
                </h4>
                <form class="listing-form" id="providerCover" method="post" action="<?php echo site_url('user/providerCover'); ?>" enctype="multipart/form-data">
                    <div class="drop-file backc">
                        <a href="#" class="w-100">
                            <label for="gellery" style="color:#fff;font-size: x-large;">Click To Upload
                                <i class="far fa-plus-square"></i></label>
                            <input type="file" name="image" id="gellery" multiple="true" style="display:none" accept="image/jpg, image/jpeg" />
                            <input type="hidden" name="pro_email" value="<?php echo $pro_info->pro_email;?>" />


                        </a>
                        <input type="submit" class="btn btn-one btn-anim" value="upload" />
                    </div>
                </form>
            </div>
            <!--
            <div class="col-lg-6 col-12" style="">
                <h4 class="title-sep3 mb-30">
                    Gallery image
                </h4>
                <form class="listing-form" id="providerGallery"  method="post" action="<?php echo site_url('user/providerGallery'); ?>" enctype="multipart/form-data">
                <div class="drop-file backg">
                    <a href="#" class="w-100">
                        <label for="cover">Click Me
                        <i class="far fa-plus-square"></i></label>
                        <input type="file" name="image" id="cover" multiple="true" style="display:none" accept="image/jpg, image/jpeg"/>
                        <input type="hidden" name="pro_email" value="<?php echo $pro_info->pro_email;?>" />
                        
                       
                    </a>
                    <input type="submit" class="btn btn-one btn-anim" value="upload" />
                </div>
                </form>
            </div>
-->
        </div>
        <!--**********************************************************************-->
    </div>
</section>

<?php } ?>
<?php include 'footer.php';?>
<!-- add-list end -->
<script>
    //profile image update
    $(document).ready(function(e) {
        $('#providerform').on('submit', (function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            var url = $('#providerform').attr('action');
            //        alert(formData);
            $.ajax({
                type: 'POST',
                url: url,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    if (data == 1) {
                        alert("Profile updated");
                    } else {
                        alert("Profile not updated");
                    }
                },
                error: function(data) {
                    console.log("error");
                    console.log(data);
                }
            });
        }));
    });
    //end profile image upload

    //cover image upload
    $(document).ready(function(e) {
        $('#providerCover').on('submit', (function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            var url = $('#providerCover').attr('action');
            //       alert('formData');
            $.ajax({
                type: 'POST',
                url: url,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    if (data == 1) {
                        alert("Cover updated");
                        location.reload();
                    } else {
                        alert("Cover not updated  PLease select an image" + data);
                    }
                },
                error: function(data) {
                    console.log("error");
                    console.log(data);
                }
            });
        }));
    });
    //cover image upload

    //Gallery image upload
    //    $(document).ready(function (e) {
    //    $('#providerGallery').on('submit',(function(e) {
    //        e.preventDefault();
    //        var formData = new FormData(this);
    //         var url= $('#providerGallery').attr('action');
    ////       alert('Gallery image');
    //        $.ajax({
    //            type:'POST',
    //            url: url,
    //            data:formData,
    //            cache:false,
    //            contentType: false,
    //            processData: false,
    //            success:function(data){
    //               if(data==1){
    //                   alert("Cover updated");
    //               }else{
    //                   alert("Cover not updated PLease select an image"+data);
    //               }
    //            },
    //            error: function(data){
    //                console.log("error");
    //                console.log(data);
    //            }
    //        });
    //    }));
    //});
    //Gallery image upload

    $(document).ready(function() {
        var readURL = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('.profile-pic').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }


        $(".file-upload").on('change', function() {
            readURL(this);
        });

        $(".upload-button").on('click', function() {
            $(".file-upload").click();
        });
    });

</script>
