<?php include 'header.php';?>


<!-- page-banner start-->
<section class="page-banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>Search</h3>
                <ul class="banner-link text-center">
                    <li>
                        <a href="index-2.html">Home</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Search</a>
                    </li>
                    <li>
                        <span class="active">Search page</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- page-banner ends-->

<!-- listings start-->
<div class="bg-w sp-100">
    <div class="container">
        <div class="filter-box mb-30">
            <form action="#" method="post">
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                        <input type="text" class="form-control" placeholder="What are you looking for?">
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                        <div class="input-wrap">
                            <i class="fa fa-crosshairs"></i>
                            <input type="text" class="form-control" placeholder="location">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                        <div class="input-wrap">
                            <select class="form-control custom-select" id="service2">
                                <option>all categories</option>
                                <option>hotel</option>
                                <option>tour</option>
                                <option>pharmacy</option>
                                <option>shops</option>
                            </select>
                        </div>
                    </div>
<!--
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                        <div class="input-wrap">
                            <i class="fas fa-dollar-sign"></i>
                            <input type="text" class="form-control" placeholder="location">
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-8 col-sm-12 col-12">
                        <div class="input-wrap range-box">
                            <p>distance radius :
                                <span class="range-value">30</span>
                                km
                            </p>
                            <div class="range-slider">
                                <input type="range" min="1" max="100" value="30" class="range-track" id="myRange">
                                <div class="fill" style="width:30%"></div>
                            </div>
                        </div>
                    </div>
-->
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                        <button type="submit" class="btn btn-one btn-anim w-100">
                            filter now</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="checklist-box mb-30">
            <div class="toggle-checklist mb-60">
                <a href="#" class="toggle-icon open">
                    <i class="fa fa-plus"></i>
                </a>
            </div>
<!--
            <div class="filter-checklist">
                <div class="row minus-pad">
                    <div class="col-md-4 mb-30">
                        <h5 class="mb-30">rating</h5>
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id1" value="value">
                                    <label for="checkbox_id1">Excellent 9+</label>
                                </div>
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id2" value="value">
                                    <label for="checkbox_id2">Superb 8+</label>
                                </div>
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id3" value="value">
                                    <label for="checkbox_id3">Very good 7+</label>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id4" value="value">
                                    <label for="checkbox_id4">Good 6+</label>
                                </div>
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id5" value="value">
                                    <label for="checkbox_id5">Pleasant 5+</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 mb-30">
                        <h5 class="mb-30">facilities</h5>
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id6" value="value">
                                    <label for="checkbox_id6">Card Payment</label>
                                </div>
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id7" value="value">
                                    <label for="checkbox_id7">Cash Payment</label>
                                </div>
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id8" value="value">
                                    <label for="checkbox_id8">Free Wi-Fi</label>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id9" value="value">
                                    <label for="checkbox_id9">Family Friendly</label>
                                </div>
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id10" value="value">
                                    <label for="checkbox_id10">Wheelchair</label>
                                </div>
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id11" value="value">
                                    <label for="checkbox_id11">Air Conditioning</label>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id12" value="value">
                                    <label for="checkbox_id12">Fitness Center</label>
                                </div>
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id13" value="value">
                                    <label for="checkbox_id13">Reservations</label>
                                </div>
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id14" value="value">
                                    <label for="checkbox_id14">Smoking Allowed</label>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id15" value="value">
                                    <label for="checkbox_id15">Swimming Pool</label>
                                </div>
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id16" value="value">
                                    <label for="checkbox_id16">Coupons</label>
                                </div>
                                <div class="w-100 mb-2">
                                    <input type="checkbox" name="checkbox" id="checkbox_id17" value="value">
                                    <label for="checkbox_id17">Pet Friendly</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
-->
        </div>
        <div class="action-list">
            <div class="row">
                <div class="col-md-3 col-6 mb-30">
                    <div class="sort-filter">
                        <div class="sort-dropdown">
                            <a href="#" class="dropdown-toggle text-capitalize" data-toggle="dropdown">
                                Popularity
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">low to high</a>
                                <a class="dropdown-item" href="#">hight to low</a>
                                <a class="dropdown-item" href="#">relevance</a>
                                <a class="dropdown-item" href="#">discount</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-6 mb-30">
                    <div class="view-mode justify-content-end d-flex">
                        <div class="grid-view views current">
                            <i class="fa fa-th"></i>
                        </div>
                        <div class="list-view views">
                            <i class="fa fa-list-ul"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-grid-view show-list">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="listing-item p-2">
                        <div class="img-holder">
                            <span class="offer">save 49%
                            </span>
                            <img src="assets/img/home/pl-slide1.jpg" alt="list">
                            <div class="rate-like centering justify-content-between">
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                </div>
                                <a class="likes" href="#">
                                    <i class="far fa-heart"></i>55
                                </a>
                            </div>
                        </div>
                        <div class="list-content p-2">
                            <ul class="ctg-info py-2 mb-3">
                                <li>
                                    <a href="#">
                                        <i class="flaticon-cutlery mr-2"></i> restaurants</a>
                                </li>
                                <li>
                                    <span class="c-theme"> open now</span>
                                </li>
                            </ul>
                            <h5 class="mb-2">
                                <a href="listing-detail.html">the lounge & bar</a>
                            </h5>
                            <p>Donec pede justo, fringilla vel, aliquet nectior</p>
                            <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="listing-item p-2">
                        <div class="img-holder">
                            <span class="offer">save 30%
                            </span>
                            <img src="assets/img/home/pl-slide2.jpg" alt="list">
                            <div class="rate-like centering justify-content-between">
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                </div>
                                <a class="likes" href="#">
                                    <i class="far fa-heart"></i>42
                                </a>
                            </div>
                        </div>
                        <div class="list-content p-2">
                            <ul class="ctg-info py-2 mb-3">
                                <li>
                                    <a href="#">
                                        <i class="flaticon-shop mr-2"></i>shopping</a>
                                </li>
                                <li>
                                    <span class="c-theme"> open now</span>
                                </li>
                            </ul>
                            <h5 class="mb-2">
                                <a href="listing-detail.html">the best shop in city</a>
                            </h5>
                            <p>Donec pede justo, fringilla vel, aliquet nectior</p>
                            <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="listing-item p-2">
                        <div class="img-holder">
                            <span class="offer">save 19%
                            </span>
                            <img src="assets/img/home/pl-slide3.jpg" alt="list">
                            <div class="rate-like centering justify-content-between">
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="far fa-star"></i>
                                </div>
                                <a class="likes" href="#">
                                    <i class="far fa-heart"></i>24
                                </a>
                            </div>
                        </div>
                        <div class="list-content p-2">
                            <ul class="ctg-info py-2 mb-3">
                                <li>
                                    <a href="#">
                                        <i class="flaticon-cheers mr-2"></i>nightlife</a>
                                </li>
                                <li>
                                    <span class="c-theme"> open now</span>
                                </li>
                            </ul>
                            <h5 class="mb-2">
                                <a href="listing-detail.html">enjoy best nightlife</a>
                            </h5>
                            <p>Donec pede justo, fringilla vel, aliquet nectior</p>
                            <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="listing-item p-2">
                        <div class="img-holder">
                            <span class="offer">save 59%
                            </span>
                            <img src="assets/img/home/pl-slide4.jpg" alt="list">
                            <div class="rate-like centering justify-content-between">
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                </div>
                                <a class="likes" href="#">
                                    <i class="far fa-heart"></i>02
                                </a>
                            </div>
                        </div>
                        <div class="list-content p-2">
                            <ul class="ctg-info py-2 mb-3">
                                <li>
                                    <a href="#">
                                        <i class="flaticon-spa mr-2"></i>beauty & spa</a>
                                </li>
                                <li>
                                    <span class="c-theme"> open now</span>
                                </li>
                            </ul>
                            <h5 class="mb-2">
                                <a href="listing-detail.html">best beauty & spa services</a>
                            </h5>
                            <p>Donec pede justo, fringilla vel, aliquet nectior</p>
                            <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="listing-item p-2">
                        <div class="img-holder">
                            <span class="offer">save 20%
                            </span>
                            <img src="assets/img/home/pl-slide5.jpg" alt="list">
                            <div class="rate-like centering justify-content-between">
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                                <a class="likes" href="#">
                                    <i class="far fa-heart"></i>42
                                </a>
                            </div>
                        </div>
                        <div class="list-content p-2">
                            <ul class="ctg-info py-2 mb-3">
                                <li>
                                    <a href="#">
                                        <i class="flaticon-find mr-2"></i>desitination</a>
                                </li>
                                <li>
                                    <span class="c-theme"> open now</span>
                                </li>
                            </ul>
                            <h5 class="mb-2">
                                <a href="listing-detail.html">best destinations in city</a>
                            </h5>
                            <p>Donec pede justo, fringilla vel, aliquet nectior</p>
                            <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="listing-item p-2">
                        <div class="img-holder">
                            <span class="offer">save 26%
                            </span>
                            <img src="assets/img/home/pl-slide6.jpg" alt="list">
                            <div class="rate-like centering justify-content-between">
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="far fa-star"></i>
                                </div>
                                <a class="likes" href="#">
                                    <i class="far fa-heart"></i>24
                                </a>
                            </div>
                        </div>
                        <div class="list-content p-2">
                            <ul class="ctg-info py-2 mb-3">
                                <li>
                                    <a href="#">
                                        <i class="flaticon-hotel mr-2"></i>hotels</a>
                                </li>
                                <li>
                                    <span class="c-theme"> open now</span>
                                </li>
                            </ul>
                            <h5 class="mb-2">
                                <a href="listing-detail.html">comfy hotels in town</a>
                            </h5>
                            <p>Donec pede justo, fringilla vel, aliquet nectior</p>
                            <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="listing-item p-2">
                        <div class="img-holder">
                            <span class="offer">save 17%
                            </span>
                            <img src="assets/img/home/pl-slide7.jpg" alt="list">
                            <div class="rate-like centering justify-content-between">
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="far fa-star"></i>
                                </div>
                                <a class="likes" href="#">
                                    <i class="far fa-heart"></i>24
                                </a>
                            </div>
                        </div>
                        <div class="list-content p-2">
                            <ul class="ctg-info py-2 mb-3">
                                <li>
                                    <a href="#">
                                        <i class="flaticon-shop mr-2"></i>shopping</a>
                                </li>
                                <li>
                                    <span class="c-theme"> open now</span>
                                </li>
                            </ul>
                            <h5 class="mb-2">
                                <a href="listing-detail.html">shop the best clothes</a>
                            </h5>
                            <p>Donec pede justo, fringilla vel, aliquet nectior</p>
                            <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="listing-item p-2">
                        <div class="img-holder">
                            <span class="offer">save 39%
                            </span>
                            <img src="assets/img/home/pl-slide8.jpg" alt="list">
                            <div class="rate-like centering justify-content-between">
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                </div>
                                <a class="likes" href="#">
                                    <i class="far fa-heart"></i>56
                                </a>
                            </div>
                        </div>
                        <div class="list-content p-2">
                            <ul class="ctg-info py-2 mb-3">
                                <li>
                                    <a href="#">
                                        <i class="flaticon-drama mr-2"></i>events</a>
                                </li>
                                <li>
                                    <span class="c-theme"> open now</span>
                                </li>
                            </ul>
                            <h5 class="mb-2">
                                <a href="listing-detail.html">enjoy the festivals</a>
                            </h5>
                            <p>Donec pede justo, fringilla vel, aliquet nectior</p>
                            <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <div class="listing-item p-2">
                        <div class="img-holder">
                            <span class="offer">save 45%
                            </span>
                            <img src="assets/img/home/pl-slide9.jpg" alt="list">
                            <div class="rate-like centering justify-content-between">
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                </div>
                                <a class="likes" href="#">
                                    <i class="far fa-heart"></i>42
                                </a>
                            </div>
                        </div>
                        <div class="list-content p-2">
                            <ul class="ctg-info py-2 mb-3">
                                <li>
                                    <a href="#">
                                        <i class="flaticon-cutlery mr-2"></i>food & drink</a>
                                </li>
                                <li>
                                    <span class="c-theme"> open now</span>
                                </li>
                            </ul>
                            <h5 class="mb-2">
                                <a href="listing-detail.html">fabulous dine restaurants</a>
                            </h5>
                            <p>Donec pede justo, fringilla vel, aliquet nectior</p>
                            <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-12 text-center mt-30">
                    <a href="#" class="btn btn-one btn-anim">load more</a>
                </div>
            </div>
        </div>
        <div class="listing-list-view">
            <div class="row">
                <div class="col-lg-6 col-12 mb-30">
                    <div class="listing-item p-2">
                        <div class="img-list">
                            <div class="img-holder">
                                <span class="offer">save 49%
                                </span>
                                <img src="assets/img/home/pl-slide1.jpg" alt="list">
                                <div class="rate-like centering justify-content-between">
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </div>
                                    <a class="likes" href="#">
                                        <i class="far fa-heart"></i>24
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="list-content">
                            <ul class="ctg-info py-2 mb-3">
                                <li>
                                    <a href="#">
                                        <i class="flaticon-cutlery mr-2"></i> restaurants</a>
                                </li>
                                <li>
                                    <span class="c-theme"> open now</span>
                                </li>
                            </ul>
                            <h5 class="mb-2">
                                <a href="listing-detail.html">the lounge & bar</a>
                            </h5>
                            <p>Donec pede justo, fringilla vel, aliquet nectior</p>
                            <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-12 mb-30">
                    <div class="listing-item p-2">
                        <div class="img-list">
                            <div class="img-holder">
                                <span class="offer">save 30%
                                </span>
                                <img src="assets/img/home/pl-slide2.jpg" alt="list">
                                <div class="rate-like centering justify-content-between">
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </div>
                                    <a class="likes" href="#">
                                        <i class="far fa-heart"></i>27
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="list-content">
                            <ul class="ctg-info py-2 mb-3">
                                <li>
                                    <a href="#">
                                        <i class="flaticon-shop mr-2"></i>shopping</a>
                                </li>
                                <li>
                                    <span class="c-theme"> open now</span>
                                </li>
                            </ul>
                            <h5 class="mb-2">
                                <a href="listing-detail.html">the best shop in city</a>
                            </h5>
                            <p>Donec pede justo, fringilla vel, aliquet nectior</p>
                            <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-12 mb-30">
                    <div class="listing-item p-2">
                        <div class="img-list">
                            <div class="img-holder">
                                <span class="offer">save 19%
                                </span>
                                <img src="assets/img/home/pl-slide3.jpg" alt="list">
                                <div class="rate-like centering justify-content-between">
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </div>
                                    <a class="likes" href="#">
                                        <i class="far fa-heart"></i>35
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="list-content">
                            <ul class="ctg-info py-2 mb-3">
                                <li>
                                    <a href="#">
                                        <i class="flaticon-cheers mr-2"></i>nightlife</a>
                                </li>
                                <li>
                                    <span class="c-theme"> open now</span>
                                </li>
                            </ul>
                            <h5 class="mb-2">
                                <a href="listing-detail.html">enjoy best nightlife</a>
                            </h5>
                            <p>Donec pede justo, fringilla vel, aliquet nectior</p>
                            <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-12 mb-30">
                    <div class="listing-item p-2">
                        <div class="img-list">
                            <div class="img-holder">
                                <span class="offer">save 59%
                                </span>
                                <img src="assets/img/home/pl-slide4.jpg" alt="list">
                                <div class="rate-like centering justify-content-between">
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </div>
                                    <a class="likes" href="#">
                                        <i class="far fa-heart"></i>75
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="list-content">
                            <ul class="ctg-info py-2 mb-3">
                                <li>
                                    <a href="#">
                                        <i class="flaticon-spa mr-2"></i>beauty & spa</a>
                                </li>
                                <li>
                                    <span class="c-theme"> open now</span>
                                </li>
                            </ul>
                            <h5 class="mb-2">
                                <a href="listing-detail.html">best beauty & spa services</a>
                            </h5>
                            <p>Donec pede justo, fringilla vel, aliquet nectior</p>
                            <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-12 mb-30">
                    <div class="listing-item p-2">
                        <div class="img-list">
                            <div class="img-holder">
                                <span class="offer">save 20%
                                </span>
                                <img src="assets/img/home/pl-slide5.jpg" alt="list">
                                <div class="rate-like centering justify-content-between">
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                    <a class="likes" href="#">
                                        <i class="far fa-heart"></i>95
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="list-content">
                            <ul class="ctg-info py-2 mb-3">
                                <li>
                                    <a href="#">
                                        <i class="flaticon-find mr-2"></i>desitination</a>
                                </li>
                                <li>
                                    <span class="c-theme"> open now</span>
                                </li>
                            </ul>
                            <h5 class="mb-2">
                                <a href="listing-detail.html">best destinations in city</a>
                            </h5>
                            <p>Donec pede justo, fringilla vel, aliquet nectior</p>
                            <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-12 mb-30">
                    <div class="listing-item p-2">
                        <div class="img-list">
                            <div class="img-holder">
                                <span class="offer">save 26%
                                </span>
                                <img src="assets/img/home/pl-slide6.jpg" alt="list">
                                <div class="rate-like centering justify-content-between">
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </div>
                                    <a class="likes" href="#">
                                        <i class="far fa-heart"></i>64
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="list-content">
                            <ul class="ctg-info py-2 mb-3">
                                <li>
                                    <a href="#">
                                        <i class="flaticon-hotel mr-2"></i>hotels</a>
                                </li>
                                <li>
                                    <span class="c-theme"> open now</span>
                                </li>
                            </ul>
                            <h5 class="mb-2">
                                <a href="listing-detail.html">comfy hotels in town</a>
                            </h5>
                            <p>Donec pede justo, fringilla vel, aliquet nectior</p>
                            <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-12 mb-30">
                    <div class="listing-item p-2">
                        <div class="img-list">
                            <div class="img-holder">
                                <span class="offer">save 16%
                                </span>
                                <img src="assets/img/home/pl-slide7.jpg" alt="list">
                                <div class="rate-like centering justify-content-between">
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </div>
                                    <a class="likes" href="#">
                                        <i class="far fa-heart"></i>31
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="list-content">
                            <ul class="ctg-info py-2 mb-3">
                                <li>
                                    <a href="#">
                                        <i class="flaticon-shop mr-2"></i>shopping</a>
                                </li>
                                <li>
                                    <span class="c-theme"> open now</span>
                                </li>
                            </ul>
                            <h5 class="mb-2">
                                <a href="listing-detail.html">shop the best clothes</a>
                            </h5>
                            <p>Donec pede justo, fringilla vel, aliquet nectior</p>
                            <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-12 mb-30">
                    <div class="listing-item p-2">
                        <div class="img-list">
                            <div class="img-holder">
                                <span class="offer">save 16%
                                </span>
                                <img src="assets/img/home/pl-slide8.jpg" alt="list">
                                <div class="rate-like centering justify-content-between">
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </div>
                                    <a class="likes" href="#">
                                        <i class="far fa-heart"></i>75
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="list-content">
                            <ul class="ctg-info py-2 mb-3">
                                <li>
                                    <a href="#">
                                        <i class="flaticon-drama mr-2"></i>events</a>
                                </li>
                                <li>
                                    <span class="c-theme"> open now</span>
                                </li>
                            </ul>
                            <h5 class="mb-2">
                                <a href="listing-detail.html">enjoy the festivals</a>
                            </h5>
                            <p>Donec pede justo, fringilla vel, aliquet nectior</p>
                            <ul class="ctg-info2 pt-2 mt-3 d-flex justify-content-between flex-wrap">
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt mr-2"></i>San Francisco</a>
                                </li>
                                <li class="mt-1">
                                    <a href="#">
                                        <i class="fa fa-phone mr-2"></i>0123-456-789</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-12 text-center mt-30">
                    <a href="#" class="btn btn-one btn-anim">load more</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- listings end -->

<!-- cta-one start-->
<section class="cta-one tri-bg-w text-lg-left text-center">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 my-lg-0 my-5 py-lg-0 py-5">
                <div class="cta-content">
                    <h3>Sign Up To Get Special Offers Every Day</h3>
                    <p>Lorem ipsum dolor sit amet, consectadetudzdae rcquisc adipiscing elit. Aenean socada commodo ligaui
                        egets dolor. </p>
                    <a href="login.html" class="btn btn-two btn-anim mt-2">
                        sign up
                    </a>
                </div>
            </div>
            <div class="col-lg-6 d-lg-block d-none">
                <div class="cta-img mt-4">
                    <img src="assets/img/home/cta-bg.png" alt="image">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- cta-one end -->
<?php include 'footer.php';?>
