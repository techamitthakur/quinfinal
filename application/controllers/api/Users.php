<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Users extends REST_Controller {
    
      /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->model('users_model');
       $this->load->model('admin_model');
    }
       

       public function find_all_GET()
       { //$key=  $this->get('key');

        // echo "dkjfkjdf".$key; die;
            // $key=$this->get("X-API-KEY");
            echo $key; 
            // if(!$this->users_model->getKey($key)){

            //     $this->response([
            //         'status'=>'false',
            //         'message'=>'incorrect api key'

            //         ],REST_Controller::HTTP_BAD_REQUEST);
            // }else{

            //     echo "eneterd";

            // }
       }

    // public function _key_exists($key)
    // {
    //     return $this->users_model->getKey('123456');
    //     return $row->value==$key;
    // }
    /*****************************
        Purpose   : To Register a new user with unique email id.
        Headers   : [ X-API-KEY => MKV_USER_244 ]
        method    : POST [form-data]
        Parameter : { name*, phone, password*, email*}
        URL       : http://localhost/api/users/signup/
        Response  : JSON 
    */
      public function register_POST() {

         if($this->input->post('type')!='' && $this->input->post('firstname')!='' && $this->input->post('lastname')!='' && $this->input->post('email')!='' && $this->input->post('password')!='')
         {
        
          if($this->input->post('type')=='user' || $this->input->post('type')=='provider')
          {
            if($this->input->post('type')=='user')
             {
                   $email=$this->users_model->emailExist($this->input->post('email'),'user');
                   //echo $email; die;
                    if($email!=true ){
                        $datau=array(
                                  'user_fname'=>$this->input->post('firstname'),
                                  'user_lname'=>$this->input->post('lastname'),
                                  'user_email'=>$this->input->post('email'),
                                  'user_password'=>hash('sha256', $this->input->post('password')),
                                  'verify'=> mt_rand(9999,100000), 
                                  'status'=>'inactive'
                                  );

                         $data1=$this->admin_model->insertUser($datau);
                      //**************mail send***************************
                      $email= $this->input->post('email');
                      $user=$this->admin_model->getUser($email);
                      $code= mt_rand(9999,100000);
                      $message="Please click on this link to verify your account <a href=";
                      $message.=base_url('user/verifyaccount?type=user&verify=');
                      $message.=$code."&userid=".$user[0]->id.">Verify link</a>";
                      $this->load->library('email');
                      $this->email->set_header('MIME-Version', '1.0');
                      $this->email->set_header('Content-type', 'text/html');
                      $this->email->set_header('charset=iso-8859-1', 'text/html');
                      $this->email->from('info@quinn.in', 'Quinn');
                      $this->email->to($this->input->post('email'));
                      $this->email->subject('Quinn Support');
                      $this->email->message($message);
                      if($this->email->send()){
                          $data=array('verify'=>$code);
                           $da=$this->admin_model->updateUserVerify($email,$data);
                           $this->response([
                                'status' => true,
                                'message' => 'An email is send to your registerd Email',
                                'data' => array()
                            ], REST_Controller::HTTP_OK);
                           exit();
                         }else{
                                $this->response([
                                'status' => false,
                                'message' => 'EMAIL_NOT_SEND',
                                'data' => array()
                            ], REST_Controller::HTTP_OK);
                                exit();
                         }           
                        }else{

                           $this->response([
                                'status' => false,
                                'message' => 'email already Exist',
                                'data' => array()
                            ], REST_Controller::HTTP_OK);
                           exit();
                        }
                  }
                         //********************mail end**********************
            if($this->input->post('type')=='provider')
             {
                 $email=$this->users_model->emailExist($this->input->post('email'),'provider');
                 if($email!=true ){
                    $datap=array(
                            'pro_fname'=>$this->input->post('firstname'),
                            'pro_lname'=>$this->input->post('lastname'),
                            'pro_email'=>$this->input->post('email'),
                            'pro_password'=>hash('sha256', $this->input->post('password')),
                            'verify'=> mt_rand(9999,100000),
                            'status'=>'inactive'
                             );
                    $data1= $this->admin_model->insertProvider($datap);

                        //**************mail send***************************
                      $email= $this->input->post('email');
                      $provider=$this->admin_model->getProvider($email);
                      $code= mt_rand(9999,100000);
                      $message="Please click on this link to verify your account <a href=";
                      $message.=base_url('user/verifyaccount?type=provider&verify=');
                      $message.=$code."&providerid=".$provider[0]->id.">Verify link</a>";
                      $this->load->library('email');
                      $this->email->set_header('MIME-Version', '1.0');
                      $this->email->set_header('Content-type', 'text/html');
                      $this->email->set_header('charset=iso-8859-1', 'text/html');
                      $this->email->from('info@quinn.in', 'Quinn');
                      $this->email->to($this->input->post('email'));
                      $this->email->subject('Quinn Support');
                      $this->email->message($message);
                      if($this->email->send()){
                          $data=array('verify'=>$code);
                           $da=$this->admin_model->updateUserVerify($email,$data);
                           $this->response([
                                'status' => true,
                                'message' => 'An email is send to your registerd Email',
                                'data' => array($data)
                            ], REST_Controller::HTTP_OK);
                           exit();
                         }else{
                       $this->response([
                                'status' => false,
                                'message' => 'EMAIL_NOT_SEND',
                                'data' => array()
                            ], REST_Controller::HTTP_OK);
                       exit();
                         }           

                         //********************mail end**********************

                    }else{
                     
                      $this->response([
                         'status' => false,
                         'message' => 'An email is already Exist',
                         'data' => array( )
                     ], REST_Controller::HTTP_OK); 
                      exit();
                 }
             }
                  }
             else
                  {
                      $this->response([
                         'status' => false,
                         'message' => 'Type incorrect',
                         'data' => array( )
                     ], REST_Controller::HTTP_OK); 
//                      exit();
                      
                  } 
             
        }else
         {
             $this->response([
                         'status' => false,
                         'message' => 'Fields Cant Be empty',
                         'data' => array( )
                     ], REST_Controller::HTTP_OK); 
//             exit();
//            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP
         }
    }
    
    

      
   /*****************************
        Purpose   : To Verify.
        Headers   : [ X-API-KEY => MKV_USER_244 ]
        method    : POST [form-data]
        Parameter : {password*, email*}
        URL       : http://localhost/api/users/verifyaccount
        Response  : JSON 
    */   
    
    public function verifyaccount_GET()
    {
        $usercheck=$this->admin_model->getUserById($this->input->get('userid'));
        $providercheck=$this->admin_model->getProviderById($this->input->get('providerid'));

     if($this->input->get('type')=='user' && $this->input->get('verify')==$usercheck[0]->verify){
              $data=array('status'=>'active');
              $th=$this->admin_model->userOtpVerify($this->input->get('userid'),$data);
   
         $this->response([
                         'status' => true,
                         'message' => 'Your Account Is verified Please login',
                         'data' => array( )
                     ], REST_Controller::HTTP_OK); 
         }else 
         
            if($this->input->get('type')=='provider' && $this->input->get('verify')==$providercheck[0]->verify)
            {
                  $data=array('status'=>'active');
                $this->admin_model->providerOtpVerify($this->input->get('providerid'),$data);
                 $this->response([
                         'status' => true,
                         'message' => 'Your Account Is verified Please login',
                         'data' => array( )
                     ], REST_Controller::HTTP_OK); 
          }else{

                 $this->response([
                         'status' => false,
                         'message' => 'Incorrect verification code',
                         'data' => array( )
                     ], REST_Controller::HTTP_OK); 
          }
}

   /*****************************
        Purpose   : To loginAccount account
        Headers   : [ X-API-KEY => MKV_USER_244 ]
        method    : POST [form-data]
        Parameter : {password*, email*}
        URL       : http://localhost/api/users/loginAccount/
        Response  : JSON 
    */   
public function loginAccount_POST()
{
  $this->form_validation->set_rules('email','Email','trim|required');
  $this->form_validation->set_rules('password','Password','required');

        if($this->form_validation->run()==true)
      {
            //*****************check user*****************
        if($this->input->post('type')=='user')
        {
         $email =    $this->input->post('email'); 
         $password = hash('sha256', $this->input->post('password')); 
         $data1=$this->admin_model->getLoginUser($email,$password);
         $user=$this->admin_model->getUser($email);
          // print_r($user); die;
         //***************************
         if($data1>0){
                   //print_r($data); 
                  $first_name= $user[0]->user_fname;
                  $this->session->set_userdata("useremail",$email);
                  $this->session->set_userdata("type","user");
                  $this->session->set_userdata("username",$first_name);
//                  $this->session->set_flashdata('action_message', '<p class=" alert alert-success text-center"">User Login Successfully</p>');
//                  redirect("user/mainindex"); 
                    $this->response([
                         'status' => true,
                         'message' => 'User Login Successfully',
                         'data' => array( )
                     ], REST_Controller::HTTP_OK); 
            } 
            else{              
                    $code= mt_rand(9999,100000);
                      $message="Please click on this link to verify your account <a href=";
                      $message.=base_url('user/verifyaccount?type=customer&verify=');
                       $message.=$code."&userid=".$user[0]->id.">Verify link</a>";
                       $this->load->library('email');
                      $this->email->set_header('MIME-Version', '1.0');
                      $this->email->set_header('Content-type', 'text/html');
                      $this->email->set_header('charset=iso-8859-1', 'text/html');
                      $this->email->from('info@quinn.in', 'Quinn');
                      $this->email->to($this->input->post('email'));
                      $this->email->subject('Quinn Support');
                     $this->email->message($message);
                      if($this->email->send()){
                          $data=array('verify'=>$code);
                       $da=$this->admin_model->updateUserVerify($email,$data);
                              $this->response([
                         'status' => true,
                         'message' => 'An email sent please check',
                         'data' => array( )
                     ], REST_Controller::HTTP_OK); 
                          
                         }else{

                            $this->response([
                         'status' => false,
                         'message' => 'Email not sent',
                         'data' => array( )
                     ], REST_Controller::HTTP_OK); 
                         }           
                  }
                  //***************************
          } else

          //*******for provider******************
        if($this->input->post('type')=='provider'){

               $email =    $this->input->post('email'); 
               $password = hash('sha256', $this->input->post('password'));      
               $data1=$this->admin_model->getLoginProvider($email,$password);
              
               // print_r($data1); die;
               if($data1>0){
                 $provider=$this->admin_model->getProvider($email);
              $first_name= $provider[0]->pro_fname;
              $this->session->set_userdata("provideremail",$email);
                  $this->session->set_userdata("type","provider");
             $this->session->set_userdata("username",$first_name);
             
              $this->response([
                         'status' => true,
                         'message' => 'Provider Login Successfully',
                         'data' => array( )
                     ], REST_Controller::HTTP_OK); 
                
          }else{
            $code= mt_rand(9999,100000);
                $message="Please click on this link to verify your account <a href=";
                $message.=base_url('user/verifyaccount?type=provider&verify=');
                $message.=$code."&providerid=".$provider[0]->id.">Verify link</a>";
                $this->load->library('email');
                $this->email->set_header('MIME-Version', '1.0');
                $this->email->set_header('Content-type', 'text/html');
                $this->email->set_header('charset=iso-8859-1', 'text/html');
                $this->email->from('info@quinn.in', 'Quinn');
                $this->email->to($this->input->post('email'));
                $this->email->subject('Quinn Support');
                $this->email->message($message);
                if($this->email->send()){
                    $data=array('verify'=>$code);
                 $da=$this->admin_model->updateProviderVerify($email,$data);
                      $this->response([
                         'status' => true,
                         'message' => 'An email Sent to your Account',
                         'data' => array( )
                     ], REST_Controller::HTTP_OK); 
                   }else{
        
                      $this->response([
                         'status' => false,
                         'message' => 'Email not sent',
                         'data' => array( )
                     ], REST_Controller::HTTP_OK); 
                   }
                 }

             
      
///end check
        } else{   
               
            $this->response([
                         'status' => false,
                         'message' => 'Your Account is not verified',
                         'data' => array( )
                     ], REST_Controller::HTTP_OK); 
      
///end check
        }

          //*******for provider end******************

}else{
        
               $this->response([
                         'status' => false,
                         'message' => 'Incorrect Login details',
                         'data' => array( )
                     ], REST_Controller::HTTP_OK); 
     
      }
    }



    
    
   /*****************************
        Purpose   : To Upadte Provider profile.
        Headers   : [ X-API-KEY => MKV_USER_244 ]
        method    : POST [form-data]
        Parameter : {email*}
        URL       : http://localhost/api/users/updateProvider/
        Response  : JSON 
    */

  //*****************************************provider**********************************************
    public function updateProvider_POST(){
        
        $this->output->set_content_type('application/json');
        $this->output->set_content_type('css', 'utf-8');
     
    
if(empty($this->input->post('pro_fname')) || empty($this->input->post('pro_lname')) || empty($this->input->post('pro_phone')) || empty($this->input->post('pro_email')) || empty($this->input->post('state')) || empty($this->input->post('city')) || empty($this->input->post('zipcode')) || empty($this->input->post('address')) ){
      
//    $data=0;
    $this->response([
                         'status' => false,
                         'message' => 'Fields Cant be empty',
                         'data' => array( )
                     ], REST_Controller::HTTP_OK); 
    }else{
        $data=array(
        'pro_fname'=>$this->input->post('pro_fname'),
        'pro_lname'=>$this->input->post('pro_lname'),
        'pro_phone'=>$this->input->post('pro_phone'),
        'pro_email'=>$this->input->post('pro_email'),
//        'facebook'=>$this->input->post('facebook'),
//        'twitter'=>$this->input->post('twitter'),
//        'country'=>$this->input->post('country'),
        'state'=>$this->input->post('state'),
        'city'=>$this->input->post('city'),
        'zipcode'=>$this->input->post('zipcode'),
        'address'=>$this->input->post('address'),
       
        );
         $data=$this->users_model->updateProviders($this->input->post('pro_email'),$data);
      if($data==1){
             $this->response([
                         'status' => true,
                         'message' => 'Provider profile Updated',
                         'data' => array( )
                     ], REST_Controller::HTTP_OK); 
            }else{
             $this->response([
                         'status' => false,
                         'message' => 'Error Provider profile not Updated',
                         'data' => array( )
                     ], REST_Controller::HTTP_OK); 
            }
        }
    }
        

     /*****************************
        Purpose   : To Upadte User profile.
        Headers   : [ X-API-KEY => MKV_USER_244 ]
        method    : POST [form-data]
        Parameter : {email*}
        URL       : http://localhost/api/users/updateUsers/
        Response  : JSON 
    */

public function updateUsers_POST(){
        
$this->output->set_content_type('application/json');
$this->output->set_content_type('css', 'utf-8');
  
    
if(empty($this->input->post('user_fname')) || empty($this->input->post('user_lname')) || empty($this->input->post('user_phone')) || empty($this->input->post('user_email')) || empty($this->input->post('state')) || empty($this->input->post('city')) || empty($this->input->post('zipcode')) || empty($this->input->post('address')) ){
      
//    $data=0;
    $this->response([
                         'status' => false,
                         'message' => 'Fields Cant be empty',
                         'data' => array( )
                     ], REST_Controller::HTTP_OK); 
    }else{
        $data=array(
        'user_fname'=>$this->input->post('user_fname'),
        'user_lname'=>$this->input->post('user_lname'),
        'user_phone'=>$this->input->post('user_phone'),
        'user_email'=>$this->input->post('user_email'),
//        'facebook'=>$this->input->post('facebook'),
//        'twitter'=>$this->input->post('twitter'),
//        'country'=>$this->input->post('country'),
        'state'=>$this->input->post('state'),
        'city'=>$this->input->post('city'),
        'zipcode'=>$this->input->post('zipcode'),
        'address'=>$this->input->post('address'),
        );
             
        $data=$this->users_model->updateUsers($this->input->post('user_email'),$data);
        if($data==1){
           $this->response([
                         'status' => true,
                         'message' => 'User profile Updated',
                         'data' => array( )
                     ], REST_Controller::HTTP_OK); 
        }else{
           $this->response([
                         'status' => false,
                         'message' => 'Error Provider profile not Updated',
                         'data' => array( )
                     ], REST_Controller::HTTP_OK); 
            }
        }
  
    }   
 /*****************************
        Purpose   : Forget Password.
        Headers   : [ X-API-KEY => MKV_USER_244 ]
        method    : POST [form-data]
        Parameter : {email*}
        URL       : http://localhost/api/users/forgetpassword/
        Response  : JSON 
    */

   public function forgetpassword_POST() {
    
        if ($this->input->post()) {
            if ($this->input->post('email') != '') 
            {
                $email = $this->input->post('email');
                
                $result = $this->users_model->whereByEmail($email);
               
                if ($result==true){
                    $emailverify=mt_rand(9999,100000);
                     $subject="test23@yopmail.com";
                    $message="message checking ".base_url();
                    $this->emailSend($email,$subject,$message);
                        

                    $data = array(
                    // 'verify' => $otp,
                    'status' => 'active',
                    );
                    $user_details = $this->users_model->updateUsers($email,$data);
                    $user_details = $this->users_model->whereByEmail($email);

                    $this->response([
                        'status' => true,
                        'message' => 'Please Check An Email Is send To Your Registered Email',
                         'verification_code' => $emailverify
                    ], REST_Controller::HTTP_OK); 
                    
                } else {
                    $this->response([
                        'status' => false,
                        'message' => 'Incorrect Email',
                        'data' => array()
                    ], REST_Controller::HTTP_OK); 
                  
                }
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'please provide OTP',
                    'data' => array()
                ], REST_Controller::HTTP_OK); 
                
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP 
        }
    }
        
    public function emailSend($from_name='',$from_email='',$subject='',$message='')
    {
        $this->load->library('email');

        $this->email->from($from_email, $from_name);
        $this->email->to('test@yopmail.com');
 
        $this->email->subject($subject);
        $this->email->message($message);

        if($this->email->send()==true){

            return true;
        }else{
             return false;
        }
    }
    
   public function changepassword_POST() {

        // $Lang = strtoupper($_SERVER['HTTP_LAGUAGE']);
        if ($this->input->post()) {

            if($this->input->post('email') == ''&& $this->input->post('newpassword') == ''){
               
                $this->response([
                    'status' => false,
                    'message' => "Fill all fields",
                    'data' => array()
                ], REST_Controller::HTTP_OK); 
                exit();
            } else {


                $oldpwd = hash("sha256", $this->input->post('newpassword'));
                $email = $this->input->post('email');
                // $result = $this->users_model->checkPassword($email,$oldpwd);
                
                
                    // $pwd = array("password" => sha256($this->input->post('newpassword')));
                    $res = $this->users_model->changePassword(["password"=>$oldpwd], $email);
                    // print_r($res);  
                    if($res==1){
                        
                        $this->response([
                            'status' => true,
                            'message' => 'Password Updated',
                            //'data' => array()
                        ], REST_Controller::HTTP_OK); 
                       
                    } else {
                        $this->response([
                            'status' => false,
                            'message' => 'Internal Server Error',
                            'data' => array()
                        ], REST_Controller::HTTP_OK); 
                        
                    }
            
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP 
        }
    }

/*****************************
        Purpose   : Contact US 
        Headers   : [ X-API-KEY => MKV-USER-244 ]
        method    : POST
        Parameter : { name*, subject, email*, message* }
        URL       : 
        Response  : JSON
    *******************************/
    public function contactus_POST() {
        //$Lang = strtoupper($_SERVER['HTTP_LAGUAGE']);
        if ($this->input->post()) {


if($this->input->post('name')=='' || $this->input->post('email')=='' || $this->input->post('message') == '' || $this->input->post('subject') == ''){


                $this->response([
                    'status' => false,
                    'message' => 'Please fill all the fields !',
                    'data' => array()
                ], REST_Controller::HTTP_OK); 
                // exit();
            } else {

                $contactus_data=array(

                    'name' => $this->input->post('name'),
                    'subject' => $this->input->post('subject'),
                    'email' => $this->input->post('email'),
                    'message' => $this->input->post('message')

                );
               
                    $name = $this->input->post('name');
                    $subject = $this->input->post('subject');
                    $email = $this->input->post('email');
                    $message = $this->input->post('message');
               
                 $send_status=$this->users_model->insertContactus($contactus_data);
                //$send_status = $this->emailSend($name,$email,$subject,$message);// send email
                $send_status=true;
                if ($send_status== true) {
                    $this->response([
                            'status' => true,
                            'message' => "Message sent Successfully We Will Contact You Soon",
                            'data' => array()
                        ], REST_Controller::HTTP_OK); 
                        // exit();
                } else {
                    $this->response([
                        'status' => false,
                        'message' => "Form Not Submitted",
                        'data' => array()
                    ], REST_Controller::HTTP_OK); 
                    // exit();
                }
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP 
        }
    }
       
/*****************************
        Purpose   : enquiry 
        Headers   : [ X-API-KEY => MKV-USER-244 ]
        method    : POST
        Parameter : { name*, phone, email*, message* }
        URL       : 
        Response  : JSON
    *******************************/
public function enquiry_POST() {
        //$Lang = strtoupper($_SERVER['HTTP_LAGUAGE']);
        if ($this->input->post()) {
                if($this->input->post('name') == '' || $this->input->post('email') == '' || $this->input->post('message') == '' || $this->input->post('address') == '' || $this->input->post('subject') == '' ){
                $this->response([
                    'status' => false,
                    'message' => 'Please fill all the fields !',
                    'data' => array()
                ], REST_Controller::HTTP_OK); 
                // exit();
            } else {
                $enquiry_data = array(
                    'name' => $this->input->post('name'),
                    'phone' => $this->input->post('phone'),
                    'email' => $this->input->post('email'),
                    'address' => $this->input->post('address'),
                    'subject' => $this->input->post('subject'),
                    'message' => $this->input->post('message'),
                );
                 $send_status=$this->users_model->insertEnquiry($enquiry_data);
                //$send_status = $this->common_library->customer_care_send_enquiry_email($enquiry_data);// 
                $send_status=1;
                
                if ($send_status > 0) {
                    $this->response([
                            'status' => true,
                            'message' => "ENQUIRY_FORM_SUBMITTED",
                            'data' => array()
                        ], REST_Controller::HTTP_OK); 
                        // exit();
                } else {
                    $this->response([
                        'status' => false,
                        'message' => "ENQUIRY_FORM_NOT_SUBMITTED",
                        'data' => array()
                    ], REST_Controller::HTTP_OK); 
                    // exit();
                }
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP 
        }
    }

/*****************************
        Purpose   : Privicy and policy
        Headers   : [ X-API-KEY => MKV-USER-244 ]
        method    : POST
        Parameter : {  }
        URL       : 
        Response  : JSON
    *******************************/
public function privacypolicy_GET() {
        //$Lang = strtoupper($_SERVER['HTTP_LAGUAGE']);
      $data=$this->admin_model->privacypolicy();
   
    $this->response([
                        'status' => false,
                        'message' => "Privacy and Policy",
                        'data' =>  $data
                    ], REST_Controller::HTTP_OK); 
    }

    
/*****************************
        Purpose   : Terms and condition 
        Headers   : [ X-API-KEY => MKV-USER-244 ]
        method    : POST
        Parameter : { name*, phone, email*, message* }
        URL       : 
        Response  : JSON
    *******************************/
public function termscondition_GET() {
        //$Lang = strtoupper($_SERVER['HTTP_LAGUAGE']);
      $data=$this->admin_model->termscondition();
    $this->response([
                        'status' => false,
                        'message' => "terms and condition",
                        'data' =>  $data
                    ], REST_Controller::HTTP_OK); 
    }
        }
