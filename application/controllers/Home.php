<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	
	 public function __construct() {
        parent::__construct();
        // if ($this->session->userdata('email')) {
        //     redirect('home/index');
        // }
        $this->load->model('home_model');
        $this->load->model('admin_model');
    }

    public function index() {


    $data['config']=$this->admin_model->getConfig();
    $data['category']=$this->admin_model->getCategory();
    $data['subcategory']=$this->admin_model->getSubCategory();
    $this->load->view('index2',$data);
        //$this->load->view('navigation',$datas);
      
       
    }
public function config_comman()
{
  # code...
}

public function about()
{
 $data['product_new']=$this->admin_model->getProductWhere('New');
   $data['services']=$this->admin_model->getServices();
    $data['about']=$this->admin_model->getAbout();
    $data['treatment']=$this->admin_model->getTreatment();
    $data['config']=$this->admin_model->getConfig();
    $data['category']=$this->admin_model->getCategory();
    $data['subcategory']=$this->admin_model->getSubCategory();
  $this->load->view('about',$data);
}

    public function getCategory()
    {
       $data['category']=$this->home_model->getCategory();

       $this->load->view('index',$data);
         //echo "ini am ini am in </h1>";
    }

   
    
    public function mainmenu()
    {
       
        $this->load->view('admin/user/menu_add');
         //echo "ini am ini am in </h1>";
    }



    public function signup()
    {
        $data['services']=$this->admin_model->getServices();
    $data['about']=$this->admin_model->getAbout();
    $data['treatment']=$this->admin_model->getTreatment();
   $data['config']=$this->admin_model->getConfig();
      $data['category']=$this->admin_model->getCategory();
    $data['subcategory']=$this->admin_model->getSubCategory();
       $this->load->view('signup',$data);

     }

        public function signupUsers()
    {

        $this->form_validation->set_rules('first_name','First_name','trim|required|max_length[100]');
        $this->form_validation->set_rules('last_name','Last_name','trim|required|max_length[100]');
        $this->form_validation->set_rules('email','Email','trim|required|max_length[50]|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password','Password','required|min_length[8]|max_length[20]');
        $this->form_validation->set_rules('terms','Terms','required="please check I accept the terms and conditions"');
        $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|min_length[8]|max_length[20]|matches[password]');
        if($this->form_validation->run()==False)
        {
             $this->load->view('signup');

        }else{

         $data=array('last_name'  =>$this->input->post('last_name'), 
                     'first_name' =>$this->input->post('first_name'), 
                     'email'      =>$this->input->post('email'), 
                     'password'   =>hash('sha256', $this->input->post('password')), 
                     'terms'   =>$this->input->post('checkbox'), 
                  );
         // print_r($data);

         $data1=$this->home_model->insertSignup($data);
        // / echo $data1;
         if($data1==true){

         // Set flash data 
                    $this->session->set_flashdata('action_message', '<p class=" alert alert-success text-center"">Account Created Successfully</p>');
                    // After that you need to used redirect function instead of load view such as 
                    redirect("home/signup"); 

         // $this->load->view('signup');
         // echo"success";
            }else{

               $this->session->set_flashdata('action_message', '<p class=" alert alert-danger text-center">Please Check your details</p>');
              // After that you need to used redirect function instead of load view such as 
              redirect("home/signup");
            }
        }

      
     }

     public function login()
    {
       //$data['category']=$this->home_model->getCategory();
    $data['services']=$this->admin_model->getServices();
    $data['about']=$this->admin_model->getAbout();
    $data['treatment']=$this->admin_model->getTreatment();
    $data['config']=$this->admin_model->getConfig();
    $data['category']=$this->admin_model->getCategory();
    $data['subcategory']=$this->admin_model->getSubCategory();

       $this->load->view('login',$data);
         //echo "ini am ini am in </h1>";
    }

      public function loginUser()
    {
        $this->form_validation->set_rules('email','Email','trim|required|max_length[50]|valid_email');
        $this->form_validation->set_rules('password','Password','required|min_length[8]|max_length[20]');
     
        if($this->form_validation->run()==False)
        {
             $this->load->view('login');

        }else{

         $email =    $this->input->post('email'); 
         $password = hash('sha256', $this->input->post('password')); 
                    
       

         $data1=$this->home_model->getLogin($email,$password);
         //echo $data1; die;
         if($data1>0){
                  $data=$this->home_model->getUser($email);
                    //print_r($data); 
                  $first_name= $data[0]->first_name;
                
                  $this->session->set_userdata("email",$email);
                  $this->session->set_userdata("username",$first_name);

                    $this->session->set_flashdata('action_message', '<p class=" alert alert-success text-center"">Login Successfully</p>');
                    
                    redirect("home/login"); 

            }else{

               $this->session->set_flashdata('action_message', '<p class=" alert alert-danger text-center">Please Check your details</p>');
             
              redirect("home/login");
            }
        }
    }

     public function logout() {
       
        //removing session  
        $this->session->unset_userdata('email');  
        $this->session->unset_userdata('username');  
        $this->session->sess_destroy();
        redirect("home");  
    }


  public function contact()
  {

   $data['services']=$this->admin_model->getServices();
   $data['about']=$this->admin_model->getAbout();
   $data['treatment']=$this->admin_model->getTreatment();
  $data['config']=$this->admin_model->getConfig();
    $this->load->view('contact',$data);
      
 



 

 
  
  public function search()
  {
        $keyword = $this->input->post('search');
        $data['results']    =   $this->admin_model->search($keyword);
        $this->load->view('search',$data);
  }

  public function viewproduct()
  {
  $id = $this->input->get('id');
  $data['category']=$this->admin_model->getCategory();
  $data['subcategory']=$this->admin_model->getSubCategory();
  $data['services']=$this->admin_model->getServices();
  $data['about']=$this->admin_model->getAbout();
  $data['treatment']=$this->admin_model->getTreatment();
  $data['config']=$this->admin_model->getConfig();
  $data['product_new']=$this->admin_model->getProductWhere('New');
        $data['viewproduct']    =   $this->admin_model->whereProduct($id);
        $this->load->view('single',$data);
  }
}
