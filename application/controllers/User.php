<?php



defined('BASEPATH') OR exit('No direct script access allowed');



class User extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->library('facebook');
        $this->load->model('users_model');
        $this->load->model('admin_model');
    }
 public function test() {
$this->load->view('search');
    }
    public function index() {
        $this->load->view('index');
    }

    public function mainindex() {
    $data['category']=$this->admin_model->getCategory();
    $data['subcategory']=$this->admin_model->getSubCategory();
    $data['testimonial']=$this->admin_model->getTestimonial();
    $this->load->view('index2',$data);
    }
    
    public function about() {
$this->load->view('about');
    }
    
    public function contact() {
$this->load->view('contact');
    }

   public function register() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('firstname','First Name','trim|required|max_length[100]');
        $this->form_validation->set_rules('lastname','Last Name','trim|required|max_length[100]');
        $this->form_validation->set_rules('email','Email','trim|required|max_length[50]|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password','Password','required|min_length[8]|max_length[20]');
        $this->form_validation->set_rules('type','Type','required');
        $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|min_length[8]|max_length[20]|matches[password]');
             if ($this->form_validation->run() == FALSE)       
                {
                  $this->load->view('login');
                }
                else
                { 
                  if($this->input->post('type')=='user'){

                        $datau=array(
                                  'user_fname'=>$this->input->post('firstname'),
                                  'user_lname'=>$this->input->post('lastname'),
                                  'user_email'=>$this->input->post('email'),
                                  'user_password'=>hash('sha256', $this->input->post('password')),
                                   'verify'=> mt_rand(9999,100000), 
                                  'status'=>'inactive'
                                  );
                         $data1=$this->admin_model->insertUser($datau);
                      //**************mail send***************************
                      $email= $this->input->post('email');
                      $user=$this->admin_model->getUser($email);
                      $code= mt_rand(9999,100000);
                      $message="Please click on this link to verify your account <a href=";
                      $message.=base_url('user/verifyaccount?type=customer&verify=');
                      $message.=$code."&userid=".$user[0]->id.">Verify link</a>";
                      $this->load->library('email');
                      $this->email->set_header('MIME-Version', '1.0');
                      $this->email->set_header('Content-type', 'text/html');
                      $this->email->set_header('charset=iso-8859-1', 'text/html');
                      $this->email->from('info@quinn.in', 'Quinn');
                      $this->email->to($this->input->post('email'));
                      $this->email->subject('Quinn Support');
                      $this->email->message($message);
                      if($this->email->send()){
                          $data=array('verify'=>$code);
                           $da=$this->admin_model->updateUserVerify($email,$data);
                           $this->session->set_flashdata('action_message', '<p class=" alert alert-danger text-center">An Verification email sent please check</p>');
                            redirect("user/login");
                         }else{
                       $this->session->set_flashdata('action_message', '<p class=" alert alert-danger text-center">Email not Verified</p>');           
                      redirect("user/login");
                         }           

                         //********************mail end**********************
                        
                     }else 
                        if($this->input->post('type')=='provider'){
                        $datap=array(
                                'pro_fname'=>$this->input->post('firstname'),
                                'pro_lname'=>$this->input->post('lastname'),
                                'pro_email'=>$this->input->post('email'),
                                'pro_password'=>hash('sha256', $this->input->post('password')),
                                'verify'=> mt_rand(9999,100000),
                                'status'=>'inactive'
                                 );
                        $data1= $this->admin_model->insertProvider($datap);

                        //**************mail send***************************
                      $email= $this->input->post('email');
                      $provider=$this->admin_model->getProvider($email);
                      $code= mt_rand(9999,100000);
                      $message="Please click on this link to verify your account <a href=";
                      $message.=base_url('user/verifyaccount?type=provider&verify=');
                      $message.=$code."&userid=".$provider[0]->id.">Verify link</a>";
                      $this->load->library('email');
                      $this->email->set_header('MIME-Version', '1.0');
                      $this->email->set_header('Content-type', 'text/html');
                      $this->email->set_header('charset=iso-8859-1', 'text/html');
                      $this->email->from('info@quinn.in', 'Quinn');
                      $this->email->to($this->input->post('email'));
                      $this->email->subject('Quinn Support');
                      $this->email->message($message);
                      if($this->email->send()){
                          $data=array('verify'=>$code);
                           $da=$this->admin_model->updateProviderVerify($email,$data);
                           $this->session->set_flashdata('action_message', '<p class=" alert alert-danger text-center">A Verification email sent please check</p>');
                            redirect("user/login");
                         }else{
                       $this->session->set_flashdata('action_message', '<p class=" alert alert-danger text-center">Email not Verified</p>');           
                      redirect("user/login");
                         }           

                         //********************mail end**********************

                    }else{
                         $this->session->set_flashdata('action_message', '<p class=" alert alert-danger text-center">Please Select one fromn list</p>');
               redirect("user/login");
                      }
                 if($data1==true){
                   $this->session->set_flashdata('action_message', '<p class=" alert alert-success text-center""> Registered Successfully</p>');
                      redirect("user/login"); 
            }else{
               $this->session->set_flashdata('action_message', '<p class=" alert alert-danger text-center">Please Check your details</p>');
               redirect("user/login");
            }
                }
    }

   public function verifyaccount()
    {
     $usercheck=$this->admin_model->getUserById($this->input->get('userid'));
       $providercheck=$this->admin_model->getProviderById($this->input->get('providerid'));
// print_r($providercheck); die;
     if($this->input->get('type')=='customer' && $this->input->get('verify')==$usercheck[0]->verify){
              $data=array('status'=>'active');
              $th=$this->admin_model->userOtpVerify($this->input->get('userid'),$data);
                // print_r($th); die;
                $this->session->set_flashdata('action_message', '<p class=" alert alert-success text-center">Your Account Is verified Please login</p>');
                  redirect("user/login"); 
         }else if($this->input->get('type')=='provider' && $this->input->get('verify')==$providercheck[0]->verify){
                  $data=array('status'=>'active');
                $this->admin_model->providerOtpVerify($this->input->get('providerid'),$data);
                 $this->session->set_flashdata('action_message', '<p class=" alert alert-success text-center">Your Account Is verified Please login</p>');
                  redirect("user/login"); 
          }else{
          $this->session->set_flashdata('action_message', '<p class=" alert alert-danger text-center">Incorrect verification code</p>');
                  redirect("user/login"); 
          }
}


  public function loginAccount()

{
  $this->form_validation->set_rules('email','Email','trim|required|max_length[50]|valid_email');
  $this->form_validation->set_rules('password','Password','required|min_length[8]|max_length[20]');

        if($this->form_validation->run()==true)
      {
            //*****************check user*****************
        if($this->input->post('type')=='user')
        {
         $email =    $this->input->post('email'); 
         $password = hash('sha256', $this->input->post('password')); 
         $data1=$this->admin_model->getLoginUser($email,$password);
         $user=$this->admin_model->getUser($email);
          // print_r($user); die;
         //***************************
         if($data1>0){
                   //print_r($data); 
                  $first_name= $user[0]->user_fname;
                  $this->session->set_userdata("useremail",$email);
                  $this->session->set_userdata("type","user");
                  $this->session->set_userdata("username",$first_name);
                  $this->session->set_flashdata('action_message', '<p class=" alert alert-success text-center"">User Login Successfully</p>');
                  redirect("user/providerprofile"); 
            } 
            else{              
                    $code= mt_rand(9999,100000);
                      $message="Please click on this link to verify your account <a href=";
                      $message.=base_url('user/verifyaccount?type=customer&verify=');
                       $message.=$code."&userid=".$user[0]->id.">Verify link</a>";
                       $this->load->library('email');
                      $this->email->set_header('MIME-Version', '1.0');
                      $this->email->set_header('Content-type', 'text/html');
                      $this->email->set_header('charset=iso-8859-1', 'text/html');
                      $this->email->from('info@quinn.in', 'Quinn');
                      $this->email->to($this->input->post('email'));
                      $this->email->subject('Quinn Support');
                     $this->email->message($message);
                      if($this->email->send()){
                          $data=array('verify'=>$code);
                       $da=$this->admin_model->updateUserVerify($email,$data);
                           $this->session->set_flashdata('action_message', '<p class=" alert alert-danger text-center">An email sent please check</p>');
                            redirect("user/login");
                         }else{
                       $this->session->set_flashdata('action_message', '<p class=" alert alert-danger text-center">Email not sent</p>');           
                    redirect("user/login");
                         }           
                  }
                  //***************************
          } else

          //*******for provider******************
        if($this->input->post('type')=='provider'){

               $email =    $this->input->post('email'); 
               $password = hash('sha256', $this->input->post('password'));      
               $data1=$this->admin_model->getLoginProvider($email,$password);
              
               // print_r($data1); die;
               if($data1>0){
                 $provider=$this->admin_model->getProvider($email);
              $first_name= $provider[0]->pro_fname;
              $this->session->set_userdata("provideremail",$email);
                  $this->session->set_userdata("type","provider");
             $this->session->set_userdata("username",$first_name);
             $this->session->set_flashdata('action_message', '<p class=" alert alert-success text-center>Provider Login Successfully</p>');
          redirect("user/userprofile"); 
            
                
          }else{
            $code= mt_rand(9999,100000);
                $message="Please click on this link to verify your account <a href=";
                $message.=base_url('user/verifyaccount?type=provider&verify=');
                $message.=$code."&providerid=".$provider[0]->id.">Verify link</a>";
                $this->load->library('email');
                $this->email->set_header('MIME-Version', '1.0');
                $this->email->set_header('Content-type', 'text/html');
                $this->email->set_header('charset=iso-8859-1', 'text/html');
                $this->email->from('info@quinn.in', 'Quinn');
                $this->email->to($this->input->post('email'));
                $this->email->subject('Quinn Support');
                $this->email->message($message);
                if($this->email->send()){
                    $data=array('verify'=>$code);
                 $da=$this->admin_model->updateProviderVerify($email,$data);
                     $this->session->set_flashdata('action_message', '<p class=" alert alert-danger text-center">An email Sent to your Account</p>');
              redirect("user/login");
                   }else{
              $this->session->set_flashdata('action_message', '<p class=" alert alert-danger text-center">Email not sent</p>');
                redirect("user/login");
                   }
                 }

             
      
///end check
        } else{   
               $this->session->set_flashdata('action_message', '<p class=" alert alert-danger text-center">You Account is not verified</p>');
              redirect("user/login");
      
///end check
        }

          //*******for provider end******************

}else{
          $this->session->set_flashdata('action_message', '<p class=" alert alert-danger text-center">Incorrect Login details</p>');
              redirect("user/login");
     
      }
    }




     public function logout() {
        //removing session  
        $this->session->unset_userdata('email');  
        $this->session->unset_userdata('username');  
             // Remove user data from session
        $this->session->unset_userdata('userData');
        $this->session->sess_destroy();
        redirect("user/mainindex");  
    }

 public function checklogin() {
//          if (empty($this->session->userdata("useremail")) || empty($this->session->userdata("provideremail"))) {
//              redirect("user/login");
//            }
}

//     public function checkproviderlogin() {
//          if (!$this->session->userdata("provideremail")) {
//              redirect("user/login");
//            }
//}
//***************social media login********************
     public function facebookLogin(){
        $userData = array();
        // Check if user is logged in
        if($this->facebook->is_authenticated()){  
            // Get user facebook profile details
            $fbUser = $this->facebook->request('post', '/me?fields=id,first_name,last_name,email,link,gender,picture');

            // Preparing data for database insertion
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid']    = !empty($fbUser['id'])?$fbUser['id']:'';;
            $userData['first_name']    = !empty($fbUser['first_name'])?$fbUser['first_name']:'';
            $userData['last_name']    = !empty($fbUser['last_name'])?$fbUser['last_name']:'';
            $userData['email']        = !empty($fbUser['email'])?$fbUser['email']:'';
            $userData['gender']        = !empty($fbUser['gender'])?$fbUser['gender']:'';
            $userData['picture']    = !empty($fbUser['picture']['data']['url'])?$fbUser['picture']['data']['url']:'';
            $userData['link']        = !empty($fbUser['link'])?$fbUser['link']:'';
            
            // Insert or update user data
            $userID = $this->users_model->checkUser($userData);
            
            // Check user data insert or update status
            if(!empty($userID)){
                $data['userData'] = $userData;
                $this->session->set_userdata('userData', $userData);
                $this->session->set_userdata('email', $userData['email']);
                $this->session->set_userdata('fbname', $userData['first_name']);
                $this->session->set_userdata('fblastname', $userData['last_name']);
                $this->session->set_userdata('type','fblogin');
            }else{
               $data['userData'] = array();
            }
            
            // Get logout URL
            $data['logoutURL'] = $this->facebook->logout_url();
            $this->session->set_userdata('logoutURL',$data['logoutURL']);
        }else{
            // Get login URL
            $data['authURL'] =  $this->facebook->login_url();
            //$this->session->set_userdata('authURL',$data['authURL']);
        }
        
        // Load login & profile view
        $this->load->view('index2',$data);
    }

    // public function logout() {
    //     // Remove local Facebook session
    //     $this->facebook->destroy_session();
    //     // Remove user data from session
    //     $this->session->unset_userdata('userData');
    //     // Redirect to login page
    //     redirect('user_authentication');
    // }
//***************Social login end************************
    
//*****************************************provider**********************************************
    public function updateProvider(){
        $this->checklogin();
        $this->output->set_content_type('application/json');
        $this->output->set_content_type('css', 'utf-8');
        $sourcePath = $_FILES['image']['tmp_name'];     
        $targetPath = 'public/upload/providerpic/'.$_FILES['image']['name']; // 
    
        if($_FILES['image']['name']!=''){
        move_uploaded_file($sourcePath,$targetPath);    
        $data=array(
        'pro_fname'=>$this->input->post('pro_fname'),
        'pro_lname'=>$this->input->post('pro_lname'),
        'pro_phone'=>$this->input->post('pro_phone'),
        'pro_email'=>$this->input->post('pro_email'),
        'facebook'=>$this->input->post('facebook'),
        'twitter'=>$this->input->post('twitter'),
        'country'=>$this->input->post('country'),
        'state'=>$this->input->post('state'),
        'city'=>$this->input->post('city'),
        'zipcode'=>$this->input->post('zipcode'),
        'address'=>$this->input->post('address'),
        'image'=>$_FILES['image']['name'],
        );
    }else{
        $data=array(
        'pro_fname'=>$this->input->post('pro_fname'),
        'pro_lname'=>$this->input->post('pro_lname'),
        'pro_phone'=>$this->input->post('pro_phone'),
        'pro_email'=>$this->input->post('pro_email'),
        'facebook'=>$this->input->post('facebook'),
        'twitter'=>$this->input->post('twitter'),
        'country'=>$this->input->post('country'),
        'state'=>$this->input->post('state'),
        'city'=>$this->input->post('city'),
        'zipcode'=>$this->input->post('zipcode'),
        'address'=>$this->input->post('address'),
       
        );
        
    }

        $data=$this->users_model->updateProviders($this->input->post('pro_email'),$data);
        if($data==1){
            echo json_encode($data);
        //            return FALSE;
            }else{
            echo json_encode($data);
        //            return FALSE;
            }
    }

public function providerCover(){

        $email=$this->input->post('pro_email');
        $config['upload_path']          = 'public/upload/providerpic/cover/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 250;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('image'))
        {
            $data = array('error' => $this->upload->display_errors());    
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $data=array(
            'cover'=>$_FILES['image']['name'],
            );
            $data=$this->users_model->updateProviders($email,$data);
        }

    
    if($data==1){
            echo json_encode($data);
        }else{
            echo json_encode($data);
        }
}

public function providerGallery(){
        $email=$this->input->post('pro_email');
        $config['upload_path']          = 'public/upload/providerpic/gallery/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 250;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('image'))
        {
            $data = array('error' => $this->upload->display_errors());                    
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $data=array(
                    'gallery'=>$_FILES['image']['name'],
                    );
            $data=$this->users_model->updateProviders($email,$data);
        }

        if($data==1){
                echo json_encode($data);
            }else{
                echo json_encode($data);
            }
} 
//********************************provider end*****************************************
    
//**********user*************
    
    public function updateUsers(){
        
        $this->output->set_content_type('application/json');
        $this->output->set_content_type('css', 'utf-8');
        $sourcePath = $_FILES['image']['tmp_name'];     
        $targetPath = 'public/upload/userpic/'.$_FILES['image']['name']; // 
    
        if($_FILES['image']['name']!=''){
        move_uploaded_file($sourcePath,$targetPath);    
       
       $data=array(
        'user_fname'=>$this->input->post('user_fname'),
        'user_lname'=>$this->input->post('user_lname'),
        'user_phone'=>$this->input->post('user_phone'),
        'user_email'=>$this->input->post('user_email'),
        'facebook'=>$this->input->post('facebook'),
        'twitter'=>$this->input->post('twitter'),
        'country'=>$this->input->post('country'),
        'state'=>$this->input->post('state'),
        'city'=>$this->input->post('city'),
        'zipcode'=>$this->input->post('zipcode'),
        'address'=>$this->input->post('address'),
        'image'=>$_FILES['image']['name'],
        );
    }else{
        $data=array(
        'user_fname'=>$this->input->post('user_fname'),
        'user_lname'=>$this->input->post('user_lname'),
        'user_phone'=>$this->input->post('user_phone'),
        'user_email'=>$this->input->post('user_email'),
        'facebook'=>$this->input->post('facebook'),
        'twitter'=>$this->input->post('twitter'),
        'country'=>$this->input->post('country'),
        'state'=>$this->input->post('state'),
        'city'=>$this->input->post('city'),
        'zipcode'=>$this->input->post('zipcode'),
        'address'=>$this->input->post('address'),
        );
        
    }
        
//        print_r($data); die;
        $data=$this->users_model->updateUsers($this->input->post('user_email'),$data);
        if($data==1){
            echo json_encode($data);
//            return FALSE;
        }else{
            echo json_encode($data);
//            return FALSE;
        }
    }
//**********user*************
    
public function userprofile()
{

  $this->checklogin();
$email=$this->session->userdata('useremail');
$data['user_info']=$this->admin_model->getUser($email);
  $this->load->view('userprofile',$data);
}

public function providerprofile()
{

  $this->checklogin();
$email=$this->session->userdata('provideremail');
$data['provider_info']=$this->admin_model->getProvider($email);
  $this->load->view('providerprofile',$data);
}


public function survey()
{
  if($this->input->post()==''){
 $data["submit"]="Data Not Submitted successfully";
  $this->load->view('index',$data);
  }else{
  $data["submit"]="Data Submitted successfully";
  $this->load->view('index',$data);
  }# code...
}
public function doServey(){

    if($this->input->post('beauty')===null){
        $beauty='null';
    }else{
         $beauty = $this->input->post('beauty');
   

    }
     if($this->input->post('food')===null){
        $food='null';
    }
    else{
        $food = $this->input->post('food');
        
    }
     if($this->input->post('medical')===null){
        $medical='null';
    }else{
        $medical =$this->input->post('medical');
        
    }
     if($this->input->post('tailoring')===null){
        $tailoring='null';
    }else{
        $tailoring = $this->input->post('tailoring');
        
    }
     if($this->input->post('teaching')===null){
        $teaching='null';
    }else{
        $teaching = $this->input->post('teaching');
        
    }
     if($this->input->post('office')===null){
        $office='null';
    }else{
        $office =$this->input->post('office');
        
    }
     if($this->input->post('fitness')===null){
        $fitness='null';
    }else{
       $fitness = $this->input->post('fitness');
        
    }
     if($this->input->post('others')===null){
        $others='null';
    }else{
        $others = $this->input->post('others');
        
    }

   
   $data = array(
        'gender' => $this->input->post('gender'),
        'services' => $this->input->post('services'),
        'beauty' => $beauty,
        'food' => $food,
        'medical' => $medical,
        'others' => $others,
        'fitness' => $fitness,
        'tailoring' => $tailoring,
        'teaching' => $teaching,
        'office' => $office,
        'name' => $this->input->post('name'),
        'phone' => $this->input->post('phone'),
        'email' => $this->input->post('email'),
        'area' => $this->input->post('area')
        );
    
    $message1='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <!--[if gte mso 9]><xml>
    <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
    </xml><![endif]-->
    <title>Quinn</title>
    <meta charset="charset=iso-8859-1" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 ">
    <meta name="format-detection" content="telephone=no">
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Signika+Negative&display=swap" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!--<![endif]-->
    <style type="text/css">
        body {
            margin: 0 !important;
            padding: 0 !important;
            -webkit-text-size-adjust: 100% !important;
            -ms-text-size-adjust: 100% !important;
            -webkit-font-smoothing: antialiased !important;
            font-family: "Signika Negative", sans-serif;

        }

        img {
            border: 0 !important;
            outline: none !important;
        }

        p {
            Margin: 0px !important;
            Padding: 0px !important;
        }

        table {
            border-collapse: collapse;
            mso-table-lspace: 0px;
            mso-table-rspace: 0px;
        }

        td,
        a,
        span {
            border-collapse: collapse;
            mso-line-height-rule: exactly;
        }

        .ExternalClass * {
            line-height: 100%;
        }

        .em_defaultlink a {
            color: inherit !important;
            text-decoration: none !important;
        }

        span.MsoHyperlink {
            mso-style-priority: 99;
            color: inherit;
        }

        span.MsoHyperlinkFollowed {
            mso-style-priority: 99;
            color: inherit;
        }

        @media only screen and (min-width:481px) and (max-width:699px) {
            .em_main_table {
                width: 100% !important;
            }

            .em_wrapper {
                width: 100% !important;
            }

            .em_hide {
                display: none !important;
            }

            .em_img {
                width: 100% !important;
                height: auto !important;
            }

            .em_h20 {
                height: 20px !important;
            }

            .em_padd {
                padding: 20px 10px !important;
            }
        }

        @media screen and (max-width: 480px) {
            .em_main_table {
                width: 100% !important;
            }

            .em_wrapper {
                width: 100% !important;
            }

            .em_hide {
                display: none !important;
            }

            .em_img {
                width: 100% !important;
                height: auto !important;
            }

            .em_h20 {
                height: 20px !important;
            }

            .em_padd {
                padding: 20px 10px !important;
            }

            .em_text1 {
                font-size: 16px !important;
                line-height: 24px !important;
            }

            u+.em_body .em_full_wrap {
                width: 100% !important;
                width: 100vw !important;
            }
        }

    </style>
</head>

<body class="em_body" style="margin:0px; padding:0px;" bgcolor="#efefef">
    <table class="em_full_wrap" valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#efefef" align="center">
        <tbody>
            <tr>
                <td valign="top" align="center">
                    <table class="em_main_table" style="width:700px;" width="700" cellspacing="0" cellpadding="0" border="0" align="center">
                        <!--Header section-->
                        <tbody>
                            <tr>
                                <td style="padding:15px;" class="em_padd" valign="top" bgcolor="#f6f7f8" align="center">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">

                                    </table>
                                </td>
                            </tr>
                            <!--//Header section-->
                            <!--Banner section-->
                            <tr>
                                <td valign="top" align="center">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                        <tbody>
                                            <tr>
                                                <td valign="top" align="center">
                                                    <p style="color:#e02217; font-size: 70px;"><img src="https://quinn.in/assets/logoblack.png"></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <!--//Banner section-->
                            <!--Content Text Section-->
                            <tr>
                                <td style="padding:35px 70px 30px;" class="em_padd" valign="top" bgcolor="#fff" align="center">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                        <tbody>
                                            <tr>
                                                <td style="font-size:16px;color:#333;" valign="top" align="left">Dear '.$this->input->post("name").',
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 16px;font-size:16px; color:#333;" valign="top" align="left">Thank you for becoming a valuable member of the QUINN family. We have received the information you shared (and its safe with us).</td>
                                            </tr>
                                            <tr>
                                                <td style="font-size:0px;height:15px;" height="15">&nbsp;</td>
                                                <!--—this is space of 15px to separate two paragraphs ---->
                                            </tr>
                                            <tr>
                                                <td style="font-size: 16px;color:#333;padding-bottom:12px;" valign="top" align="left">
                                                    We will be in touch with you Very Soon with exciting offers &amp; information about the Website &amp; App Launch !!</td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 16px;color:#333;padding-bottom:12px;" valign="top" align="left">
                                                    We are so excited to hear from you !! Stay tuned with Quinn !
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="color:#333;padding-bottom:12px;" valign="top" align="left">
                                                  <h2>Quinn Team</h2>
                                                </td>
                                            </tr>
                                            <tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                            <!--//Content Text Section-->
                            <!--Footer Section-->
                            <tr>
                                <td style="padding:38px 30px;" class="em_padd" valign="top" bgcolor="#f6f7f8" align="center">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                        <tbody>
                                            <tr>
                                                <td style="padding-bottom:16px;" valign="top" align="center">
                                                    <table cellspacing="0" cellpadding="0" border="0" align="center">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top" align="center"><a href="#" target="_blank" style="text-decoration:none;"><i class="fa fa-facebook-square" style="color: #333" aria-hidden="true"></i></a></td>
                                                                <td style="width:6px;" width="6">&nbsp;</td>
                                                                <td valign="top" align="center"><a href="#" target="_blank" style="text-decoration:none;"><i class="fa fa-instagram" aria-hidden="true" style="color: #333"></i></a></td>
                                                                <td style="width:6px;" width="6">&nbsp;</td>
                                                                <td valign="top" align="center"><a href="#" target="_blank" style="text-decoration:none;"><i class="fa fa-twitter" style="color: #55acee" aria-hidden="true"></i></a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

</body>

</html>';
    
        $this->load->library('email');


        // $config['protocol']    = 'smtp';
        // $config['smtp_host']    = 'smtp.gmail.com';
        // $config['smtp_port']    = '465';
        // // $config['smtp_timeout'] = '7';
        // $config['smtp_user']    = 'kcmk37283@gmail.com';
        // $config['smtp_pass']    = 'kcmk@123';
        // $config['charset']    = 'utf-8';
        // $config['newline']    = "\r\n";
        // $config['mailtype'] = 'text'; // or html
        // $config['validation'] = TRUE; // bool whether to validate email or not      

        // $this->email->initialize($config);

        // echo $this->email->print_debugger();

      $this->email->from('info@quinn.in', 'Quinn');
      $this->email->to($this->input->post('email'));
       $this->email->set_header('MIME-Version', '1.0');
    $this->email->set_header('Content-type', 'text/html');
    $this->email->set_header('charset=iso-8859-1', 'text/html');
      $this->email->subject('Quinn Support');
     $this->email->message($message1);
      if($this->email->send()){
        $result = $this->users_model->doServey($data);
       echo json_encode(['result' => 1]);
        }else{
           echo json_encode(['result' => 0]);
           echo $this->email->print_debugger();
      }
     }   
       
 
  //************************get in touxch************************
 public function getintouch()
 {
     
   $data = array(
    'name' => $this->input->post('name'),
    'email' => $this->input->post('email'),
    'phone' => $this->input->post('phone'),
    'services' => $this->input->post('services'),
    'area' => $this->input->post('area')

    );
    
    $message='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <!--[if gte mso 9]><xml>
    <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
    </xml><![endif]-->
    <title>Quinn</title>
    <meta charset="charset=iso-8859-1" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 ">
    <meta name="format-detection" content="telephone=no">
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Signika+Negative&display=swap" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!--<![endif]-->
    <style type="text/css">
        body {
            margin: 0 !important;
            padding: 0 !important;
            -webkit-text-size-adjust: 100% !important;
            -ms-text-size-adjust: 100% !important;
            -webkit-font-smoothing: antialiased !important;
            font-family: "Signika Negative", sans-serif;

        }

        img {
            border: 0 !important;
            outline: none !important;
        }

        p {
            Margin: 0px !important;
            Padding: 0px !important;
        }

        table {
            border-collapse: collapse;
            mso-table-lspace: 0px;
            mso-table-rspace: 0px;
        }

        td,
        a,
        span {
            border-collapse: collapse;
            mso-line-height-rule: exactly;
        }

        .ExternalClass * {
            line-height: 100%;
        }

        .em_defaultlink a {
            color: inherit !important;
            text-decoration: none !important;
        }

        span.MsoHyperlink {
            mso-style-priority: 99;
            color: inherit;
        }

        span.MsoHyperlinkFollowed {
            mso-style-priority: 99;
            color: inherit;
        }

        @media only screen and (min-width:481px) and (max-width:699px) {
            .em_main_table {
                width: 100% !important;
            }

            .em_wrapper {
                width: 100% !important;
            }

            .em_hide {
                display: none !important;
            }

            .em_img {
                width: 100% !important;
                height: auto !important;
            }

            .em_h20 {
                height: 20px !important;
            }

            .em_padd {
                padding: 20px 10px !important;
            }
        }

        @media screen and (max-width: 480px) {
            .em_main_table {
                width: 100% !important;
            }

            .em_wrapper {
                width: 100% !important;
            }

            .em_hide {
                display: none !important;
            }

            .em_img {
                width: 100% !important;
                height: auto !important;
            }

            .em_h20 {
                height: 20px !important;
            }

            .em_padd {
                padding: 20px 10px !important;
            }

            .em_text1 {
                font-size: 16px !important;
                line-height: 24px !important;
            }

            u+.em_body .em_full_wrap {
                width: 100% !important;
                width: 100vw !important;
            }
        }

    </style>
</head>

<body class="em_body" style="margin:0px; padding:0px;" bgcolor="#efefef">
    <table class="em_full_wrap" valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#efefef" align="center">
        <tbody>
            <tr>
                <td valign="top" align="center">
                    <table class="em_main_table" style="width:700px;" width="700" cellspacing="0" cellpadding="0" border="0" align="center">
                        <!--Header section-->
                        <tbody>
                            <tr>
                                <td style="padding:15px;" class="em_padd" valign="top" bgcolor="#f6f7f8" align="center">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">

                                    </table>
                                </td>
                            </tr>
                            <!--//Header section-->
                            <!--Banner section-->
                            <tr>
                                <td valign="top" align="center">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                        <tbody>
                                            <tr>
                                                <td valign="top" align="center">
                                                    <p style="color:#e02217; font-size: 70px;"><img src="https://quinn.in/assets/logoblack.png"></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <!--//Banner section-->
                            <!--Content Text Section-->
                            <tr>
                                <td style="padding:35px 70px 30px;" class="em_padd" valign="top" bgcolor="#fff" align="center">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                        <tbody>
                                            <tr>
                                                <td style="font-size:16px;color:#333;" valign="top" align="left">Dear '.$this->input->post("name").',
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 16px;font-size:16px; color:#333;" valign="top" align="left">Thank you for becoming a valuable member of the QUINN family. We have received the information you shared (and its safe with us).</td>
                                            </tr>
                                            <tr>
                                                <td style="font-size:0px;height:15px;" height="15">&nbsp;</td>
                                                <!--—this is space of 15px to separate two paragraphs ---->
                                            </tr>
                                            <tr>
                                                <td style="font-size: 16px;color:#333;padding-bottom:12px;" valign="top" align="left">
                                                    We will be in touch with you Very Soon with exciting offers &amp; information about the Website &amp; App Launch !!</td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 16px;color:#333;padding-bottom:12px;" valign="top" align="left">
                                                    We are so excited to hear from you !! Stay tuned with Quinn !
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="color:#333;padding-bottom:12px;" valign="top" align="left">
                                                  <h2>Quinn Team</h2>
                                                </td>
                                            </tr>
                                            <tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                            <!--//Content Text Section-->
                            <!--Footer Section-->
                            <tr>
                                <td style="padding:38px 30px;" class="em_padd" valign="top" bgcolor="#f6f7f8" align="center">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                        <tbody>
                                            <tr>
                                                <td style="padding-bottom:16px;" valign="top" align="center">
                                                    <table cellspacing="0" cellpadding="0" border="0" align="center">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top" align="center"><a href="#" target="_blank" style="text-decoration:none;"><i class="fa fa-facebook-square" style="color: #333" aria-hidden="true"></i></a></td>
                                                                <td style="width:6px;" width="6">&nbsp;</td>
                                                                <td valign="top" align="center"><a href="#" target="_blank" style="text-decoration:none;"><i class="fa fa-instagram" aria-hidden="true" style="color: #333"></i></a></td>
                                                                <td style="width:6px;" width="6">&nbsp;</td>
                                                                <td valign="top" align="center"><a href="#" target="_blank" style="text-decoration:none;"><i class="fa fa-twitter" style="color: #55acee" aria-hidden="true"></i></a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

</body>

</html>

';
    
    $this->load->library('email');
    $this->email->set_header('MIME-Version', '1.0');
    $this->email->set_header('Content-type', 'text/html');
    $this->email->set_header('charset=iso-8859-1', 'text/html');
     $this->email->from('info@quinn.in', 'Quinn');
      $this->email->to($this->input->post('email'));
      // $this->email->cc('another@another-example.com');
      // $this->email->bcc('them@their-example.com');
      $this->email->subject('Quinn Support');
      $this->email->message($message);
      
   
  
 if($this->email->send()){

 $da=$this->users_model->getInTouch($data);
    echo json_encode(["result"=>1]);

   }else{

echo json_encode(["result"=>0]);
   
   }
 }
 //************************get in touxch************************
 

 //************************Login************************

public function login()

{
  $data['authURL'] =  $this->facebook->login_url();
  $this->load->view('login',$data);
}





public function verifyFunction($code)

{



 $message="Plaase click on this link to verify your account <a href=";



$message.=base_url('user/verifyaccount?verify=');

$message.=$code.">Verify link</a>";





  $this->load->library('email');

    $this->email->set_header('MIME-Version', '1.0');

    $this->email->set_header('Content-type', 'text/html');

    $this->email->set_header('charset=iso-8859-1', 'text/html');

     $this->email->from('info@quinn.in', 'Quinn');

      $this->email->to($this->input->post('email'));

      // $this->email->cc('another@another-example.com');

      // $this->email->bcc('them@their-example.com');

      $this->email->subject('Quinn Support');

      $this->email->message($message);

      

   

  

 if($this->email->send()){



 $da=$this->users_model->getInTouch($data);

    // echo json_encode(["result"=>1]);



   }else{



// echo json_encode(["result"=>0]);

   

   }

}


// ----------------chat module----------------------
public function chatpage()
{       
    $this->checklogin();
            if($this->session->has_userdata('provideremail')){
                
                $email=$this->session->userdata('provideremail');
                $type=$this->session->userdata('type');
//            print_r($email); die;
                 $data['chatdetail']=$this->users_model->getchatbyid($email);
                $data['senderdetail']=$this->users_model->getbyemail($email,$type);
//                print_r($data);
                $this->load->view('message',$data);
                
            }
             if($this->session->has_userdata('useremail')){
                
                
              $email=$this->session->userdata('useremail');
                 $type=$this->session->userdata('type');
//            print_r($email); die;
                 $data['chatdetail']=$this->users_model->getchatbyid($email);
                $data['senderdetail']=$this->users_model->getbyemail($email,$type);
//                print_r($data);
                $this->load->view('message',$data);
                
            }
//            
  
}

    
    public function chat()
{   
         $this->output->set_content_type('application/json');
        $data = array(
            'message'=>$this->input->post('message'),
            'senderid'=>$this->input->post('senderid'),
                   );
//       print_r($data); die;   
        $da=$this->users_model->chatinsert($data);
        $ds['chatdetail'] = $this->users_model->getchatbyid($this->input->post('senderid'));
        $content_wrapper = $this->load->view('chatwrapper', $ds, true);
        $this->output->set_output(json_encode(['content_wrapper' => $content_wrapper]));
        return false;
        //print_r($this->input->post());
  
}

    
    
// ----------------chat module----------------------


 //************************get in touxch************************

//********testimonial********************
   
    
//********testimonial********************

}
