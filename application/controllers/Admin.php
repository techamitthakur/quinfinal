<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    /**
     * Description Admin  controller.

     */
    public function __construct() {
        parent::__construct();
        $this->load->model('admin_model');
    }

    public function index() {
        $this->load->view('admin/login');
    }
    
    public function queries(){
    
    $data['querydetails']=$this->admin_model->getqueryDetails();
//    print_r($data); die;
    $this->load->view('admin/user/querydetails',$data);
}

    public function checkLogin() {
        $this->output->set_content_type('application/json');
        $this->form_validation->set_rules('admin_email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('admin_password', 'Password', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->output->set_output(json_encode(['result' => 0, 'errors' => $this->form_validation->error_array()]));
            return FALSE;
        }
        $result = $this->admin_model->checkLogin();
        //print_r($result); die;
        if ($result) {
            $this->session->set_userdata('admin_email', $result['admin_email']);
            $this->output->set_output(json_encode(['result' => 1, 'url' => base_url('admin/dashboard'), 'msg' => 'Loading..']));
            return FALSE;
        } else {
            $this->output->set_output(json_encode(['result' => -1, 'msg' => 'Invalid Email/username or password']));
            return FALSE;
        }
    }

    public function dashboard() {
        if (empty($this->session->userdata('admin_email'))) {
            redirect(base_url('admin'));
        }
        $this->load->view('admin/commons/header');
        $this->load->view('admin/commons/sidebar');
        $this->load->view('admin/index');
        $this->load->view('admin/commons/footer');
    }

    public function logout() {
        $result = $this->admin_model->logout();
//        print_r($result);
        if ($result) {
            $this->session->unset_userdata('admin_email');
            redirect(base_url('admin'));
        }
    }


    



  //************************************category******************************************* 

     public function category_view()

    {

    $data['category']=$this->admin_model->getCategory();

    //print_r($data); die;

    $this->load->view('admin/user/category/category_view',$data);

         //echo "ini am ini am in </h1>";

    }

   public function category_add()
  {         
    if(!$this->input->post()=="")
    {
               $this->load->helper(array('form', 'url'));
                $this->load->library('form_validation');
                $this->form_validation->set_rules('category', 'category', 'required');                     
               if ($this->form_validation->run() == FALSE)       
                {        
                  $data['category']=$this->admin_model->getCategory();
                         // $this->load->view('admin/user/sub_category_add',$data);
                         $this->load->view('admin/user/category/category_add',$data);
                }
                else
                { 
                //************data base entry***************************
                  $data = array(
                                'category_name' =>$this->input->post('category'),
                                'status' =>$this->input->post('active')
                               );        
                          $check=$this->admin_model->insertCategory($data);

                          if($check>0)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Added Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/category_view");

                              }

                            else

                            {

                             

                              $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Added</p>');

                              // After that you need to used redirect function instead of load view such as 

                              redirect("admin/category_view");

                             }
                          }

}
                          else{



                    //$data['sub_category']=$this->admin_model->getSubCategory();

             $data['category']=$this->admin_model->getCategory();

             // $this->load->view('admin/user/sub_category/sub_category_edit',$data);

             $this->load->view('admin/user/category/category_add',$data);



        }

      
}

         


      public function category_edit($id)

  {

                // $this->load->helper(array('form', 'url'));

                // $this->load->library('form_validation');

                // $this->form_validation->set_rules('category', 'Username', 'required');

                // $this->form_validation->set_rules('subcategory', 'subcategory', 'required');

                       

                // if ($this->form_validation->run() == FALSE)       

                // {

                //      $data['sub_category']=$this->admin_model->whereSubCategory($id);

                //      $data['category']=$this->admin_model->getCategory();

                //     $this->load->view('admin/user/sub_category/sub_category_edit',$data);

                // }

                // else

                // { 

    //print_r($this->input->post()); die;

                             $data = array(

                                'category_name' =>$this->input->post('category'),

                                // 'sub_name' =>$this->input->post('subcategory'),

                                'status' =>$this->input->post('active')

                               );



                          $check=$this->admin_model->updateCategory($id,$data);

                          if($check==1)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Updated Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/category_view"); 

                            }

                          else

                          {

                                $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Updated</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/category_view"); 

                          }

          //}

  }



   public function category_fetch_edit($id)

     {

        // echo $id; die;

        $data['category']=$this->admin_model->whereCategory($id);

         //$data['category']=$this->admin_model->getCategory();

        $this->load->view('admin/user/category/category_edit',$data);



     }



      public function category_delete($id){



     $data=$this->admin_model->deleteCategory($id);

     if($data==true){

    

            $this->session->set_flashdata('action_message', '<span class="text-success">Subcategory deleted successfully</span>');

            redirect("admin/category_view");

        } else {

            $this->session->set_flashdata('error_message', '<span class="text-danger">Please Try Again </span>');

            redirect("admin/category_view");

        }



}

//*****************************category controller*****************************















//*************************sub category controller*************************

     public function sub_category_view()

    {
     $data['sub_category']=$this->admin_model->getSubCategory();
    $this->load->view('admin/user/sub_category/sub_category_view',$data);
    }



// public function up()

// {

//   print_r($this->input->post());

//   echo $_FILES['userfile']['name'];

//   # code...

// }

      public function sub_category_add()

      {         



        

                if(!$this->input->post()=="")

                {



                $this->load->helper(array('form', 'url'));



                $this->load->library('form_validation');



                $this->form_validation->set_rules('category', 'category', 'required');

                $this->form_validation->set_rules('subcategory', 'subcategory', 'required');

                       

                if ($this->form_validation->run() == FALSE)       

                {        

                  $data['category']=$this->admin_model->getCategory();

                         // $this->load->view('admin/user/sub_category_add',$data);

                         $this->load->view('admin/user/sub_category/sub_category_add',$data);

                }

                else

                { 



                 //************data base entry***************************

                  $data = array(

                                'cat_id' =>$this->input->post('category'),
                                'subcategory_name' =>$this->input->post('subcategory'),
                                'status' =>$this->input->post('active')

                               );

                 

                          $check=$this->admin_model->insertSubCategory($data);

                          if($check>0)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Added Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/sub_category_view");

                            }

                          else

                          {

                           

                            $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Added</p>');

                            // After that you need to used redirect function instead of load view such as 

                            redirect("admin/sub_category_view");

                           }

         //**********************end image upload*****************************

                      

               //}   //******************end******************************

          //}

        }



      }else{



                    //$data['sub_category']=$this->admin_model->getSubCategory();

             $data['category']=$this->admin_model->getCategory();

             // $this->load->view('admin/user/sub_category/sub_category_edit',$data);

             $this->load->view('admin/user/sub_category/sub_category_add',$data);



        }

      

  }







    public function sub_category_fetch_edit($id)

     {

        // echo $id; die;

        $data['sub_category']=$this->admin_model->whereSubCategory($id);

         $data['category']=$this->admin_model->getCategory();

        $this->load->view('admin/user/sub_category/sub_category_edit',$data);



     }





  public function sub_category_edit($id)

  {

                $this->load->helper(array('form', 'url'));

                $this->load->library('form_validation');

                $this->form_validation->set_rules('category', 'Category', 'required');

                $this->form_validation->set_rules('subcategory', 'Subcategory', 'required');

                $this->form_validation->set_rules('active', 'Active', 'required');

                       

                if ($this->form_validation->run() == FALSE)       

                {

                     $data['sub_category']=$this->admin_model->whereSubCategory($id);

                     $data['category']=$this->admin_model->getCategory();

                    $this->load->view('admin/user/sub_category/sub_category_edit',$data);

                }

                else

                { 

                             $data = array(

                                'cat_id' =>$this->input->post('category'),

                                'sub_name' =>$this->input->post('subcategory'),

                                 'url_id' =>preg_replace("/[^a-zA-Z0-9]+/", "", $this->input->post('subcategory')),

                                'active' =>$this->input->post('active')

                               );



                          $check=$this->admin_model->updateSubCategory($id,$data);

                          if($check==1)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Updated Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/sub_category_view");

                            }

                          else

                          {

                                $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Updated</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/sub_category_view");

                                

                          }

          }

  }



 public function sub_category_delete($id){



 $data=$this->admin_model->deleteSubCategory($id);

 if($data==true){

    

            $this->session->set_flashdata('action_message', '<span class="text-success">Subcategory deleted successfully</span>');

            redirect("admin/sub_category_view");

        } else {

            $this->session->set_flashdata('error_message', '<span class="text-danger">Please Try Again </span>');

            redirect("admin/sub_category_view");

        }



}



//*******************************************end Subcategory********************************************************







  //************************************About******************************************* 

     public function about_view()

    {

    $data['about']=$this->admin_model->getAbout();

    //print_r($data); die;

    $this->load->view('admin/user/about/about_view',$data);

         //echo "ini am ini am in </h1>";

    }





   public function about_fetch_edit($id)

     {

        // echo $id; die;

        $data['about']=$this->admin_model->whereAbout($id);

         //$data['category']=$this->admin_model->getCategory();

        $this->load->view('admin/user/about/about_edit',$data);



     }



      public function about_add()

      {



        if(!$this->input->post()==""){

                          //print_r($this->input->post()); die;





                  $data = array(

                                //'cat_id' =>$this->input->post('cat_id'),

                                'sub_id' =>$this->input->post('sub_id'),

                                'heading' =>$this->input->post('heading'),

                                'content' =>$this->input->post('content')

                                //'image' =>$this->input->post('image')

                               );





                  //print_r($data); die();  

                          $check=$this->admin_model->insertAbout($data);

                          if($check>0)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Added Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/about_view");

                            }

                          else

                          {

                            $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Added</p>');

                            // After that you need to used redirect function instead of load view such as 

                            redirect("admin/about_view");

                           }

          }else{



            // echo $id; die;

             $data['sub_category']=$this->admin_model->getSubCategory('About');

             $data['category']=$this->admin_model->getCategory();

             // $this->load->view('admin/user/sub_category/sub_category_edit',$data);

             $this->load->view('admin/user/about/about_add',$data);

          }

    }



      public function about_edit($id)

        {

                

                             $data = array(

                                //'cat_id' =>$this->input->post('cat_id'),

                                'sub_id' =>$this->input->post('sub_id'),

                                'heading' =>$this->input->post('heading'),

                                'content' =>$this->input->post('content'),

                               );



                          $check=$this->admin_model->updateAbout($id,$data);

                          if($check==1)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Updated Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/about_view"); 

                            }

                          else

                          {

                                $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Updated</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/about_view"); 

                          }

          //}

  }



      public function about_delete($id){



     $data=$this->admin_model->deleteAbout($id);

     if($data==true){

    

            $this->session->set_flashdata('action_message', '<span class="text-success">Subcategory deleted successfully</span>');

            redirect("admin/about_view");

        } else {

            $this->session->set_flashdata('error_message', '<span class="text-danger">Please Try Again </span>');

            redirect("admin/about_view");

        }



}

//*************************About controller*************************





//************************************Services******************************************* 

     public function services_view()

    {

    $data['services']=$this->admin_model->getServices();

    //print_r($data); die;

    $this->load->view('admin/user/services/services_view',$data);

         //echo "ini am ini am in </h1>";

    }





   public function services_fetch_edit($id)

     {

        // echo $id; die;

        $data['services']=$this->admin_model->whereServices($id);

          $data['sub_category']=$this->admin_model->getSubCategory('Services');

             $data['category']=$this->admin_model->getCategory();

         $this->load->view('admin/user/services/services_edit',$data);



     }



      public function services_add()

      {



        if(!$this->input->post()==""){

                          //print_r($this->input->post()); die;



                  //**********************image upload*****************************



                $config['upload_path']          = './public/upload/services/';

                $config['allowed_types']        = 'gif|jpg|png';

                $config['max_size']             = 2000;

                $config['max_width']            = 2000;

                $config['max_height']           = 2000;



                $this->load->library('upload', $config);

              

                if ( ! $this->upload->do_upload('file_name'))

                {

                        $error = array('error' => $this->upload->display_errors());



                        $this->load->view('admin/user/services/services_add', $error);

                }

                else

                {



                        $data = array('upload_data' => $this->upload->data());

                        // print_r($data['upload_data']['file_name']); die;



                        $this->load->view('admin/user/services/services_add', $data);

               



          //**********************image upload*****************************



                

                   $data = array(

                                //'sub_heading' =>$this->input->post('sub_heading'),

                                //'sub_id' =>$this->input->post('sub_id'),

                                'heading' =>$this->input->post('heading'),

                                'image' =>$data['upload_data']['file_name'],

                                'content' =>$this->input->post('content'),

                                'active' =>$this->input->post('active')

                               );





                  //print_r($data); die();  

                          $check=$this->admin_model->insertServices($data);

                          if($check>0)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Added Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/services_view");

                            }

                          else

                          {

                            $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Added</p>');

                            // After that you need to used redirect function instead of load view such as 

                            redirect("admin/services_view");

                           }

         //**********************end image upload*****************************

                           

                }





          }else{



            // echo $id; die;

             $data['sub_category']=$this->admin_model->getSubCategory('Services');

             $data['category']=$this->admin_model->getCategory();

             // $this->load->view('admin/user/sub_category/sub_category_edit',$data);

             $this->load->view('admin/user/services/services_add',$data);

          }

    }



      public function services_edit($id)

        {

                



                        if(is_uploaded_file($_FILES['file_name']['tmp_name'])) {







                $config['upload_path']          = './public/upload/services/';

                $config['allowed_types']        = 'gif|jpg|png';

                $config['max_size']             = 2000;

                $config['max_width']            = 2000;

                $config['max_height']           = 2000;



                $this->load->library('upload', $config);

              

                if ( ! $this->upload->do_upload('file_name'))

                {

                        $error = array('error' => $this->upload->display_errors());



                        $this->load->view('admin/user/services/services_edit', $error);

                }

                else

                {



                        $data = array('upload_data' => $this->upload->data());

                        // print_r($data['upload_data']['file_name']); die;



                        $this->load->view('admin/user/services/services_edit', $data);

               



          //**********************image upload*****************************



                   $data = array(

                              //'sub_heading' =>$this->input->post('sub_heading'),

                                //'sub_id' =>$this->input->post('sub_id'),

                                'heading' =>$this->input->post('heading'),

                                'content' =>$this->input->post('content'),

                                'active' =>$this->input->post('active'),

                                'image' =>$data['upload_data']['file_name']

                               );



                          $check=$this->admin_model->updateServices($id,$data);

                          if($check==1)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Updated Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/services_view"); 

                            }

                          else

                          {

                                $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Updated</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/services_view"); 

                          }

         //**********************end image upload*****************************

                           

                }



                             $data = array(

                              //'sub_heading' =>$this->input->post('sub_heading'),

                               // 'sub_id' =>$this->input->post('sub_id'),

                                'heading' =>$this->input->post('heading'),

                                'content' =>$this->input->post('content'),

                                'active' =>$this->input->post('active'),

                                'image' =>$data['upload_data']['file_name']

                               );



                          $check=$this->admin_model->updateServices($id,$data);

                          if($check==1)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Updated Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/services_view"); 

                            }

                          else

                          {

                                $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Updated</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/services_view"); 

                          }



                    }else{



















                    }

          //}

  }



      public function services_delete($id){



     $data=$this->admin_model->deleteServices($id);

     if($data==true){

    

            $this->session->set_flashdata('action_message', '<span class="text-success">Subcategory deleted successfully</span>');

            redirect("admin/services_view");

        } else {

            $this->session->set_flashdata('error_message', '<span class="text-danger">Please Try Again </span>');

            redirect("admin/services_view");

        }



}

//*************************Services end controller*************************



  //************************************Treatment******************************************* 

     public function treatment_view()

    {

    $data['treatment']=$this->admin_model->getTreatment();

    //print_r($data); die;

    $this->load->view('admin/user/treatment/treatment_view',$data);

         //echo "ini am ini am in </h1>";

    }





   public function treatment_fetch_edit($id)

     {

        // echo $id; die;

        $data['treatment']=$this->admin_model->whereTreatment($id);

          $data['sub_category']=$this->admin_model->getSubCategory('Treatment');

          //print_r($data);die;

             $data['category']=$this->admin_model->getCategory();

        $this->load->view('admin/user/treatment/treatment_edit',$data);



     }



      public function treatment_add()

      {



        if(!$this->input->post()==""){

                          //print_r($this->input->post()); die;





                  $data = array(

                                'sub_heading' =>$this->input->post('sub_heading'),

                                'sub_id' =>$this->input->post('sub_id'),

                                'heading' =>$this->input->post('heading'),

                                'content' =>$this->input->post('content'),

                                'active' =>$this->input->post('active')

                                //'image' =>$this->input->post('image')

                               );





                  //print_r($data); die();  

                          $check=$this->admin_model->insertTreatment($data);

                          if($check>0)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Added Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/treatment_view");

                            }

                          else

                          {

                            $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Added</p>');

                            // After that you need to used redirect function instead of load view such as 

                            redirect("admin/treatment_view");

                           }

          }else{



            // echo $id; die;

             $data['sub_category']=$this->admin_model->getSubCategory('Treatment');

             $data['category']=$this->admin_model->getCategory();

             // $this->load->view('admin/user/sub_category/sub_category_edit',$data);

             $this->load->view('admin/user/treatment/treatment_add',$data);

          }

    }



      public function treatment_edit($id)

        {

                

                             $data = array(

                                'sub_heading' =>$this->input->post('sub_heading'),

                                'sub_id' =>$this->input->post('sub_id'),

                                'heading' =>$this->input->post('heading'),

                                'content' =>$this->input->post('content'),

                                'active' =>$this->input->post('active')

                               );



                          $check=$this->admin_model->updateTreatment($id,$data);

                          if($check==1)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Updated Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/treatment_view"); 

                            }

                          else

                          {

                                $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Updated</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/treatment_view"); 

                          }

          //}

  }



      public function treatment_delete($id){



     $data=$this->admin_model->deleteTreatment($id);

     if($data==true){

    

            $this->session->set_flashdata('action_message', '<span class="text-success">data deleted successfully</span>');

            redirect("admin/treatment_view");

        } else {

            $this->session->set_flashdata('error_message', '<span class="text-danger">Please Try Again </span>');

            redirect("admin/treatment_view");

        }



}

//*************************Treatment end controller*************************





  //************************************Product******************************************* 

     public function product_view()

    {

    $data['product']=$this->admin_model->getProduct();

    //print_r($data); die;

    $this->load->view('admin/user/product/product_view',$data);

         //echo "ini am ini am in </h1>";

    }





   public function product_fetch_edit($id)

     {

        // echo $id; die;

        $data['product']=$this->admin_model->whereProduct($id);

         //$data['category']=$this->admin_model->getCategory();

        $this->load->view('admin/user/product/product_edit',$data);



     }



      public function product_add()

      {



        if(!$this->input->post()==""){



           //if(is_uploaded_file($_FILES['file_name']['tmp_name'])) {

        

                         // print_r($this->input->post());

          //**********************image upload*****************************



                $config['upload_path']          = './public/upload/product/';

                $config['allowed_types']        = '*';

                $config['max_size']             = 2000;

                $config['max_width']            = 2000;

                $config['max_height']           = 2000;

                $this->load->library('upload', $config);

              

                if ( ! $this->upload->do_upload('file_name'))

                {

                        $error = array('error' => $this->upload->display_errors());



                        $this->load->view('admin/user/product/product_add', $error);

                }

                else

                {



                        $data = array('upload_data' => $this->upload->data());

                        // print_r($data['upload_data']['file_name']); die;



                        $this->load->view('admin/user/product/product_add', $data);

               



          //**********************image upload*****************************



                  $data = array(

                                'subcat_id' =>$this->input->post('subcat_id'),

                                'product_name' =>$this->input->post('product_name'),

                                'product_type' =>$this->input->post('product_type'),

                                'product_description' =>$this->input->post('product_description'),

                                'product_price' =>$this->input->post('product_price'),

                                'product_offer' =>$this->input->post('product_offer'),

                                'product_image' =>$data['upload_data']['file_name'],

                                'active' =>$this->input->post('active')

                                //'image' =>$this->input->post('image')

                               );





                  //print_r($data); die();  

                          $check=$this->admin_model->insertProduct($data);

                          if($check>0)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Added Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/product_view");

                            }

                          else

                          {

                            $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Added</p>');

                            // After that you need to used redirect function instead of load view such as 

                            redirect("admin/product_view");

                           }

         //**********************end image upload*****************************

                           

                }





          }else{



            // // echo $id; die;

           $data['subcategory']=$this->admin_model->getSubCategory();

            //  $data['category']=$this->admin_model->getCategory();

            //  // $this->load->view('admin/user/sub_category/sub_category_edit',$data);

             $this->load->view('admin/user/product/product_add',$data);

          }

    }



      public function product_edit($id)

        {





            if(is_uploaded_file($_FILES['file_name']['tmp_name'])) {

                //**********************image upload*****************************



                $config['upload_path']          = './public/upload/product/';

                $config['allowed_types']        = '*';

                $config['max_size']             = 2048;

                $config['max_width']            = 2048;

                $config['max_height']           = 2048;



                $this->load->library('upload', $config);

              

                if ( ! $this->upload->do_upload('file_name'))

                {

                        $error = array('error' => $this->upload->display_errors());



                        $this->load->view('admin/user/product/product_edit', $error);

                }

                else

                {

                        $data = array('upload_data' => $this->upload->data());

                        // print_r($data['upload_data']['file_name']); die;



                        $this->load->view('admin/user/product/product_edit', $data);

               



          //**********************image upload*****************************

                

                             $data = array(

                                  'subcat_id' =>$this->input->post('subcat_id'),

                                 'product_name' =>$this->input->post('product_name'),

                                'product_type' =>$this->input->post('product_type'),

                                'product_description' =>$this->input->post('product_description'),

                                'product_price' =>$this->input->post('product_price'),

                                'product_offer' =>$this->input->post('product_offer'),

                                'product_image' =>$data['upload_data']['file_name'],

                                'active' =>$this->input->post('active')

                               );



                          $check=$this->admin_model->updateProduct($id,$data);

                          if($check==1)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Updated Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/product_view"); 

                            }

                          else

                          {

                                $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Updated</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/product_view"); 

                          }

                        }



                  }else{



                   $data = array(

                                   'subcat_id' =>$this->input->post('subcat_id'),

                                'product_name' =>$this->input->post('product_name'),

                                'product_type' =>$this->input->post('product_type'),

                                'product_description' =>$this->input->post('product_description'),

                                'product_price' =>$this->input->post('product_price'),

                                'product_offer' =>$this->input->post('product_offer'),

                              

                                'active' =>$this->input->post('active')

                                //'image' =>$this->input->post('image')

                               );





                  //print_r($data); die();  

                          $check=$this->admin_model->updateProduct($id,$data);

                          if($check>0)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Added Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/product_view");

                            }

                          else

                          {

                            $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Added</p>');

                            // After that you need to used redirect function instead of load view such as 

                            redirect("admin/product_view");

                           }





                }

          //}

  }



      public function product_delete($id){



     $data=$this->admin_model->deleteProduct($id);

     if($data==true){

    

            $this->session->set_flashdata('action_message', '<span class="text-success">Subcategory deleted successfully</span>');

            redirect("admin/product_view");

        } else {

            $this->session->set_flashdata('error_message', '<span class="text-danger">Please Try Again </span>');

            redirect("admin/product_view");

        }



}

//*************************Product end controller*************************



//************************************Slider******************************************* 

     public function slider_view()

    {

    $data['slider']=$this->admin_model->getSlider();

    //print_r($data); die;

    $this->load->view('admin/user/slider/slider_view',$data);

         //echo "ini am ini am in </h1>";

    }





   public function slider_fetch_edit($id)

     {

        // echo $id; die;

        $data['slider']=$this->admin_model->whereSlider($id);

         //$data['category']=$this->admin_model->getCategory();

        $this->load->view('admin/user/slider/slider_edit',$data);



     }



      public function slider_add()

      {

                         // print_r($this->input->post());

          //**********************image upload*****************************



                $config['upload_path']          = './public/upload/slider/';

                $config['allowed_types']        = '*';

                $config['max_size']             = 2048;

                $config['max_width']            = 3000;

                $config['max_height']           = 3000;



                $this->load->library('upload', $config);

              

                if ( ! $this->upload->do_upload('file_name'))

                {

                        $error = array('error' => $this->upload->display_errors());



                        $this->load->view('admin/user/slider/slider_add', $error);

                }

                else

                {

                        $data = array('upload_data' => $this->upload->data());

                        // print_r($data['upload_data']['file_name']); die;



                        $this->load->view('admin/user/slider/slider_add', $data);

               



          //**********************image upload*****************************



                  $data = array(

                       

                                'image' =>$data['upload_data']['file_name'],

                                'active' =>$this->input->post('active')

                                //'image' =>$this->input->post('image')

                               );





                  //print_r($data); die();  

                          $check=$this->admin_model->insertSlider($data);

                          if($check>0)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Added Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/slider_view");

                            }

                          else

                          {

                            $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Added</p>');

                            // After that you need to used redirect function instead of load view such as 

                            redirect("admin/slider_view");

                           }

         //**********************end image upload*****************************

                           

                }

         

    }



      public function slider_edit($id)

        {                   



               if(is_uploaded_file($_FILES['file_name']['tmp_name'])) {



                             //**********************image upload*****************************



                $config['upload_path']          = './public/upload/slider/';

                $config['allowed_types']        = 'gif|jpg|png';

                $config['max_size']             = 2048;

                $config['max_width']            = 3000;

                $config['max_height']           = 3000;



                $this->load->library('upload', $config);

              

                if ( ! $this->upload->do_upload('file_name'))

                {

                        $error = array('error' => $this->upload->display_errors());



                        $this->load->view('admin/user/slider/slider_edit', $error);

                }

                else

                {

                        $data = array('upload_data' => $this->upload->data());

                        // print_r($data['upload_data']['file_name']); die;



                        $this->load->view('admin/user/slider/slider_edit', $data);

               



          //**********************image upload*****************************



                

                             $data = array(

                                'image' =>$data['upload_data']['file_name'],

                                'active' =>$this->input->post('active')

                               );



                          $check=$this->admin_model->updateSlider($id,$data);

                          if($check==1)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Updated Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/slider_view"); 

                            }

                          else

                          {

                                $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Updated</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/slider_view"); 

                          }

          }

        }else{





                             $data = array(

                                //'image' =>$data['upload_data']['file_name'],

                                'active' =>$this->input->post('active')

                               );



                          $check=$this->admin_model->updateSlider($id,$data);

                          if($check==1)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Updated Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/slider_view"); 

                            }

                          else

                          {

                                $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Updated</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/slider_view"); 

                          }







        }

  }



      public function slider_delete($id){



     $data=$this->admin_model->deleteSlider($id);

     if($data==true){

    

            $this->session->set_flashdata('action_message', '<span class="text-success">Subcategory deleted successfully</span>');

            redirect("admin/slider_view");

        } else {

            $this->session->set_flashdata('error_message', '<span class="text-danger">Please Try Again </span>');

            redirect("admin/slider_view");

        }



}

//*************************slider end controller*************************



//************************************Brand******************************************* 

     public function brand_view()

    {

    $data['brand']=$this->admin_model->getBrand();

    //print_r($data); die;

    $this->load->view('admin/user/brand/brand_view',$data);

         //echo "ini am ini am in </h1>";

    }





   public function brand_fetch_edit($id)

     {

        // echo $id; die;

        $data['brand']=$this->admin_model->whereBrand($id);

         //$data['category']=$this->admin_model->getCategory();

        $this->load->view('admin/user/brand/brand_edit',$data);



     }



      public function brand_add()

      {



        

        

                         // print_r($this->input->post());

          //**********************image upload*****************************



                $config['upload_path']          = './public/upload/brand/';

                $config['allowed_types']        = '*';

                $config['max_size']             = 2000;

                $config['max_width']            = 2000;

                $config['max_height']           = 2000;



                $this->load->library('upload', $config);

              

                if ( ! $this->upload->do_upload('file_name'))

                {

                        $error = array('error' => $this->upload->display_errors());



                        $this->load->view('admin/user/brand/brand_add', $error);

                }

                else

                {

                        $data = array('upload_data' => $this->upload->data());

                        // print_r($data['upload_data']['file_name']); die;



                        $this->load->view('admin/user/brand/brand_add', $data);

               



          //**********************image upload*****************************



                  $data = array(

                       

                                'image' =>$data['upload_data']['file_name'],

                                'brand_name' =>$this->input->post('brand_name'),

                                'active' =>$this->input->post('active')

                                //'image' =>$this->input->post('image')

                               );





                  //print_r($data); die();  

                          $check=$this->admin_model->insertBrand($data);

                          if($check>0)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Added Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/brand_view");

                            }

                          else

                          {

                            $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Added</p>');

                            // After that you need to used redirect function instead of load view such as 

                            redirect("admin/brand_view");

                           }

         //**********************end image upload*****************************

                           

                }

         

    }



      public function brand_edit($id)

        {                 

  



             if(is_uploaded_file($_FILES['file_name']['tmp_name'])) {

          //**********************image upload*****************************



                $config['upload_path']          = './public/upload/brand/';

                $config['allowed_types']        = '*';

                $config['max_size']             = 2000;

                $config['max_width']            = 2000;

                $config['max_height']           = 2000;

                $this->load->library('upload', $config);

               print_r($this->upload->do_upload('file_name'));

               echo($this->upload->do_upload('file_name'));

              

                if ( ! $this->upload->do_upload('file_name'))

                {

                        $error = array('error' => $this->upload->display_errors());



                        $this->load->view('admin/user/brand/brand_edit', $error);

                }

                else

                {

                        $data = array('upload_data' => $this->upload->data());

                        // print_r($data['upload_data']['file_name']); die;



                        $this->load->view('admin/user/brand/brand_edit', $data);

               



          //**********************image upload*****************************

                

                             $data = array(

                                'image' =>$_FILES['file_name']['name'],

                                'brand_name' =>$this->input->post('brand_name'),

                                'active' =>$this->input->post('active')

                               );



                          $check=$this->admin_model->updateBrand($id,$data);

                          if($check==1)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Updated Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/brand_view"); 

                            }

                          else

                          {

                                $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Updated</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/brand_view"); 

                          }

          }

        }else{



                 $data = array(

                                'brand_name' =>$this->input->post('brand_name'),

                                'active' =>$this->input->post('active')

                               );



                          $check=$this->admin_model->updateBrand($id,$data);

                          if($check==1)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Updated Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/brand_view"); 

                            }

                          else

                          {

                                $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Updated</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/brand_view"); 

                          }
             }

  }



      public function brand_delete($id){



     $data=$this->admin_model->deleteBrand($id);

     if($data==true){

    

            $this->session->set_flashdata('action_message', '<span class="text-success">Subcategory deleted successfully</span>');

            redirect("admin/brand_view");

        } else {

            $this->session->set_flashdata('error_message', '<span class="text-danger">Please Try Again </span>');

            redirect("admin/brand_view");

        }



}

//*************************Brand end controller*************************







//************************************Testimonial******************************************* 

     public function testimonial_view()
    {
    $data['testimonial']=$this->admin_model->getTestimonial();
    //print_r($data); die;
    $this->load->view('admin/user/testimonial/testimonial_view',$data);
         //echo "ini am ini am in </h1>";
    }

   public function testimonial_fetch_edit($id)
     {
        // echo $id; die;
        $data['testimonial']=$this->admin_model->whereTestimonial($id);
         //$data['category']=$this->admin_model->getCategory();
        $this->load->view('admin/user/testimonial/testimonial_edit',$data);
     }

    public function testimonial_add()
    {
         $this->load->view('admin/user/testimonial/testimonial_add');
    }
    
      public function testimonial_insert()
      {
                         // print_r($this->input->post());
          //**********************image upload*****************************
                $config['upload_path']          = './public/upload/testimonial/';
                $config['allowed_types']        = '*';
                $config['max_size']             = 2048;
                $config['max_width']            = 3000;
                $config['max_height']           = 3000;
                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload('file_name'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        $this->load->view('admin/user/testimonial/testimonial_add', $error);
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        // print_r($data['upload_data']['file_name']); die;
                        $this->load->view('admin/user/testimonial/testimonial_add', $data);
          //**********************image upload*****************************
                  $data = array(
                                'name' =>$this->input->post('name'),
                                'designation' =>$this->input->post('heading'),
                                'content' =>$this->input->post('content'),
                                'image' =>$data['upload_data']['file_name'],
                                'status' =>$this->input->post('active')
                                //'image' =>$this->input->post('image')
                               );
                  //print_r($data); die();  
                          $check=$this->admin_model->insertTestimonial($data);
                          if($check>0)
                            {
                                // Set flash data 
                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Added Successfully</p>');
                                // After that you need to used redirect function instead of load view such as 
                                redirect("admin/testimonial_view");
                            }
                          else
                          {
                            $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Added</p>');
                            // After that you need to used redirect function instead of load view such as 
                            redirect("admin/testimonial_view");
                           }
         //**********************end image upload*****************************
                    }
    }

      public function testimonial_edit($id)
        {                   
          if(is_uploaded_file($_FILES['file_name']['tmp_name'])) {
                    //**********************image upload*****************************
                $config['upload_path']          = './public/upload/testimonial/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 2048;
                $config['max_width']            = 3000;
                $config['max_height']           = 3000;
                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload('file_name'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        $this->load->view('admin/user/testimonial/testimonial_edit', $error);
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        // print_r($data['upload_data']['file_name']); die;
                        $this->load->view('admin/user/testimonial/testimonial_edit', $data);
             //**********************image upload*****************************
                         $data = array(
                         'name' =>$this->input->post('name'),
                         'designation' =>$this->input->post('heading'),
                         'content' =>$this->input->post('content'),
                         'image' =>$data['upload_data']['file_name'],
                        'active' =>$this->input->post('active')
                        //'image' =>$this->input->post('image')
                               );
                          $check=$this->admin_model->updateTestimonial($id,$data);
                          if($check==1)
                            {
                                // Set flash data 
                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Updated Successfully</p>');
                                // After that you need to used redirect function instead of load view such as 
                                redirect("admin/testimonial_view"); 
                            }
                          else
                          {
                            $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Updated</p>');
                                // After that you need to used redirect function instead of load view such as 
                                redirect("admin/testimonial_view"); 
                          }
          }
        }else{
                     $data = array(
                                //'image' =>$data['upload_data']['file_name'],
                               'name' =>$this->input->post('name'),
                                  'designation' =>$this->input->post('heading'),
                                  'content' =>$this->input->post('content'),
                                  'active' =>$this->input->post('active')
                                //'image' =>$this->input->post('image')
                               );
                          $check=$this->admin_model->updateTestimonial($id,$data);
                          if($check==1)
                            {
                                // Set flash data 
                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Updated Successfully</p>');
                                // After that you need to used redirect function instead of load view such as 
                                redirect("admin/testimonial_view"); 
                            }
                          else
                          {
                              $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Updated</p>');
                                // After that you need to used redirect function instead of load view such as 
                                redirect("admin/testimonial_view"); 
                          }
        }
  }

  public function testimonial_delete($id){
     $data=$this->admin_model->deleteTestimonial($id);
     if($data==true){
            $this->session->set_flashdata('action_message', '<span class="text-success">Subcategory deleted successfully</span>');
            redirect("admin/testimonial_view");
        } else {
            $this->session->set_flashdata('error_message', '<span class="text-danger">Please Try Again </span>');
            redirect("admin/testimonial_view");
        }
}

//*************************Testimonial end controller*************************



//************************************Team******************************************* 

     public function team_view()

    {

    $data['team']=$this->admin_model->getTeam();

    //print_r($data); die;

    $this->load->view('admin/user/team/team_view',$data);

         //echo "ini am ini am in </h1>";

    }





   public function team_fetch_edit($id)

     {

        // echo $id; die;

        $data['team']=$this->admin_model->whereTeam($id);

         //$data['category']=$this->admin_model->getCategory();

        $this->load->view('admin/user/team/team_edit',$data);



     }



      public function team_add()

      {

                         // print_r($this->input->post());

          //**********************image upload*****************************



                $config['upload_path']          = './public/upload/team/';

                $config['allowed_types']        = '*';

                $config['max_size']             = 2048;

                $config['max_width']            = 3000;

                $config['max_height']           = 3000;



                $this->load->library('upload', $config);

              

                if ( ! $this->upload->do_upload('file_name'))

                {

                        $error = array('error' => $this->upload->display_errors());



                        $this->load->view('admin/user/team/team_add', $error);

                }

                else

                {

                        $data = array('upload_data' => $this->upload->data());

                        // print_r($data['upload_data']['file_name']); die;



                        $this->load->view('admin/user/team/team_add', $data);

               



          //**********************image upload*****************************



                  $data = array(

                                  'name' =>$this->input->post('name'),

                                  'heading' =>$this->input->post('heading'),

                                  'content' =>$this->input->post('content'),

                                 

                                'image' =>$data['upload_data']['file_name'],

                                'active' =>$this->input->post('active')

                                //'image' =>$this->input->post('image')

                               );





                  //print_r($data); die();  

                          $check=$this->admin_model->insertTeam($data);

                          if($check>0)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Added Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/team_view");

                            }

                          else

                          {

                            $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Added</p>');

                            // After that you need to used redirect function instead of load view such as 

                            redirect("admin/team_view");

                           }

         //**********************end image upload*****************************

                           

                }

         

    }



      public function team_edit($id)

        {                   



               if(is_uploaded_file($_FILES['file_name']['tmp_name'])) {



                             //**********************image upload*****************************



                $config['upload_path']          = './public/upload/team/';

                $config['allowed_types']        = 'gif|jpg|png';

                $config['max_size']             = 2048;

                $config['max_width']            = 3000;

                $config['max_height']           = 3000;



                $this->load->library('upload', $config);

              

                if ( ! $this->upload->do_upload('file_name'))

                {

                        $error = array('error' => $this->upload->display_errors());



                        $this->load->view('admin/user/team/team_edit', $error);

                }

                else

                {

                        $data = array('upload_data' => $this->upload->data());

                        // print_r($data['upload_data']['file_name']); die;



                        $this->load->view('admin/user/team/team_edit', $data);

               



          //**********************image upload*****************************



                

                             $data = array(

                                 'name' =>$this->input->post('name'),

                                  'heading' =>$this->input->post('heading'),

                                  'content' =>$this->input->post('content'),

                                 

                                'image' =>$data['upload_data']['file_name'],

                                'active' =>$this->input->post('active')

                                //'image' =>$this->input->post('image')

                               );



                          $check=$this->admin_model->updateTeam($id,$data);

                          if($check==1)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Updated Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/team_view"); 

                            }

                          else

                          {

                                $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Updated</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/team_view"); 

                          }

          }

        }else{





                             $data = array(

                                //'image' =>$data['upload_data']['file_name'],

                                'name' =>$this->input->post('name'),

                                  'heading' =>$this->input->post('heading'),

                                  'content' =>$this->input->post('content'),

                                 

                                 'active' =>$this->input->post('active')

                                //'image' =>$this->input->post('image')

                               );



                          $check=$this->admin_model->updateTeam($id,$data);

                          if($check==1)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Updated Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/team_view"); 

                            }

                          else

                          {

                                $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Updated</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/team_view"); 

                          }







        }

  }



      public function team_delete($id){



     $data=$this->admin_model->deleteTeam($id);

     if($data==true){

    

            $this->session->set_flashdata('action_message', '<span class="text-success">Subcategory deleted successfully</span>');

            redirect("admin/founder_view");

        } else {

            $this->session->set_flashdata('error_message', '<span class="text-danger">Please Try Again </span>');

            redirect("admin/founder_view");

        }



}

//*************************Team end controller*************************



//************************************Founder******************************************* 

     public function founder_view()

    {

    $data['founder']=$this->admin_model->getFounder();

    //print_r($data); die;

    $this->load->view('admin/user/founder/founder_view',$data);

         //echo "ini am ini am in </h1>";

    }





   public function founder_fetch_edit($id)

     {

        // echo $id; die;

        $data['founder']=$this->admin_model->whereFounder($id);

         //$data['category']=$this->admin_model->getCategory();

        $this->load->view('admin/user/founder/founder_edit',$data);



     }



      public function founder_add()

      {

                         // print_r($this->input->post());

          //**********************image upload*****************************



                $config['upload_path']          = './public/upload/';

                $config['allowed_types']        = '*';

                $config['max_size']             = 2048;

                $config['max_width']            = 3000;

                $config['max_height']           = 3000;



                $this->load->library('upload', $config);

              

                if ( ! $this->upload->do_upload('file_name'))

                {

                        $error = array('error' => $this->upload->display_errors());



                        $this->load->view('admin/user/founder/founder_add', $error);

                }

                else

                {

                        $data = array('upload_data' => $this->upload->data());

                        // print_r($data['upload_data']['file_name']); die;



                        $this->load->view('admin/user/founder/founder_add', $data);

               



          //**********************image upload*****************************



                  $data = array(

                                  'name' =>$this->input->post('name'),

                                  'heading' =>$this->input->post('heading'),

                                  'content' =>$this->input->post('content'),

                                 

                                'image' =>$data['upload_data']['file_name'],

                                'active' =>$this->input->post('active')

                                //'image' =>$this->input->post('image')

                               );





                  //print_r($data); die();  

                          $check=$this->admin_model->insertFounder($data);

                          if($check>0)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Added Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/founder_view");

                            }

                          else

                          {

                            $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Added</p>');

                            // After that you need to used redirect function instead of load view such as 

                            redirect("admin/founder_view");

                           }

         //**********************end image upload*****************************

                           

                }

         

    }



      public function founder_edit($id)

        {                   



               if(is_uploaded_file($_FILES['file_name']['tmp_name'])) {



                             //**********************image upload*****************************



                $config['upload_path']          = './public/upload/';

                $config['allowed_types']        = 'gif|jpg|png';

                $config['max_size']             = 2048;

                $config['max_width']            = 3000;

                $config['max_height']           = 3000;



                $this->load->library('upload', $config);

              

                if ( ! $this->upload->do_upload('file_name'))

                {

                        $error = array('error' => $this->upload->display_errors());



                        $this->load->view('admin/user/founder/founder_edit', $error);

                }

                else

                {

                        $data = array('upload_data' => $this->upload->data());

                        // print_r($data['upload_data']['file_name']); die;



                        $this->load->view('admin/user/founder/founder_edit', $data);

               



          //**********************image upload*****************************



                

                             $data = array(

                                 'name' =>$this->input->post('name'),

                                  'heading' =>$this->input->post('heading'),

                                  'content' =>$this->input->post('content'),

                                 

                                'image' =>$data['upload_data']['file_name'],

                                'active' =>$this->input->post('active')

                                //'image' =>$this->input->post('image')

                               );



                          $check=$this->admin_model->updateFounder($id,$data);

                          if($check==1)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Updated Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/founder_view"); 

                            }

                          else

                          {

                                $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Updated</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/founder_view"); 

                          }

          }

        }else{





                             $data = array(

                                //'image' =>$data['upload_data']['file_name'],

                                'name' =>$this->input->post('name'),

                                  'heading' =>$this->input->post('heading'),

                                  'content' =>$this->input->post('content'),

                                 

                                 'active' =>$this->input->post('active')

                                //'image' =>$this->input->post('image')

                               );



                          $check=$this->admin_model->updateFounder($id,$data);

                          if($check==1)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Updated Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/founder_view"); 

                            }

                          else

                          {

                                $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Updated</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/founder_view"); 

                          }







        }

  }



      public function founder_delete($id){



     $data=$this->admin_model->deleteFounder($id);

     if($data==true){

    

            $this->session->set_flashdata('action_message', '<span class="text-success">Subcategory deleted successfully</span>');

            redirect("admin/founder_view");

        } else {

            $this->session->set_flashdata('error_message', '<span class="text-danger">Please Try Again </span>');

            redirect("admin/founder_view");

        }



}

//*************************Founder end controller*************************





//***********************Website Config********************************** 

     public function config_view()

    {

    $data['config']=$this->admin_model->getConfig();

    //print_r($data); die;

   $this->load->view('admin/user/config/config_view',$data);

         //echo "ini am ini am in </h1>";

    }





   public function config_fetch_edit()

     {

        // echo $id; die;

        $data['config']=$this->admin_model->whereConfig('1');

         //$data['category']=$this->admin_model->getCategory();

        $this->load->view('admin/user/config/config_edit',$data);



     }



     

      public function config_edit($id)

        {                 





         

             if(is_uploaded_file($_FILES['file_name']['tmp_name'])) {

          //**********************image upload*****************************



                $config['upload_path']          = './public/upload/';

                $config['allowed_types']        = 'gif|jpg|png';

                $config['max_size']             = 2000;

                $config['max_width']            = 2000;

                $config['max_height']           = 2000;

                $this->load->library('upload', $config);

               print_r($this->upload->do_upload('file_name'));

               echo($this->upload->do_upload('file_name'));

              

                if ( ! $this->upload->do_upload('file_name'))

                {

                        $error = array('error' => $this->upload->display_errors());



                        $this->load->view('admin/user/config/config_edit', $error);

                }

                else

                {

                        $data = array('upload_data' => $this->upload->data());

                        // print_r($data['upload_data']['file_name']); die;



                        $this->load->view('admin/user/config/config_edit', $data);

               



          //**********************image upload*****************************

                

                             $data = array(

                                'logo' =>$data['upload_data']['file_name'],

                                'logo_name' =>$this->input->post('logo_name'),

                                'contact' =>$this->input->post('contact'),

                                'email' =>$this->input->post('email'),

                                'address' =>$this->input->post('address'),

                                'facebook' =>$this->input->post('facebook'),

                                'instagram' =>$this->input->post('instagram'),

                                'twitter' =>$this->input->post('twitter')

                                // 'link' =>$data['upload_data']['file_name'],

                               

                               );



                          $check=$this->admin_model->updateBrand($id,$data);

                          if($check==1)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Updated Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/config_view"); 

                            }

                          else

                          {

                                $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Updated</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/config_view"); 

                          }

          }



        }else{

                

                             $data = array(

                                //'logo' =>$data['upload_data']['file_name'],

                                'logo_name' =>$this->input->post('logo_name'),

                                'contact' =>$this->input->post('contact'),

                                'email' =>$this->input->post('email'),

                                'address' =>$this->input->post('address'),

                                'facebook' =>$this->input->post('facebook'),

                                'instagram' =>$this->input->post('instagram'),

                                'twitter' =>$this->input->post('twitter')

                                // 'link' =>$data['upload_data']['file_name'],

                               

                               );



                          $check=$this->admin_model->updateConfig($id,$data);

                          if($check==1)

                            {

                                // Set flash data 

                                $this->session->set_flashdata('action_message', '<p class="text-success">Data Updated Successfully</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/config_view"); 

                            }

                          else

                          {

                                $this->session->set_flashdata('error_message', '<p class="text-danger">Data not Updated</p>');

                                // After that you need to used redirect function instead of load view such as 

                                redirect("admin/config_view"); 

                          }

          }

  }



}
