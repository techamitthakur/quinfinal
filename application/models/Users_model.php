<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

    /**
     * Description Admin  controller.

     */
    public function __construct() {
        parent::__construct();
        $this->tableName = 'users';
        $this->primaryKey = 'id';
    }
//*****************************************************
     public function insertUsers($data)
    {
       
        $query=$this->db->insert("tbl_user",$data);
        $id = $this->db->insert_id();
            if ($id > 0) {
                return $id;
            } else {
                return false;
            }
    }

       
   public function getUsers()
    {
        
        $this->db->order_by("id","ASC");
        $query=$this->db->get("tbl_user");
        return $query->result();
    }
 public function getKey($key)
    {
        
        $this->db->where("key",$key);
        return $this->db->get("tbl_keys")->num_rows()>0;
        //return $query;
    }

     public function getlogin($email,$pass)
    {
        
        $this->db->where("status","active");
         $this->db->where("email",$email);
          $this->db->where("password",$pass);
        $query=$this->db->get("tbl_user");
        return $query->result();
    }

     public function checkPassword($email,$pass)
    {
        
        $this->db->where("status","active");
         $this->db->where("email",$email);
          $this->db->where("password",$pass);
        $query=$this->db->get("tbl_user");
       if ($query->result()==true) {
                return 1;
            } else {
                return 0;
            }
        
    }


    function changePassword($value1 = FALSE,$value2 = FALSE) {
        $this->db->where('email', $value2);
        if ($this->db->update('tbl_user', $value1)) {
            return 1;
        } else {
            return 0;
        }
    }

      public function updateUsers($email,$data)
    {
        //$this->db->where("active","active");
        $this->db->where("user_email",$email);
        $query=$this->db->update("tbl_user",$data);
        if($query==true){
        return 1;
        }
        else
        {
        return 0;
        }
    }

       public function updateProviders($email,$data)
    {
        $this->db->where("status","active");
        $this->db->where("pro_email",$email);
        $query=$this->db->update("tbl_provider",$data);
         
        if($query==true){
        return 1;
        }
        else
        {
        return 0;
        }
    }
    public function whereUsers($id)
    {
        // echo $id; die;
        //$this->db->where("status","active");
        $this->db->where("id",$id);
        $query=$this->db->get("tbl_user");
        return $query->result();
      
    } 

    public function emailExist($email,$type)
    {
        if($type=='user'){
        // echo $id; die;
        //$this->db->where("status","active");
        $this->db->where("user_email",$email);
        $query=$this->db->get("tbl_user");
         if ($query->result()==true) {
                return true;
            } else {
                return false;
            }
        
        }
         if($type=='provider'){
        // echo $id; die;
        //$this->db->where("status","active");
        $this->db->where("pro_email",$email);
        $query=$this->db->get("tbl_provider");
        if ($query->result()==true) {
                return true;
            } else {
                return false;
            }
        
        }
        
    } 

    public function activeUsers($email,$verify)
    {
        //$this->db->where("status","active");
        $this->db->where("email",$email);
        $this->db->where("verify",$verify);
        $query=$this->db->get("tbl_user");
       

       if ($query->result()==true) {
                return true;
            } else {
                return false;
            }
        
    }

  public function getWhere($data)
    {
        //$this->db->where("status","active");
        $this->db->where("email",$data);
        $query=$this->db->get("tbl_user");
        //print_r($query->num_rows()); die;

       if ($query->num_rows() > 0) {
            $checkQuery = 1;
        } else {
            $checkQuery = 0;
        }
         return $checkQuery;
    }


     public function deleteUsers($id)
    {
        //$this->db->where("active","active");
        $this->db->where('id', $id);
        $query=$this->db->delete('tbl_user');
        if($query==true){
        return true;
    }else{
        return false;
    }
    }


 public function insertPortfolio($data)
    {
       
        $query=$this->db->insert("portfolio",$data);
        $id = $this->db->insert_id();
            if ($id > 0) {
                return $id;
            } else {
                return false;
            }
    }

    public function insertContactus($data)
    {
       
        $query=$this->db->insert("contact",$data);
        $id = $this->db->insert_id();
            if ($id > 0) {
                return $id;
            } else {
                return false;
            }
    }

    public function insertEnquiry($data)
    {
       
        $query=$this->db->insert("enquiry",$data);
        $id = $this->db->insert_id();
            if ($id > 0) {
                return $id;
            } else {
                return false;
            }
    }
//*****************************************************

  /*
     * Insert / Update facebook profile data into the database
     * @param array the data for inserting into the table
     */
    public function checkUser($userData = array()){
        if(!empty($userData)){
            //check whether user data already exists in database with same oauth info
            $this->db->select($this->primaryKey);
            $this->db->from($this->tableName);
            $this->db->where(array('oauth_provider'=>$userData['oauth_provider'], 'oauth_uid'=>$userData['oauth_uid']));
            $prevQuery = $this->db->get();
            $prevCheck = $prevQuery->num_rows();
            
            if($prevCheck > 0){
                $prevResult = $prevQuery->row_array();
                
                //update user data
                $userData['modified'] = date("Y-m-d H:i:s");
                $update = $this->db->update($this->tableName, $userData, array('id' => $prevResult['id']));
                
                //get user ID
                $userID = $prevResult['id'];
            }else{
                //insert user data
                $userData['created']  = date("Y-m-d H:i:s");
                $userData['modified'] = date("Y-m-d H:i:s");
                $insert = $this->db->insert($this->tableName, $userData);
                
                //get user ID
                $userID = $this->db->insert_id();
            }
        }
        
        //return user ID
        return $userID?$userID:FALSE;
    }

 public function countSubCat($data)
    {
        //$this->db->where("status","active");
        $this->db->where("cat_id",$data);
        $query=$this->db->get("tbl_subcategory");
        //print_r($query->num_rows()); die;

       if ($query->num_rows() > 0) {
            $checkQuery = $query->num_rows();
        } else {
            $checkQuery = 0;
        }
         return $checkQuery;
    }
    
    
    //********caht model***********
       public function chatinsert($data)
        {
        //$this->db->where("status","active");
//           print_r($data); die;
      $query=$this->db->insert("tbl_chat",$data);
//           print_r($query); die;
        $id = $this->db->insert_id();
            if ($id > 0) {
                 $this->db->where("id",$id);
                 $query=$this->db->get("tbl_chat");
                return $query->result_array();
            } else {
                return false;
            }
        
        }
    
    public function getbyemail($data,$type)
    {
        if($type=='user'){
        $this->db->where("user_email",$data);
        $query=$this->db->get("tbl_user");
        if ($query->num_rows() > 0) {
            $checkQuery = $query->result();
        } else {
            $checkQuery = 0;
        }
         return $checkQuery;
        }
        
         if($type=='provider'){
        $this->db->where("pro_email",$data);
        $query=$this->db->get("tbl_provider");
        if ($query->num_rows() > 0) {
            $checkQuery = $query->result();
        } else {
            $checkQuery = 0;
        }
         return $checkQuery;
        }
    }
    
       public function getchatbyid($data)
    {
//        if($type=='user'){
        $this->db->where("senderid",$data);
        $query=$this->db->get("tbl_chat");
        if ($query->num_rows() > 0) {
            $checkQuery = $query->result();
        } else {
            $checkQuery = 0;
        }
         return $checkQuery;
//        }
        
//         if($type=='provider'){
//        $this->db->where("pro_email",$data);
//        $query=$this->db->get("tbl_chat");
//        if ($query->num_rows() > 0) {
//            $checkQuery = $query->result();
//        } else {
//            $checkQuery = 0;
//        }
//         return $checkQuery;
//        }
    }
//     public function logout() {
//         $email = $this->session->tbl_userdata('tbl_user_email');
//         $data = array(
//             'active' => 0
//         );
//         $this->db->update('tbl_user', $data, ['tbl_user_email' => $email]);
// //        echo $this->db->last_query(); die;
//         return $this->db->affected_rows();
//     }

//       public function getCategory()
//     {
        
//         $this->db->order_by("id","ASC");
//         $query=$this->db->get("category");
//         return $query->result();
//     }
//       public function updateCategory($id,$data)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->update("category",$data);
//         if($query==true){
//         return 1;
//         }
//         else
//         {
//         return 0;
//         }
//     }
//     public function whereCategory($id)
//     {
//         $this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->get("category");
//         return $query->result();
//     }

//      public function deleteCategory($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where('id', $id);
//         $query=$this->db->delete('category');
//         if($query==true){
//         return true;
//     }else{
//         return false;
//     }
//     }

// //*******************sub category*******************
//     public function insertSubCategory($data)
//     {
       
//         $query=$this->db->insert("sub_category",$data);
//         $id = $this->db->insert_id();
//             if ($id > 0) {
//                 return $id;
//             } else {
//                 return false;
//             }
//     }

//    public function getSubCategory($where='')
//     {       
//         if($where==''){
         
//         $this->db->order_by("id","DESC");
//         $query=$this->db->get("sub_category");
//         return $query->result();
//         }else{

//              $this->db->order_by("id","DESC");
//        $this->db->where("cat_id",$where);
//         $query=$this->db->get("sub_category");
//         return $query->result();


//         }
//     }

//       public function whereSubCategory($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->get("sub_category");
//         return $query->result();
//     }

//       public function updateSubCategory($id,$data)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->update("sub_category",$data);
//         if($query==true){
//         return 1;
//         }
//         else
//         {
//         return 0;
//         }
//     }

//     public function deleteSubCategory($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where('id', $id);
//         $query=$this->db->delete('sub_category');
//         if($query==true){
//         return true;
//     }else{
//         return false;
//     }
//     }
//     //*************************sub category***************************

   
//     //*******************About*******************
//     public function insertAbout($data)
//     {
       
//         $query=$this->db->insert("about",$data);
//         $id = $this->db->insert_id();
//             if ($id > 0) {
//                 return $id;
//             } else {
//                 return false;
//             }
//     }

//    public function getAbout()
//     {
//          $this->db->where("active","active");
//         $this->db->order_by("id","ASC");
//         $query=$this->db->get("about");
//         return $query->result();
//     }

//       public function whereAbout($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->get("about");
//         return $query->result();
//     }

//       public function updateAbout($id,$data)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->update("about",$data);
//         if($query==true){
//         return 1;
//         }
//         else
//         {
//         return 0;
//         }
//     }

//     public function deleteAbout($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where('id', $id);
//         $query=$this->db->delete('about');
//         if($query==true){
//         return true;
//     }else{
//         return false;
//     }
//     }
//     //*************************About***************************

//       //*******************Services*******************
//     public function insertServices($data)
//     {
       
//         $query=$this->db->insert("services",$data);
//         $id = $this->db->insert_id();
//             if ($id > 0) {
//                 return $id;
//             } else {
//                 return false;
//             }
//     }

//    public function getServices()
//     {
//          $this->db->where("active","active");
//         $this->db->order_by("id","DESC");
//         $query=$this->db->get("services");
//         return $query->result();
//     }

//       public function whereServices($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->get("services");
//         return $query->result();
//     }

//       public function updateServices($id,$data)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->update("services",$data);
//         if($query==true){
//         return 1;
//         }
//         else
//         {
//         return 0;
//         }
//     }

//     public function deleteServices($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where('id', $id);
//         $query=$this->db->delete('services');
//         if($query==true){
//         return true;
//     }else{
//         return false;
//     }
//     }
//     //*************************Services***************************

//      //*******************Treatment*******************
//     public function insertTreatment($data)
//     {
       
//         $query=$this->db->insert("treatment",$data);
//         $id = $this->db->insert_id();
//             if ($id > 0) {
//                 return $id;
//             } else {
//                 return false;
//             }
//     }

//    public function getTreatment()
//     {
//          $this->db->where("active","active");
//         $this->db->order_by("id","DESC");
//         $query=$this->db->get("treatment");
//         return $query->result();
//     }

//       public function whereTreatment($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->get("treatment");
//         return $query->result();
//     }
//         public function getwhereTreatment()
//     {
//        $this->db->group_by("sub_heading");
//         //$this->db->where("sub_heading",$data);
//         $query=$this->db->get("treatment");
//         return $query->result();
//     }

//       public function updateTreatment($id,$data)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->update("treatment",$data);
//         if($query==true){
//         return 1;
//         }
//         else
//         {
//         return 0;
//         }
//     }

//     public function deleteTreatment($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where('id', $id);
//         $query=$this->db->delete('treatment');
//         if($query==true){
//         return true;
//     }else{
//         return false;
//     }
//     }
//     //*************************Treatment***************************

//   //*******************Product*******************
//     public function insertProduct($data)
//     {
       
//         $query=$this->db->insert("product",$data);
//         $id = $this->db->insert_id();
//             if ($id > 0) {
//                 return $id;
//             } else {
//                 return false;
//             }
//     }

//    public function getProduct()
//     {
//          $this->db->where("active","active");
//         $this->db->order_by("id","DESC");
//         $query=$this->db->get("product");
//         return $query->result();
//     }

//       public function whereProduct($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->get("product");
//         return $query->result();
//     }

//       public function updateProduct($id,$data)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->update("product",$data);
//         if($query==true){
//         return 1;
//         }
//         else
//         {
//         return 0;
//         }
//     }

//     public function deleteProduct($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where('id', $id);
//         $query=$this->db->delete('product');
//         if($query==true){
//         return true;
//     }else{
//         return false;
//     }
//     }
//     //*************************end Product***************************

//     //*************************Slider*******************************
//     public function insertSlider($data)
//     {
       
//         $query=$this->db->insert("slider",$data);
//         $id = $this->db->insert_id();
//             if ($id > 0) {
//                 return $id;
//             } else {
//                 return false;
//             }
//     }

//    public function getSlider()
//     {
//          $this->db->where("active","active");
//         $this->db->order_by("id","DESC");
//         $query=$this->db->get("slider");
//         return $query->result();
//     }

//       public function whereSlider($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->get("slider");
//         return $query->result();
//     }

//       public function updateSlider($id,$data)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->update("slider",$data);
//         if($query==true){
//         return 1;
//         }
//         else
//         {
//         return 0;
//         }
//     }

//     public function deleteSlider($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where('id', $id);
//         $query=$this->db->delete('slider');
//         if($query==true){
//         return true;
//     }else{
//         return false;
//     }
//     }


//     //*************************end Slider***************************



//     //*************************Brand*******************************
//     public function insertBrand($data)
//     {
       
//         $query=$this->db->insert("brand",$data);
//         $id = $this->db->insert_id();
//             if ($id > 0) {
//                 return $id;
//             } else {
//                 return false;
//             }
//     }

//    public function getBrand()
//     {
//          $this->db->where("active","active");
//         $this->db->order_by("id","DESC");
//         $query=$this->db->get("brand");
//         return $query->result();
//     }

//       public function whereBrand($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->get("brand");
//         return $query->result();
//     }

//       public function updateBrand($id,$data)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->update("brand",$data);
//         if($query==true){
//         return 1;
//         }
//         else
//         {
//         return 0;
//         }
//     }

//     public function deleteBrand($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where('id', $id);
//         $query=$this->db->delete('brand');
//         if($query==true){
//         return true;
//     }else{
//         return false;
//     }
//     }


//     //*************************end Brand***************************

//      //*************************testimonial*******************************
//     public function insertTestimonial($data)
//     {
       
//         $query=$this->db->insert("testimonial",$data);
//         $id = $this->db->insert_id();
//             if ($id > 0) {
//                 return $id;
//             } else {
//                 return false;
//             }
//     }

//    public function getTestimonial()
//     {
//          $this->db->where("active","active");
//         $this->db->order_by("id","DESC");
//         $query=$this->db->get("testimonial");
//         return $query->result();
//     }

//       public function whereTestimonial($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->get("testimonial");
//         return $query->result();
//     }

//       public function updateTestimonial($id,$data)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->update("testimonial",$data);
//         if($query==true){
//         return 1;
//         }
//         else
//         {
//         return 0;
//         }
//     }

//     public function deleteTestimonial($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where('id', $id);
//         $query=$this->db->delete('testimonial');
//         if($query==true){
//         return true;
//     }else{
//         return false;
//     }
//     }


//     //*************************end testimonial***************************

//      //*************************Team*******************************
//     public function insertTeam($data)
//     {
       
//         $query=$this->db->insert("team",$data);
//         $id = $this->db->insert_id();
//             if ($id > 0) {
//                 return $id;
//             } else {
//                 return false;
//             }
//     }

//    public function getTeam()
//     {
//          $this->db->where("active","active");
//         $this->db->order_by("id","DESC");
//         $query=$this->db->get("team");
//         return $query->result();
//     }

//       public function whereTeam($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->get("team");
//         return $query->result();
//     }

//       public function updateTeam($id,$data)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->update("team",$data);
//         if($query==true){
//         return 1;
//         }
//         else
//         {
//         return 0;
//         }
//     }

//     public function deleteTeam($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where('id', $id);
//         $query=$this->db->delete('team');
//         if($query==true){
//         return true;
//     }else{
//         return false;
//     }
//     }


//     //*************************end team***************************


//      //*************************Team*******************************
//     public function insertFounder($data)
//     {
       
//         $query=$this->db->insert("founder",$data);
//         $id = $this->db->insert_id();
//             if ($id > 0) {
//                 return $id;
//             } else {
//                 return false;
//             }
//     }

//    public function getFounder()
//     {
//          $this->db->where("active","active");
//         $this->db->order_by("id","DESC");
//         $query=$this->db->get("founder");
//         return $query->result();
//     }

//       public function whereFounder($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->get("founder");
//         return $query->result();
//     }

//       public function updateFounder($id,$data)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->update("founder",$data);
//         if($query==true){
//         return 1;
//         }
//         else
//         {
//         return 0;
//         }
//     }

//     public function deleteFounder($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where('id', $id);
//         $query=$this->db->delete('founder');
//         if($query==true){
//         return true;
//     }else{
//         return false;
//     }
//     }


//     //*************************end team***************************

//       //*************************Website config*******************************
//     // public function insertConfig($data)
//     // {
       
//     //     $query=$this->db->insert("config",$data);
//     //     $id = $this->db->insert_id();
//     //         if ($id > 0) {
//     //             return $id;
//     //         } else {
//     //             return false;
//     //         }
//     // }

//    public function getConfig()
//     {
         
//         $this->db->order_by("id","DESC");
//         $query=$this->db->get("config");
//         return $query->result();
//     }

//       public function whereConfig($id)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->get("config");
//         return $query->result();
//     }

//       public function updateConfig($id,$data)
//     {
//         //$this->db->where("active","active");
//         $this->db->where("id",$id);
//         $query=$this->db->update("config",$data);
//         if($query==true){
//         return 1;
//         }
//         else
//         {
//         return 0;
//         }
//     }

//     // public function deleteConfig($id)
//     // {
//     //     //$this->db->where("active","active");
//     //     $this->db->where('id', $id);
//     //     $query=$this->db->delete('config');
//     //     if($query==true){
//     //     return true;
//     // }else{
//     //     return false;
//     // }
//     // }


//     //*************************end Brand***************************

// //////*****************front********************************

// public function getProductWhere($name)
//     {
         
//         $this->db->order_by("id","DESC");
//         $this->db->where("product_offer",$name);
//         $this->db->or_where("product_type",$name);
//         $query=$this->db->get("product");
//         return $query->result();
//     }
//     public function filterProduct($filter)
//     {
//         $this->db->order_by($filter,"DESC");
//         $query=$this->db->get("product");
//         return $query->result();
//     }

//      function search($keyword)
//     {
//         $this->db->like('product_name',$keyword);
       
//         $query  =   $this->db->get('product');
//         return $query->result();
//     }
//////*****************front********************************
public function doServey($data){
    // echo $this->input->post('services');die;
    // $beauty=$food=$others=$fitness=$tailoring=$teaching=$office=$medical="";
    // if(!empty($this- ){


    // if($this->input->post('beauty')===null){
    //     $beauty='null';
    // }else{
    //      $beauty = implode(",", $this->input->post('beauty'));
   

    // }
    //  if($this->input->post('food')===null){
    //     $food='null';
    // }
    // else{
    //     $food = implode(",", $this->input->post('food'));
        
    // }
    //  if($this->input->post('medical')===null){
    //     $medical='null';
    // }else{
    //     $medical = implode(",", $this->input->post('medical'));
        
    // }
    //  if($this->input->post('tailoring')===null){
    //     $tailoring='null';
    // }else{
    //     $tailoring = implode(",", $this->input->post('tailoring'));
        
    // }
    //  if($this->input->post('teaching')===null){
    //     $teaching='null';
    // }else{
    //     $teaching = implode(",", $this->input->post('teaching'));
        
    // }
    //  if($this->input->post('office')===null){
    //     $office='null';
    // }else{
    //     $office = implode(",", $this->input->post('office'));
        
    // }
    //  if($this->input->post('fitness')===null){
    //     $fitness='null';
    // }else{
    //    $fitness = implode(",", $this->input->post('fitness'));
        
    // }
    //  if($this->input->post('others')===null){
    //     $others='null';
    // }else{
    //     $others = implode(",", $this->input->post('others'));
        
    // }

       
    
    // print_r($services);die;
    
    $this->db->insert('tbl_survey', $data);
    return $this->db->insert_id();
}

public function getInTouch($data){

    $this->db->insert('tbl_survey', $data);
    return $this->db->insert_id();

}

}

