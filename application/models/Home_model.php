<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {

    /**
     * Description Admin  controller.

     */
    public function __construct() {
        parent::__construct();

    }

   
    public function getCategory($value='')
    {   $this->db->order_by("id","ASC");
        $this->db->where("active","active");
  
        $query=$this->db->get("category");
        return $query->result_array();
    }



 public function insertSignup($data)
    {
        
        $query=$this->db->insert("useraccount",$data);
        $id=$this->db->insert_id();
                      
        if($id>0){

                  return true;
            } else {
                return false;
            }
        
    }

 public function getLogin($email,$password)
    {
        $this->db->where("email",$email);
        $this->db->where("password",$password);

        $query=$this->db->get("useraccount");
        $num = $query->num_rows();
        if($num>0){

        return 1;
    }else{

        return 0;
    }
    }

    public function getUser($email)
    {
        $this->db->where("email",$email);
        $query=$this->db->get("useraccount");
        
        if($query==true){

        return $query->result();
    }else{

        return false;
    }
    }

















    // public function checkLogin() {
    //     $data = array(
    //         'email' => $this->security->xss_clean($this->input->post('email')),
    //         'password' => $this->security->xss_clean(hash('sha256', $this->input->post('password')))
    //     );
    //     $query = $this->db->get_where('users', $data);
    //     $row = $query->row_array();

    //     if ($row) {
    //         $count = $row['numlogins'] + 1;
    //         $date = date('Y/m/d');
    //         date_default_timezone_set("Asia/Kolkata");
    //         $time = date("h:i:sa");
    //         $data1 = array(
    //             'last_login' => $date . " " . $time,
    //             'active' => 1,
    //             'numlogins' => $count
    //         );
    //         $this->db->update('users', $data1, ['id' => $row['id']]);
    //         $this->db->affected_rows();
    //         return $row;
    //     }
    // }

//     public function logout() {
//         $email = $this->session->userdata('email');
//         $data = array(
//             'active' => 0
//         );
//         $this->db->update('users', $data, ['email' => $email]);
// //        echo $this->db->last_query(); die;
//         return $this->db->affected_rows();
//     }

//     public function getAccessLevel() {
//         $this->db->order_by('access_id', 'desc');
//         $query = $this->db->get('access_level');
//         return $query->result_array();
//     }

//     public function getAccessLevelById($id) {
//         $query = $this->db->get_where('access_level', ['access_id' => $id]);
//         return $query->row_array();
//     }

//     public function doAccessLevel() {
//         $data = array(
//             'access_name' => $this->security->xss_clean($this->input->post('access_name'))
//         );
//         $this->db->insert('access_level', $data);
//         return $this->db->insert_id();
//     }

//     public function doeditAccessLevel($id) {
//         $data = array(
//             'access_name' => $this->security->xss_clean($this->input->post('access_name'))
//         );
//         $this->db->update('access_level', $data, ['access_id' => $id]);
//         return $this->db->affected_rows();
//     }

//     public function deleteAccessLevel($id) {
//         $this->db->delete('access_level', ['access_id' => $id]);
//         return $this->db->affected_rows();
//     }

//     public function doAdduser() {
//         $date = date('Y/m/d');
//         date_default_timezone_set("Asia/Kolkata");
//         $time = date("h:i:sa");
//         $data = array(
//             'first_name' => $this->security->xss_clean($this->input->post('first_name')),
//             'last_name' => $this->security->xss_clean($this->input->post('last_name')),
//             'password' => $this->security->xss_clean(hash('sha256', $this->input->post('password'))),
//             'email' => $this->security->xss_clean($this->input->post('email')),
//             'created_on' => $date . " " . $time,
//             'phone' => $this->security->xss_clean($this->input->post('phone')),
//             'access_level' => $this->security->xss_clean($this->input->post('group'))
//         );
//         $this->db->insert('users', $data);
//         return $this->db->insert_id();
//     }

//     public function getUsers() {
//         $this->db->order_by("id", "desc");
//         $this->db->where("deleted", '0');
//         $query = $this->db->get("users");
//         return $query->result_array();
//     }

//     public function getUserById($id) {
//         $query = $this->db->get_where('users', ['id' => $id]);
//         return $query->row_array();
//     }

//     public function doEditUser($id) {
//         $data = array(
//             'first_name' => $this->security->xss_clean($this->input->post('first_name')),
//             'last_name' => $this->security->xss_clean($this->input->post('last_name')),
//             'email' => $this->security->xss_clean($this->input->post('email')),
//             'phone' => $this->security->xss_clean($this->input->post('phone')),
//             'access_level' => $this->security->xss_clean($this->input->post('group'))
//         );
//         $this->db->update('users', $data, ['id' => $id]);
//         return $this->db->affected_rows();
//     }

//     public function deleteUser($id) {
//         $this->db->update('users', ['deleted' => 1], ['id' => $id]);
//         return $this->db->affected_rows();
//     }

//     public function getUserGroups() {
// //        $this->db->from('users_membergroups um');
// //        $this->db->join('membergroups m', 'm.id = um.membergroup_id');
// //        $this->db->join('users u', 'u.id=um.user_id');
//         $query = $this->db->get('membergroups');
//         return $query->result_array();
//     }

//     public function totalUserGroups() {
//         $query = $this->db->get('users_membergroups');
//         return $query->result_array();
//     }

//     public function doAdduserGroups() {
//         $data = array(
//             'name' => $this->security->xss_clean($this->input->post('name')),
//             'description' => $this->security->xss_clean($this->input->post('description'))
//         );
//         $this->db->insert('membergroups', $data);
//         return $this->db->insert_id();
//     }

//     public function getUserGroupById($id) {
//         $query = $this->db->get_where('membergroups', ['id' => $id]);
//         return $query->row_array();
//     }

//     public function doEditUserGroup($id) {
//         $data = array(
//             'name' => $this->security->xss_clean($this->input->post('name')),
//             'description' => $this->security->xss_clean($this->input->post('description'))
//         );
//         $this->db->update('membergroups', $data, ['id' => $id]);
//         return $this->db->affected_rows();
//     }

//     public function deleteUserGroup($id) {
//         $this->db->delete('membergroups', ['id' => $id]);
//         $this->db->delete('users_membergroups', ['membergroup_id' => $id]);
//         return $this->db->affected_rows();
//     }

//     public function getGroupMember($id) {
//         $query = $this->db->get_where("membergroups", ['id' => $id]);
//         return $query->row_array();
//     }

//     public function getGroupUsersMember($id) {
//         $this->db->select('u.id,u.first_name,u.last_name');
//         $this->db->from('users_membergroups um');
//         $this->db->join('users u', 'u.id=um.user_id');
//         $this->db->where('membergroup_id', $id);
//         $query = $this->db->get();
//         return $query->result_array();
//     }

//     public function getGroupUserUnmember($id) {
//         $query = $this->db
//                 ->select('u.id, u.email, u.first_name, u.last_name')
//                 ->from('users u')
//                 ->where('u.id NOT IN (SELECT user_id FROM users_membergroups WHERE membergroup_id=' . $id . ')')
//                 ->where('u.deleted', 0)
//                 ->get();
//         return $query->result_array();
//     }

//     public function doAddMember($id) {
//         if ($this->input->post('member')) {
//             $member = implode(',', $this->input->post('member'));
//             $members = explode(',', $member);

//             foreach ($members as $member) {
//                 $query = $this->db->get_where('users_membergroups', ['membergroup_id' => $id, 'user_id' => $member]);
//                 $row = $query->row_array();
//                 if (!$row) {
//                     $this->db->insert("users_membergroups", ['membergroup_id' => $id, 'user_id' => $member]);
//                     $this->db->insert_id();
//                 }
//             }
//         }
//         if ($this->input->post('unmember')) {
//             $unmember = implode(',', $this->input->post('unmember'));
//             $unmembers = explode(',', $unmember);
//             foreach ($unmembers as $unmember) {
//                 $query = $this->db->get_where('users_membergroups', ['membergroup_id' => $id, 'user_id' => $unmember]);
//                 $row = $query->row_array();
//                 if ($row) {
//                     $this->db->delete("users_membergroups", ['membergroup_id' => $id, 'user_id' => $unmember]);
//                     $this->db->affected_rows();
//                 }
//             }
//         }
//         return true;
//     }

}
