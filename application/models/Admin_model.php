<?php



defined('BASEPATH') OR exit('No direct script access allowed');



class Admin_model extends CI_Model {

    /**
     * Description Admin  controller.
     */

    public function __construct() {
        parent::__construct();
        
    }

    public function checkLogin() {
        $data = array(
            'admin_email' => $this->security->xss_clean($this->input->post('admin_email')),
            'admin_password' => $this->security->xss_clean(hash('sha256', $this->input->post('admin_password')))
        );
        $query = $this->db->get_where('tbl_admin_user', $data);
        $row = $query->row_array();
        if ($row) {
            $count = $row['numlogins'] + 1;
            $date = date('Y/m/d');
            date_default_timezone_set("Asia/Kolkata");
            $time = date("h:i:sa");
            $data1 = array(
                'last_login' => $date . " " . $time,
                'active' => 1,
                'numlogins' => $count
            );
            $this->db->update('tbl_admin_user', $data1, ['id' => $row['id']]);
            $this->db->affected_rows();
            return $row;
        }
    }

  public function insertUser($data)
    {
        $query=$this->db->insert("tbl_user",$data);
        $id = $this->db->insert_id();
            if ($id > 0) {
                return $id;
            } else {
                return 0;
            }
    }

 public function getLoginUser($email,$password)
    {   
         $this->db->where("status","active");
        $this->db->where("user_email",$email);
        $this->db->where("user_password",$password);
        $query=$this->db->get("tbl_user");
        $num = $query->num_rows();
        if($num>0){
        return 1;
            }else{
                return 0;
            }
     }

    public function getUser($email)
    {    
        // $this->db->where("status","active");
        $this->db->where("user_email",$email);
        $query=$this->db->get("tbl_user");      
        if($query==true){
        return $query->result();
        }else{
        return false;
        }
    }

 public function getUserById($id)
    {   
        // $this->db->where("status","active");
        $this->db->where("id",$id);
        $query=$this->db->get("tbl_user");
        if($query==true){
        return $query->result();
    }else{
        return false;
    }
    }

 public function insertProvider($data)
    {
        $query=$this->db->insert("tbl_provider",$data);
        $id = $this->db->insert_id();
            if ($id > 0) {
                return $id;
            } else {
                return false;
            }
    }

 public function getLoginProvider($email,$password)

    {    $this->db->where("status","active");
        $this->db->where("pro_email",$email);
        $this->db->where("pro_password",$password);
        $query=$this->db->get("tbl_provider");
        $num = $query->num_rows();
        if($num>0){
        return 1;
    }else{
        return 0;
    }
    }

    public function getProvider($email)
    {   
        // $this->db->where("status","active");
        $this->db->where("pro_email",$email);
        $query=$this->db->get("tbl_provider");
        if($query==true){
        return $query->result();
    }else{
        return false;
    }
    }

    public function getProviderById($id)

    {   

        // $this->db->where("status","active");

        $this->db->where("id",$id);

        $query=$this->db->get("tbl_provider");

        

        if($query==true){



        return $query->result();

    }else{



        return false;

    }

    }


   public function userOtpVerify($id,$data)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->update("tbl_user",$data);

        if($query==true){

        return 1;

        }

        else

        {

        return 0;

        }

    }

  public function providerOtpVerify($id,$data)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->update("tbl_provider",$data);

        if($query==true){

        return 1;

        }

        else

        {

        return 0;

        }

    }

   public function updateUserVerify($email,$data)

    {

        //$this->db->where("active","active");

        $this->db->where("user_email",$email);

        $query=$this->db->update("tbl_user",$data);

        if($query==true){

        return 1;

        }

        else

        {

        return 0;

        }

    }





 public function updateProviderVerify($email,$data)

    {

        //$this->db->where("active","active");

        $this->db->where("pro_email",$email);

        $query=$this->db->update("tbl_provider",$data);

        if($query==true){

        return 1;

        }

        else

        {

        return 0;

        }

    }



    public function logout() {

        $email = $this->session->userdata('admin_email');

        $data = array(

            'active' => 0

        );

        $this->db->update('tbl_admin_user', $data, ['admin_email' => $email]);

//        echo $this->db->last_query(); die;

        return $this->db->affected_rows();

    }


public function insertCategory($data)

    {

       

        $query=$this->db->insert("tbl_category",$data);

        $id = $this->db->insert_id();

            if ($id > 0) {

                return $id;

            } else {

                return false;

            }

    }

      public function getCategory()

    {

        

        $this->db->order_by("id","ASC");

        $query=$this->db->get("tbl_category");

        return $query->result();

    }

      public function updateCategory($id,$data)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->update("tbl_category",$data);

        if($query==true){

        return 1;

        }

        else

        {

        return 0;

        }

    }

    public function whereCategory($id)

    {

        $this->db->where("status","active");

        $this->db->where("id",$id);

        $query=$this->db->get("tbl_category");

        return $query->result();

    }



     public function deleteCategory($id)

    {

        //$this->db->where("active","active");

        $this->db->where('id', $id);

        $query=$this->db->delete('tbl_category');

        if($query==true){

        return true;

    }else{

        return false;

    }

    }



//*******************sub category*******************

    public function insertSubCategory($data)

    {
  $query=$this->db->insert("tbl_subcategory",$data);
        $id = $this->db->insert_id();
            if ($id > 0) {
                return $id;
            } else {
                return false;
            }
    }

   public function getSubCategory($where='')
    {       
        if($where==''){
        $this->db->order_by("id","DESC");
        $query=$this->db->get("tbl_subcategory");
        return $query->result();
        }else{
             $this->db->order_by("id","DESC");
       $this->db->where("cat_id",$where);
        $query=$this->db->get("tbl_subcategory");
        return $query->result();
        }
    }



      public function whereSubCategory($id)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->get("tbl_subcategory");

        return $query->result();

    }



      public function updateSubCategory($id,$data)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->update("tbl_subcategory",$data);

        if($query==true){

        return 1;

        }

        else

        {

        return 0;

        }

    }



    public function deleteSubCategory($id)

    {

        //$this->db->where("active","active");

        $this->db->where('id', $id);

        $query=$this->db->delete('tbl_subcategory');

        if($query==true){

        return true;

    }else{

        return false;

    }

    }

    //*************************sub category***************************



   

    //*******************About*******************

    public function insertAbout($data)

    {

       

        $query=$this->db->insert("tbl_about",$data);

        $id = $this->db->insert_id();

            if ($id > 0) {

                return $id;

            } else {

                return false;

            }

    }



   public function getAbout()

    {

         $this->db->where("active","active");

        $this->db->order_by("id","ASC");

        $query=$this->db->get("tbl_about");

        return $query->result();

    }



      public function whereAbout($id)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->get("tbl_about");

        return $query->result();

    }



      public function updateAbout($id,$data)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->update("tbl_about",$data);

        if($query==true){

        return 1;

        }

        else

        {

        return 0;

        }

    }



    public function deleteAbout($id)

    {

        //$this->db->where("active","active");

        $this->db->where('id', $id);

        $query=$this->db->delete('tbl_about');

        if($query==true){

        return true;

    }else{

        return false;

    }

    }

    //*************************About***************************



      //*******************Services*******************

    public function insertServices($data)

    {

       

        $query=$this->db->insert("tbl_services",$data);

        $id = $this->db->insert_id();

            if ($id > 0) {

                return $id;

            } else {

                return false;

            }

    }



   public function getServices()

    {

         $this->db->where("active","active");

        $this->db->order_by("id","DESC");

        $query=$this->db->get("tbl_services");

        return $query->result();

    }



      public function whereServices($id)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->get("tbl_services");

        return $query->result();

    }



      public function updateServices($id,$data)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->update("tbl_services",$data);

        if($query==true){

        return 1;

        }

        else

        {

        return 0;

        }

    }



    public function deleteServices($id)

    {

        //$this->db->where("active","active");

        $this->db->where('id', $id);

        $query=$this->db->delete('tbl_services');

        if($query==true){

        return true;

    }else{

        return false;

    }

    }

    //*************************Services***************************



     //*******************Treatment*******************

    public function insertTreatment($data)

    {

       

        $query=$this->db->insert("treatment",$data);

        $id = $this->db->insert_id();

            if ($id > 0) {

                return $id;

            } else {

                return false;

            }

    }



   public function getTreatment()

    {

         $this->db->where("active","active");

        $this->db->order_by("id","DESC");

        $query=$this->db->get("treatment");

        return $query->result();

    }



      public function whereTreatment($id)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->get("treatment");

        return $query->result();

    }

        public function getwhereTreatment()

    {

       $this->db->group_by("sub_heading");

        //$this->db->where("sub_heading",$data);

        $query=$this->db->get("treatment");

        return $query->result();

    }



      public function updateTreatment($id,$data)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->update("treatment",$data);

        if($query==true){

        return 1;

        }

        else

        {

        return 0;

        }

    }



    public function deleteTreatment($id)

    {

        //$this->db->where("active","active");

        $this->db->where('id', $id);

        $query=$this->db->delete('treatment');

        if($query==true){

        return true;

    }else{

        return false;

    }

    }

    //*************************Treatment***************************



  //*******************Product*******************

    public function insertProduct($data)

    {

       

        $query=$this->db->insert("product",$data);

        $id = $this->db->insert_id();

            if ($id > 0) {

                return $id;

            } else {

                return false;

            }

    }



   public function getProduct()

    {

         $this->db->where("active","active");

        $this->db->order_by("id","DESC");

        $query=$this->db->get("product");

        return $query->result();

    }



      public function whereProduct($id)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->get("product");

        return $query->result();

    }



      public function updateProduct($id,$data)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->update("product",$data);

        if($query==true){

        return 1;

        }

        else

        {

        return 0;

        }

    }



    public function deleteProduct($id)

    {

        //$this->db->where("active","active");

        $this->db->where('id', $id);

        $query=$this->db->delete('product');

        if($query==true){

        return true;

    }else{

        return false;

    }

    }

    //*************************end Product***************************



    //*************************Slider*******************************

    public function insertSlider($data)

    {

       

        $query=$this->db->insert("slider",$data);

        $id = $this->db->insert_id();

            if ($id > 0) {

                return $id;

            } else {

                return false;

            }

    }



   public function getSlider()

    {

         $this->db->where("active","active");

        $this->db->order_by("id","DESC");

        $query=$this->db->get("slider");

        return $query->result();

    }



      public function whereSlider($id)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->get("slider");

        return $query->result();

    }



      public function updateSlider($id,$data)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->update("slider",$data);

        if($query==true){

        return 1;

        }

        else

        {

        return 0;

        }

    }



    public function deleteSlider($id)

    {

        //$this->db->where("active","active");

        $this->db->where('id', $id);

        $query=$this->db->delete('slider');

        if($query==true){

        return true;

    }else{

        return false;

    }

    }





    //*************************end Slider***************************







    //*************************Brand*******************************

    public function insertBrand($data)

    {

       

        $query=$this->db->insert("brand",$data);

        $id = $this->db->insert_id();

            if ($id > 0) {

                return $id;

            } else {

                return false;

            }

    }



   public function getBrand()

    {

         $this->db->where("active","active");

        $this->db->order_by("id","DESC");

        $query=$this->db->get("brand");

        return $query->result();

    }



      public function whereBrand($id)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->get("brand");

        return $query->result();

    }



      public function updateBrand($id,$data)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->update("brand",$data);

        if($query==true){

        return 1;

        }

        else

        {

        return 0;

        }

    }



    public function deleteBrand($id)

    {

        //$this->db->where("active","active");

        $this->db->where('id', $id);

        $query=$this->db->delete('brand');

        if($query==true){

        return true;

    }else{

        return false;

    }

    }





    //*************************end Brand***************************



     //*************************testimonial*******************************

    public function insertTestimonial($data)

    {

       

        $query=$this->db->insert("tbl_testimonial",$data);

        $id = $this->db->insert_id();

            if ($id > 0) {

                return $id;

            } else {

                return false;

            }

    }



   public function getTestimonial()

    {

         $this->db->where("status","active");

        $this->db->order_by("id","DESC");

        $query=$this->db->get("tbl_testimonial");

        return $query->result();

    }



      public function whereTestimonial($id)

    { //$this->db->where("active","active");
        $this->db->where("id",$id);
        $query=$this->db->get("tbl_testimonial");
        return $query->result();
    }



      public function updateTestimonial($id,$data)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->update("tbl_testimonial",$data);

        if($query==true){

        return 1;

        }

        else

        {

        return 0;

        }

    }



    public function deleteTestimonial($id)

    {

        //$this->db->where("active","active");

        $this->db->where('id', $id);

        $query=$this->db->delete('tbl_testimonial');

        if($query==true){

        return true;

    }else{

        return false;

    }

    }





    //*************************end testimonial***************************



     //*************************Team*******************************

    public function insertTeam($data)

    {

       

        $query=$this->db->insert("team",$data);

        $id = $this->db->insert_id();

            if ($id > 0) {

                return $id;

            } else {

                return false;

            }

    }



   public function getTeam()

    {

         $this->db->where("active","active");

        $this->db->order_by("id","DESC");

        $query=$this->db->get("team");

        return $query->result();

    }



      public function whereTeam($id)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->get("team");

        return $query->result();

    }



      public function updateTeam($id,$data)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->update("team",$data);

        if($query==true){

        return 1;

        }

        else

        {

        return 0;

        }

    }



    public function deleteTeam($id)

    {

        //$this->db->where("active","active");

        $this->db->where('id', $id);

        $query=$this->db->delete('team');

        if($query==true){

        return true;

    }else{

        return false;

    }

    }





    //*************************end team***************************





     //*************************Team*******************************

    public function insertFounder($data)

    {

       

        $query=$this->db->insert("founder",$data);

        $id = $this->db->insert_id();

            if ($id > 0) {

                return $id;

            } else {

                return false;

            }

    }



   public function getFounder()

    {

         $this->db->where("active","active");

        $this->db->order_by("id","DESC");

        $query=$this->db->get("founder");

        return $query->result();

    }



      public function whereFounder($id)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->get("founder");

        return $query->result();

    }



      public function updateFounder($id,$data)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->update("founder",$data);

        if($query==true){

        return 1;

        }

        else

        {

        return 0;

        }

    }



    public function deleteFounder($id)

    {

        //$this->db->where("active","active");

        $this->db->where('id', $id);

        $query=$this->db->delete('founder');

        if($query==true){

        return true;

    }else{

        return false;

    }

    }





    //*************************end team***************************



      //*************************Website config*******************************

    // public function insertConfig($data)

    // {

       

    //     $query=$this->db->insert("config",$data);

    //     $id = $this->db->insert_id();

    //         if ($id > 0) {

    //             return $id;

    //         } else {

    //             return false;

    //         }

    // }



   public function getConfig()

    {

         

        $this->db->order_by("id","DESC");

        $query=$this->db->get("config");

        return $query->result();

    }



      public function whereConfig($id)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->get("config");

        return $query->result();

    }



      public function updateConfig($id,$data)

    {

        //$this->db->where("active","active");

        $this->db->where("id",$id);

        $query=$this->db->update("config",$data);

        if($query==true){

        return 1;

        }

        else

        {

        return 0;

        }

    }



    // public function deleteConfig($id)

    // {

    //     //$this->db->where("active","active");

    //     $this->db->where('id', $id);

    //     $query=$this->db->delete('config');

    //     if($query==true){

    //     return true;

    // }else{

    //     return false;

    // }

    // }





    //*************************end Brand***************************



//////*****************front********************************



public function getProductWhere($name)

    {

         

        $this->db->order_by("id","DESC");

        $this->db->where("product_offer",$name);

        $this->db->or_where("product_type",$name);

        $query=$this->db->get("product");

        return $query->result();

    }

    public function filterProduct($filter)

    {

        $this->db->order_by($filter,"DESC");

        $query=$this->db->get("product");

        return $query->result();

    }



     function search($keyword)

    {

        $this->db->like('product_name',$keyword);

       

        $query  =   $this->db->get('product');

        return $query->result();

    }

//////*****************front********************************

  public function termscondition()

    {

       
        $query=$this->db->get("tbl_terms_condition");

        return $query->result_array();

    }
  public function privacypolicy()

    {

       
        $query=$this->db->get("tbl_privacy_policy");

        return $query->result_array();

    }  
    
    public function getqueryDetails()
    {   
        $query=$this->db->get("tbl_survey");
        return $query->result();

    }



}

