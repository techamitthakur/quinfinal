var Event = function () {
    this.__construct = function () {
        this.dataTable();
        this.Login();
        this.users();
        this.addAccess();
        this.addUser();
        this.editAccess();
        this.deleteAccess();
        this.editUser();
        this.addMember();
        this.removeMember();
        this.deleteUser();
    };
    this.dataTable = function () {
        $(document).ready(function () {
            $('#myTable').DataTable({
                destroy: true
            });
        });
    };
    this.Login = function () {
        $(document).on('submit', '#loginform', function (evt) {
//            alert("okk");
            evt.preventDefault();
            var url = $(this).attr("action");
            var postdata = $(this).serialize();
            $.post(url, postdata, function (out) {
                $(".form-group > .error").remove();
                if (out.result === 0) {
                    for (var i in out.errors) {
                        $("#" + i).parents(".form-group").append('<span class="error">' + out.errors[i] + '</span>');
                        $("#" + i).focus();
                    }
                }
                if (out.result === -1) {
                    var message = '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                    $("#error_msg").removeClass('alert-warning alert-success').addClass('alert alert-danger alert-dismissable').show();
                    $("#error_msg").html(message + out.msg);
                    $("#error_msg").fadeOut(2000);


                }
                if (out.result === 1) {
                    var message = '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                    $("#error_msg").removeClass('alert-danger alert-danger').addClass('alert alert-success alert-dismissable').show();
                    $("#error_msg").html(message + out.msg);
                    $("#error_msg").fadeOut(2000);
                    window.setTimeout(function () {
                        window.location.href = out.url;
                    }, 1000);
                }
            });
        });
    };
    this.users = function () {
        $(document).on('click', '.side-menu, .back', function (evt) {
//            alert('okkk');
            evt.preventDefault();
            var url = $(this).attr('href');
            $.post(url, '', function (out) {
                $('.page-wrapper').html('');
                if (out.result === 1) {
                    $('.page-wrapper').html(out.page_wrapper);
//                    var url = $(".edit-access-data").attr('url');
//                    $(".edit-access-data").load(url);
                    $('#myTable').DataTable({
                        destroy: true
                    });
                }
            });
        });
    };

    this.addAccess = function () {

        $(document).on('submit', '#add-access', function (evt) {
            // alert("okk");
            evt.preventDefault();
            var url = $(this).attr("action");
            var postdata = $(this).serialize();
            $.post(url, postdata, function (out) {
                $(".form-group > .error").remove();
                if (out.result === 0) {
                    for (var i in out.errors) {
                        $("#" + i).parents(".form-group").append('<span class="error">' + out.errors[i] + '</span>');
                        $("#" + i).focus();
                    }
                }
                if (out.result === -1) {
                    var message = '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                    $("#error_msg").removeClass('alert-warning alert-success').addClass('alert alert-danger alert-dismissable').show();
                    $("#error_msg").html(message + out.msg);
                    $("#error_msg").fadeOut(2000);


                }
                if (out.result === 1) {
                    var message = '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                    $("#error_msg").removeClass('alert-warning alert-danger').addClass('alert alert-success alert-dismissable').show();
                    $("#error_msg").html(message + out.msg);
                    var urldata = $("#access-alldata").attr('url');
                    $("#access-alldata").load(urldata);
                    $("#error_msg").fadeOut(2000);
                    var url = $(".edit-access-data").attr('url');
                    $(".edit-access-data").load(url);
                    $("input").val("");
                    $("input").focus();

                }
            });
        });
    };
    this.addUser = function () {

        $(document).on('submit', '#add-useres, #add-user-groups, #add-member', function (evt) {
//             alert("okk");
            evt.preventDefault();
            var url = $(this).attr("action");
            var postdata = $(this).serialize();
            $.post(url, postdata, function (out) {
                $(".form-group > .error").remove();
                if (out.result === 0) {
                    for (var i in out.errors) {
                        $("#" + i).parents(".form-group").append('<span class="error">' + out.errors[i] + '</span>');
                        $("#" + i).focus();
                    }
                }
                if (out.result === -1) {
                    var message = '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                    $("#error_msg").removeClass('alert-warning alert-success').addClass('alert alert-danger alert-dismissable').show();
                    $("#error_msg").html(message + out.msg);
                    $("#error_msg").fadeOut(2000);


                }
//                $('.page-wrapper').html('');

                if (out.result === 1) {
                    $('.page-wrapper').html(out.page_wrapper);
                    $('#myTable').DataTable({
                        destroy: true
                    });
                }
            });
        });
    };
    this.editAccess = function () {

        $(document).on('click', '.edit-access', function (evt) {
            evt.preventDefault();
            var url = $(this).attr('href');
            // alert(url);
            $(".edit-access-data").load(url);
        });
    };
    this.editUser = function () {

        $(document).on('click', '.edit-user', function (evt) {
            evt.preventDefault();
            var url = $(this).attr('href');
            // alert(url);
            $.post(url, '', function (out) {
                $('.page-wrapper').html('');
                if (out.result === 1) {
//                    alert(out.page_wrapper);
                    $('.page-wrapper').html(out.page_wrapper);
                }
            });
        });
    };

    this.addMember = function () {

        $(document).on('click', '.unmember', function (evt) {
            evt.preventDefault();
            var data = $(this).html();
            var replace_data = data.replace("unmember[]", "member[]");
            $(this).remove();
            var id = $(this).attr("id");
            $(".add-member").append('<a class="member" href="javascript:;" id="' + id + '">' + replace_data + '</a>');
        });
    };

    this.removeMember = function () {

        $(document).on('click', '.member', function (evt) {
            evt.preventDefault();
            var data = $(this).html();
            var replace_data = data.replace("member[]", "unmember[]");
            var id = $(this).attr("id");
            $(this).remove();
            $(".remove-member").append('<a href="javascript:;" class="unmember" id="' + id + '">' + replace_data + '</a>');
        });
    };
    this.deleteAccess = function () {

        $(document).on('click', '.delete-access', function (evt) {
            evt.preventDefault();
            var a = confirm('Are You Soure Want To Delete');
            if (a == true)
            {
                var url = $(this).attr('href');
                // alert(url);
                $.ajax({
                    url: url,
                    success: function ()
                    {
                        var urldata = $("#access-alldata").attr('url');
                        $("#access-alldata").load(urldata);
                    }
                })
            } else
            {
                return false;
            }
            // url="",
        });
    };
    this.deleteUser = function () {

        $(document).on('click', '.delete-user', function (evt) {
            evt.preventDefault();
            var a = confirm('Are You Soure Want To Delete');
            if (a == true)
            {
                var url = $(this).attr('href');
                // alert(url);
                $(this).parent("td").parent("tr").remove();
                $.ajax({
                    url: url,
                    success: function (res)
                    {

                    }
                })
            } else
            {
                return false;
            }
            // url="",
        });
    };
    this.__construct();
};
var obj = new Event();